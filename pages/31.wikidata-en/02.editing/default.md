---
title: 'Editing Wikidata'
length: '180 min'
objectives:
    - 'to understand how Wikidata content is collected and added to the database'
    - 'to learn how to add content to Wikidata, and how to do mass additions to Wikidata'
    - 'to discover how the wikidata community collaborate to build the database'
    - 'to understand how quality processes are build into the project'
    - 'to get introduced to the wonders of getting answers to complicated questions with query systems'
---

##1.- Questions about last session and discussion about homework

##2.- Introduction to editing
→ Editing overview and review of data structure
The “Introduction to editing” section typically ensures that participants will be able to: 
    • Find the editing button and contribute to the label and description of a Wikidata item 
    • Find the editing button for an existing property, modify the property with a qualifier and add reference to a Wikidata item. 

Review of the fundamental part of a WikiData item (label, identifiers, statements, qualifiers, references, properties and values)
Demonstration of how to edit
Discovery of Recoin tool, meant to identify missing information on an item

→ Editing activity (on laptop)

* Take the WikiData tutorials : create an item and add a statement
* Then create or improve an item
* Analyze a few items with Recoin
* Further populate items (students should also add references and sources)
* Feedback : collective report by each student about what was done, difficulties met, questions, thoughts.
Tips
    • You should demonstrate editing on an item that you have identified before the event, including adding a label, description and common property for that kind of item. Remember to indicate where the edit buttons are for each type of edit. 
    • You should demonstrate how to add a reference to an existing statement. Much of the value of Wikidata over the long term, comes from the ability to reference statements to reliable sources of material. Like when demonstrating referencing during Wikipedia editing workshops, its important to prepare the reference ahead of time. Remember to describe the common fields for references on Wikidata (if you have custom gadgets for copying references or filling in references using Citoid, make sure to describe how they change the interface). 
    • If you have custom gadgets or tools installed on your Wikidata account, consider disabling them or discussing how your interface is different from the standard interface for Wikidata.

Read : 
*https://www.wikidata.org/wiki/Wikidata:Planning_a_Wikidata_Workshop/Adding_basic_statements_to_Wikidata_items_by_engaging_newbies
*https://www.wikidata.org/wiki/Wikidata:Tours

##3.- Wikidata editing processes
The goal of this section is to better understand Wikidata editing processes, in particular insisting on transparency and the possibility to track every edit

→ Editing and collaboration features presentation
Taking a tour of various features of WikiData, useful for the editor : Userpage, TalkPage, WatchList, Own Contributions
Taking a tour of features helpful to the community to track activity on the site : RecentChange, history of item page, Other users pages, Interaction with Users on their talk pages, list of other participants contributions
Presentation of WikiData strategy based on SoftSecurity and HardSecurity
Demonstration on contributions provided by the trainees during the previous activity
→ Activity  (on laptop)
* Each student takes the time to explore what has just been described. Trainer can ask each participant to write a message on the talk page of another participant etc.

##4.- Data quality and Data evaluation

→ Quality and evaluation presentation
Provide an overview of content already available on WikiData
How to evaluate the quality of an article
Using ShowCase items to build an item
Further exploring sources and citations (how to deal with conflicting facts, what is a good source of data, what is not)
Discovery of tools to help fill in gaps, in depth and wide (examples can include TABernacle, Mix'n'Match, the WikiData Game, The Distributed game, Wikishootme, Terminator. To be chosen depending on the audience)
Introduction to a tool for mass editing : quickstatements
Presentation will include discussion, questions and answers
Tips
Showcase or demonstrations can typically include: 
* Mix'n'Match : the tool list entries of some external database and allow users to match them against Wikidata items
* WikiShootMe : this is a tool to show Wikidata items, Wikipedia articles, and Commons images with coordinates, all on the same map. 
It facilitates addition of images to Wikimedia projects. 
* Wikidata games : A set of games to quickly add statements to Wikidata.

Read
*https://tools.wmflabs.org/wikidata-game/
*https://tools.wmflabs.org/wikidata-game/distributed/
*https://tools.wmflabs.org/mix-n-match/#/
*https://tools.wmflabs.org/wikishootme/#lat=43.35953739293013&lng=5.325894166828497&zoom=15
*https://tools.wmflabs.org/quickstatements/#/

##5.- Basic introduction to WikiData queries using Query Helper
Participants will be able to: 
    • Understand how to read and modify a basic Wikidata Query using both SPARQL and the Query Helper 
    • Understand how to construct super basic queries that return multiple variables and labels. 
    • Understand what kinds of visualizations are available for using Wikidata Queries 
What are queries and what is Sparql

→ Discovery of the Wikidata Query Service 
Demonstration with a simple query from examples and modification of the initial query. Show different visualizations. 
→ Activity (on laptop if time permit)
* Participants play with the Query Service: exploring the examples with different visualization options, modification of the examples for the most daring.

##6.- Debrief and questions
To wrap up the session, the trainer will facilitate a debriefing moment where participants are encouraged to express their questions, doubts, ideas and feelings toward the topics discussed.
Introduction to next session
The following session will be dedicated to queries

##7.- Homework
* search for 3 ideas of querry questions to submit at the next sessions
* read https://www.wikidata.org/wiki/Wikidata:Glossary

##References for the trainer
* https://www.wikidata.org/wiki/Wikidata:Planning_a_Wikidata_workshop
* https://dashboard.wikiedu.org/training/wikidata-professional/evaluating-wikidata
* https://www.wikidata.org/wiki/Help:Contents
* https://www.wikidata.org/wiki/Wikidata:Tools
* https://www.wikidata.org/wiki/Wikidata:Wikidata_educational_resources