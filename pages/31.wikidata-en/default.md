---
title: Wikidata
lang_menu_title: Wikidata
lang_id: wikidata
lang_title: en
length: '10 hours'
objectives:
    - 'A solid contextual background to understand the benefits of Wikidata and how it relates to the other Wikimedia projects but also how it relates to other databases available on the Internet'
    - 'The participant will acquire digital skills in being able to add content to Wikidata but also in being able to write queries in Sparql to get answers on difficult questions'
    - 'The participant will be be able to present Wikidata to others and will have enough background to identify how it could be used in professional background'
    - 'The participant will better understand which quality processes and evaluating systems favor the constitution of quality data that every person and every machine can use'
materials:
    - 'PC with internet connexion'
    - 'Internet connexion'
    - Beamer
    - 'Paper et pen'
    - Flipchart
    - Wikidata
skills:
    Keywords:
        subs:
            WikiData: null
            Database: null
            'Knowledge Graph': null
            Wikipedia: null
            Community: null
            CC0: null
            'Free licences': null
            Collaboration: null
            SPARQL: null
---

WikiData is an open data platform that belongs to the Wikimedia family of websites. It hosts 57 millions items as of June 2019. Wikidata is a free and open knowledge base that contains various data types (eg, text, images, quantities, coordinates, geographical shapes, dates...).
The basic entity in Wikidata is an Item. An item can be a thing, a place, a person, an idea, or anything else. What’s specific to Wikidata is that the information is stored in a rigidly structured way to makes it possible for both humans and machines to process.
Although it is now a knowledge base fairly widely used by machines, it is still largely unknown from the public as well as from its potential instructors, least understood. The intent of this module is to provide a Train the Trainer course that will provide them with a solid overview of Wikidata and opportunities that are attached to this open source project.

This module will be divided into 4 sessions:

### First session: Discovering and understanding WikiData 
This session will provide an overview of what Wikidata is. It will cover its history and allow the participants to understand the why and the benefits of its existence, for the wikimedia projects in particular, and for the Internet in general. The session will cover Wikidata basics, data structure, elements of vocabulary, and essential design features, but also wikidata community and governance overview so that participants capture the fact wikidata, as technical as it seems, is primarily a social construction. Participants will explore Wikidata by themselves and create an account. 

### Second session: Editing WikiData
The second session will aim to get participants to add content to Wikidata on their own and thus discover some of the editing fundamental processes. It will also take a deep dive into data quality and data evaluation systems, as well as discover most popular tools that allow to fill the gaps in depth and wide, as well as tools for mass editing and import. 

### Third session: Querying WikiData
This session is meant to get participants discover and experiment the query service for simple queries, then discover sparql querying language for more complex in depth requests through a step-by-step methodology, incrementally increasing the difficulty. The session will include hands-on activities. 

### Fourth session: Applying WikiData
This session will help participants to understand the applied value of Wikidata, particularly for applications like visualization of complex data, but also to identify potential applications for Wikidata in their own work, whether through visualization, embedding Wikidata in their own software, or using Wikidata on Wikimedia projects. The second part of the session will teach them how to connect with the Wikidata community for a more effective collaboration.