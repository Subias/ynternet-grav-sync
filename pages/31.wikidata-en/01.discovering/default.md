---
title: 'Discovering and understanding Wikidata'
length: '180 minutes'
objectives:
    - 'to provide an overview of what Wikidata is'
    - 'to explain how the data is stored in the project and why'
    - 'to discover the wikidata community and some key elements of community governance'
    - 'to understand the interface and create an account'
---

 ##1.- Introduction
→ Group discussion
Trainer will start the introduction to the module by asking participants about their current and previous experience with wikidata or any wikimedia project. They will also be invited to share information about the context of their participation to the session.
→ Query opening
The goal would be to help them see how WikiData may practically impact their daily lives and attracts their attention. Show how Wikidata can help “answer” questions that previously would be very hard to do in either domain-specific or non-Wikimedia projects. Think of sharing some examples of questions that Wikipedia theoretically could answer, but would require reading large amounts of Wikipedia (or other sources) to collect that information. Adapt example queries to the audience.  

The results of one or two queries will be proposed.
1. First demo
Ask the audience about the mayor of their city. Is she a woman ? If they do not know, where can they find the information ? mention google, but also personal assistants such as Siri
Ask the audience about cities in their country with women mayors. Where or how can they find the information ? 
Ask them list of countries ordered by the number of their cities with female mayor. Where or how can they find the info ? 
Run the query. "list of countries ordered by the number of their cities with female mayor" to demonstrate use of data hosted in wikidata
2. Second demo (targetted for a professional audience)
Run the query list of scholarly articles with the word "zika" in the title. 
Use to demonstrate link of WikiData with other databases : (here PubMed_ID)
Show third party reuse on scholia : https://tools.wmflabs.org/scholia/topic/Q202864

##2.- Overview of Wikidata
→ Introduction to Wikidata
This section typically ensures that participants will be able to: 
    1. Understand the goals of Wikidata 
    2. Understand how Wikidata relates to other Wikimedia Projects, especially Wikipedia 
    3. Understand why they should be motivated to contribute or use Wikidata, especially in the context of their own work
    4. Elements it would include are
* Key concepts : Describing the basic characteristics of Wikidata (free cc0, collaborative, multilingual and language agnostic, interconnected) and it’s goals (repository of human knowledge, support for the Wikimedia projects and 3rd parties, hub in the linked open data web) + general content stats + general access stats
* How Wikidata serves Wikimedia projects, and how it fills important gaps in infrastructure for the community. I.e. Interlanguage links, or helping prevent Wikipedia Infoboxes go out of date in local language Wikis, especially for knowledge not “local” to the language (i.e. a politician from another language/culture). This should include the history of WikiData creation, and examples of impacts on Wikipedia and Wikimedia Commons (structured data for Commons)
* The WikiData community (WM DE, who runs it, governance brief overview)
* Why WikiData is important to the Internet (examples: CC0 licence allow WD to be the knowledge base for many AI such as Siri or Alexa; data from cultural institutions and civic information more accessible, transparent and neutral process)
 
→ Quizz and restitution 
A few questions with multiple answers to select. Can be done in pair. After completion, collective restitution and further explanations if required.

##3.- Data Structure and how is Data stored in WikiData
→ Presentation of data structure in WikiData

The goal of this section typically ensures that participants will be able to:
    1. Understand the basic semantic data model of Resource Description Framework (RDF) and how it compares with the full Wikidata model. 
    2. Understand different concepts relevant to editing Wikidata and how to figure out what content belong in those fields, including: label, description, statement, property, qualifier and references. 
    3. Understand the relationship between “human readable information” (i.e. labels and descriptions) and “machine readable data” (i.e. Q#s and P#s) in the Wikidata interface. 

Elements covered
* Vocabulary (types of databases, identifiers, linked data, open data)
* The benefits and disadvantages of linked data
* Presentation of the different elements of WikiData (items, properties, statements, identifiers, qualifiers, references and rank)
* Reading WikiData. Compare a Wikipedia entry, a Wikidata entry and a Reasonator entry. Understanding the interface of WikiData 

→ Activity (on laptop)

Individually, participants are asked to
* Go to Q4859840. What is it ? What could it be confused with ?
* Now go to Q1492. In how many places is the information about W1492 used ?
* Use the search bar to find a WikiData item you are interested in. Explore its properties and references. Note how it is organized, what could be missing, whether there are sources etc. Think how to improve it.

Tips:
    • One of the best ways to introduce the RDF/Triple data model is outside of the Wikidata interface. For example from statements that describe the world in language (i.e Earth → highest-point → Mount Everest) transitioning it into statements with labels (i.e. Earth (Q2) → highest-point (P610) → Mount Everest (Q513) ) and showing the final machine-readable statements (Q2 → P610 → Q513). Adapt to the audience
    • Introducing the data model with a quality item (many presentations use Q42 for the Geek culture joke associated with the item, other quality items can be found in Showcase_items

Sources
* https://www.wikidata.org/wiki/Wikidata:In_one_page
* https://www.wikidata.org/wiki/Wikidata:Showcase_items

##4.- WikiData is build through collaboration

→ Wikidata community
The goal of this section typically ensures that participants will be able to understand that WikiData is primarily a social construction.
* Who adds content to WikiData (humans, machines run by humans, other sources)
* Who defines rules and how. The different roles of the community. 
* Behavior and social rules. 
* Editorial rules. What gets in and what does not. References. 
* Benefits of having an account 
Tips:
    • If your audience is interested in contributing to Wikidata at scale or for professional purposes, it’s important to explain how Wikidata decides what the data model, properties and items are. For example, it is important explain the social dynamic of “anyone can create a Q number, but P numbers undergo a tightly controlled community consultation process”. 

→ First editing steps (on laptop) 

Get students to create their own account (or plan ahead, there is a limit to daily account creation from same IP). Add a message on their talk page, with a link to the "training session page" set up in advance. Make sure to collect all username created during the session. Suggestions to users:
1) create an account. Introduce yourself in the user page. 
2) Change the preferences to set the favorite language. 
3) Add gadgets in preferences: "Recoin", "Reasonator", and "labelLister"
4) Drop a message on the training page 
5) Use the search bar to find a WikiData item you are interested in. Check its completion with Recoin. 

##5.- Debrief and questions / introduction to next session

To wrap up the session, the trainer will facilitate a debriefing moment where participants are encouraged to express their questions, doubts, ideas and feelings toward the topics discussed.
Next session will be about learning how to add content to WikiData

##6.- Homework
Before the second session, the participant will be asked to think of which entries s-he would like to improve, search for information, including references and sources, to be shortly presented to the group at the beginning of next session.

##References for the trainer
* https://www.wikidata.org/wiki/Wikidata:Planning_a_Wikidata_workshop
* https://dashboard.wikiedu.org/training/wikidata-professional/evaluating-wikidata
* https://www.wikidata.org/wiki/Help:Contents
* https://www.wikidata.org/wiki/Wikidata:Tools
* https://www.wikidata.org/wiki/Wikidata:Wikidata_educational_resources