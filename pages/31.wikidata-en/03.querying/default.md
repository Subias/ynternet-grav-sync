---
title: 'Querying Wikidata'
length: '120 min'
objectives:
    - 'to examine the interest of query language and tools to get answers to complicated questions'
    - 'to motivate participants to learn how to do basic queries or even advanced queries'
    - 'to learn how to do queries in SPARQL'
---

##1.- Queries
→ Understand the Query Service
The query service will have been very briefly introduced during session 2. This will be looked at more in depth.
→ discover SPARQL in depth and know how to do queries
However, with many audiences in professional situations it is necessary to provide a separate environment for better understanding the Sparql languages and how to build strong queries.

→ Participant create their own queries (in laptop)

Let audience write their own queries with the help of the Query Service
Tips
    • Remember, for many audiences, it will be the first time that they work with a Query language (SQL, SPARQL, etc). Therefore its very important that you start from the most basic elements, and incrementally increase the complexity of the information. A good tactic for doing this, is building several queries of different complexity in front of the audience. For example queries, see Wikidata:SPARQL query service/Building a query. 
    • Make sure to demonstrate how to take an example query and modify it to suit a separate need. Many individuals will not immediately be writing Wikidata queries from scratch, but instead modifying existing queries. The Wikidata Query helper is particularly useful for audiences modifying queries. 
    • Make sure to include both guided query writing, where the instructors guides the writing of the query, and a window of time for the audience to write their own queries. Having participants write their own query, reinforces the process and skills of modifying or writing a query, so that they can confidently do so in the future 
    • If the audience is mostly Wikimedians, make sure to spend time showing them how to apply Wikidata Queries to various tools of used by the Wikimedia community, including Petscan, Listeriabot, and Histropedia.

Sources and reads
* https://www.wikidata.org/wiki/Wikidata:SPARQL_query_service/Wikidata_Query_Help
* https://www.wikidata.org/wiki/Wikidata:SPARQL_query_service/queries/examples
* https://www.wikidata.org/wiki/File:Querying_Wikidata_with_SPARQL_for_Absolute_Beginners.webm

##2.- Debrief and questions
To wrap up the session, the trainer will facilitate a debriefing moment where participants are encouraged to express their questions, doubts, ideas and feelings toward the topics discussed.

##3.- Explanations about the homework
Set up a campaign on ISA tool and ask the participants to participate to the campaign before the next session 
https://tools.wmflabs.org/isa/campaigns

##4.- Homework
Set up a campaign on ISA tool and ask the participants to participate to the campaign before the next session 
https://tools.wmflabs.org/isa/campaigns