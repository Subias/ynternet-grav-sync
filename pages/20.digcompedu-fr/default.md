---
title: 'Cadre DigCompEdu pour une éducation commune et ouverte'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'DigCompEdu framework for a common and open education'
lang_id: 'DigCompEdu framework for a common and open education'
lang_title: fr
skills:
    'Mots clés':
        subs:
            DigCompEdu: null
            Cadre: null
            'Compétences numériques': null
            'Éducation ouverte': null
            'Éducation des adultes': null
            'Culture FLOSS': null
            Enseignement: null
            Compétences: null
materials:
    - 'Ordinateur personnel'
    - 'Connexion Internet'
    - 'Cadre DigCompEdu'
    - 'Papier et stylos'
objectives:
    - 'Comprendre le cadre DigCompEdu'
    - 'Mapper vos propres compétences sur le cadre'
    - 'Planifier la mise à niveau des compétences numériques à des fins éducatives'
    - 'Comment mettre en place des activités pédagogiques créatives grâce aux compétences numériques'
length: '8 heures'
---

"… Un cadre scientifiquement solide décrivant ce que signifie pour les éducateurs une compétence numérique. Il fournit un cadre de référence général pour soutenir le développement des compétences numériques spécifiques aux éducateurs en Europe".Ce module donne un aperçu du cadre de compétences numériques pour les éducateurs. Ce cadre européen détaille 22 compétences organisées en six domaines. L'accent n'est pas mis sur les compétences techniques. Le cadre vise plutôt à détailler comment les technologies numériques peuvent être utilisées pour améliorer et innover l'éducation et la formation. Le module se concentrera sur les domaines très pertinents pour l'éducation des adultes, expliquant comment le cadre peut être d'une grande importance pour les éducateurs et quels sont les avantages de développer de telles compétences en utilisant des technologies open source. Des exemples pratiques seront donnés en référence aux six domaines afin que les étudiants sachent quelles ressources ouvertes et quels outils sont disponibles sur le terrain.

## Contexte
L'objectif du module est de démontrer et d'engager les apprenants sur la façon dont:
- Comprendre le cadre DigCompEdu.
- Mapper vos propres compétences sur le cadre lanifier la mise à niveau des compétences numériques à des fins éducatives
- Comment mettre en place des activités pédagogiques créatives grâce aux compétences numériques

Le but de cette formation est de présenter aux participants le cadre DigCompEdu afin qu'ils puissent profiter du document pour l'enseignement et l'apprentissage.
À la fin des sessions, les participants seront en mesure de comprendre le cadre et d'identifier les outils et méthodologies existants qui peuvent aider à développer les compétences décrites dans DigCompEdu.

Ce module sera divisé en 2 séances :

### Première séances : Le cadre DigCompEdu
Dans cette première session, les participants seront initiés au cadre afin de leur fournir les notions théoriques sur la structure et les objectifs du document. Comprendre et pouvoir reconnaître les compétences numériques nécessaires au 21e siècle est un facteur clé dans l'éducation. Les participants seront préparés afin d'identifier leurs lacunes et de cartographier leurs compétences lors de la deuxième session.

### Deuxième séance : Comment utiliser le cadre pour cartographier vos compétences et améliorer les connaissances
Les connaissances des participants sur le cadre seront mises en pratique. À partir des six domaines du DigCompEdu, les formateurs et les enseignants devront cartographier leurs propres compétences afin d'identifier les lacunes et de planifier leur mise à niveau. Certaines études de cas seront présentées afin de fournir aux participants des exemples concrets sur la façon de tirer parti du cadre pour leur contexte éducatif.