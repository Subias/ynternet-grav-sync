---
title: 'Open coding con Scratch'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Open coding with Scratch'
lang_id: 'Open coding with Scratch'
lang_title: it
length: '10 ore'
objectives:
    - 'Programmare in Scratch per lo sviluppo di storie interattive, giochi e animazioni'
    - 'Utilizzare Scratch per lavorare in modo collaborativo (condividere un progetto dall’editor Online di Scratch, visualizzare e modificare progetti di altri utenti)'
    - 'Far conoscere ai partecipanti la Filosofia Scratch ed apprendere come programmare su Scratch'
    - 'Condividere I loro progetti e utilizzare progetti di altri utenti'
materials:
    - 'Personal computer'
    - 'Connessione Internet'
    - 'Programma Scratch'
skills:
    'Parole chiave':
        subs:
            Scratch: null
            'open coding': null
            'filosofia scratch': null
            'elaborazione sequenziale': null
            'code block': null
            blocchi: null
            progetti: null
            stage: null
            scripts: null
---

Scratch è un linguaggio di programmazione gratuito e una comunità online sviluppata dal MIT, che può essere utilizzato per creare giochi, animazioni, canzoni, e condividerli online.

Scratch viene utilizzato da persone di tutte le età in molteplici contesti. L’abilità di codificare programmi per computer è una parte importante dell’istruzione nella società odierna. Quando le persone imparano a programmare in Scratch apprendono strategie importanti per risolvere problemi, elaborare progetti, e comunicare idee.

Le statistiche sul sito ufficiale del linguaggio di programmazione mostrano più di 40 milioni di progetti condivisi da oltre 40 milioni di utenti e quasi 40 milioni di visite mensili.

Questo modulo sarà diviso in 3 sessioni:
### Prima sessione: Filosofia Scratch: “Immagina, programma, condividi” 
La filosofia di Scratch incoraggia la condivisione , il riutilizzo e la combinazione di codici. Questa sessione fornirà una panoramica su tale filosofia "Immagina, Programma, Condividi”.
Gli utenti possono creare i loro personali progetti o riutilizzare i progetti di qualcun altro. I progetti creati e modificati con Scratch sono concessi in licenza Creative Commons Attribution-Share Alike License.

La sessione ha lo scopo di fornire una maggiore comprensione del background di Scratch e dei principi per la creazione, la programmazione e la condivisione del materiale. 
Al termine della sessione, i partecipanti saranno in grado di esaminare l'uso e i vantaggi della filosofia di Scratch nell'educazione degli adulti.. 
### Seconda sessione: Open coding con la programmazione visuale gratuita Scratch 
Quando si impara a programmare con Scratch, si apprendono importanti strategie per risolvere problemi, elaborare progetti e comunicare idee. Programmare con Scratch, quindi, aiuta gli utenti a pensare in maniera creativa, ragionare secondo uno schema e lavorare in modo collaborativo – Competenze essenziali nel ventunesimo secolo.

Questa seconda sessione mira ad introdurre il linguaggio di programmazione sviluppato dal MIT.
I partecipanti apprenderanno come codificare in Scratch per sviluppare storie interattive, giochi e animazioni. 
### Terza sessione: L’efficacia di Scratch nel creare un ambiente di programmazione collaborativo
Questo modulo ha lo scopo di illustrare i fondamentali della comunità online 
La Sessione ha lo scopo di supportare gli studenti nell'individuazione della comunità online di Scratch, e nel trarvi vantaggio. I partecipanti impareranno a condividere un progetto dallo Scratch Online Editor, a visualizzare e modificare il progetto di un altro utente traendo allo stesso tempo ispirazione dagli altri.