---
title: Wikidata
lang_menu_title: Wikidata
lang_id: wikidata
lang_title: ct
length: '10 hours'
objectives:
    - 'Analitzar un context sòlid per a comprendre els beneficis de Wikidata i com es relaciona amb els altres projectes de Wikimedia, però també com es relaciona amb altres bases de dades disponibles a Internet'
    - 'Adquirir habilitats digitals per a poder afegir contingut a Wikidata i també per a poder escriure consultes en SPARQL i obtenir respostes a consultes complexes'
    - 'Tenir la capacitat de presentar Wikidata als altres i conèixer suficients antecedents com per identificar com es podria utilitzar en un context professional'
    - 'Comprendre quins processos de qualitat i sistemes d''avaluació afavoreixen la construcció i recopilació de dades de qualitat, de manera que cada persona i cada màquina els puguin utilitzar'
materials:
    - 'Ordinador personal o telèfon intel·ligent o tauleta digital'
    - 'Connexió a Internet'
    - Projector
    - 'Paper i bolígrafs'
    - Paperògrafs
    - Wikidata
skills:
    'Paraules clau':
        subs:
            WikiData: null
            'Base de dades': null
            'Gràfic de coneixement': null
            Wikipedia: null
            Comunitat: null
            CC0: null
            'llicències llibres': null
            collaboració: null
            SPARQL: null
---

Wikidata és una plataforma de dades obertes que pertany a la família de llocs web de projectes. Al juny de 2019 albergava 57 milions d'articles. Wikidata és una base de coneixement oberta i gratuïta que conté diversos tipus de dades (per exemple text, imatges, quantitats, coordenades, formes geogràfiques, dates ...). L'entitat bàsica en Wikidata és un article. Un article pot ser una cosa: un lloc, una persona, una idea o qualsevol altra element. El que és específic de Wikidata és que la informació s'emmagatzema de forma rígida i estructurada perquè els humans i les màquines puguin processar-la. Tot i que ara és una base de coneixement bastant utilitzada per les màquines, encara és en gran part desconeguda pel públic, i te un potencial poc conegut per part de les persones formadores. La intenció d'aquest mòdul és proporcionar un curs de formació de formadores que proporcionarà una visió general i sòlida sobre Wikidata i que presentarà les oportunitats que s'associen a aquest projecte de codi obert.

Aquest mòdul es dividirà en 4 sessions:

### Primera sessió: Descobrint i entenent què és i com funciona Wikidata
Aquesta sessió proporcionarà una visió general del que és Wikidata. Es presentarà la seva història i les participants podran comprendre el per què i els beneficis de la seva existència, per als projectes de wikimedia en particular, i per a Internet en general. La sessió cobrirà conceptes bàsics de Wikidata: estructura de dades, elements de vocabulari i les seves característiques de disseny principals, però també s'oferirà una visió general de la comunitat i la governança de Wikidata, perquè les participants comprenguin el fet que Wikidata, per tècnica que sembli, és principalment una construcció social. Les participants exploraran Wikidata per si mateixes i crearan un compte.

### Segona sessió: Edició de Wikidata
La segona sessió tindrà com a objectiu aconseguir que les participants afegeixin contingut a Wikidata pel seu compte i descobreixin alguns dels processos fonamentals d'edició. També s’aprofundirà en els sistemes de qualitat i avaluació de dades, i es descobriran les utilitats més populars, així com les eines disponibles per a l'edició i per a la importació en massa.

### Tercera sessió: Consultar Wikidata
Aquesta sessió està destinada a fer que les participants descobreixin i experimentin el servei de consultes per a consultes simples, per posteriorment descobrir el llenguatge de consulta SPARQL per a peticions més complexes, a través d'una metodologia de pas a pas, augmentant gradualment la dificultat. La sessió inclourà activitats pràctiques.

### Quarta sessió: Aplicar Wikidata
Aquesta sessió ajudarà a les participants a comprendre el valor aplicat de Wikidata, particularment per a aplicacions com la visualització de dades complexes, però també a identificar possibles aplicacions per a Wikidata en el seu propi treball, ja sigui a través de la visualització, incrustant Wikidata en el seu propi programari o usant Wikidata en Projectes d'altres projectes. La segona part de la sessió els ensenyarà com connectar amb la comunitat de Wikidata per a una col·laboració més efectiva.