---
title: 'L’avvento dei copyleft e delle licenze libere'
lang_menu_title: Copyleft
lang_id: Copyleft
lang_title: it
length: '10 ore'
materials:
    - 'Personal computer (Smartphone o tablet connesso ad Internet)'
    - 'Connessione internet'
    - Proiettore
    - 'Carta e penna'
    - Flipchart
    - 'Slidewiki Academy'
objectives:
    - 'Il copyright e i diritti d''autore sono stati oggetto di critiche, con lo sviluppo di tecnologie che facilitano la copia e la condivisione delle informazioni'
    - 'Gli obiettivi del copyleft e del FLOSS sono al servizio delle comunità e dei contenuti educativi'
    - 'Il quadro Creative Commons rivolto agli autori che preferiscono condividere il loro lavoro e arricchire il patrimonio comune (i Commons) può essere attivato'
skills:
    'Parole chiave':
        subs:
            'Licenza FLOSS': null
            'Free/Libre Open-Source Software': null
            'Politica economica': null
            Copyleft: null
            'Movimento FLOSS': null
            'Quadro delle licenze': null
            'cultura FLOSS': null
            Netizenship: null
            GPL: null
            EUPL: null
---

"Tutti I diritti riservati", "Marchio", "brevetto", "Copia o riproduzione limitate all’uso strettamente privato" ... Quando parliamo di "cultura", veniamo riportati al concetto di appropriazione (proprietà), in questo caso intellettuale. Tuttavia, il pensiero comune nella cultura libera è che le idee appartengono a tutti, e sono, un po’ come l’aria e l’acqua, i nostri bisogni primari. La cultura del copyleft, nota anche come cultura libera, è nata dal mondo del software e dai numerosi contributori che avevano una caratteristica in comune: il loro senso di bene collettivo.

L’espressione "free software" si riferisce alla libertà, non al prezzo. Per capire il concetto, si deve pensare a “libertà di espressione”, non ad “accesso libero”. Ispirate da questa innovativa mentalità riguardante la gestione della produzione creativa, altre iniziative hanno trasferito gradualmente il copyleft al di fuori del mondo del software.

# Contesto
L’obiettivo della sessione è coinvolgere gli studenti e dimostrare loro praticamente come:
-	Copyright e diritto di autore sono stati oggetto di critica, con lo sviluppo di tecnologie che favoriscono la copia e la condivisione di informazioni .
-	Gli obiettivi di copyleft e FLOSS sono utili a comunità e contenuti didattici
-	Si può applicare il  Creative Commons framework rivolto agli autori che preferiscono condividere il loro lavoro e arricchire il patrimonio comune (i Commons).
Lo studente sarà capace di descrivere gli aspetti etici, legali, sociali, economici e di impatto a favore e a sfavore della licenza FLOSS. Dopo aver deciso quali licenze/piattaforme/servizi sono più utili per sé stessi e la loro comunità, il ricercatore creerà un profilo personale per mostrare il suo profilo di ricerca e i relativi risultati.

Le sessioni forniranno un quadro politico e storico delle licenze FLOSS e promuoverle nell’ambito dell’Educazione degli Adulti. Ciò stimolerà la partecipazione intenzionale alla cultura aperta come parte della Netizenship attraverso specifiche piattaforme e strumenti. 

Questo modulo sarà diviso in 3 sessioni:
### Prima sessione: Copyche ? Comprensione ed uso delle licenze FLOSS
Questa sessione fornirà un quadro storico e politico delle licenze copyleft e FLOSS, i mezzi per comprendere ed essere in grado di mettere in uso le licenze FLOSS come strumento per l'evoluzione della proprietà, della comunità e del diritto d'autore nella società e nella tecnologia. I partecipanti saranno in grado di identificare copyleft e  licenze FLOSS, e riconoscerne gli  obiettivi.
### Seconda sessione: Le licenze FLOSS sono ovunque negli scenari di vita quotidiana
La seconda sessione  ha come scopo quello di contestualizzare le licenze FLOSS, attraverso casi di studio. I partecipanti si impegneranno in un’analisi critica dell’uso delle licenze FLOSS in vari organismi (Europei ed internazionali) e definiranno una politica preliminare per la propria organizzazione.
### Terza  sessione: Licenze FLOSS per educazione aperta, arte e apprendimento collettivo   
Questa sessione permetterà un’analisi critica dell’importanza delle licenze FLOSS nell’ambito dell’istruzione non formale. Proseguirà poi esaminando i vantaggi delle licenze copyleft e delle licenze libere in diversi ambienti educativi e connetterà i partecipanti con i movimenti di licenze FLOSS esistenti.