---
title: 'FLOSS resources for employment'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'FLOSS resources for employment'
lang_id: 'FLOSS resources for employment'
lang_title: en
materials:
    - 'Personal computer (Smartphone or tablet connected to the Internet)'
    - 'Internet connexion'
    - Beamer
    - 'Paper and pens'
    - Flipchart
    - Speakers
skills:
    Keywords:
        subs:
            'Algorithms and Employability': null
            'Curriculum Vitae': null
            Recruitment: null
            Skills: null
            Competences: null
            'Online opportunities': null
            Networking: null
            'Entrepreneurship online': null
            'Donation culture': null
            Wikinomics: null
            'Online campaigns': null
            Fundraising: null
            Crowdsourcing: null
            'Mass collaboration': null
objectives:
    - 'Provide background to FLOSS skills for employment'
    - 'Provide background to Human Resources methods'
    - 'Recognize the benefits and constraints of algorithms in HR'
    - 'Understand open algorithms as a new paradigm'
    - 'Understand the use of algorithms in the recruitment process'
    - 'Understand how to adapt a CV to have a better acceptance of algorithms'
    - 'Learn how to create a good online CV, portfolio or a social media profile'
    - 'Learn how to create and update a good work profile online'
    - 'Understand how important it is to be part of an online community in order to seize new economic opportunities'
    - 'Understand how the market is changing and how online opportunities can facilitate entrepreneurship and personal branding'
    - 'Learn new ways of being entrepreneurial with FLOSS practices and tools'
    - 'Know how to run a crowdfunding campaign online'
    - 'Understand different economic models like the donation culture and the wikinomics, among others'
    - 'Know how to improve their personal communication, collaboration and branding skills'
length: '15 hours'
---

## Introduction
75 percent of job applications are rejected before they are seen by human eyes. Before your resume reaches the hands of a live person, it often must pass muster with what is known as an applicant tracking system. An applicant tracking system — or ATS, for short — is a type of software used by recruiters and employers during the hiring process to collect, sort, scan, and rank the job applications they receive for their open positions.
Choosing the right person for a job position can be challenging. Algorithms are, in part, our opinions embedded in a form of a computer code. Algorithms consider the simple fact that hiring is essentially a prediction problem. When a manager reads through resumes of job applicants, she/he is trying to predict which applicants will perform well in the job and which won’t. Statistical algorithms are built for prediction of problems/costs and can be helpful in improving human decision-making. Algorithms can also have a darker side as they reflect human biases and prejudices that lead to machine learning mistakes and misinterpretations (perpetuating and reinforcing discrimination in hiring practices). The question now is not whether to use algorithms for selecting applicants, but how to get the most out of the machines.
The fundamental principles of resume writing remain constant for generations, but evolving technologies mean more aspects of the application and hiring processes take place online than ever before. By staying up to date with current best practices, participants will be better prepared to apply to job vacancies. 

## Context
In the internet, the income generation possibilities are endless. We need to identify a niche, develop a business plan and work hard to succeed. There’s numerous businesses that an internet entrepreneur can develop, the model of business don’t define an internet entrepreneur. 
Every organisation and project needs revenue sources to implement activities, whether the activities will be face-to-face or online. In this module we will also be focusing on the donation culture, wikinomics , fundraising and other economic models at the heart of web 2.0

This module will be divided into 3 sessions:

First session: Algorithms and Employability
The goal of the session is to present the concepts of algorithms and employability and how to adapt to a new HR world where besides having to impress a job recruiter, job seekers also have to be attractive to the machines they use. This session will generate discussions and debates on the problem HR algorithms raise and also on the solutions they bring. The main goal of this session is to open participants to the idea that recruitment algorithms will keep growing and we need to better adapt to them in order to be successful.

Second session: How to use FLOSS practices and create an attractive CV for algorithms
The focus of this session will be on how to create a CV online. Participants will be invited to create their own CV using digital platforms and tools. Also, participants will discover how to present their competences and skills when creating a good online CV that will be appealing to both humans and machines. 

Third session: FLOSS Opportunities and Online Entrepreneurship
In this session participants will have the chance to discover many other opportunities the digital world creates for developing a career and project ideas. The main focus of this session will be on online entrepreneurship and how the digital world creates opportunities to raise money. Different economic models will be explored like crowdfunding, the donation culture and the Freemium model.