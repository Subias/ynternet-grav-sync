---
title: 'Algorithms and Employability'
objectives:
    - 'to provide an introduction to FLOSS skills for employment and Human Resources methods'
    - 'to facilitate the understanding of the use of algorithms in the recruitment process and recognise the benefits and constraints'
    - 'to discover how to algorithms in the job recruitment area work'
    - 'to motivate be part of an online community in order to seize new work/economic opportunities'
length: '300 min'
---

##Introduction
Trainer will start the introduction to the module by asking participants about their previous experiences with applying to job offers online. What were the steps they did, how did they presented their profile and what were the difficulties they encounter. 
This discussion will be the starting point of a group discussion and aims to help participants to brainstorm about how the job searching world is changing and how job seekers need to readapt to new tools and new ways of practice in order to be successful.

##From closed data to open Algorithms: a new paradigm for social good
The datafication of the world is fed by the automatic generation of massive data through the use of digital devices that surround us: cellphones and tablets, e-devices, security cameras, credit cards, badges and satellites. However, little of the generated information is actually used to improve people’s lives or in the design of better public policies. During this session, the trainer will discuss how the massive collection of data can have a positive impact in society and especially in HR. The trainer will explain how the massive collection of data works and how it allows  companies to create better algorithms in order to make the recruitment process easier and faster. The aim of this session is to help participants to understand how the “open data movement” has shown how opening up data could foster public innovation, foster civic engagement, accountability and transparency. 

##Algorithms and Artificial Intelligence in the Employability process
After the initial discussion about the participant’s personal experiences with algorithms of recruitment, the trainer will briefly show a presentation aiming to explain exactly what a hiring algorithm is, what different types of recruitment algorithms exist and what are the positive and negative sides of them in Human Resources. It is important to discuss with participants the benefits these tools can have in the process of employee selection. Specifically, participants will learn what are the benefits for companies and what are the benefits for jobseekers. In fact, participants will discover how algorithms can be very helpful in finding the right person to the right job, how they reduce costs and speed the hiring process as they pre-select the most suitable CVs for the position. On the other hand, participants will also have an opportunity to reflect on the negative side and the issues that they bring as machine learning processes. The trainer will then share with participants a [Youtube video](https://www.youtube.com/watch?v=fxIBkHjyABs) on how an Applicant Tracking System (ATS) works. 
After this, trainer will invite the participants to comment on the following [infographic chart](https://www.talentlyft.com/en/blog/article/114/15-recruiting-trends-in-2019-infographic). Then, participants will be invited to discuss the main findings in the infographic. Trainer will then facilitate the group discussion.

##Create an algorithm in a group
For the next activity, participants will be invited to form smaller groups. In those groups, they should come up with a recipe to fry an egg. Each step, materials, ingredients and quantity of the ingredients should be specified. After this, participants should create a simple formula (a recipe) in the shape of direct instructions (like an algorithm). Participants will share their algorithm to fry an egg and compare with the other groups. It will be interesting to see if participants come up with the same recipe and if they do, what are the differences in the language they used to describe the instructions, and the impact of those differences. Participants will then be invited to reflect on the impact language had on the recipe to fry an egg. The trainer will then make the comparison with algorithms for employability.

##Being able to use job searching platforms and apply/research vacancies and job positions online
Participants will list the main online platforms they use (and know they use them) to find job vacancies and to apply for those positions. The trainer will guide a group discussion while participants share their personal experiences with this type of algorithms.  Particular attention will be given to how participants have done in the past to find job positions  and to the way they submitted their application (did they create an online CV in the platform? did they attach a file and annexes?). The trainer will then help participants  to understand how they should select the concrete way to apply to each job position, according to the type of position and platform.

##Algorithms and the FLOSS skills for employment
Adapting to algorithm also means adapting to new ways of perceiving and reporting one’s experience and skills. In fact, HR algorithms demand a different approach to how we show our value and competences in the job market. In this session, the trainer will help participants to understand what are the most valuable competences and skills today for the job market and how they are connected with the FLOSS movement. The trainer will then help participants understand how to present their skills and competences  in a way that both the job market and the recruitment algorithm feel attracted to.

##Homework
Participants should read the article and watch the video below and share them in any social media platform they use.
[Algorithmic Hiring: Why Hire By Numbers?](https://hiring.monster.com/employer-resources/recruiting-strategies/workforce-planning/hiring-algorithms/) (30min)

[Talent 5.0 - Taking Recruitment Practices to a New Level](https://www.youtube.com/watch?v=DiaZsat2rwM) | Stefanie Stanislawski | TEDxUniMannheim (30min)

If participants find any other resource or material, they should add it to the group in Diigo.

##References
* [Open algorithms: A new paradigm for using private data for social good](https://www.devex.com/news/open-algorithms-a-new-paradigm-for-using-private-data-for-social-good-88434)
* [Supercharged HR conference: Who is managing your career, your boss or an algorithm? - 25th October 2018](https://www.youtube.com/watch?v=54Dg-wMidsI)
* Help Wanted: An Examination of Hiring Algorithms, Equity, and Bias - Miranda Bogen and Aaron Rieke PDF
* [How to Make Your Resume ATS-Friendly](https://www.livecareer.com/resources/resumes/basics/optimizing-resume-applicant-tracking-systems)
