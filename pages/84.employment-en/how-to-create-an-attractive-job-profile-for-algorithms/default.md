---
title: 'How to create an attractive job profile for algorithms'
objectives:
    - 'to provide an introduction to how participants can adapt to the new ways of hiring and applying to jobs online'
    - 'to facilitate the transition to new ways to present professional skills and competences that algorithms can recognise'
    - 'to discover how to create and update a good work profile/linkedin profile online as well as a good online cv that is attractive to algorithms'
    - 'to motivate participants to create new CVs that are more attractive to algorithms'
length: '300 min'
---

##Introduction
Group discussion on main difficulties participants had last time they applied to an online job position
The session will start with a group discussion on what were the difficulties and problems participants faced the last time they used and HR platform or the last time they applied for a job position online. The aim of the goal is to list the most common difficulties people encounter when they are looking and applying for work on the internet. It is  important to help participants understand that platforms have limitations but they also offer opportunities.

##Design and plan a good CV online
Trainer will start this session by discussing with participants what are the most used platforms and tools at the moment to look for jobs. The discussion is aimed at brainstorming the most popular tools and trends that participants have used before. 
After this, the trainer, and also through group discussions, will list the main tips and advices on how to make a good CV. The trainer will then help participants to understand the differences between writing a CV that will be read by a human and a CV that is going to be selected by a machine. The aim of this session is to help participants to discover the tips and suggestions to create a CV that is appealing to HR algorithms.

##Use keywords to describe their skills and competences
After naming the most important and popular tips and advices to create an attractive CV to algorithms, the trainer will guide participants to understand how to present their skills, competences and work experience when creating a CV. To create a CV that is attractive to algorithms, it is very important to use the right key words and to present the job applicant in the best possible way. In this session, participants will learn how to present and write their work experience in a way that both the human and the machine recognize. The hiring algorithms demand from job seekers a readjustment of practices and perspectives in order to adjust to the job market today.

##Create an online CV
After the brainstorming discussion, the trainer will invite participants to choose an online platform and create their online CV using what they learnt in the previous exercises. The trainer will support the participants by helping them overcome difficulties and by helping them tailor their CV to their needs. It is not expected that participants have the chance to finish their CVs during the class, but that they create a solid bone structure for the document that they can finish at home and when more convenient to them.

##Group discussion
To wrap up the session, the trainer will ask participants what are the inputs they take away from the experience and what were the main difficulties they faced during the construction of the online CV. This  is a moment to participants share their ideas and questions, but also to learn from each other’s experiences.

##Homework
Participants should read the article and watch the video below and share them in any social media platform they use.
* [Beat the Robots: How to Get Your Resume Past the System & Into Human Hands](https://www.themuse.com/advice/beat-the-robots-how-to-get-your-resume-past-the-system-into-human-hands) (30min)
* [How to optimise your CV for the algorithms](https://social.hays.com/2018/01/04/optimise-cv-algorithms/) (30min)


##References
* [Indeed](https://www.indeed.ch/): job search online platform
* [How To Get Your Resume Past an Algorithm](https://www.youtube.com/watch?v=PVK7tG1LRP8) | WorkingNation
* [How to Get Your Resume Past resume Screening Software (2019)](https://www.youtube.com/watch?v=gxsI-tgM-ZE)