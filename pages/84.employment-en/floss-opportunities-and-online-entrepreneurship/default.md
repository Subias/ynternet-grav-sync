---
title: 'FLOSS opportunities and Online Entrepreneurship'
objectives:
    - 'to provide an introduction to different economic models like the donation culture, the wikinomics, fundraising campaigns, among others'
    - 'to facilitate entrepreneurship and personal branding'
    - 'to discover how the market is changing and how online opportunities can facilitate entrepreneurship and personal branding'
    - 'to motivate new ways of being entrepreneurial'
length: '5 hours'
---

##Introduction
Trainer starts the session by asking participants what was the last item they bought or sold on the internet. From this initial question, trainer will guide a conversation on what it means to be an online entrepreneur. Participants are encouraged to discuss freely their perceptions of how it is possible to be entrepreneur on the internet and what are the benefits and difficulties. Participants will be encouraged to express their doubts and fears concerning entrepreneurship and to learn by sharing individual/personal experiences.

##Online entrepreneurship
Trainer will go through what it means to be an online entrepreneur and what different shapes  fundraise for money on the internet can have. Trainer will explain the different skills and competences an online online entrepreneur must have in order to be successful and how crowdfunding can help develop personal goals and projects. The main goal of this session is to understand how the internet is creating new ways and opportunities for people to collaborate online. The focus of this exercise should be on how important it is to be part of a collaborative online community in order to seize new opportunities and to understand how the market is changing. How online opportunities can facilitate entrepreneurship and personal branding will also be discussed with participants. This segment of the training session is supported by a slide presentation prepared in advance and shared with participants.
Then, the trainer will share a Youtube video https://www.youtube.com/watch?v=yq5zahARudA. After seeing the video, participants should discuss what they saw. The aim of this exercise is to see a specific example of a young online entrepreneur and to know more about his journey to success.

##Online Opportunities and the FLOSS culture
It is very important to understand how the FLOSS culture impacts on the online entrepreneurship and how they can mutually help each other. The trainer will help participants to understand how important it is to be part of an online community in order to seize new economic opportunities and how important it is to share our work back with the community. Crowdfunding and massive online collaboration will be some of the examples the trainer will present. The trainer will guide a group discussion around the topics of the FLOSS movement and the opportunities it creates for online entrepreneurship.

##Different economic models in online entrepreneurship
Participants will be invited to explore some different economic models frequently used in online entrepreneurship and when developing new products. Every organisation and project needs revenue sources to implement activities, whether the activities will be face-to-face or online, in this module we will be focusing on the donation culture, wikinomics , fundraising and other economic models at the heart of web 2.0:
Also, it is important to discuss the principles behind this new business models that have participation and massive online collaboration at their cores. The goal is to understand and recognise online opportunities to finance personal projects and at the same time learn new ways to turn the internet into opportunities for development. This segment of the training session will be supported by a slide presentation prepared and shared with participants in advance.

##How to run a fundraising campaign and how to boost their brand
 The trainer will present some different platforms and tools frequently used in online entrepreneurship to raise money online (crowdfunding platforms, donations platforms, massive online collaboration, etc). It is not the goal of this session to go deeper in any of the tools presented but to present some of the online platforms created to help fundraise and crowdsourcing. It is important to understand new economic models (like the donations model, premium services, wikinomics, among others) and  to be able to select according to the needs of the project.
Trainer will also help participants  to discover the role that personal branding and e-identity can have in online entrepreneurship. During this session, the trainer will help participants understand and identify what makes a good eportfolio and how they can boost their personal image and the image of their company online.

##Homework
Participants should read the article and watch the video below and share them in any social media platform they use.
[How To Write a Business Plan To Start Your Own Business](https://www.youtube.com/watch?v=Fqch5OrUPvA) (30min).
[What they don't tell you about entrepreneurship](https://www.youtube.com/watch?v=f6nxcfbDfZo) | Mark Leruste | TEDxCardiff (30min)


##References
* [Zero Equals One: Creating A Business From Nothing](https://www.youtube.com/watch?v=CzJUCxwz8hk) | Riley Csernica | TEDxCharleston
* [Entrepreneurship Lesson: Through Instagram](https://www.youtube.com/watch?v=eHqfV1AnFg0) | Abe Lim | TEDxMonashUniversityMalaysia