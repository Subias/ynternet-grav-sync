---
title: 'Emprenedoria en línia amb eines FLOSS'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Online entrepreneurship with FLOSS tools'
lang_id: 'Online entrepreneurship with FLOSS tools'
lang_title: ct
length: '10 horas'
objectives:
    - 'Provide skills for developing a business idea into a real business'
    - 'Teach the SWOT analysis'
    - 'Support  learners in defining and understanding market and the different kinds of competition that a business face'
    - 'The product life cycle and the marketing mix'
    - 'Teach basic elements on budgeting'
materials:
    - 'Personal computer'
    - 'Internet connexion'
    - Bearmer
    - 'Basic idea of FLOSS competences'
skills:
    Keywords:
        subs:
            'Online business': null
            Entrepreneurship: null
---

## Introduction

## Methodology
