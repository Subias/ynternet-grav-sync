---
title: 'L''aparició de llicències copyleft i FLOSS'
lang_menu_title: Copyleft
lang_id: Copyleft
lang_title: ct
length: '10 hours'
materials:
    - 'Ordinador personal o telèfon intel·ligent o tauleta digital'
    - 'Connexió a Internet'
    - Projector
    - 'Paper i bolígrafs'
    - Paperògrafs
    - Slidewiki
objectives:
    - 'Els drets d''autor i la reserva de tots els drets d''autor (copyright) han estat objecte de crítiques, amb motiu del desenvolupament de tecnologies que faciliten la còpia i l''intercanvi d''informació'
    - 'Els objectius de copyleft i del FLOSS serveixen a les comunitats educatives i al desenvolupament dels continguts i el coneixement'
    - 'Es pot impulsar el marc "Creative Commons" dirigit a persones autores que prefereixin compartir el seu treball i enriquir el patrimoni comú (els comuns, en anglès "the commons")'
skills:
    'Paraules clau':
        subs:
            'Llicència FLOSS': null
            'Programari de codi obert gratuït / lliure': null
            'Economia política': null
            Copyleft: null
            'Moviment FLOSS': null
            'Marc de llicències': null
            'Cultura FLOSS': null
            Ciutaania: null
            GPL: null
            EUPL: null
---

"Tots els drets reservats", "Marca registrada", "patent", "còpia o reproducció limitada a l'ús estrictament privat" ... Quan parlem de "cultura", sempre tornem a la noció d'apropiació (propietat), en aquest cas, intel·lectual. No obstant això, la tendència a la cultura lliure és que el coneixement pertany a la comunitat i és, en petita mesura, com l'aire i l'aigua, una necessitat bàsica. La cultura copyleft, també anomenada cultura lliure, va néixer de l'món del programari i de les moltes persones que van fer contribucions i que tenien una cosa en comú: el seu sentit de bé comú.

L'expressió "programari lliure" (free software) fa èmfasi en la llibertat, no en el preu. Per comprendre el concepte, hem de pensar en "llibertat d'expressió", no en el "accés lliure". Inspirades en aquesta forma innovadora de pensar sobre com gestionar la producció creativa, altres iniciatives han impulsat gradualment el copyleft en el món del programari.

# Context
L'objectiu de la sessió és demostrar de forma pràctica i atractiva que:
- El dret d'autor i el concepte de “tots els drets reservats” han estat objecte de crítiques, sorgides juntament amb el desenvolupament de tecnologies que faciliten la còpia i l'intercanvi d'informació.
- Els objectius de l'copyleft i de l'FLOSS serveixen a les comunitats educatives i a la producció dels continguts.
- Es pot impulsar el marc "Creative Commons" dirigit a persones autores que prefereixin compartir el seu treball i enriquir el patrimoni comú (dels Comuns, en anglès "the commons").

L'alumnat podrà descriure els arguments ètics, legals, socials, econòmics i d'impacte a favor i en contra de l'FLOSS. Després de decidir quines plataformes / eines / serveis són més útils per a ells i la seva comunitat, la participant desenvoluparà un perfil personal per mostrar les seves fites i resultats.

Les sessions proporcionaran un marc històric i polític de les tecnologies FLOSS i promouran l'ús de FLOSS en l'educació de persones adultes. Estimularem la participació intencional en la cultura lliure i oberta com a part de l'concepte de ciutadania a través de plataformes i eines específiques.

Aquest mòdul es dividirà en 3 sessions:
### Primera sessió: ¿Copiar què? Comprensió i ús de les llicències FLOSS
Aquesta sessió proporcionarà un marc històric i polític de les llicències copyleft i FLOSS, per a poder comprendre i utilitzar les llicències FLOSS com una eina per a l'evolució de les qüestions de propietat, comunitat i drets d'autor al voltant de la societat i la tecnologia. Les participants podran identificar llicències copyleft i FLOSS, i reconèixer els seus objectius.

### Segona sessió: podem trobar llicències FLOSS a tot arreu.
Aquesta segona sessió tindrà com a objectiu establir el context al voltant de les llicències del FLOSS a través d'l'estudi de casos reals. Les persones participants realitzaran una anàlisi crítica de l'ús de les llicències de FLOSS en diverses entitats (UE i internacionals) i el disseny una política de llicència inicial per a la seva pròpia organització.

### Tercera sessió: llicències de FLOSS per a l'educació oberta, l'art i l'aprenentatge col·lectiu.
Aquesta sessió permetrà una anàlisi crítica de la importància de les llicències FLOSS en el camp de la formació no formal. Examinarem els avantatges del copyleft i de les llicències gratuïtes en diferents entorns educatius i connectarem als participants amb els moviments de llicències FLOSS existents.