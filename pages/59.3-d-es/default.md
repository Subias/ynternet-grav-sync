---
title: 'Tecnologías de impresión 3D de código abierto'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Open-source 3-D printing Technologies'
lang_id: 'Open-source 3-D printing Technologies'
lang_title: es
length: '10 horas y 30 minutos'
objectives:
    - 'Beneficiarse de la fabricación digital'
    - 'Utilizar tecnologías digitales abiertas para crear, manipular, preparar y publicar contenido 3D'
    - 'Utilizar tecnologías digitales abiertas para encontrar y aprovechar el repositorio de modelos 3D'
materials:
    - 'Ordenador personal'
    - 'Conexión a Internet'
    - Tinkercard
    - Scketchfab
skills:
    'Palabras clave':
        subs:
            'Fabricación aditiva': null
            'Fabricación digital': null
            'Impresora 3D de código abierto': null
---

_Los significativos avances en las tecnologías de fabricación aditiva (AM), comúnmente conocidas como impresión 3D, han transformado en la última década las formas en que los productos se diseñan, desarrollan, fabrican y distribuyen._

_La capacidad y las ventajas de la impresión 3D sobre la fabricación tradicional abren muchas oportunidades para las empresas verticales (aquellas que ofrecen bienes y servicios específicos para una industria, comercio, profesión u otro grupo de clientes con necesidades especializadas): desde el diseño y desarrollo de productos, el servicio de personalización, hasta la reestructuración de la cadena de suministro para una mayor eficiencia._ 
**Comisión Europea valora positivamente la naturaleza disruptiva de la impresión 3D**

La introducción de la fabricación aditiva (AM por sus siglas en inglés), mejor conocida como impresión 3D, surge como una tecnología disruptiva que trae consigo varios cambios e impactos en la producción tradicional. Un número creciente de empresas y nuevos modelos de negocio basados en la AM están surgiendo, creando grandes oportunidades para la economía y la sociedad. Después de proporcionar al alumnado una formación básica sobre la AM, fabricación digital e impresión 3D de código abierto (primera sesión), el curso implicará un aprendizaje intensivo de hardware y software de código abierto y plataformas de código abierto para la creación de objetos 3D.

Las sesiones permitirán al alumnado adquirir algunas habilidades técnicas necesarias para aprovechar al máximo todas las oportunidades que ofrecen las tecnologías 3D. Las personas participantes experimentarán el proceso de diseño diseñando un objeto utilizando tecnologías de impresión 3D de código abierto.

## Contexto
Las impresoras 3D se están convirtiendo rápidamente en un dispositivo de fabricación ampliamente utilizado que ha revolucionado la industria manufacturera. El propósito de este curso de capacitación es presentar a los participantes la AM y los beneficios más poderosos de las tecnologías de impresión 3D y el uso de tecnologías digitales abiertas.
Al final de las sesiones, las personas participantes podrán crear objetos 3D utilizando tecnologías digitales abiertas, compartiendo sus propios proyectos y utilizando los proyectos de otros usuarios.

Este módulo se dividirá en 2 sesiones:
### Primera sesión: transformación digital en la "Fabricación aditiva" (AM)
En las últimas décadas, las comunicaciones, el mundo del diseño la arquitectura y la ingeniería han experimentado sus propias revoluciones digitales. El término "transformación digital" se refiere al uso de datos digitales, conectividad y procesamiento que abarca todos los aspectos de la actividad de fabricación. La AM nos brinda la oportunidad de repensar por completo la forma en que los productos llegan al usuario final. La sesión tiene como objetivo proporcionar a los alumnos una visión general amplia del diseño y la fabricación digital, presentando al alumnado la impresión 3D, cómo funciona y cómo se puede utilizar en diversas industrias. Estos/as aprenderán qué es la impresión 3D, cómo funcionan las impresoras 3D y los tipos de objetos que pueden hacer con esta tecnología.
### Segunda sesión: exploremos tecnologías digitales abiertas para diseñar, desarrollar, personalizar y compartir productos 3D
El futuro de la fabricación es digital. Las impresoras 3D se están convirtiendo rápidamente en un dispositivo de fabricación ampliamente utilizado que ha revolucionado la industria manufacturera. En el trabajo de impresión 3D, todo comienza con un modelo 3D. Las sesiones tienen como objetivo introducir al software y plataformas de código abierto para diseñar y compartir contenidos en 3D. Los/las estudiantes experimentarán el proceso de diseño diseñando / encontrando objetos 3D usando Tinkercad (plataforma para publicar, compartir y descubrir contenido 3D en la web, dispositivos móviles, AR y VRI) y Sketchfab (herramienta para publicar y encontrar modelos 3D en línea). Los/las participantes adquirirán las habilidades técnicas necesarias para comenzar en la AM y para aprovechar al máximo todas las oportunidades que ofrecen las tecnologías 3D.

Las impresoras 3D se están convirtiendo rápidamente en un dispositivo de fabricación ampliamente utilizado que ha revolucionado la industria manufacturera. El propósito de este curso de capacitación es presentar a las personas participantes la AM y los beneficios más poderosos de las tecnologías de impresión 3D y el uso de tecnologías digitales abiertas. Al final de las sesiones, los/las participantes podrán crear objetos 3D utilizando tecnologías digitales abiertas, compartiendo sus propios proyectos y utilizando los proyectos de otros/as usuarios.