---
title: 'Système d''exploitation ouvert comme transition vers FLOSS: GNU / Linux'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Open operating system as a transition to FLOSS: GNU / Linux'
lang_id: 'Open operating system as a transition to FLOSS: GNU / Linux'
lang_title: fr
length: '10 hours and 30 minutes'
objectives:
    - 'to provide a save environment to participants in order to migrate to GNU / Linux in their work spaces'
    - 'to create a support network among participants'
    - 'to promote the creation of a peer-to-peer network, to provide support among the different agents of the territory involved in the dissemination of PLL'
materials:
    - 'Computer with internet connection'
    - 'Update web browser'
    - 'A PC where you can do the installation tests (you can lose the saved data)'
    - '1 or 2 USB for OS installations'
skills:
    Keywords:
        subs:
            'GNU / Linux': null
            Linus: null
            OS: null
            'Operative Systems': null
            migration: null
            'system administration': null
---

## Introduction

## Context