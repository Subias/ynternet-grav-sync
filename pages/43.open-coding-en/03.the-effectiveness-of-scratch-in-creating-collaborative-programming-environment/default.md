---
title: 'The effectiveness of Scratch in creating collaborative programming environment'
length: '3 hours'
objectives:
    - 'to identify the benefits of the Scratch online community'
    - 'to encourage the sharing, reuse, and combination of code'
    - 'to foster learners’ collaborative learning'
---

##Join and benefit from the Scratch online community 
Presentation and analysis of the benefits of Scratch online community. It has a [large and active membership](https://scratch.mit.edu/statistics/) designed for users who want to create and share projects. 
Besides sharing a project from the Scratch Online Editor, users can also view and remix another user’s project, getting inspired by others at the same time

##Homework
1. Go to http://www.scratch.mit.edu and log in. Select the title of the project to share and make it accessible by all the users. 
2. Go to the Scratch Library, choose a project and remix it. 

##References
* [Scratch Community Guidelines](https://en.scratch-wiki.info/wiki/Community_Guidelines)
* [Project sharing](https://en.scratch-wiki.info/wiki/Project_Sharing)
* [Remix](https://en.scratch-wiki.info/wiki/Remix)