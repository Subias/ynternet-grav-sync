---
title: 'DigCompEdu-kader voor een gemeenschappelijk en open onderwijs'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'DigCompEdu framework for a common and open education'
lang_id: 'DigCompEdu framework for a common and open education'
lang_title: nl
skills:
    Sleutelwoorden:
        subs:
            DigCompEdu: null
            Kader: null
            'Digitale competenties': null
            'Open onderwijs': null
            Volwassenenonderwijs: null
            'FLOSS cultuur': null
            'Leren, aanleren': null
            Competenties: null
materials:
    - PC
    - Internetverbinding
    - 'DigCompEdu framework'
    - 'Papier en schrijfgerief'
objectives:
    - 'Het DigCompEdu-kader begrijpen'
    - 'In kaart brengen van eigen competenties op het platform'
    - 'Het plannen van de bijscholing van digitale competenties voor onderwijsdoeleinden'
    - 'Hoe creatieve onderwijsactiviteiten kunnen worden opgezet dankzij digitale competenties'
length: '8 hours'
---

"Een wetenschappelijk verantwoord kader dat beschrijft wat het betekent voor opvoeders om digitaal competent te zijn. Het biedt een algemeen referentiekader om de ontwikkeling van opvoeder specifieke digitale competenties in Europa te ondersteunen." Deze module geeft een overzicht van The Digital competence framework for Educators. In dit Europees kader worden 22 competenties belicht, georganiseerd in zes verschillende domeinen. De focus ligt echter niet op technische vaardigheden. Het doel van het kader is eerder om in detail te beschrijven hoe digitale technologieën kunnen worden gebruikt om onderwijs en opleidingen te verbeteren en te innoveren. De module richt zich op die gebieden die zeer relevant zijn voor het volwassenenonderwijs en legt uit hoe het kader van groot belang kan zijn voor leerkrachten en wat de voordelen zijn van het ontwikkelen van dergelijke competenties met behulp van open-source technologieën. Er zullen praktische voorbeelden worden gegeven met betrekking tot de zes domeinen, zodat de studenten weten welke open middelen en instrumenten er in het veld beschikbaar zijn.

## Context
Het doel van de module is om leerlingen praktisch te demonstreren en te betrekken hoe:
- het DigCompEdu-kader te begrijpen. 
- het in kaart brengen van je eigen competenties op het framework
- het plannen van de bijscholing van digitale competenties voor onderwijsdoeleinden
- creatieve onderwijsactiviteiten op te zetten dankzij digitale competenties

Het doel van deze vorming is om de deelnemers kennis te laten maken met het DigCompEdu framework, zodat ze het document kunnen gebruiken om te leren en aan te leren.
Aan het einde van de sessies zijn de deelnemers in staat om het framework te begrijpen en bestaande hulpmiddelen en methodologieën te identificeren die kunnen helpen bij het ontwikkelen van vaardigheden die in DigCompEdu zijn beschreven.

Deze module zal worden opgedeeld in 2 sessies:

### Eerste sessie: Het DigCompEdu framework
In deze eerste sessie zullen de deelnemers kennismaken met het framework om hen te voorzien van de theorie over de structuur en de doelstellingen van het document. Het begrijpen en kunnen herkennen van digitale competenties die nodig zijn in de 21e eeuw is een belangrijke doelstelling in het onderwijs. De deelnemers worden voorbereid om in de tweede sessie hun uitdagingen te identificeren en hun competenties in kaart te brengen.

### Tweede sessie: Hoe u het kader kunt gebruiken om uw competenties in kaart te brengen en de kennis van de deelnemers over het kader te vergroten
Vanuit de zes gebieden van de DigCompEdu worden trainers en leerkrachten gevraagd hun eigen competenties in kaart te brengen om zo uitdagingen in kaart te brengen en hun bijscholing te plannen. Er zullen enkele casestudies worden gepresenteerd om de deelnemers een aantal concrete voorbeelden te geven van hoe ze voordeel kunnen halen uit het raamwerk voor hun onderwijscontext.