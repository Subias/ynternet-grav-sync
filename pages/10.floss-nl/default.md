---
title: 'De FLOSS cultuur'
lang_menu_title: 'The FLOSS culture'
lang_id: 'The FLOSS culture'
lang_title: nl
length: '10 hours'
objectives:
    - 'Om de belangrijkste concepten achter de FLOSS-cultuur in kaart te brengen en te verkennen'
    - 'De onderlinge afhankelijkheid van FLOSS en Open Educational Resources (Open onderwijsmateriaal of OER''s) te begrijpen'
    - 'Om ze te koppelen aan de opleiding van Open Educational Resources (Open onderwijsmateriaal of OER''s)'
    - 'Om de ethische, juridische, sociale, economische en impact-argumenten voor en tegen FLOSS te ontdekken'
    - 'Om te beslissen welke platformen/tools/diensten het meest nuttig zijn voor zichzelf en hun gemeenschap'
materials:
    - 'Persoonlijke computer (smartphone of tablet verbonden met het internet)'
    - Internetverbinding
    - Projector
    - 'Papier en pennen'
    - Flipchart
skills:
    Sleutelwoorden:
        subs:
            FLOSS: null
            'Vrije/Libre Open-Source Software': null
            'Politieke economie': null
            Copyleft: null
            FLOSS-beweging: null
            Beleidskader: null
            FLOSS-cultuur: null
            'Netizenship / Digitaal burgerschap': null
image: open_AE1.png
---

![https://proto4.ynternet.org/#overview](open_AE1.png)
# De fundamenten van FLOSS: motivatie, ideologie en praktijk
Hoewel FLOSS-praktijken geen nieuw fenomeen zijn, lijken veel aspecten van dit domein nog steeds onbekend. Dit opleidingsprogramma voorziet concrete methodieken en tools die FLOSS vormen en illustreert de basisprincipes achter de vrije/libre open source beweging en de beste voorbeelden van FLOSS praktijken in Europa. Ons doel is om leerlingen te ondersteunen bij het overwegen en gebruiken van Free/Libre Open-Source Software als een instrument voor sociale en economische ontwikkeling. In het algemeen biedt het trainings scenario een historisch en beleidsvormend kader voor FLOSS-technologieën.

Het is ontworpen om het gebruik van FLOSS in het volwassenenonderwijs te bevorderen en de opzettelijke deelname aan de vrije en open cultuur te stimuleren als onderdeel van wat wij het "Netizenship" noemen. Netizenship verwijst naar een bredere manier van zijn en handelen als burgers van het internet. Dit met name gericht op het begrijpen van de commons, het bewust communiceren en het aannemen van een actieve benadering van internet praktijken en -technologieën.

![](floss.jpg)

##Context
Het doel van deze sessie is om de leerlingen praktisch te demonstreren hoe FLOSS samenwerking bevordert en bijdragen van verschillende partijen in software productie- en innovatie processen alsook hen te betrekken bij de manier waarop. FLOSS heeft een groot sociaal-economisch potentieel door open standaarden, waardoor lock-in wordt vermeden en flexibele oplossingen mogelijk zijn. De leerling kan de ethische, juridische, sociale, economische en impact argumenten voor en tegen FLOSS beschrijven.

Nadat hij heeft besloten welke platformen/instrumenten/diensten het meest nuttig zijn voor zichzelf en hun gemeenschap, zal de onderzoeker een persoonlijk profiel ontwikkelen zodat zijn onderzoeksprofiel en -output duidelijk wordt. De sessies zullen een historisch en beleidsvormend kader van FLOSS-technologieën bieden en het gebruik van FLOSS in het volwassenenonderwijs bevorderen. Het zal de doelbewuste deelname aan de vrije en open cultuur als onderdeel van het Netizenship stimuleren door middel van specifieke platformen en instrumenten.

Deze module zal worden opgedeeld in 3 sessies:

### Eerste sessie: De FLOSS-fundamenten: motivaties, ideologie en praktijk 
Deze sessie geeft de deelnemers de gelegenheid om de belangrijkste concepten achter de FLOSS-cultuur in kaart te brengen en te verkennen. Ook de verschillende doelstellingen binnen de FLOSS-beweging zullen worden onderzocht. Tijdens de sessie zullen verschillende inzichten in deze concepten, doelen en resultaten worden gedemonstreerd. We koppelen ze aan de vorming van Open Educational Resources (Open onderwijsmateriaal of OER's).

### Tweede sessie: FLOSS is overal
De tweede sessie is bedoeld om de huidige stand van zaken rond FLOSS te bepalen. De FLOSS-praktijken in de EU-landen zullen worden gepresenteerd en vergeleken. De deelnemers zullen een kritische analyse maken van het beleidskader van FLOSS in de EU en een eerste beleid voor hun eigen organisatie opstellen.

### Derde sessie: FLOSS als collectief leren, FLOSS in collectief leren
Deze sessie maakt een kritische analyse mogelijk van het belang van FLOSS en Open Educational Resources op het gebied van non-formele training. Er wordt verder ingegaan op het gebruik en de voordelen van open digitale technologieën in het onderwijs, terwijl de actieve en creatieve betrokkenheid van leerlingen wordt gestimuleerd door middel van FLOSS-technologieën, zowel theoretisch als in de praktijk.