---
title: 'Open-source 3-D printtechnologieën'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Open-source 3-D printing Technologies'
lang_id: 'Open-source 3-D printing Technologies'
lang_title: nl
length: '10 hours and 30 minutes'
objectives:
    - 'Om te profiteren van Digitale Fabricage'
    - 'Open digitale technologieën gebruiken om 3D-content te creëren, te manipuleren, voor te bereiden en te publiceren'
    - 'Gebruik maken van open digitale technologieën om 3D-modelopslagplaatsen te vinden en te benutten'
materials:
    - PC
    - Internetverbinding
    - Tinkercard
    - Scketchfab
skills:
    Sleutelwoorden:
        subs:
            'Additieve productie': null
            'Digitale fabricage': null
            'Open source 3D printer': null
---

_De aanzienlijke vooruitgang op het gebied van "Additive Manufacturing" (AM), algemeen bekend als 3D-printing, heeft de afgelopen tien jaar de manier waarop producten worden ontworpen, ontwikkeld, gefabriceerd en gedistribueerd, getransformeerd._

_Het vermogen en de voordelen van 3D-printing ten opzichte van traditionele productiemogelijkheden bieden tal van mogelijkheden voor verticalen, variërend van productontwerp en -ontwikkeling en maatwerk tot herstructurering van de toeleveringsketen voor een hogere efficiëntie._ 
**European Commission, The disruptive nature of 3D printing**

De introductie van additive manufacturing (AM), beter bekend als 3D printing, komt naar voren als een verstorende technologie die een aantal veranderingen en effecten op het traditionele product met zich meebrengt. Een groeiend aantal bedrijven en nieuwe bedrijfsmodellen op basis van AM zijn ontstaan, wat grote kansen biedt voor de economie en de maatschappij. Na de studenten te hebben voorzien van een basis achtergrond van AM, digitale fabricatie en Open Source 3D Printing (1ste sessie), zal de cursus een intensief leren van open source hardware en software en open source platformen voor het maken van 3D-objecten inhouden.

De sessies zullen de studenten de mogelijkheid bieden om enkele technische vaardigheden te verwerven die nodig zijn om alle mogelijkheden van de 3D-technologieën ten volle te benutten. De deelnemers zullen het ontwerpproces ervaren door een object te ontwerpen met behulp van open-source 3D-print technologieën.

## Context
3D-printers zijn hard op weg een veelgebruikt productieapparaat te worden dat een revolutie teweeg heeft gebracht in de productie-industrie. Het doel van deze training is om de deelnemers kennis te laten maken met AM en de meest krachtige voordelen van de 3D print technologieën en het gebruik van open digitale technologieën. 
Aan het einde van de sessies zullen de deelnemers in staat zijn om 3D objecten te creëren met behulp van open digitale technologieën, het delen van hun eigen projecten en het gebruik van projecten van andere gebruikers.

Deze module zal worden opgedeeld in 2 sessies:
### Eerste sessie: Digitale transformatie in de productie "Additive Manufacturing" (AM)
In de afgelopen decennia hebben communicatie, beeldvorming, architectuur en techniek allemaal hun eigen digitale revoluties ondergaan. De term "digitale transformatie" verwijst naar het gebruik van digitale data, connectiviteit en verwerking en omvat elk aspect van de productieactiviteit. AM biedt ons de mogelijkheid om de manier waarop producten hun weg naar de eindgebruiker vinden, volledig te heroverwegen. Het doel van de sessie is om leerlingen een breed overzicht te geven van het ontwerp en de digitale productie, waarbij ze kennismaken met het 3D-printen, hoe het werkt en hoe het gebruikt kan worden in verschillende industrieën. De leerlingen leren wat 3D printen is, hoe 3D printen werkt en wat voor soort objecten je kunt maken met deze technologie.
### Tweede sessie: Laten we open digitale technologieën onderzoeken om 3D-producten te ontwerpen, te ontwikkelen, aan te passen en te delen
De toekomst van de productie is digitaal. 3D-printers zijn hard op weg een veelgebruikt productieapparaat te worden dat een revolutie in de productie-industrie teweeg heeft gebracht. In 3D Printing Work begint alles met een 3D model. De sessie is gericht op het introduceren van open source software en platforms voor het ontwerpen en delen van 3D-content. Studenten zullen het ontwerpproces ervaren door het ontwerpen/zoeken van 3D-objecten met behulp van Tinkercad (platform om 3D-content te publiceren, te delen en te ontdekken op het web, mobiel, AR en VRI) en Sketchfab (tool om 3D-modellen online te publiceren en te vinden). De deelnemers zullen de technische vaardigheden verwerven om in AM aan de slag te gaan die nodig zijn om alle mogelijkheden van de 3D-technologieën ten volle te benutten.

3D-printers zijn hard op weg een veelgebruikt productieapparaat te worden dat een revolutie teweeg heeft gebracht in de productie-industrie. Het doel van deze training is om de deelnemers kennis te laten maken met AM en de meest krachtige voordelen van de 3D print technologieën en het gebruik van open digitale technologieën. Aan het einde van de sessies zullen de deelnemers in staat zijn om 3D objecten te creëren met behulp van open digitale technologieën, het delen van hun eigen projecten en het gebruik van projecten van andere gebruikers.