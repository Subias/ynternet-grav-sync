---
title: 'Bienvenue à l''Académie OPENAE !'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Welcome to the OPENAE Academy'
lang_id: 'Welcome to the OPENAE Academy'
lang_title: fr
---

Les scénarios des cours de formation sont les éléments fondamentaux de la boîte à outils OPEN - AE (résultat intellectuel 2). Ils ont été élaborés en tenant compte à la fois de la recherche documentaire et de la recherche sur le terrain des meilleures pratiques concernant l'enseignement des technologies open source dans chaque pays/région partenaire (résultat intellectuel 1). Les modules ont été élaborés sur la base du statu quo et des besoins de chaque pays en ce qui concerne la culture des logiciels libres.

Ces scénarios permettent de guider les éducateurs et les apprenants pendant la période de formation, tant dans les cours ou les étudiants (qui sont les formateures en les centres de compétances numérique) assiste à les cours en tant des exercices en ligne. Dans chaque pays pilote, les partenaires seront responsables de l'organisation des groupes de formation avec un minimum de 8 et un maximum de 10 participants sélectionnés parmi les e-facilitateurs et les prestataires de formation dans le domaine de l'éducation non formelle des adultes.

Le matériel d'apprentissage (présentations ou autres) est enregistré et mis en ligne sur un référentiel collaboratif ouvert appelé SlideWiki. La plateforme [SlideWiki](https://slidewiki.org/deck/127952-2/open-ae-_-academy-slidewiki/deck/127952-2?language=en), en code source libre et en accès libre, utilise des méthodes de crowdsourcing afin de permettre la création, le partage, la réutilisation et le remixage de didacticiels libres. 

### Notre Motivation
De plus en plus, nous voyons des aptitudes et des compétences numériques liées à des solutions logicielles propriétaires. Si les technologies FLOSS sont censées être ouvertes et librement accessibles, la plupart des utilisateurs de FLOSS possèdent déjà certaines compétences en matière de licence et de propriété lorsqu'ils décident d'utiliser des FLOSS. Les nouveaux utilisateurs peu qualifiés sont souvent intimidés ou peu sûrs de leurs propres capacités à utiliser les technologies FLOSS, et peuvent donc choisir d'utiliser des options propriétaires car certaines marques sont davantage associées à des compétences.  Le projet OPEN-AE vise à combler ce fossé et à promouvoir des pratiques et des outils pour rendre la culture ouverte et les logiciels libres plus accessibles aux nouveaux utilisateurs.