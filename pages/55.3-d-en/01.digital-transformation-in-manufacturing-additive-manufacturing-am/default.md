---
title: 'Digital transformation in manufacturing "Additive Manufacturing" (AM)'
length: '180 min'
objectives:
    - 'to identify the key features and benefits of AM'
    - 'to discover how 3D Printers work and the types of objects users can make using this technology'
    - 'to explore the potential of AM as a tool for addressing the Digital Transformation challenges'
---

##Introduction
Description of the activity
→ Group discussion
Trainer will start the introduction to the module by asking participants about their previous knowledge/experience in the field of digital fabrication. Participants will focus on the pros and cons of digital fabrication. Answers will be documented and then reused to customize the training material.

##“What is Additive Manufacturing?”
The history of digital fabrication - from hand production to 3D Printing. All began with the industrial revolution.  Workers transitioned from hand production to new methods carried out with the help of machines. 
Digital fabrication is a type of manufacturing process where the machine used is controlled by a computer. The term Digital Manufacturing has somewhat of a broad meaning that could describe many parts of today’s modern manufacturing process.
This industry is complex and there are many modern fabrication techniques, the most common forms of digital fabrication are: 
* CNC Machining: shapes are cut out of wooden sheets
* 3D Printing: objects are built up out of layers of metal or plastic
* Laser Cutting: materials like metal are burnt or melted by a laser beam
AM also known as 3D printing, is a process that creates a physical object from a digital design. AM printing enables users to produce complex shapes using less material than traditional manufacturing methods. But how Does 3D Printing Work? It all starts with a 3D model. There are many different  3D modeling open source software (e.g.[Tinkercad](https://www.tinkercad.com/), [Skecthfab](https://sketchfab.com/login?next=/feed))  for creating a 3D model or download it from a 3D repository.

![](3D1.png)

##“What You Get – The benefits of Digital Transformation in Manufacturing”
Additive manufacturing allows the creation of lighter, more complex designs that are too difficult or too expensive to build using traditional dies, molds, milling and machining.
AM encompasses many forms of technologies and materials as 3D printing is being used in almost all industries we could think of. From fashion to car manufacturing, 3D printing is changing the way the world designs, creates, and engineers products.

![](3D2.png)

[EPPM](https://www.eppm.com/industry-news/professionals-reveal-3d-printing-benefits/)

“The industry outlook for 3D printing will potentially have a greater impact on the world over the next 20 years than all of the innovations from the industrial revolution combined,” according to Deloitte.
Digital fabrication comes with a range of benefits – some obvious and some less intuitive, from rapid response, reduced lead times, rapid manufacturing and use of unique materials and economies of scale. AM application is limitless, it should be seen as a cluster of diverse industries with a myriad of different applications.  The following are some of the industries that are set to be transformed by 3D printing; construction, automobile, fashion, healthcare, culture (e.g. museum) etc.

##Homework
Participants should watch some video about AM and take notes while they watch the videos:
* [3D Printing and Digital Fabrication](https://www.youtube.com/watch?v=zBAHmLE7_0I)
* [Digital fabrication, a tale of two worlds: Christian Weichel at TEDxLancasterU](https://www.youtube.com/watch?v=nm-qtr1atrA)

##References
* [Introduction to digital fabrication](https://www.youtube.com/watch?v=fv9_n76QVPk)
* [Getting started with digital fabrication](https://blackspectacles.com/videos/what-is-digital-fabrication)
* [What is additive manufacturing?](https://www.ge.com/additive/additive-manufacturing)   
* [3D Printing](https://3dprinting.com/what-is-3d-printing/)
* [What is 3D printing?](https://www.youtube.com/watch?v=Vx0Z6LplaMU)
* [Wikipedia – 3D Printing](https://en.wikipedia.org/wiki/3D_printing)
