---
title: 'Open-source 3-D printing Technologies'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Open-source 3-D printing Technologies'
lang_id: 'Open-source 3-D printing Technologies'
lang_title: en
length: '10 hours and 30 minutes'
objectives:
    - 'Benefit from the Digital Fabrication'
    - 'Use open digital technologies to create, manipulate, prepare and publish 3D content'
    - 'Use open digital technologies to find and take advantage of 3D model repository'
materials:
    - 'Personal computer'
    - 'Internet connexion'
    - Tinkercard
    - Scketchfab
skills:
    Keywords:
        subs:
            'Additive manufcturing': null
            'Digital fabrication': null
            'Open source 3D printer': null
---

_Significant advances in additive manufacturing (AM) technologies, commonly known as 3D printing, over the past decade have trans-formed the ways in which products are designed, developed, manufac-tured, and distributed._

_3D printing’s ability and advantages over traditional manufacturing open plenty of opportunities for verticals, spanning from product design and development, customization service, to restructuring of supply chain for higher efficiency._ 
**European Commission, The disruptive nature of 3D printing**

The introduction of additive manufacturing (AM), better known as 3D printing, emerges as a disruptive technology that brings with it several changes and impacts to the traditional product. A growing number of companies and new business models based on AM are emerging, cre-ating great opportunities for the economy and society.
After providing students with a basic background of AM, digital fabrica-tion and Open Source 3D Printing (1st session), the Course will involve an intensive learning of open source hardware and software and open source platforms for creation of 3D objects.

The sessions will allow learners to acquire some technical skills needed to take full advantage of all the opportunities offered by 3D technologies. Participants will experience the design process by designing an object using Open-source 3-D printing technologies.

## Context
3D printers are fast becoming a widely used manufacturing device which has revolutionised the manufacturing industry. The purpose of this training course is to introduce participants to AM and the most powerful benefits of the 3D Printing Technologies and the use of open digital technologies. 
At the end of the sessions, participants will be able to create 3D objects using open digital technologies, sharing their own projects and using other user’s projects.

This module will be divided into 2 sessions:

### First session: Digital transformation in manufacturing "Additive Manufacturing" (AM)
In recent decades, communications, imaging, architecture and engineering have all undergone their own digital revolutions. The term “digital transformation” refers to the use of digital data, connectivity, and processing encompasses every aspect of manufacturing activity. AM provides us with an opportunity to completely rethink the way products find their way to the end user. 
The session aims to provide learners with a broad overview of design and digital fabrication, introducing learners to the 3D printing, how it works and how it can be used in various industries. 
Students will learn what 3D Printing is, how 3D Printers work, and the types of objects you can make using this technology.

### Second session: Let’s explore open digital technologies to design, develop, customize and share 3D products
The future of manufacturing is digital. 3D printers are fast becoming a widely used manufacturing device which has revolutionised the manufacturing industry. In 3D Printing Work, all starts with a 3D model.
The session aims at introducing open source software and platforms for designing and sharing 3D contents. Students will experience the design process by designing/finding 3D objects using Tinkercad (platform to publish, share, and discover 3D content on web, mobile, AR, and VRI) and Sketchfab (tool for publishing and finding 3D Models online).
Participants will acquire the technical skills to get started in AM needed to take full advantage of all the opportunities offered by 3D technologies.