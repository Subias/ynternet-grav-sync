---
title: 'Let’s explore open digital technologies to design, customize and share 3D products'
length: '6 hours'
objectives:
    - 'to explore 3D printing software settings'
    - 'to recognize the principles of designing models for 3D printing'
    - 'to produce models and prototypes'
    - 'to use open digital technologies to design, publish, share, and discover 3D content'
    - 'to encourage the sharing and reuse of 3D content'
    - 'to explore the potential of the open digital technologies as a tool for addressing the Digital Transformation challenges'
    - 'to benefit from collaborative programming environment'
---

##Introduction
Description of the activity
→ Group discussion on open digital technologies to design and share 3D content 
Trainer will start the introduction to the module by asking participants about their 3D modelling experience. 

##Design, create and invent with new media: what one actually learns from 3D printing?
Presentation and analysis of the benefits of 3D printing. Considering that hat digital fabrication is the merging of the human with the technical, the result is a creative product formed from their ideas and executed through a series of complex design decisions. So, through 3D design and making, users have the opportunity to develop multiple skills, not only professional (proficiency in 3D) , but also in spatial development and a variety of mathematical concepts as well as personal ones.

In fact, the use of digital fabrication technologies promotes curiosity-driven, self-directed, creative learning. It supports users to think creatively, reason systematically– essential skills for life in the 21st century.

##The 3D design software Tinkercad 
Tinkercad is a free and easy to use 3D design software that helps 3D starters all over the world think, create and make. 
Tinkercad was founded as a company in 2010 in the European Union by Kai Backman and his cofounder Mikko Mononen, with a goal to make 3D modeling, especially the design of physical items, accessible to the general public, and allow users to publish their designs under a Creative Commons license.

Since it became available, it has become a popular platform for creating models for 3D modeling and  printing.
Tinkercad uses a simplified constructive solid geometry method of constructing models. A design is made up of primitive shapes that are either "solid" or "hole". Combining solids and holes together, new shapes can be created, which in turn can be assigned the property of solid or hole.  Shapes can be imported in three formats: STL and OBJ for 3D, and 2-dimensional SVG shapes for extruding into 3D shapes. Tinkercad exports models in STL or OBJ formats, ready for 3D printing.

3D starters can learn the basics of creating digital 3D models that could be printed on a 3D printer. Users can work their way through several tutorials to learn the basics of the program for creating and sharing their Tinkercad Projects.

##Sketchfab: the importance of the collaborative programming environment 
Sketchfab is the world's largest platform to publish, share, and discover 3D content on web, mobile, AR, and VR. Such open digital technology plays a key role in empowering a new era of creativity by making it easy for anyone to publish and find 3D content online.
Being the largest platform for immersive and interactive 3D, Sketchfab allows users to share their 3D work showcasing their skills, as well as share and embed 3D models anywhere online.

##Homework
####TINKERCAD
* Login to your Tinkercad account and, following the [instructions](https://www.tinkercad.com/learn/overview/OEFU95SIYKFZ0ZY;collectionId=O2C1PXBIQ2KHCOD) make a ring using cylinder shapes. After designing a 3D basic ring, you are now ready to design a pair of glasses.  
####SKETCHFAB
* Upload your 3D Model to your Sketchfab profile

Go to _this model page_ and click Download under the viewer to download the FBX file available. Once you have a 3D model ready, it’s time to upload. _Login_ to your Sketchfab account.
* Check the Appearance

Once the model is done uploading, users can take advantage of a number of _Sketchfab tools_ to make it look even better.
* 3DSetting

Using the Scene tab, can change your _model's orientation_ by 90° increments around the x-, y-, or z-axis, or you can fine-tune it by selecting “Show advanced rotation”.
* And now... publish and share your 3D Model!

It is now time to publish your 3D Model so people can see and use it!  Under Manage Your Model) you can to make your 3D model visible online and downloadable. Then, show off your model on your social media. 

##References
* [What is Tinkercad?](https://www.youtube.com/watch?v=ZMe22tYVisI)
* [How to create a Project in Tinkercad](https://www.youtube.com/watch?v=VVxakDjgzqM)
* [Tinkercad | Beginner Tea Cup Modeling](https://www.youtube.com/watch?v=ROztt1nlpSk)
* [Getting started with Sketchfab](https://help.sketchfab.com/hc/en-us/articles/203994889-Getting-Started-with-Sketchfab)
* [Sketchfab Video Tutorials](https://www.youtube.com/channel/UCX0J-m95VqCQIIiQ23fP5tg)
* [Wikipedia](https://en.wikipedia.org/wiki/Sketchfab)