---
title: 'Marc DigCompEdu per a una educació compartida i oberta'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'DigCompEdu framework for a common and open education'
lang_id: 'DigCompEdu framework for a common and open education'
lang_title: ct
skills:
    'Paraules clau':
        subs:
            DigCompEdu: null
            'Marc de referència': null
            'Competències digitals': null
            'Educació oberta': null
            'Educació de persones adultes': null
            'Cultura de FLOSS': null
            Formació: null
            Habilitats: null
materials:
    - 'Ordinador personal o telèfon intel·ligent o tauleta digital'
    - 'Connexió a Internet'
    - 'Marc de competències digitals DigCompEdu'
    - 'Paper i bolígrafs'
objectives:
    - 'Comprendre el marc DigCompEdu'
    - 'Situar les competències personals en el marc europeu'
    - 'Planificar la millora de les competències digitals amb finalitats educativas'
    - 'Saber organitzar activitats formatives creatives amb el suport de les competències digitals'
length: '8 hores'
---

"... un marc científicament sòlid que descriu el que significa per a les educadores ser digitalment competents. Proporciona un marc de referència general per donar suport al desenvolupament de competències digitals específiques per a educadores a Europa ". Aquest mòdul proporciona una visió general de el marc de competència digital per a educadores, que presenta 22 competències organitzades en sis àrees. L'enfocament no està en les habilitats tècniques, per contra, el marc té com a objectiu detallar com les tecnologies digitals es poden utilitzar per millorar i innovar l'educació i la formació. El mòdul es centrarà en aquelles àrees altament rellevants per a l'educació d'adults, explicant com el marc pot ser de gran importància per a les educadores i quins són els beneficis de desenvolupar aquestes competències utilitzant tecnologies de codi obert. Es donaran exemples pràctics amb referència a les sis àrees perquè les persones participants sàpiguen quins recursos i eines obertes estan disponibles.

## Context
El propòsit d'aquesta formació és presentar a les participants el marc DigCompEdu perquè puguin aprofitar el document en els processos d'ensenyament i aprenentatge.
A la fi de les sessions, les participants podran comprendre el marc i identificar les eines i metodologies existents que poden ajudar a desenvolupar les habilitats descrites en DigCompEdu. També podran construir el mapa de les competències personals de cara a planificar la millora d’aquestes amb finalitat educativa i configurar activitats de formació creatives amb el suport d’aquestes competències digitals.

Aquest mòdul es dividirà en 2 sessions:

### Primera sessió: el marc DigCompEdu
En aquesta primera sessió, analitzarem el marc europeu per obtenir les nocions teòriques sobre l'estructura i els objectius de el document. Comprendre i ser capaç de reconèixer les competències digitals necessàries en el segle XXI és un factor clau en l'educació. Les participants estaran preparades per identificar les seves debilitats i mapejar les seves competències en la següent sessió.

### Segona sessió: com utilitzar el marc per mapejar les competències i habilitats personals
Es posarà en pràctica el coneixement de les participants sobre el marc i, a partir de les sis àrees de DigCompEdu, es requerirà que les formadores situïn en el marc les seves pròpies competències, per tal d’identificar debilitats i planificar la millora de les seves habilitats i coneixements. Es presentaran alguns estudis de cas per proporcionar a les participants alguns exemples concrets sobre com aprofitar el marc per als seus contextos educatius i formatius.