---
title: 'Copywhat ? Understanding and use of FLOSS licences'
length: '180 min'
objectives:
    - 'to map and explore the main concepts behind FLOSS licences'
    - 'to understand the interdependence of various FLOSS and non-FLOSS licences'
    - 'to discover the ethical, legal, social, economic arguments for and against FLOSS licences'
    - 'to decide which licence/platforms/ are most useful for themselves and their project, and community'
---

##Introduction
Description of the activity
→ Group discussion
Trainer will start the introduction to the module by asking participants about their current and previous experiences to copyright and copyleft. Answers will be documented and then reused to customize the training material.

##The emergence of copyleft
Networks constitute the new social morphology of our societies, and the diffusion of networking logic substantially modifies the operations and outcomes in processes of production, experience of power and culture.” says philosopher Manuel Castels. This process is at the same time a threat and opportunity around “enclosing the commons of the mind” (see: J. Boyle, [The Public Domain](http://www.thepublicdomain.org/download): Enclosing the Commons of the Mind (New Haven, CT: Yale University Press, 2008), 221). A counter  to enclosing  the  commons of the mind is the emergence of copyleft licenses. A copyleft license employs copyright law to maintain the openness of intellectual property. The first license, the GNU General Public Licenses (GNU GPL), was born out of the FLOSS movement. An open content licensing system by Creative Commons (CC) did for cultural content what GNU GPL did for software; maintain its openness. The goal of these licenses is to maximize the use of information while minimizing  transaction  costs.

##Distinguishing a copyleft license (group work)
In this activity, we will revisit the documentation results of the introductory session in order to Distinguishing a copyleft license.

##Homework
Participants should study the [Wikipedia Copyleft article](https://en.wikipedia.org/wiki/Copyleft) in the language of their choice and then complete the External Links session with an example of their own.

##References
* A collection of related resources is available on a collective - social bookmarking space: [Diigo - eCulture](https://groups.diigo.com/group/e_culture).
* [CNU - Licenses - Copyleft](https://www.gnu.org/licenses/copyleft.html).
* [The Ethical Open Source Licence](https://firstdonoharm.dev).