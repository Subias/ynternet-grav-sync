---
title: 'FLOSS licences in real life scenarios'
length: '240 min'
objectives:
    - 'to provide examples of FLOSS licences in real life scenarios'
    - 'to analyse the impact of the use of FLOSS licences'
    - 'to discover the connection with the EU policy framework on copyright'
    - 'to motivate educated policy contribution in the FLOSS area'
---

##Introduction
Description of the activity
→ Group discussion on “how is your project licensed today?”
Trainer will start the introduction to the module by asking participants about the licensing initiatives around them - mostly coming from other organisations or the EU. Answers will be documented and then reused to customize the training material.

##FLOSS in real life scenarios
How are the projects below licenced? 
* [Farm Hack](https://farmhack.org/tools)
* [Open Motors](https://www.openmotors.co/)
* [Fold-it](https://fold.it/portal/)
* [Open Insulin Project](https://openinsulin.org/)
* [Wikihouse](https://www.wikihouse.cc/ )
* Any wikipedia article  

More licenses and concepts:
[The Peer Production License](https://wiki.p2pfoundation.net/Peer_Production_License)
[The case of Copyfarleft](https://wiki.p2pfoundation.net/Copyfarleft)

##An EU policy framework for FLOSS licenses
Presentation and analysis of the main EU FLOSS policy initiatives including the European Union Public Licence (the ‘EUPL’) applies to the Work (as defined below) which is provided under the terms of this Licence. Any use of the Work, other than as authorised under this Licence is prohibited (to the extent such use is covered by a right of the copyright holder of the Work). Compatibility and interoperability.
https://joinup.ec.europa.eu/collection/eupl/news/understanding-eupl-v12
https://joinup.ec.europa.eu/collection/eupl/news/eupl-or-gplv3-comparison-t

##Your license to save the world
Everyone opens his/her laptop
Join a group to design and share your policy proposal for a license that you think will make the world a better place to live in. Choose a theme and try to find ways and solutions as to license products with your intention. We will use platform design techniques to document ideas and challenges for these licenses.

##Homework
See in the following video: [Free software, free society: Richard Stallman at TEDxGeneva 2014](https://www.youtube.com/watch?v=Ag1AKIl_2GM), and then post in the comments section another video that explains - demonstrates similar arguments. Share your thoughts on the new video

##References
* "[How to choose a license for your own work](https://www.gnu.org/licenses/license-recommendations.html#libraries)". Free Software Foundation's Licensing and Compliance Lab.
* [Free software, free society: Richard Stallman at TEDxGeneva 2014](https://www.youtube.com/watch?v=Ag1AKIl_2GM)
* [The open data institute](https://theodi.org)