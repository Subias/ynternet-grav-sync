---
title: 'The emergence of copyleft and FLOSS licences'
lang_menu_title: Copyleft
lang_id: Copyleft
lang_title: en
length: '10 hours'
materials:
    - 'Personal computer (Smartphone or tablet connected to the Internet)'
    - 'Internet connexion'
    - Beamer
    - 'Paper and pens'
    - Flipchart
    - 'Slidewiki Academy'
objectives:
    - 'Copyright and author''s rights have been the target of criticism, with the development of technologies that facilitate information copying and sharing'
    - 'The goals of copyleft and FLOSS serve educational communities and content'
    - 'The Creative Commons framework aimed at authors who prefer to share their work and enrich the common heritage (the Commons) can be activated'
skills:
    Keywords:
        subs:
            'FLOSS licences': null
            'Free/Libre Open-Source Software': null
            'Political economy': null
            Copyleft: null
            'FLOSS movement': null
            'Licence framework': null
            'FLOSS culture': null
            Netizenship: null
            GPL: null
            EUPL: null
---

"All rights reserved", "Trademark", "patent", "copying or reproduction limited to strictly private use" ... When we talk about "culture", we are always brought back to the notion of appropriation (property), in this case, intellectual. Yet, the trend in free culture, is that ideas belong to everyone, and are, to a small extent like the air and water, our basic needs. The copyleft culture, also called free culture, was born from the world of software and the many contributors who had one thing in common: their sense of the common good.

The expression "free software" refers to freedom, not price. To understand the concept, you have to think of "freedom of expression", not "free access". Inspired by this innovative way of thinking about how to handle creative output, other initiatives have gradually moved copyleft out of the software world.

# Context
The goal of the session is to practically demonstrate and engage learners on how: 
- Copyright and author's rights have been the target of criticism, with the development of technologies that facilitate information copying and sharing.
- The goals of copyleft and FLOSS serve educational communities and content 
- The Creative Commons framework aimed at authors who prefer to share their work and enrich the common heritage (the Commons) can be activated.

The learner will be able to describe the ethical, legal, social, economic, and impact arguments for and against FLOSS licencing. After deciding which licence/platforms/tools/services are most useful for themselves and their community, the researcher will develop a personal profile for showcasing their research profile and outputs.

The sessions will provide a historical and policy framework of FLOSS licences and promote their in Adult Education. It will stimulate the intentional participation in the free and open culture as part of the Netizenship through specific platforms and tools.

This module will be divided into 3 sessions:
### First session: Copywhat ? Understanding and use of FLOSS licences 
This session will provide a historical and policy framework of copyleft and FLOSS licences. Understanding and being able to put in use FLOSS  licences as a tool for evolving property, community and copyright questions around society and technology. Participants will be able to identify copyleft and FLOSS  licences and recognise their goals.

### Second session: FLOSS licences in real life scenarios is everywhere
The second session will aim at setting the context around FLOSS licences with actual case studies Participants will engage in a critical analysis of the use of FLOSS licences in various entities (EU and international) and design an initial licence policy for their own organisation.

### Third session: FLOSS licences for open education, art and collective learning
This session will allow for a critical analysis of the importance of FLOSS licences  in the field of non-formal training. It will continue by examining the advantages of copyleft and free licences in different educational environments and connect participants with existing FLOSS licencing movements.