---
title: 'The FLOSS fundamentals: motivations, ideology and practice'
length: '180 minutes'
objectives:
    - 'to map and explore the main concepts behind FLOSS culture'
    - 'to understand the interdependence of FLOSS and Open Educational Resources (OERs)'
    - 'to link them to the formation of Open Educational Resources (OERs)'
    - 'to discover the ethical, legal, social, economic, and impact arguments for and against FLOSS'
    - 'to decide which platforms/tools/services are most useful for themselves and their community'
---

This session will give the opportunity to its participants to map and explore the main concepts behind FLOSS culture. The different goals within the FLOSS movement will be also examined. During the session, diverse understandings of these concepts, goals and results will be demonstrated. We will link them to the formation of Open Educational Resources (OERs).

##1.- Introduction
Trainer will start the introduction to the module by asking participants about their current and previous experiences to FLOSS culture. Answers will be documented and then reused to customize the training material.

##2.- The FLOSS culture
FLOSS will be reviewed as a conceptual phenomenon based on two basic elements: community and modularity. These elements are set in a parallel mode allowing to dynamically place initiatives and move them according to a third element: need to change to a commons oriented production.
A coherent overview of FLOSS  as socio-economic continuum linked to: ideology, technology and the need for a profound change of production to commons oriented system will be explored

##3.- FLOSS arguments
In this activity, we will revisit the documentation results of the introductory session in order to find and develop arguments (for and against) using FLOSS in our everyday lives.

##Homework
Participants should listen to the [Techno-capitalism's moral and intellectual calamities](http://radioopensource.org/tech-master-disaster-part-one) podcast and post a comment in the [comments section](http://radioopensource.org/tech-master-disaster-part-one/#comments) 
If participants find any other resource or material, they should add it to the group in [Diigo](https://www.diigo.com/).

##References
1. A collection of related resources is available on a [collective-social bookmarking space](https://groups.diigo.com/group/e_culture).
2. Benkler, Y. (2006).The Wealth of Networks: How Social Production TransformsMarkets and Freedom. Yale University Press.
3. Salus, Peter H. (March 28, 2005). "A History of Free and Open Source". Groklaw. Retrieved 2015-06-22.