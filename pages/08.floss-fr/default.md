---
title: 'Culture FLOSS'
lang_menu_title: 'The FLOSS culture'
lang_id: 'The FLOSS culture'
lang_title: fr
length: '10 heures'
objectives:
    - 'Recenser et explorer les concepts principaux qui constituent la culture du FLOSS'
    - 'Comprendre l''interdépendance des ressources éducatives libres et ouvertes (REL)'
    - 'Les relier à la formation de ressources éducatives ouvertes (REL)'
    - 'Découvrir les arguments éthiques, juridiques, sociaux, économiques et d''impact pour et contre les FLOSS'
    - 'Décider quelles sont les plateformes/outils/services les plus utiles pour eux-mêmes et leur communauté'
materials:
    - 'Ordinateur personnel (connecté à internet)'
    - 'Connexion Internet'
    - Bearmer
    - 'Papier et stylos'
    - Flipchart
skills:
    'Mots clés':
        subs:
            FLOSS: null
            'Logiciels libres': null
            'Économie politique': null
            Copyleft: null
            'Mouvement FLOSS': null
            'Cadre politique': null
            'Culture FLOSS': null
            Netizenship: null
image: open_AE1.png
---

![https://proto4.ynternet.org/#overview](open_AE1.png)
# Culture FLOSS
Bien que les pratiques et les péages du FLOSS ne soient pas un phénomène nouveau, de nombreux aspects de ce domaine semblent encore inconnus. Ce scénario de formation fournit des pratiques et des outils concrets formant FLOSS, illustrant les principes fondamentaux du mouvement open source libre / libre et l'état de l'art du FLOSS en Europe. Notre objectif est d'aider les apprenants à considérer et à utiliser les logiciels libres / libres comme outil de développement social et économique.
Dans l'ensemble, le scénario de formation fournit un cadre historique et politique des technologies FLOSS. Il est conçu pour promouvoir l'utilisation du FLOSS dans l'éducation des adultes et stimuler la participation intentionnelle à la culture libre et ouverte dans le cadre de ce que nous appelons la «Netizenship». La Netizenship fait référence à une manière plus large d'être et d'agir en tant que citoyens d'Internet, en se concentrant en particulier sur la compréhension des biens communs, la communication avec intention et l'adoption d'une approche active des pratiques et technologies d'Internet.

![](floss.jpg)

##Contexte
Le but de la session est de démontrer et d'engager les apprenants sur la façon dont:
FLOSS encourage la collaboration et les contributions de différentes parties dans la production de logiciels et les processus d'innovation.
FLOSS possède un grand potentiel socio-économique grâce à des normes ouvertes, évitant le verrouillage et permettant des solutions flexibles.

L'apprenant sera capable de décrire les arguments éthiques, juridiques, sociaux, économiques et d'impact pour et contre FLOSS. Après avoir décidé quelles plates-formes / outils / services sont les plus utiles pour eux-mêmes et leur communauté, le chercheur développera un profil personnel pour présenter leur profil de recherche et leurs résultats.
Les sessions fourniront un cadre historique et politique des technologies FLOSS et favoriseront l'utilisation de FLOSS dans l'éducation des adultes. Il stimulera la participation intentionnelle à la culture libre et ouverte dans le cadre de la Netizenship à travers des plateformes et des outils spécifiques.

Ce module sera divisé en 3 séances :
### Première séance : Les fondamentaux de FLOSS: motivations, idéologie et pratique
Cette séance donnera l'occasion à ses participants de cartographier et d'explorer les principaux concepts de la culture FLOSS. Les différents objectifs du mouvement FLOSS seront également examinés. Au cours de la session, diverses compréhensions de ces concepts, objectifs et résultats seront démontrées. Nous les relierons à la formation des ressources éducatives libres (REL).
### Deuxième séance : FLOSS est partout
La deuxième séance visera à définir l'état de l'art autour de FLOSS. Les pratiques FLOSS dans les pays de l'UE seront présentées et comparées. Les participants entreprendront une analyse critique du cadre politique du FLOSS dans l'UE et concevront une politique initiale pour leur propre organisation.
### Troisième séance : FLOSS comme apprentissage collectif, FLOSS dans l'apprentissage collectif
Cette séance permettra une analyse critique de l'importance des FLOSS et des REL dans le domaine de la formation non formelle. Elle continuera en examinant l'utilisation et les avantages des technologies numériques ouvertes dans l'éducation, tout en encourageant l'engagement actif et créatif des apprenants grâce aux technologies FLOSS à la fois en tant que théorie et pratique.