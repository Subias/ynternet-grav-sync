---
title: 'La aparición de licencias copyleft y FLOSS'
lang_menu_title: Copyleft
lang_id: Copyleft
lang_title: es
length: '10 horas'
materials:
    - 'Ordenador personal o teléfono inteligente o tableta digital'
    - 'Conexión a Internet'
    - Proyector
    - 'Papel y boligrafos'
    - Rotafolio
    - Slidewiki
objectives:
    - 'Los derechos de autor y la reserva de todos los derechos de autor (copyright) han sido objeto de críticas, con motivo del desarrollo de tecnologías que facilitan la copia y el intercambio de información'
    - 'Los objetivos de copyleft y del FLOSS sirven a las comunidades educativas y al desarrollo de los contenidos y el conocimiento'
    - 'Se puede impulsar el marco “Creative Commons” dirigido a personas autoras que prefieran compartir su trabajo y enriquecer el patrimonio común (los Comunes, en inglés “the commons”)'
skills:
    'Palabras clave':
        subs:
            'Licencia FLOSS': null
            'Software de código abierto gratuito / libre': null
            'Economía política': null
            Copyleft: null
            'Movimiento FLOSS': null
            'Marco de licencias': null
            Ciudadania: null
            'Cultura FLOSS': null
            GPL: null
            EUPL: null
---

"Todos los derechos reservados", "Marca registrada", "patente", "copia o reproducción limitada al uso estrictamente privado" ... Cuando hablamos de "cultura", siempre volvemos a la noción de apropiación (propiedad), en este caso, intelectual. Sin embargo, la tendencia en la cultura libre es que el conocimiento pertenece a la comunidad y es, en pequeña medida, como el aire y el agua, una necesidad básica. La cultura copyleft, también llamada cultura libre, nació del mundo del software y de las muchas personas que hicieron contribuciones y que tenían una cosa en común: su sentido del bien común.

La expresión "software libre" (free software) hace énfasis en la libertad, y no en el precio como algunas personas podrían pensar. Para comprender el concepto, debemos pensar en "libertad de expresión", no en el "acceso libre". Inspiradas en esta forma innovadora de pensar acerca de cómo manejar la producción creativa, otras iniciativas han impulsado gradualmente el copyleft en el mundo del software.

# Contexto
El objetivo de la sesión es demostrar de forma práctica y atractiva que: 
- El derecho de autor y el concepto de “todos los derechos reservados” han sido objeto de críticas, surgidas junto con el desarrollo de tecnologías que facilitan la copia y el intercambio de información.
- Los objetivos del copyleft y del FLOSS sirven a las comunidades educativas y a la producción de los contenidos.
- Se puede impulsar el marco “Creative Commons” dirigido a personas autoras que prefieran compartir su trabajo y enriquecer el patrimonio común (los Comunes, en inglés “the commons”).

El alumnado podrá describir los argumentos éticos, legales, sociales, económicos y de impacto a favor y en contra del FLOSS. Después de decidir qué plataformas / herramientas / servicios son más útiles para ellos y su comunidad, la participante desarrollará un perfil personal para mostrar sus metas y resultados.

Las sesiones proporcionarán un marco histórico y político de las tecnologías FLOSS y promoverán el uso de FLOSS en la educación de personas adultas. Estimularemos la participación intencional en la cultura libre y abierta como parte del concepto de ciudadanía a través de plataformas y herramientas específicas.

Este módulo se dividirá en 3 sesiones:
### Primera sesión: ¿Copiar qué? Comprensión y uso de las licencias FLOSS
Esta sesión proporcionará un marco histórico y político de las licencias copyleft y FLOSS, para poder comprender y utilizar las licencias FLOSS como una herramienta para la evolución de las cuestiones de propiedad, comunidad y derechos de autor en torno a la sociedad y la tecnología. Las participantes podrán identificar licencias copyleft y FLOSS, y reconocer sus objetivos.

### Segunda sesión: podemos encontrar licencias FLOSS en todas partes
Esta segunda sesión tendrá como objetivo establecer el contexto alrededor de las licencias de FLOSS a través del estudio de casos reales. Las personas participantes realizarán un análisis crítico del uso de las licencias de FLOSS en varias entidades (UE e internacionales) y el diseño una política de licencia inicial para su propia organización.

### Tercera sesión: licencias de FLOSS para la educación abierta, el arte y el aprendizaje colectivo
Esta sesión permitirá un análisis crítico de la importancia de las licencias FLOSS en el campo de la formación no formal. Examinaremos las ventajas del copyleft y de las licencias gratuitas en diferentes entornos educativos y conectaremos a los participantes con los movimientos de licencias FLOSS existentes.