---
title: Wikidata
lang_menu_title: Wikidata
lang_id: wikidata
lang_title: es
length: '10 horas'
objectives:
    - 'Analizar un contexto sólido para comprender los beneficios de Wikidata y cómo se relaciona con los otros proyectos de Wikimedia, pero también cómo se relaciona con otras bases de datos disponibles en Internet'
    - 'Adquirir habilidades digitales para poder agregar contenido a Wikidata pero también para poder escribir consultas en Sparql para obtener respuestas a consultas complejas'
    - 'Tener la capacidad de presentar Wikidata a los demás y conocer suficientes antecedentes como para identificar cómo podría usarse en un contexto profesional'
    - 'Comprender qué procesos de calidad y sistemas de evaluación favorecen la construcción y recopilación de datos de calidad, de manera que las personas y las máquinas los puedan utilizar'
materials:
    - 'Ordenador personal o teléfono inteligente o tableta digital'
    - 'Conexión a Internet'
    - Proyector
    - 'Papel y boligrafos'
    - Rotafolio
    - Wikidata
skills:
    'Palabras clave':
        subs:
            WikiData: null
            'Base de datos': null
            'Gráfico de conocimiento': null
            Wikipedia: null
            Comunidad: null
            CC0: null
            'Licencias libres': null
            Colaboración: null
            SPARQL: null
---

Wikidata es una plataforma de datos abiertos que pertenece a la familia de sitios web de proyectos. En junio de 2019 albergaba 57 millones de artículos. Wikidata es una base de conocimiento abierta y gratuita que contiene varios tipos de datos (por ejemplo texto, imágenes, cantidades, coordenadas, formas geográficas, fechas ...). La entidad básica en Wikidata es un artículo. Un artículo puede ser una cosa: un lugar, una persona, una idea o cualquier otra elemento. Lo específico de Wikidata es que la información se almacena de forma rígida y estructurada para que los humanos y las máquinas puedan procesarla. Aunque ahora es una base de conocimiento bastante utilizada por las máquinas, es en gran parte desconocida por el público, y tiene un potencial poco comprendido por parte de las personas formadoras. La intención de este módulo es proporcionar un curso de formación de formadoras que proporcionará una visión general y sólida sobre Wikidata y que presentará las oportunidades que se asocian a este proyecto de código abierto.

Este módulo se dividirá en 4 sesiones:

### Primera sesión: Descubriendo y entendiendo qué es y cómo funciona WikiData.
Esta sesión proporcionará una visión general de lo que es Wikidata. Se presentará su historia y las participantes podrán comprender el por qué y los beneficios de su existencia, para los proyectos de wikimedia en particular, y para Internet en general. La sesión cubrirá conceptos básicos de Wikidata: estructura de datos, elementos de vocabulario y sus características de diseño principales, pero también se ofrecerá una visión general de la comunidad y la gobernanza de Wikidata, para que las participantes comprendan el hecho de que wikidata, por técnica que parezca, es principalmente una construcción social. Las participantes explorarán Wikidata por sí mismas y crearán una cuenta.

### Segunda sesión: Edición de WikiData
La segunda sesión tendrá como objetivo lograr que las participantes agreguen contenido a Wikidata por su cuenta y descubran algunos de los procesos fundamentales de edición. También profundizará en los sistemas de calidad y evaluación de datos, y se descubrirán las utilidades más populares, así como las herramientas disponibles para la edición y para la importación en masa.

### Tercera sesión: Consultar WikiData
Esta sesión está destinada a hacer que las participantes descubran y experimenten el servicio de consultas para consultas simples, para posteriormente descubrir el lenguaje de consulta sparql para peticiones más complejas, a través de una metodología de paso a paso, aumentando gradualmente la dificultad. La sesión incluirá actividades prácticas.

### Cuarta sesión: aplicar WikiData
Esta sesión ayudará a las participantes a comprender el valor aplicado de Wikidata, particularmente para aplicaciones como la visualización de datos complejos, pero también a identificar posibles aplicaciones para Wikidata en su propio trabajo, ya sea a través de la visualización, incrustando Wikidata en su propio software o usando Wikidata en Proyectos de Wikimedia. La segunda parte de la sesión les enseñará cómo conectarse con la comunidad de Wikidata para una colaboración más efectiva.