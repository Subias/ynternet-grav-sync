---
title: 'The FLOSS culture'
lang_menu_title: 'The FLOSS culture'
lang_id: 'The FLOSS culture'
lang_title: en
length: '10 hours'
objectives:
    - 'To map and explore the main concepts behind FLOSS culture'
    - 'To understand the interdependence of FLOSS and Open Educational Resources (OERs)'
    - 'To link them to the formation of Open Educational Resources (OERs)'
    - 'To discover the ethical, legal, social, economic, and impact arguments for and against FLOSS'
    - 'To decide which platforms/tools/services are most useful for themselves and their community'
materials:
    - 'Personal computer (smartph0one or tablet connected to the internet)'
    - 'Internet connexion'
    - Bearmer
    - 'Paper and pens'
    - Flipchart
skills:
    Keywords:
        subs:
            FLOSS: null
            'Free/Libre Open-Source Software': null
            'Political economy': null
            Copyleft: null
            'FLOSS movement': null
            'Policy framework': null
            'FLOSS culture': null
            Netizenship: null
image: open_AE1.png
---

![https://proto4.ynternet.org/#overview](open_AE1.png)
# The FLOSS fundamentals: motivations, ideology and practice
Although FLOSS practices and tolls are no new phenomenon, many aspects of this domain still appear unknown. This training scenario provides with concrete practices and tools forming FLOSS, illustrating the fundamentals behind the free/libre open source movement and the state of the art of FLOSS in Europe. Our goal is to support learners to consider and use Free/Libre Open-Source Software as a tool for social and economic development.
Overall, the training scenario 	provides a historical and policy framework of FLOSS technologies. It is designed to promote the use of FLOSS in Adult Education and stimulate the intentional participation in the free and open culture as part of what we call the “Netizenship”. Netizenship refers to a wider way of being and acting as citizens of the internet, particularly focusing on understanding the commons, communicating with intention and adopting an active approach towards internet practices and technologies.

![](floss.jpg)

##Context
The goal of the session is to practically demonstrate and engage learners on how: 
FLOSS promotes collaboration and contributions from different parties in software production and innovation processes.
FLOSS holds great socio economic potential through open standards, avoiding lock-in and allowing for flexible solutions.
The learner will be able to describe the ethical, legal, social, economic, and impact arguments for and against FLOSS. After deciding which platforms/tools/services are most useful for themselves and their community, the researcher will develop a personal profile for showcasing their research profile and outputs.
The sessions will provide a historical and policy framework of FLOSS technologies and promote the use of FLOSS in Adult Education. It will stimulate the intentional participation in the free and open culture as part of the Netizenship through specific platforms and tools.

This module will be divided into 3 sessions:

### First session: The FLOSS fundamentals: motivations, ideology and practice 
This session will give the opportunity to its participants to map and explore the main concepts behind FLOSS culture. The different goals within the FLOSS movement will be also examined. During the session, diverse understandings of these concepts, goals and results will be demonstrated. We will link them to the formation of Open Educational Resources (OERs).

### Second session: FLOSS is everywhere
The second session will aim at setting the state of the art around FLOSS. FLOSS practices in EU countries will be presented and compared. Participants will engage in a critical analysis of the policy framework of FLOSS in EU and design an initial policy for their own organisation.

### Third session: FLOSS as collective learning, FLOSS in collective learning
This session will allow for a critical analysis of the importance of FLOSS and OERs in the field of non-formal training. It will continue by examining the use and advantages of open digital technologies in education, while foster learners' active and creative engagement through FLOSS technologies both as a theory and practice.