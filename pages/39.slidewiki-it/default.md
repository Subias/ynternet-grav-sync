---
title: SlideWiki
lang_menu_title: SlideWiki
lang_id: slidewiki
lang_title: it
length: '10 ore'
objectives:
    - 'Comprendere il framework teorico delle applicazioni Web per la gestione dei contenuti didattici '
    - 'Promuovere l’utilizzo dell’applicazione web Slidewiki per l’Educazione Aperta degli Adulti'
    - 'Promuovere la (co)creazione e condivisione di contenuti didattici'
    - 'Utilizzare  utilizzare sistemi di authoring OpenCourseWare web-based, come SlideWiki'
materiels:
    - 'Personal computer (connesso a Internet)'
    - 'Connessione internet'
    - Bearmer
    - 'Carta e penne'
    - Flipchart
skills:
    Keywords:
        subs:
            Wikipedia: null
            Comunità: null
            CC0: null
            'Licenze libere': null
            Collaborazione: null
            SlideWiki: null
            Presentazioni: null
            'Software aperto e libero': null
            'Cultura FLOSS': null
            'Collaborazione online': null
---

SlideWiki è un è un sistema di authoring [OpenCourseWare](https://en.wikipedia.org/wiki/OpenCourseWare) basato sul web aperto. Supporta la creazione e la gestione dei contenuti di apprendimento e strumenti per la collaborazione /crowd-sourcing, traduzione, comunicazione e valutazione.

SlideWiki è un'applicazione Web che facilita la collaborazione sui contenuti didattici. Con SlideWiki gli utenti possono creare slides, collaborarvi e organizzarle in presentazioni. Le presentazioni possono essere organizzate gerarchicamente, in modo da strutturarle in base al loro contenuto. Viene promossa la formazione di un archivio a contenuto educativo (diverso da testi) tramite  la collaborazione su larga scala (crowd-sourcing).

Slides, presentazioni, diagrammi, test di valutazione ecc. vengono caricati dagli utenti per essere poi riutilizzati da altri utenti. Questi contenuti sono creati principalmente da tutor, insegnanti, docenti e professori, individualmente o in piccoli gruppi. Il contenuto sviluppato può essere condiviso online (ad esempio utilizzando Slideshare, OpenStudy, Google Docs). 
SlideWiki è una piattaforma, dove grandi comunità di insegnanti, docenti e accademici hanno la possibilità di creare contenuti educativi sofisticati in modo collaborativo e aperto. SlideWiki permette di diffondere i contenuti e di educare studenti e peer-researchers più rapidamente, dal momento che l'onere di creare e strutturare il nuovo campo può essere distribuito tra una grande comunità.

Gli specialisti possono concentrarsi sulla creazione di contenuti educativi nella loro particolare area di competenza; tali contenuti possono tuttavia essere facilmente integrati con degli altri, ristrutturati e riproposti. Un aspetto particolare, facilitato da SlideWiki, è il multilinguismo. Poiché tutti i contenuti hanno diverse versioni e sono dettagliatamente strutturati, è facile tradurli semiautomaticamente e tenere traccia delle modifiche in varie versioni linguistiche dello stesso contenuto.


Questo modulo sarà diviso in 3 sessioni:

###Prima sessione - Scoprire e comprendere SlideWiki
Il fine di questa sessione e quello di introdurre i partecipanti alla piattaforma SlideWiki e lasciare spazio alla discussione sui temi di creazione di contenuti e relativo utilizzo da parte di altre persone, diritto d’autore, ma anche disponibilità/accessibilità delle informazioni in contesti educativi. Durante questa sessione i partecipanti impareranno come creare le proprie presentazioni e come importare contenuti nella piattaforma.  
###Seconda sessione – Sperimentare con SlideWiki: creazione e collaborazione su SlideWiki
Il fulcro di questa sessione sarà la modifica delle presentazioni importate dalle raccolte personali, e quelle rese disponibili dalla community Slide Wiki. Il processo di modifica e co-creazione plasma nuove opportunità che i partecipanti avranno la possibilità di esplorare durante questa sessione.
###Terza sessione - Creare corsi online usando SlideWiki e contenuti didattici organizzati
Durante questa ultima parte i partecipanti apprenderanno come creare curriculum didattici e organizzare lezioni online per un determinato argomento; apprenderanno anche in che modo la piattaforma SlideWiki crea nuove opportunità per stimolare l’apprendimento online per gli adulti.