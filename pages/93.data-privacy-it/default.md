---
title: 'Data privacy culture: a FLOSS driven view ITA'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Data privacy culture: a FLOSS driven view'
lang_id: 'Data privacy culture: a FLOSS driven view'
lang_title: it
length: '10 hours'
objectives:
    - 'Présenter le cadre légal et sensibiliser aux enjeux concernant la protection des données d’usages'
    - 'Sensibiliser à la plus-value des logiciels libres pour augmenter la protection des données personnelles d’usages'
    - 'Permettre à l’animateur de situer ses usages et le degré de respect des données personnelles dans son organisation'
materials:
    - 'Ordinateur personnel (smartphone ou tablette connecté)'
    - Internet
    - Bearmer
    - 'Papier et stylos'
    - 'Tableau à feuilles'
    - 'Connexion mobiles Slidewiki Academy'
skills:
    Keywords:
        subs:
            'Protection des données personnelles': null
            RGPD: null
            'Data privacy': null
            'Big data': null
            Surveillance: null
            Publicités: null
            Manipulation: null
            'Influencer les comportements': null
            FLOSS: null
            Libre: null
            'Open source': null
---

## Introduction


## Context
