---
title: 'Une culture de la protection des données'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Data privacy culture: a FLOSS driven view'
lang_id: 'Data privacy culture: a FLOSS driven view'
lang_title: fr
length: '10 hours'
objectives:
    - 'Présenter le cadre légal et sensibiliser aux enjeux concernant la protection des données d’usages'
    - 'Sensibiliser à la plus-value des logiciels libres pour augmenter la protection des données personnelles d’usages'
    - 'Permettre à l’animateur de situer ses usages et le degré de respect des données personnelles dans son organisation'
materials:
    - 'Ordinateur personnel (smartphone ou tablette connecté)'
    - Internet
    - Bearmer
    - 'Papier et stylos'
    - 'Tableau à feuilles'
    - 'Connexion mobiles Slidewiki Academy'
skills:
    Keywords:
        subs:
            'Protection des données personnelles': null
            RGPD: null
            'Data privacy': null
            'Big data': null
            Surveillance: null
            Publicités: null
            Manipulation: null
            'Influencer les comportements': null
            FLOSS: null
            Libre: null
            'Open source': null
---

# Introduction
Bien que l’utilisation des données personnelles à des fins commerciales se soit implicitement généralisée, le respect de la vie privée est considéré par l’Europe comme un droit fondamental. Les logiciels libres tentent encore et toujours de résister à la surveillance généralisée « soi-disant volontaire » de nos vies privées. Pour protéger les données d’usages directement à la source, dans le design même de l’outil, la culture FLOSS tente de ne rien cacher dans le logiciel qui nous surveille (et elle le prouve en donnant accès au code source).

Ce scénario de formation encourage des pratiques conscientes des enjeux liés à la collecte des données, en présentant les bonnes pratiques de base et la législation Européenne. Notre objectif est d’inviter les apprenants à considérer la possibilité d’utiliser des logiciels libres ou open source pour augmenter la sécurité de leurs données d’usages. Nous aimerions aussi permettre à l’animateur multimédia de mieux situer les limites de sa pratique professionnelle pour favoriser le respect des données personnelles des animateurs et de leurs bénéficiaires. Globalement, le scénario de formation fournit un rappel du cadre législatif européens, et des enjeux relatifs au profilage des habitudes par la surveillance des usages personnels à des fins publicitaires de manipulation des comportements.
Il est conçu en 3 sessions :
### Première séance : Principes de base de la Protection des données
Découvrir le cadre légal et sensibiliser aux enjeux et aux principes de base. Le « b.a.-ba » de la protection des données personnelles d’usages et les bonnes habitudes.
### Deuxième séance :  Le logiciel libre et la protection des données privée ?
Aborder les spécificités de la culture FLOSS et des outils disponibles, et encourager à choisir des outils dont le design « Libre » est plus respectueux de la vie privée des animateurs et de leurs bénéficiaires.
### Troisième séance : Situer et limiter sa pratique (et celle de son organisation) pour augmenter la protection des données.
Permettre à l’animateur de travailler en connaissance de cause dans un cadre protégé. Dans le but de situer et limiter ses pratiques professionnelles en participant à l’amélioration de la politique de protection des données de son organisation.

# Contexte
Le but de la session est d'impliquer de manière consciente et pratique les apprenants pour :
* Présenter le cadre légal et sensibiliser aux enjeux concernant la protection des données d’usages. L'apprenant sera en mesure de se situer légalement par rapport au RGPD. Il comprendra les enjeux généraux et les bonnes habitudes liés à la protection des données personnelles.
* Sensibiliser à la plus-value des logiciels libres pour augmenter la protection des données personnelles d’usages. En vue de décider consciemment quels plates-formes / outils / services sont les plus respectueux de la vie privée et de la liberté des usagers.
* Permettre à l’animateur de situer ses usages et le degré de respect des données personnelles dans son organisation. Pour l’encourager à choisir en connaissance de cause des règles et des outils dont le design « Libre » permet plus de confidentialité.

Les sessions encourageront l’utilisation du logiciel libre dans l’éducation non formelle des adultes, en stimulant une participation intentionnelle à la culture FLOSS. Les participants procéderont à une analyse critique des possibilités d’utilisation des outils FLOSS dans leur quotidien. En prenant conscience de l’importance d’avoir une politique de protection des données dans leur propre organisation ; pour mieux sécuriser leurs pratiques  professionnelles en inclusion numérique.