---
title: 'Comunicación interpersonal intencional con FLOSS'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Intentional interpersonal communication with FLOSS'
lang_id: 'Intentional interpersonal communication with FLOSS'
lang_title: es
objectives:
    - 'The good practices of intentional interpersonal communication'
    - 'How FLOSS culture relates to social media rules and common uses of information'
    - 'Cases of intentional communication online like the Wikipedia community'
materials:
    - 'Personal computer (smartph0one or tablet connected to the internet)'
    - 'Internet connexion'
    - Bearmer
    - 'Paper and pens'
    - Flipchart
    - 'Slidewiki Academy'
skills:
    Keywords:
        subs:
            'FLOSS movement': null
            'FLOSS culture': null
            Netizenship: null
            'Intentional communication': null
            'Civic empowerment': null
            'Community engagement': null
            'FLOSS tools and skills': null
length: '10 hours'
---

## Introduction

## Context