---
title: 'Programación con herramientas abiertas: Scrath'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Open coding with Scratch'
lang_id: 'Open coding with Scratch'
lang_title: es
length: '10 horas'
objectives:
    - 'Programar en Scratch para desarrollar historias interactivas, juegos y animación'
    - 'Usar Scratch para trabajar en colaboración (compartir un proyecto desde el editor en línea de Scratch, ver y mezclar el proyecto de otra persona usuaria)'
    - 'Presentar a las participantes la filosofía Scratch y aprender a codificar en Scratch'
    - 'Compartir proyectos propios y usar proyectos de otras usuarias'
materials:
    - 'Ordenador personal'
    - 'Conexión a internet'
    - 'Programa Scratch'
skills:
    'Palabras clave':
        subs:
            Scratch: null
            'Código abierto': null
            'Filosofía de Scratch': null
            'Procesamiento secuencial': null
            'Bloque de código': null
            Bloques: null
            Proyectos: null
            Etapa: null
            'Rutinas - scripts': null
---

Scratch es un lenguaje de programación gratuito y una comunidad en línea desarrollada por el MIT que se puede utilizar para crear juegos, animaciones, canciones y compartirlas en línea.

Scratch es utilizado por personas de todas las edades en una amplia variedad de entornos. La capacidad de elaborar programas de ordenador es una parte importante de la alfabetización en la sociedad actual. Cuando las personas aprenden a programar en Scratch, aprenden estrategias importantes para resolver problemas, diseñar proyectos y comunicar ideas.

Las estadísticas en el sitio web oficial muestran más de 40 millones de proyectos compartidos por más de 40 millones de usuarias y casi 40 millones de visitas mensuales al sitio web.

Este módulo se dividirá en 3 sesiones:

### Primera sesión: Filosofía Scratch: "Imagina, programa, comparte"
La filosofía de Scratch fomenta el intercambio, la reutilización y la combinación de código. Esta sesión proporcionará una visión general de la filosofía Scratch "Imagina, programa, comparte".
Las personas usuarias pueden crear sus propios proyectos o reutilizar el proyecto de otra persona. Los proyectos creados y remezclados con Scratch tienen licencia de Creative Commons Atribución-Compartir Igual (Attribution-Share Alike License).
La sesión tiene como objetivo obtener una mayor comprensión de los antecedentes y principios de Scratch para crear, programar y compartir material.
Al final de la sesión, los participantes podrán examinar el uso y las ventajas de la Filosofía Scratch en la educación de adultos.

### Segunda sesión: Programación abierta con la programación visual gratuita Scratch
Cuando las personas aprenden a codificar en Scratch, aprenden estrategias importantes para resolver problemas, diseñar proyectos y comunicar ideas. Por lo tanto, la codificación en Scratch ayuda a los usuarios a pensar de manera creativa, razonar sistemáticamente y trabajar en colaboración, habilidades esenciales para la vida en el siglo XXI.
La segunda sesión tiene como objetivo presentar el lenguaje de programación desarrollado por el MIT.
Las participantes aprenderán a codificar en Scratch para desarrollar historias interactivas, juegos y animación.

### Tercera sesión: La efectividad de Scratch en la creación de un entorno de programación colaborativa
Este módulo tiene como objetivo ilustrar los fundamentos de la comunidad en línea.
La sesión tiene como objetivo ayudar a los alumnos a identificar y beneficiarse de la comunidad en línea de Scratch. Las participantes aprenderán cómo compartir un proyecto desde el Editor en línea de Scratch, y cómo ver y combinar con el proyecto de otro/a usuario/a, encontrando al mismo tiempo inspiración en otras personas usuarias de Scratch.
