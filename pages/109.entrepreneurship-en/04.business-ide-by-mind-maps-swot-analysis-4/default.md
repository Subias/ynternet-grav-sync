---
title: Budgeting
objectives:
    - 'See statement outlining estimated project costs to support the project'
    - 'A budget should include all the Direct Costs and Indirect Costs required to carry out the project'
    - 'Direct Costs – Expenses that are specifically associated with the product or service'
---

## Group work
Choose any product of your choice and make a list of direct and indirect costs involved in its production.

![](image13.png)

## Cost-Benefit Analysis
Prior to taking on a new project, conducting a cost-benefit analysis                                                would help evaluating all the potential costs and revenues that may                                                 be generated if the project is completed. 
The outcome of the analysis will determine whether the project is financially feasible or if another project should be pursued.
A cost-benefit analysis is carried out in three steps: 
Compile a comprehensive list of all the costs and benefits associated with the project. 
Costs should include direct and indirect costs, intangible costs, opportunity costs and the cost of potential risks. 
Benefits should include all direct and indirect revenues and intangible benefits, and increased sales from customers. 

Apply a common monetary measurement to all items on the list. Be realistic; do not underestimate costs or overestimate benefits.
The final step is to quantitatively compare the results of the costs                                              and benefits to determine if the benefits outweigh the costs. 
If so, then the decision is to go forward with project. 
If not, a review of the project is to be carried out to see if adjustments can be made to either increase benefits and/or decrease costs to make the project viable. If not, the project may be abandoned.

![](image14.png)

## Worked Example at Cost benefit Analysis

![](image15.png)

1.  Strategic Plan
Every organisation should have a goal to accomplish. This is written in the Vision and Mission Statement. 
A strategic plan is the HOW the organisation plans to achieve its mission.
The first step in the budgeting process is having a written strategic plan. This ensures that organisational resources are used to support the strategy and development of the organisation.
 2. Business Goals
To implement its strategic plan, an organisation takes the annual business goals and develop them to be able to achieve them. 
The budget provides the financial resources to achieve these goals. 
Example: An organisation needs to expand its premises. There need to be money budgeted for this to happen. 
3. Revenue Projections
Revenue (profit) projections should be based on past financial performance, as well as projected growth income.  
The projected growth may be tied to organisational goals and planned initiatives that will initiate business growth.
Example: If there is a goal to increase sales by 10%, those sales projections should be part of the revenue projections for the year.
4. Fixed Cost Projections
Projecting fixed costs are predictable monthly costs that do not change, such as employee compensation costs, facility expenses, utilities, mortgage/rent payments, insurance, etc.
Fixed costs do not change and are a minimum expense that need to be funded in the budget.   
5. Variable Cost Projections
Variable costs are costs that fluctuate from month to month, such as supply costs, overtime costs, etc.  These are expenses that should be budgeted and controlled.
Example: A temporary increase in overtime due to Christmas sales should be budgeted.
6. Annual Goal Expenses
Projects related to goals should also be given budgets and incorporated into the annual budget. 
Projections of costs should be identified, laid out and incorporated into the departmental budget that is responsible for completing the goal.
Example: If the sales department has a goal of increasing sales by 10%, costs associated with the increased sales (additional marketing materials, travel, entertainment) should be incorporated into that budget.
7. Target Profit Margin
Profits are important for all organisations and healthy profit margins are a strong indicator of the strength of an organisation. 
Every organisation, whether they are for-profit or not-for-profit, should have a targeted profit margin.  Profit margins allow for reinvestment into the facilities and development of the organisation.  
8. Board Approval
The management of the organisation should approve the budget and keep current with budget performance. 
The management should also review monthly financial statements for the following reasons.
To monitor budget performance
To be familiar with all expenditures
To safeguard the organisation against misappropriation of funds or fraud.
9. Budget Review
A budget review committee should meet on a monthly basis to monitor performance against goals. 
This committee should review budget variances and assess issues associated with budget overages.
It is important to do this on a monthly basis so there can be a correction to overspending or modification to the budget if needed.  
Waiting until the end of the year to make corrections could have a negative impact on the final budget outcome.
10. Dealing With Budget Variances
Budget variances should be reviewed with the responsible department manager and questions should be raised as to what caused the variance.  
Sometimes unforeseen situations arise that cannot be avoided so it is also important to have an emergency fund to help with those unplanned expenditures. 
Example: If a system suddenly goes down and needs to be replaced, this would be a budget variance that needs to be funded.
Good budgeting processes can help the organisation to develop, while poor budgeting and monitoring of budgets can blindside an organisation and affect its long-term financial health, viability, and eventually, customers.


## Defining the Capital Needed to Start a Business
Some definitions: 
#### A balance sheet:
* This is a financial statement that gives investors an idea as to what the company owns and owes at a specific point in time. 
* It is made up of three segments which are: 
* the company’s assets, 
* the company’s liabilities, and 
* the amount invested by shareholders (owner’s equity).

#### Assets:
* An asset is a resource with economic value that a company owns with the expectation that it will provide future benefit. 
* Assets are reported on a company's balance sheet. 
* An asset can be thought of as something that in the future can generate cash flow, reduce expenses, improve sales and adds value to the firm.

#### Liabilities:
* A liability is a company's financial debt or obligations that arise during the course of its business operations.
* Liabilities are settled over time through the transfer of economic benefits including money, goods or services. 
* Liabilities include loans, mortgages, etc. 
* These are recorded on the balance sheet.

#### Owner’s Equity:
* This is the value of an asset less the amount of all liabilities on that asset. 
* It can be represented with the accounting equation: Assets - Liabilities = Equity
* In finance in general, you can think of equity as one’s degree ownership in any asset after all debts associated with that asset are paid off. 
* Example: A car or house with no outstanding debt is considered entirely the owner's equity because he or she can readily sell the item for cash, and pocket the resultant sum.

#### Capital requirement: 
* This is the sum of funds that your company needs to achieve its goals. 
* Plainly speaking: How much money do you need until your business is up and running?
* You can calculate the capital requirements by adding founding expenses (notary fees, legal fees, registration fees, real estate broker costs, etc.), investments and start-up costs together. 
* By subtracting your equity capital (owner’s equity) from the capital requirements, you calculate how much external capital you are going to need: Equity – Capital requirements = External capital required 
* Capital requirements planning is closely linked with all other parts of your business plan, because its follow-up costs have to be considered in the planning.

## Considerations: 
The startup expenses have to be considered. For most startups, revenue in the first few months is not sufficient to cover the cost. You are usually busy acquiring customers and processing orders, before you can finally write your first invoices and get paid. You still need to be able to compensate for expenses in these difficult first months. The capital requirement for the startup phase is equivalent to the minimum of the monthly cash and cash loss.

Do not forget to consider interest expenses and repayments in your capital requirement. 
It could be necessary to plan for higher capital requirements, so you can satisfy both your operating costs and mortgage payments. 
Plan a reserve for contingencies, such as delayed orders, higher renovation expenses or new, unplanned assets. Calculate how much deviation a worst-case scenario would result in, for both the investment and the startup phase.

