---
title: 'SWOT Analysis'
objectives:
    - 'How to conduct a SWOT analysis'
    - 'Questions to ask during a SWOT analysis'
    - 'Example of a SWOT analysis'
---

# Introduction
S.W.O.T. is an acronym that stands for Strengths, Weaknesses, Opportunities, and Threats. A SWOT analysis is an organized list of your business’s greatest strengths, weaknesses, opportunities, and threats.
Strengths and weaknesses are internal to the company (think: reputation, patents, location). You can change them over time but not without some work. Opportunities and threats are external (think: suppliers, competitors, prices)—they are out there in the market, happening whether you like it or not. You can’t change them.

Existing businesses can use a SWOT analysis, at any time, to assess a changing environment and respond proactively. In fact, I recommend conducting a strategy review meeting at least once a year that begins with a SWOT analysis.
New businesses should use a SWOT analysis as a part of their planning process. There is no “one size fits all” plan for your business, and thinking about your new business in terms of its unique “SWOTs” will put you on the right track right away, and save you from a lot of headaches later on.

# How to conduct a SWOT analysis
To get the most complete, objective results, a SWOT analysis is best conducted by a group of people with different perspectives and stakes in your company. Management, sales, customer service, and even customers can all contribute valid insight. Moreover, the SWOT analysis process is an opportunity to bring your team together and encourage their participation in and adherence to your company’s resulting strategy.
A SWOT analysis is typically conducted using a four-square SWOT analysis template, but you could also just make lists for each category. Use the method that makes it easiest for you to organize and understand the results.

I recommend holding a brainstorming session to identify the factors in each of the four categories. Alternatively, you could ask team members to individually complete our free SWOT analysis template, and then meet to discuss and compile the results. As you work through each category, don’t be too concerned about elaborating at first; bullet points may be the best way to begin. Just capture the factors you believe are relevant in each of the four areas.
Once you are finished brainstorming, create a final, prioritized version of your SWOT analysis, listing the factors in each category in order of highest priority at the top to lowest priority at the bottom.
## Questions to ask during a SWOT analysis
I’ve compiled some questions below to help you develop each section of your SWOT analysis. There are certainly other questions you could ask; these are just meant to get you started.

### Strengths
Strengths describe the positive attributes, tangible and intangible, internal to your organization. They are within your control.
* What do you do well?
* What internal resources do you have? Think about the following:
    * Positive attributes of people, such as knowledge, background, education, credentials, network, reputation, or skills.
    * Tangible assets of the company, such as capital, credit, existing customers or distribution channels, patents, or technology.
* What advantages do you have over your competition?
* Do you have strong research and development capabilities? Manufacturing facilities?
* What other positive aspects, internal to your business, add value or offer you a competitive advantage?

### Weaknesses (internal, negative factors)
Weaknesses are aspects of your business that detract from the value you offer or place you at a competitive disadvantage. You need to enhance these areas in order to compete with your best competitor.
* What factors that are within your control detract from your ability to obtain or maintain a competitive edge?
* What areas need improvement to accomplish your objectives or compete with your strongest competitor?
* What does your business lack (for example, expertise or access to skills or technology)?
* Does your business have limited resources?
* Is your business in a poor location?

### Opportunities (external, positive factors)
Opportunities are external attractive factors that represent reasons your business is likely to prosper.
* What opportunities exist in your market or the environment that you can benefit from?
* Is the perception of your business positive?
* Has there been recent market growth or have there been other changes in the market that create an opportunity?
* Is the opportunity ongoing, or is there just a window for it? In other words, how critical is your timing?

### Threats (external, negative factors)
Threats include external factors beyond your control that could place your strategy, or the business itself, at risk. You have no control over these, but you may benefit by having contingency plans to address them if they should occur.
* Who are your existing or potential competitors?
* What factors beyond your control could place your business at risk?
* Are there challenges created by an unfavourable trend or development that may lead to deteriorating revenues or profits?
* What situations might threaten your marketing efforts?
* Has there been a significant change in supplier prices or the availability of raw materials?
* What about shifts in consumer behaviour, the economy, or government regulations that could reduce your sales?
* Has a new product or technology been introduced that makes your products, equipment, or services obsolete?

## Examples of a SWOT analysis
For illustration, here’s a brief SWOT example from a hypothetical, medium-sized computer store in the United States:

![](image1a.jpg)

TOWS analysis: Developing strategies from your SWOT analysis
Once you have identified and prioritized your SWOT results, you can use them to develop short-term and long-term strategies for your business. After all, the true value of this exercise is in using the results to maximize the positive influences on your business and minimize the negative ones.
But how do you turn your SWOT results into strategies? One way to do this is to consider how your company’s strengths, weaknesses, opportunities, and threats overlap with each other. This is sometimes called a TOWS analysis.
For example, look at the strengths you identified, and then come up with ways to use those strengths to maximize the opportunities (these are strength-opportunity strategies). Then, look at how those same strengths can be used to minimize the threats you identified (these are strength-threats strategies).
Continuing this process, use the opportunities you identified to develop strategies that will minimize the weaknesses (weakness-opportunity strategies) or avoid the threats (weakness-threats strategies).

The following table might help you organize the strategies in each area:

![](image2a.jpg)

Once you’ve developed strategies and included them in your strategic plan, be sure to schedule regular review meetings. Use these meetings to talk about why the results of your strategies are different from what you’d planned (because they always will be) and decide what your team will do going forward.

## SWOT Summary
![](image3a.png)

## Homework
Try an example using a FLOSS product like Libre software
You are now encouraged to carry out your own SWAT analysis according to your own business idea.

## References
[Smart Women Project](http://smartwomenproject.eu/)