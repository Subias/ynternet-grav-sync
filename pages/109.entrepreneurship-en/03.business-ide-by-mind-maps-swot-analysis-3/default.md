---
title: 'Defining the Market'
objectives:
    - 'Geographic segmentation'
    - 'Customer segmentation'
    - Competition
    - 'Maturity of the market'
---

# Geography segmentation
What is Geographic Segmentation?
Geographic segmentation is when a business divides its market on the basis of location. There are several ways that a market can be geographically segmented. You can divide your market by geographical areas, such as by city, county, state, region, country, or international region. You can also divide the market into rural, suburban, and urban market segments. And, you can segment a market by climate or total population in each area.
Advantages of Geographic Segmentation

So what are the advantages of geographic segmentation? Let's look at some.
* It's an effective approach for businesses with large national or international markets because different consumers in different regions have different needs, wants, and cultural characteristics that can be specifically targeted.
* It can also be an effective approach for small businesses with limited budgets. They can focus on their defined area and not expend needless marketing dollars on approaches ill-suited for their target geographic segment.
* It works well in different areas of population density. Consumers in an urban environment often have different needs and wants than people in suburban and rural environments. There are even cultural differences between these three areas.
* It's relatively easy to break your market down to geographic segments.

# Customer segmentation
Segmenting a market is sound practice. It enables you to develop a deeper understanding of your customers and discover what makes them tick. When you're communicating a message, it will be more effective if the recipient of the message finds it relevant.
Segmentation is simply a way of arranging your customers into smaller groups according to type. These distinct sub-groups or segments should be characterised by particular attributes. Now you can target specific, relevant marketing messages at each group.
And it's not just about what you say. How you communicate is also vital, and segmentation often requires a carefully structured marketing mix. That's because some customers may prefer the direct approach, such as telephone marketing, while others respond better to a local advertising campaign.
First steps to customer segmentation
Segmentation doesn't have to be complex. For a small company, it could be about recognising that you have two or three distinct customer types with different needs. My philosophy is to always start with the simple question: Who do we want to talk to? The answer could be simple - customers. Segmentation principles can then add several layers of intelligence, based on key differentials, such as:
spending patterns;
gender;
where they live;
age;
socio-economic group.
What is important is not surface differences, but those differences that actually affect buying behaviour. What triggers each person to buy? If you run a hairdressing salon, for example, the type of offers you might make to customer groups would certainly differ on gender and age lines. If you own a mail order business, you might be better off analysing buying patterns and splitting customers into groups according to how much they spend, how often they buy or what products they are most interested in.
Targeted selling
By increasing your understanding of what your customers are buying, you can also maximise opportunities for cross-selling or up-selling. I'm reminded of the builders merchant who sells a tonne of bricks but doesn't cross-sell by selling the sand and cement. By grouping together all the customers who regularly buy certain products, you can target them with relevant offers encouraging them to increase their spend.
Not only is a relevant marketing message more effective as a sales tool, it is also about good customer service. A piece of communication that acknowledges what you bought and when is much more impactful than a one-size-fits-all message. What's more, if you are a regular customer, a targeted message shows that you are appreciated and valued. Conversely, a general message, which doesn't acknowledge previous custom, could well make you feel unloved and taken for granted.

# Competition
The Four Types of Market Structures
There are quite a few different market structures that can characterize an economy. However, if you are just getting started with this topic, you may want to look at the four basic types of market structures first. Namely perfect competition, monopolistic competition, oligopoly, and monopoly. Each of them has their own set of characteristics and assumptions, which in turn affect the decision making of firms and the profits they can make.
It is important to note that not all of these market structures actually exist in reality, some of them are just theoretical constructs. Nevertheless, they are of critical importance, because they can illustrate relevant aspects of competition firms’ decision making. Hence, they will help you to understand the underlying economic principles. With that being said, let’s look at them in more detail.

## Perfect Competition
Perfect competition describes a market structure, where a large number of small firms compete against each other. In this scenario, a single firm does not have any significant market power. As a result, the industry as a whole produces the socially optimal level of output, because none of the firms have the ability to influence market prices.
The idea of perfect competition builds on a number of assumptions: (1) all firms maximize profits (2) there is free entry and exit to the market, (3) all firms sell completely identical (i.e. homogenous) goods, (4) there are no consumer preferences. By looking at those assumptions it becomes quite obvious, that we will hardly ever find perfect competition in reality. This is an important aspect, because it is the only market structure that can (theoretically) result in a socially optimal level of output.
Probably the best example of a market with almost perfect competition we can find in reality is the stock market. If you are looking for more information on perfect competition, you can also check our post on perfect competition vs imperfect competition.

## Monopolistic Competition
Monopolistic competition also refers to a market structure, where a large number of small firms compete against each other. However, unlike in perfect competition, the firms in monopolistic competition sell similar, but slightly differentiated products. This gives them a certain degree of market power which allows them to charge higher prices within a certain range.
Monopolistic competition builds on the following assumptions: (1) all firms maximize profits (2) there is free entry and exit to the market, (3) firms sell differentiated products (4) consumers may prefer one product over the other. Now, those assumptions are a bit closer to reality than the ones we looked at in perfect competition. However, this market structure will no longer result in a socially optimal level of output, because the firms have more power and can influence market prices to a certain degree.
An example of monopolistic competition is the market for cereals. There is a huge number of different brands (e.g. Cap’n Crunch, Lucky Charms, Froot Loops, Apple Jacks). Most of them probably taste slightly different, but at the end of the day, they are all breakfast cereals.

## Oligopoly
An oligopoly describes a market structure which is dominated by only a small number firms. This results in a state of limited competition. The firms can either compete against each other or collaborate. By doing so they can use their collective market power to drive up prices and earn more profit.
The oligopolistic market structure builds on the following assumptions: (1) all firms maximize profits, (2) oligopolies can set prices, (3) there are barriers to entry and exit in the market, (4) products may be homogenous or differentiated, and (5) there is only a few firms that dominate the market. Unfortunately, it is not clearly defined what a «few» firms means exactly. As a rule of thumb, we say that an oligopoly typically consists of about 3-5 dominant firms.
To give an example of an oligopoly, let’s look at the market for gaming consoles. This market is dominated by three powerful companies: Microsoft, Sony, and Nintendo. This leaves all of them with a significant amount of market power.

## Monopoly
A monopoly refers to a market structure where a single firm controls the entire market. In this scenario, the firm has the highest level of market power, as consumers do not have any alternatives. As a result, monopilists often reduce output to increase prices and earn more profit.
The following assumptions are made when we talk about monopolies: (1) the monopolist maximizes profit, (2) it can set the price, (3) there are high barriers to entry and exit, (4) there is only one firm that dominates the entire market.
From the perspective of society, most monopolies are usually not desirable, because they result in lower outputs and higher prices compared to competitive markets. Therefore, they are often regulated by the government. An example of a real life monopoly could be Monsanto. About 80% of all corn harvested in the US is trademarked by this company. That gives Monsanto an extremely high level of market power. You can find additional information about monopolies our post on monopoly power.

# Maturity of the market
After the Introduction and Growth stages, a product passes into the Maturity stage. The third of the product life cycle stages can be quite a challenging time for manufacturers. In the first two stages companies try to establish a market and then grow sales of their product to achieve as large a share of that market as possible. However, during the Maturity stage, the primary focus for most companies will be maintaining their market share in the face of a number of different challenges.![](image9.jpg)
### Challenges of the Maturity Stage:
* Sales Volumes Peak: After the steady increase in sales during the Growth stage, the market starts to become saturated as there are fewer new customers. The majority of the consumers who are ever going to purchase the product have already done so.
* Decreasing Market Share: Another characteristic of the Maturity stage is the large volume of manufacturers who are all competing for a share of the market. With this stage of the product life cycle often seeing the highest levels of competition, it becomes increasingly challenging for companies to maintain their market share.
* Profits Start to Decrease: While this stage may be when the market as a whole makes the most profit, it is often the part of the product life cycle where a lot of manufacturers can start to see their profits decrease. Profits will have to be shared amongst all of the competitors in the market, and with sales likely to peak during this stage, any manufacturer that loses market share, and experiences a fall in sales, is likely to see a subsequent fall in profits. This decrease in profits could be compounded by the falling prices that are often seen when the sheer number of competitors forces some of them to try attracting more customers by competing on price.
### Benefits of the Maturity Stage
* Continued Reduction in Costs: Just as economies of scale in the Growth stage helped to reduce costs, developments in production can lead to more efficient ways to manufacture high volumes of a particular product, helping to lower costs even further.
* Increased Market Share Through Differentiation: While the market may reach saturation during the Maturity stage, manufacturers might be able to grow their market share and increase profits in other ways. Through the use of innovative marketing campaigns and by offering more diverse product features, companies can actually improve their market share through differentiation and there are plenty of product life cycle examples of businesses being able to achieve this.

## Product Life Cycle Management in the Maturity Stage
The Maturity stage of the product life cycle presents manufacturers with a wide range of challenges. With sales reaching their peak and the market becoming saturated, it can be very difficult for companies to maintain their profits, let alone continue trying to increase them, especially in the face of what is usually fairly intense competition. During this stage, it is organizations that look for innovative ways to make their product more appealing to the consumer that will maintain, and perhaps even increase, their market share.