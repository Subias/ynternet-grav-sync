---
title: 'Online entrepreneurship with FLOSS tools'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Online entrepreneurship with FLOSS tools'
lang_id: 'Online entrepreneurship with FLOSS tools'
lang_title: en
length: '10 hours'
objectives:
    - 'Provide skills for developing a business idea into a real business'
    - 'Teach the SWOT analysis'
    - 'Support  learners in defining and understanding market and the different kinds of competition that a business face'
    - 'The product life cycle and the marketing mix'
    - 'Teach basic elements on budgeting'
materials:
    - 'Personal computer'
    - 'Internet connexion'
    - Bearmer
    - 'Basic idea of FLOSS competences'
skills:
    Keywords:
        subs:
            'Online business': null
            Entrepreneurship: null
---

# Introduction
The online world offers a lot of opportunities to grow a business. Knowing how to put your business online and reach out to new clients can be a great way to grow your business. Furthermore, online entrepreneurship offers flexibility to allow the business owner to determine their hours. 

The are many businesses that can benefit from being online. More than 90% of customers use the internet to find a business, this is true even for restaurants, hotels, tourist attractions, consultants, schools ect. Not being online risks potential customers from not finding your business.

Creating a business plan is a useful and versatile tool and can be easily created with versatile and flexible tools. In today’s global and highly competitive business environment, enterprises, whether large or small, cannot hope to compete and grow without proper planning.

Online sales never been better, and the prediction is that they continue to grow until 2021, as statistics from the ‘Statista – The Statistics Portal’ shows. This type of commerce has been evolving year after year and getting new fans. Virtual stores are nothing more than shop windows full of products available for sale, the online sales market has noticed the need to invest in marketing strategies to know what customers think of your products and follow the after-sales process. 
Any business can be put online from sport classes, to language learning, business consultants, personal and life coaches, caterers, craftspeople.

Online shopping is one of the most popular online activities in the world and in 2016 alone, retail e-commerce sales worldwide ascended to 1.86 trillion US dollars. For the year of 2021, online sales are projected to grow to 4.48 trillion US dollars. That shows how much an entrepreneur and his business can benefit from the digital Era to escalate the business, no matter in what area.

The total number of digital shoppers worldwide is increasing as well. It grew by over 100 million between 2011 and 2012 and it kept on growing. And women have a big part on this thanks to flexible hours allowing a better work life balance. In 2012, a Greenfield survey found that women account for 58% of online spending in the U.S.

## Methodology
* Deliberation: Discussion groups and discussion forums
* Execution: drafting plans by students

This module is divided into four differentiated thematic blocks:
### First session: Business Idea Development and Evaluation by Mind maps SWOT Analysis
In this session we will work on analysis a business idea using SWOT analysis and mind maps.

### Second session: SWOT analysis
This session will work on this method of analyse

### Third session: Defining the market
This session will focus on customer segmentation, and understanding competition.

### Forth session: Budgeting
What is a budget and what are ten steps to follow a budget.
