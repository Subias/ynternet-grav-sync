---
title: 'Business IDE by Mind maps SWOT Analysis'
objectives:
    - 'Define your business idea using mind maps'
    - 'Analysis the idea using a SWOT analysis'
---

# Introduction
Who are we?
Individually, introduce yourselves by answering these four questions briefly to everyone in class:

* What is your business idea? 
* What is your product? 
* Who is your product targeted at? (Who will your customers be?)
* Where do you stand right now? 

Example: 

“My business idea is to start selling homemade products. I would like to specialise in sugar-free marmalades, targeted at people following low-sugar diets / diabetics. Right now I am preparing these marmalades for my relatives and friends, and sometimes as gifts.”
Now work on developing those ideas
Brainstorming Tool 
This will help you develop a wide range of ideas.

Start with one keyword and write down all the points/ideas you can think of.

Do not criticise any ideas (if you are doing it in a group).

Example:

![](image1.png)

# A Powerful Approach to Note-Taking
Mind Mapping is a useful technique that helps you learn more effectively, improves the way that you record information, and supports and enhances creative problem solving.
By using Mind Maps, you can quickly identify and understand the structure of a subject. You can see the way that pieces of information fit together, as well as recording the raw facts contained in normal notes.
More than this, Mind Maps help you remember information, as they hold it in a format that your mind finds easy to recall and quick to review. Read on to find out more.
### About Mind Maps
Mind Maps were popularized by author and consultant, Tony Buzan. They use a two-dimensional structure, instead of the list format conventionally used to take notes.
Mind Maps are more compact than conventional notes, often taking up one side of paper. This helps you to make associations easily, and generate new ideas . If you find out more information after you have drawn a Mind Map, then you can easily integrate it with little disruption.
More than this, Mind Mapping helps you break large projects or topics down into manageable chunks, so that you can plan effectively without getting overwhelmed and without forgetting something important.
A good Mind Map shows the "shape" of the subject, the relative importance of individual points, and the way in which facts relate to one another. This means that they're very quick to review, as you can often refresh information in your mind just by glancing at one. In this way, they can be effective mnemonics – remembering the shape and structure of a Mind Map can give you the cues you need to remember the information within it.
When created using colours and images or drawings, a Mind Map can even resemble a work of art!

Mind Maps are useful for:
* Brainstorming  – individually, and as a group.
* Summarizing information, and note taking.
* Consolidating information from different research sources.
* Thinking through complex problems.
* Presenting information in a format that shows the overall structure of your subject.
* Studying and memorizing information.
### Drawing Basic Mind Maps
To draw a Mind Map, follow these steps:

1. Write the title of the subject you're exploring in the center of the page, and draw a circle around it. This is shown by the circle marked in figure 1, below.
(Our simple example shows someone brainstorming actions needed to deliver a successful presentation.)

![](image2.jpg)**Figure 1**


2. As you come across major subdivisions or subheadings of the topic (or important facts that relate to the subject) draw lines out from this circle. Label these lines with these subdivisions or subheadings. (See figure 2, below.)

![](image3.jpg)**Figure 2**


3. As you "burrow" into the subject and uncover another level of information (further subheadings, or individual facts) belonging to the subheadings, draw these as lines linked to the subheading lines. These are shown in figure 3.

![](image4.jpg)**Figure 3**


4. Then, for individual facts or ideas, draw lines out from the appropriate heading line and label them. These are shown in Figure 4.

![](image5.jpg)**Figure 4**


5. As you come across new information, link it in to the Mind Map appropriately.

A complete Mind Map may have main topic lines radiating in all directions from the center. Sub-topics and facts will branch off these, like branches and twigs from the trunk of a tree. You don't need to worry about the structure you produce, as this will evolve of its own accord.
### Using Mind Maps Effectively
Once you understand how to take notes in Mind Map format, you can develop your own conventions for taking them further. The following suggestions can help you draw impactful Mind Maps:
* Use Single Words or Simple Phrases – Many words in normal writing are padding, as they ensure that facts are conveyed in the correct context, and in a format that is pleasant to read.

In Mind Maps, single strong words and short, meaningful phrases can convey the same meaning more potently. Excess words just clutter the Mind Map.
* Print Words – Joined up or indistinct writing is more difficult to read.
* Use Colour to Separate Different Ideas – This will help you to separate ideas where necessary. It also helps you to visualize the Mind Map for recall. Colour can help to show the organization of the subject.
* Use Symbols and Images – Pictures can help you to remember information more effectively than words, so, where a symbol or picture means something to you, use it. 
* Using Cross-Linkages – Information in one part of a Mind Map may relate to another part. Here you can draw lines to show the cross-linkages. This helps you to see how one part of the subject affects another.

## Homework
Example: Let’s mind-map the topic of ‘Health’

![](image6.png)


![](image7.png)