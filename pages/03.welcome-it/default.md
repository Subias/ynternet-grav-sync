---
title: 'Benvenuti nell''Accademia di OPENAE!'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Welcome to the OPENAE Academy'
lang_id: 'Welcome to the OPENAE Academy'
lang_title: it
---

Gli scenari dei corsi di formazione sono gli elementi fondamentali del toolkit di [OPEN - AE](https://open-ae.eu/open-ae-curriculum/) (Prodotto intellettuale 2). Sono stati sviluppati tenendo conto sia della ricerca documentale che delle migliori pratiche relative all'insegnamento delle tecnologie open source in ciascun paese / regione partner (prodotto intellettuale 1). I moduli sono stati sviluppati in base allo stato attuale e alle esigenze di ciascun paese per quanto riguarda la cultura FLOSS.

Questi scenari mirano a guidare i formatori e gli studenti durante il periodo di formazione sia nelle lezioni in presenza che negli esercizi online. In ciascun paese in cui verrà sperimentato il corso, i partner saranno responsabili dell'organizzazione di gruppi di formazione con un minimo di 8 e un massimo di 10 partecipanti selezionati tra facilitatore digitali e centri di formazione nel campo dell'educazione non formale degli adulti.


Il materiale didattico (presentazioni o altro) viene archiviato e documentato online su un repository collaborativo aperto chiamato SlideWiki. La piattaforma open source ad accesso aperto [SlideWiki](https://slidewiki.org/deck/127952-2/open-ae-_-academy-slidewiki/deck/127952-2?language=en) utilizza metodi di crowdsourcing per supportare la creazione, la condivisione, il riutilizzo e la combinazione di corsi aperti.

### La nostra motivazione
Vediamo sempre più spesso abilità e competenze digitali ancorate a soluzioni software proprietarie. Invece le tecnologie FLOSS sono pensate per essere aperte e liberamente accessibili, la maggior parte degli utenti FLOSS ha già alcune competenze in materia di licenze e proprietà quando decide di utilizzare FLOSS. I nuovi utenti con scarse competenze sono spesso intimiditi o insicuri con le proprie capacità di utilizzare le tecnologie FLOSS e quindi possono scegliere di utilizzare opzioni proprietarie perché alcuni marchi sono più associati alle competenze. Il progetto OPEN-AE mira a colmare questa lacuna e promuovere pratiche e strumenti per rendere la cultura aperta e il software libero più accessibili per i nuovi utenti.