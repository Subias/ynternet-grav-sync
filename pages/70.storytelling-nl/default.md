---
title: 'Digital Storytelling: maak en deel verhalen met open digitale technologie'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Digital Storytelling: create and share stories using open digital technologies'
lang_id: 'Digital Storytelling: create and share stories using open digital technologies'
lang_title: nl
length: '12 hours'
objectives:
    - 'Het gebruik en de integratie van verschillende media (tekst, beeld, geluid, video) in een naadloze online-omgeving'
    - 'Open digitale technologieën gebruiken om verhalen te creëren en te delen'
    - 'Een digitaal verhaal produceren en delen om de personalisering en actieve betrokkenheid van de leerlingen te stimuleren'
    - 'Het verkennen van samenwerkingsmethoden in kleine groepsactiviteiten om van elkaar te leren'
materials:
    - PC
    - Internetverbinding
    - 'Dixit Kaarten'
skills:
    Sleutelwoorden:
        subs:
            Storytellins: null
            'Digital story': null
            Storyboard: null
            'Storytelling circle': null
            Multimedia: null
            'Interactief verhalen delen': null
            'Open source software': null
---

Digital storytelling (DS) is een eenvoudige en toegankelijke manier om een verhaal te vertellen door middel van een korte film. DS is een techniek dat gebruik maakt van de tools van de digitale technologie om verhalen over ons leven te vertellen. De DS is een essentieel onderdeel van het moderne onderwijssysteem, een actief leermiddel dat leerlingen vaardigheden en competenties kan bieden die nodig zijn om open source digitale verhalen te creëren, te beheren en te delen. Om een digitaal verhaal te kunnen produceren, moeten leerlingen een digitaal vertelproces doorlopen:

![](storytelling1.png)

Bij het maken van een digitaal verhaal moeten de makers een videobewerking maken van beelden, geluid, muziek, tekst en stem. Dus, een verscheidenheid aan vaardigheden kunnen worden ontwikkeld en geoefend via het maken van digitale verhalen. Door gebruik te maken van de DS-methodologie hebben leerlingen de mogelijkheid om verhalen te maken en te delen met behulp van open digitale technologieën (bijv. Open source videobewerkingssoftware; Open source audiobewerkingssoftware enz.)

De DS biedt de leerlingen de mogelijkheid om hun eigen persoonlijke ervaringen uit te drukken, deze te organiseren in een verhaal (zelfreflectie) en gebruik te maken van digitale technieken om persoonlijke verhalen te vertellen. Naast het feit dat het een krachtig technologisch instrument is om de digitale competentie van de leerling te verbeteren in het gebruik van open software die een verscheidenheid aan multimedia combineert (tekst, stilstaande beelden, audio, video en webpublicaties), kan het DS ook een effectief instrument zijn dat de leerling de vier essentiële 21e eeuwse vaardigheden biedt (kritisch denken; creativiteit; samenwerking; communicatie).

## Context
Het doel van deze training is om de deelnemers kennis te laten maken met de DS en te leren hoe ze het kunnen gebruiken als een methode om verhalen te vertellen door gebruik te maken van open digitale technologieën.
Aan het einde van de sessies kunnen de deelnemers een storyboard voor hun eigen digitale verhaal maken, relevante materialen voor hun digitale verhaal (beelden, stem, muziek, geluiden, teksten, titels) creëren en verzamelen door middel van open digitale technologieën.
De sessies zijn ook bedoeld om meer inzicht te krijgen in ethische richtlijnen, copyright, Creative Commons licentie en toestemming.

Deze module zal worden opgedeeld in 2 sessies:
### Eerste sessie: Een inleiding tot DS: achtergrond en voordelen
DS is een multimediale benadering van het delen van een verhaal op basis van de integratie van fotografie, geluid, tekst en muziek om het digitale verhaal van de maker te delen in een digitaal-mediaformaat. DS is een methode, die ons helpt om ons te concentreren op onszelf en op ons eigen verhaal. Het helpt ons om ons verhaal te verwoorden, te dramatiseren, te illustreren met foto's en te vertellen aan anderen. De sessie is bedoeld om meer inzicht te krijgen in de DS-achtergrond en de principes voor het creëren, verzamelen en delen van digitale verhalen met behulp van open digitale technologieën. 

De sessie richt zich ook op de potentiële impact van de DS-methodologie. Het is zelfs zo dat leerlingen een verscheidenheid aan communicatieve elementen binnen een narratieve structuur kunnen combineren om hun digitale verhaal te creëren en te delen, wat een aantal belangrijke voordelen heeft:
* Het verbetert de digitale competentie van lerenden en ondersteunt hen om zich te bekwamen in open source video- en audio software.
* Het geeft leerlingen een nieuwe manier om samen te werken

Aan het einde van de sessie kunnen de deelnemers herkennen wat wel en wat niet een digitaal verhaal is, waarbij de belangrijkste kenmerken en stappen van het DS worden geïdentificeerd. De deelnemers kunnen ook de voordelen DS in het volwassenenonderwijs herkennen, met speciale aandacht voor de ontwikkeling van de vaardigheden van de eenentwintigste eeuw. Wanneer mensen leren om hun verhaal te creëren en te delen met behulp van de DS-methodologie, ontwikkelen ze ook de vaardigheden van de 21e eeuw die de burgers van vandaag nodig hebben om in het informatietijdperk succesvol te zijn in hun carrière.

### Tweede sessie: Creëer je krachtige digitale verhaal open digitale technologieën!
De tweede sessie heeft als doel de productie van een digitaal verhaal mogelijk te maken, inclusief storyboard, materiaal, montage. Deelnemers leren hoe ze een storyboard van hun verhaal kunnen maken, met behulp van open software die een verscheidenheid aan multimedia (tekst, stilstaande beelden, audio, video en webpublicatie) combineert en het gratis licentie materiaal op het web kunnen herkennen.

Het doel van deze training is om de deelnemers kennis te laten maken met de DS en te leren hoe ze het kunnen gebruiken als een methode om verhalen te vertellen door gebruik te maken van open digitale technologieën. Aan het einde van de sessies kunnen de deelnemers een storyboard voor hun eigen digitale verhaal maken, relevante materialen voor hun digitale verhaal (beelden, stem, muziek, geluiden, teksten, titels) creëren en verzamelen door middel van open digitale technologieën. De sessies zijn ook bedoeld om meer inzicht te krijgen in ethische richtlijnen, copyright, Creative Commons licentie en toestemming.