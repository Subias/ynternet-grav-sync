---
title: 'Marco DigCompEdu para una educación compartida y abierta'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'DigCompEdu framework for a common and open education'
lang_id: 'DigCompEdu framework for a common and open education'
lang_title: es
skills:
    'Palabras clave':
        subs:
            DigCompEdu: null
            'Marco de referencia': null
            'Competencias digitales': null
            'Educación abierta': null
            'Educación de personas adultas': null
            'Cultura FLOSS': null
            Formación: null
            Habilidadess: null
materials:
    - 'Ordenador personal o teléfono inteligente o tableta digital'
    - 'Conexión a Internet'
    - 'Marco de competencias digitales DigCompEdu'
    - 'Papel y boligrafos'
objectives:
    - 'Comprender el marco DigCompEdu'
    - 'Situar las competencias personales en el marco europeo'
    - 'Planificar la mejora de las competencias digitales con fines educativos'
    - 'Saber organizar actividades formativas creativas con el apoyo de las competencias digitales'
length: '8 horas'
---

“... un marco científicamente sólido que describe lo que significa para los educadores ser digitalmente competentes. Proporciona un marco de referencia general para apoyar el desarrollo de competencias digitales específicas para educadores en Europa ". Este módulo proporciona una visión general del marco de competencia digital para educadores, que presenta 22 competencias organizadas en seis áreas. El enfoque no está en las habilidades técnicas; por el contrario, el marco tiene como objetivo detallar cómo las tecnologías digitales se pueden utilizar para mejorar e innovar la educación y la formación. El módulo se centrará en aquellas áreas altamente relevantes para la educación de adultos, explicando cómo el marco puede ser de gran importancia para los educadores y cuáles son los beneficios de desarrollar tales competencias utilizando tecnologías de código abierto. Se darán ejemplos prácticos con referencia a las seis áreas para que las personas participantes sepan qué recursos y herramientas abiertas están disponibles.

## Contexto
El propósito de esta formación es presentar a las participantes el marco DigCompEdu para que puedan aprovechar el documento en los procesos de enseñanza y aprendizaje. 
Al final de las sesiones, las participantes podrán comprender el marco e identificar las herramientas y metodologías existentes que pueden ayudar a desarrollar las habilidades descritas en DigCompEdu. También podrán construir su propio mapa las competencias personales de cara a planificar la mejora de las mismas con fines educativos, y configurar actividades de formación creativas con el apoyo de las competencias digitales.

Este módulo se dividirá en 2 sesiones:

### Primera sesión: el marco DigCompEdu
En esta primera sesión, analizaremos el marco europeo para obtener las nociones teóricas sobre la estructura y los objetivos del documento. Comprender y ser capaz de reconocer las competencias digitales necesarias en el siglo XXI es un factor clave en la educación. Las participantes estarán preparadas para identificar sus debilidades y mapear sus competencias en la siguiente sesión.

### Segunda sesión: cómo usar el marco para mapear las competencias y habilidades personales
Se pondrá en práctica el conocimiento de las participantes sobre el marco y, a partir de las seis áreas de DigCompEdu, se requerirá que las formadoras sitúen sus propias competencias para identificar debilidades y planificar la mejora de sus habilidades y conocimientos. Se presentarán algunos estudios de caso para proporcionar a las participantes algunos ejemplos concretos sobre cómo aprovechar el marco para sus contextos educativos y formativos.