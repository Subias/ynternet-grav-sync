---
title: 'Digital Storytelling: create and share stories using open digital technologies'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Digital Storytelling: create and share stories using open digital technologies'
lang_id: 'Digital Storytelling: create and share stories using open digital technologies'
lang_title: en
length: '12 hours'
objectives:
    - 'Use and integrate different media (text, images, sound, video) into a seamless online environment'
    - 'Use open digital technologies to create and share stories'
    - 'Produce and share a digital story to stimulate personalization and learners’ active engagement'
    - 'Explore methods of collaboration in small-group activities to learn from each other'
materials:
    - 'Personal computer'
    - 'Internet connexion'
    - 'Dixit Cards'
skills:
    Keywords:
        subs:
            Storytellins: null
            'Digital story': null
            Storyboard: null
            'Storytelling circle': null
            Multimedia: null
            'Interactive storytelling': null
            'Open source software': null
---

Digital storytelling (DS) is a simple and accessible way to tell a story through a short film. DS is a craft that uses the tools of digital technology to tell stories about our lives.
DS is an essential part of modern world education system, an active learning tool which can provide learners with skills and competencies needed for creating, managing and sharing open source digital stories. 
In order to produce a digital story, learners have to work through a Digital Storytelling Process:

![](storytelling1.png)

When creating a digital story, creators are required to produce a video editing images, sound, music, text and voice. So, a variety of skills can be developed and practiced via the making of digital stories. By using the DS methodology, learners have the opportunity to create and share stories using open digital technologies (e.g. Open source Video editing software; Open source Audio editing software etc). 

DS allows learners to express their own personal experiences, organize them in a story (self-reflection and use digital techniques to tell personal stories. Besides being a powerful technology tool for enhancing learners’ digital competence in using open software that combines a variety of multimedia (text, still images, audio, video and web publishing),  DS can be also an effective tool which provides learners with the four C’s of 21st Century skills (Critical thinking; Creativity; Collaboration; Communication).

## Context
The purpose of this training course is to introduce participants to DS and learn how to use it as a method for telling stories by using open digital technologies.
At the end of the sessions participants will be able to create a storyboard for their own digital story, create and collect relevant materials for their digital story (images, voice, music, sounds, texts, titles) through open digital technologies.
The sessions also aim to gain greater understanding of ethical guidelines, copyright, Creative Commons license and consent.

This module will be divided into 2 sessions:
### First session: An introduction to DS: background and benefits  
DS is a multimedia approach to sharing a narrative based on the integration of photography, sound, text, music to share the creator’s digital story in a digital-media format. DS is a method, that helps us to focus on ourselves and on our own story. It assists us to phrase our story, to dramatize it, illustrate it with photographs and to tell it to others.
The session aims to gain greater understanding of DS background and principles for creating, collecting and sharing digital stories using open digital technologies. 

The session also focuses on the potential impact of the DS methodology. In fact, getting leaners to combine a variety of communicative elements within a narrative structure to create and share their digital story has some key benefits: 
* It enhances learners’ digital competence, supporting them to become proficient with open source video and audio software
* It gives learners a new way to collaborate

At the end of the session, participants will be able to recognize what is and what is not a digital story, identifying the main features and steps of DS. Participants will be also able to identify the advantages of the DS in adult education, with a special focus on the development of the twenty-first century skills. In fact, when people learn to create and share their story using the DS methodology, they also develop the 21st century skills that today’s citizens need to succeed in their careers during the Information Age.

### Second session: Create your powerful digital story open digital technologies!
The second session aims at enabling the production of a digital story, including storyboard, material, editing. 
Participants will learn how to create a storyboard of their story, using open software that combines a variety of multimedia (text, still images, audio, video and web publishing) and recognize the free license material on the web.

The purpose of this training course is to introduce participants to DS and learn how to use it as a method for telling stories by using open digital technologies.
At the end of the sessions participants will be able to create a storyboard for their own digital story, create and collect relevant materials for their digital story (images, voice, music, sounds, texts, titles) through open digital technologies.
The sessions also aim to gain greater understanding of ethical guidelines, copyright, Creative Commons license and consent.