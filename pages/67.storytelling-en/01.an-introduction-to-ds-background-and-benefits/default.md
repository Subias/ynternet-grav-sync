---
title: 'An introduction to DS: background and benefits'
length: '5 hours'
objectives:
    - 'to discover the background, history, features and steps of DS'
    - 'to explore the potential of DS as a method for telling stories by using open digital technologies'
    - 'to recognize what is and what is not a digital story'
    - 'to empower the 21st century skills'
---

##Introduction
Description of the activity
→ Ice breaker: Creative storytelling
Trainer will start the introduction to the module by asking participants to pull out a random Dixit card and tell a story about it.  Participants are then asked to work in small groups and pull out a random card. The group goal is to collectively build a story using their Dixit card.  Answers will be documented and then reused to customize the training material.

##The art of digital storytelling: History and Framework 
 “Digital storytelling is the manifestation of the ancient art of storytelling. It uses digital media to create visual stories to show, share, and in the process, preserve as history. Digital stories derive their impact by weaving together images, music, narrative and voice to give depth and dimension to the narrative. By using the Internet and other forms of digital distribution, digital stories can be seen across distances and boundaries to create new communities through a sense of shared meaning (Marsha Rossiter and Penny A. Garcia, University of Wisconsin, USA  2008).
 
The DS movement was started by Joe Lambert and the late Dana Atchley who were interested in making media production available to “ordinary people.”  Since the 1990s, Lambert and his colleagues at the _Center for Digital Storytelling_ (Berkley, CA) have continued to experiment with the Digital Storytelling Workshop.
In Europe the method was formally introduced in 2003, when the regional service BBC Wales has started its own program and centre based on the experiences of other digital storytelling courses held in various locations.

##What makes DS unique 
The process of DS is based on the following steps:  
* Find it – the story which is important for you
* Tell it – with your own words and photographs
* Share it – with each other, or with anyone through the internet

In addition, when producing a digital story, creators need to think about the project’s purpose and craft with a goal. One of the greatest strengths of DS is the ability to develop learners’ critical thinking and decision-making skills. Learning the skill of life storytelling supports us to become more comfortable in our own life and impact the lives of those around us. Storytelling encourages creative writing, creative thinking and problem solving. So, DS can provide the opportunity for the participants to get to know themselves and each other better. It can be also crucial for sharing and collaboration, as it allows users to work in group, learning from each other.

DS brings great benefits. This is the reason why the DS methodology is used in a wide range of contexts, from education (formal and non-formal) to business, Individual purpose, self-development and improvement of creative skills.

The creation ofshort movies by combining computer-based images, text, recorded audio narration, video clips and music in order to present information on various topics is also crucial in term of em-powerment. In fact, DS can be a powerful technology tool for promoting competency with technology and digital fluency.

##Homework
Choose the topic of the story and then answer the following questions:
* What is the most relevant part of the story? Why?
* Is the story comprehensible?
* What is the length of the story? 

Then, structure the story by answering these questions:

* Who are the characters in the story (Who)?
* What is the story about (What)?
* Where does the story take place (Where)?
* When does the story take place (When)?
* What is the starting point of the story (Why)?

Prepare a draft script, approximately 250 words. Learners will be then asked to read their scripts to the group during the session. Together with the story script, find and bring photographs of yourself or any images, letters, documents you feel will illustrate your story - If you will use photographs that are of friends or family members, it is best to get their permission first.

##References
* [Wikipedia](https://en.wikipedia.org/wiki/Digital_storytelling)
* [English and Digital Literacies](https://opencourses.uoa.gr/modules/document/file.php/ENL10/Instructional Package/PDFs/Unit3a_Digital_storytelling.pdf)   
* [The history of digital storytelling](https://digitalistortenetmeseles.hu/en/history/) 
* [Introduction to Digital Storytelling](https://opencourses.uoa.gr/modules/document/file.php/ENL10/Instructional Package/PDFs/Unit3a_Digital_storytelling.pdf)
* [StoryCenter: What’s your Story?](https://www.storycenter.org/about)
* [Digital Storytelling Toolkit](https://www.womenwin.org/files/pdfs/DST/DST_Toolkit_june_2014_web_version.pdf)
* [Learning through Arts and Technology](https://www.faae.org/learning-through-arts-and-technology-digital-storytelling)
