---
title: 'Com crear un Fablab'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'How to run a FabLab'
lang_id: 'How to run a FabLab'
lang_title: ct
Materials:
    - 'Personal computer'
    - 'Internet connexion'
length: '10 hours'
objectives:
    - 'Identify what’s a FabLab and the culture that’s involved.'
    - 'Be able to start a FabLab initiative, understand which are the first steps and which technologies are more suitable to start'
    - 'Know, detect and know how to use sources of information that provide resources that are useful in the framework of action of a FabLab'
    - 'Explore FabLab practice communities that use open technologies and tools'
    - 'Enable the participant to provide information, resources, models to FabLabs communities of practice'
    - 'Explore the local community to make the link between company - university - citizenship and public administrations (promote the quadruple helix model)'
skills:
    Keywords:
        subs:
            FabLab: null
            MakerSpace: null
            HackSpace: null
            'Open Design': null
            'Open Hardware': null
            'Open Software': null
            'Do it Yourself (DIY)': null
            'Learning by doing (LBD)': null
            PBL: null
---

## Introduction


## Context
