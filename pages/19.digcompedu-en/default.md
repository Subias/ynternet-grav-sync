---
title: 'DigCompEdu framework for a common and open education'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'DigCompEdu framework for a common and open education'
lang_id: 'DigCompEdu framework for a common and open education'
lang_title: en
skills:
    Keywords:
        subs:
            DigCompEdu: null
            Framework: null
            'Digital competences': null
            'Open education': null
            'Adult education': null
            'FLOSS culture': null
            Teaching: null
            Skills: null
materials:
    - 'Personal computer'
    - 'Internet connection'
    - 'DigCompEdu framework'
    - 'Paper and pens'
objectives:
    - 'Understand the DigCompEdu framework'
    - 'To map your own competences onto the framework'
    - 'to plan the upskilling of digital competences for educational purpose'
    - 'how to set up creative teaching activities thanks to digital competences'
length: '8 hours'
---

“…a scientifically sound framework describing what it means for educators to be digitally competent. It provides a general reference frame to support the development of educator-specific digital competences in Europe.”
This module provides an overview of The Digital competence framework for Educators. This European framework details 22 competencies organized in six Areas. The focus is not on technical skills. Rather, the framework aims to detail how digital technologies can be used to enhance and innovate education and training. The module will focus on those areas highly relevant for Adult education explaining how the framework can be of high importance for educators and what are the benefits of developing such competencies using open source technologies. Practical examples will be given with reference to the six areas in order for students to know what open resources and tools are available on the field.

## Context
The goal of the module is to practically demonstrate and engage learners on how:
- Understand the DigCompEdu framework. 
- To map your own competences onto the framework
to plan the upskilling of digital competences for educational purpose
- How to set up creative teaching activities thanks to digital competences

The purpose of this training course is to introduce participants to the DigCompEdu framework in order for them to take advantage of the document for teaching and learning.
At the end of the sessions participants will be able to understand the framework and identify existing tools and methodologies that can help in develop skills described in DigCompEdu.

This module will be divided into 2 sessions:

### First session: The DigCompEdu framework 
In this first session, participants will be introduced to the framework in order to provide them with the theoretical notions about the structure and the aims of the document.  Understanding and being able to recognize digital competences needed in the 21st century is a key factor in education. Participants will be prepared in order to identify their gaps and map their competences in the second session.

### Second session: How to use the framework to map your competences and upskilling
Participants’ knowledge about the framework will be brought into practice. Starting from the six areas of the DigCompEdu, trainers and teachers will be required to map their own competences in order to identify gaps and plan their upskilling. Some case studies will be presented as to provide participants with some concrete examples on how to take advantages of the framework for their educational contexts.
