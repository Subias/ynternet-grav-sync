---
title: 'The DigCompEdu framework'
objectives:
    - 'to understand the principles of DigCompEdu for improving digital skills in teaching'
    - 'to promote the use of common European frameworks in Adult education'
length: '160 min'
---

##Introduction 
Trainer will start the introduction to the module by asking participants about their knowledge of the framework and other European frameworks, namely DigComp and DigCompOrg. Answers will be documented and then reused to customize the training material.

##“The principles of DigComEdu”
The teaching professions face rapidly changing demands, which require a new, broader and more sophisticated set of competences than before. The ubiquity of digital devices and applications, in particular, requires educators to develop their digital competence.
The European Framework for the Digital Competence of Educators (DigCompEdu) is a scientifically sound framework describing what it means for educators to be digitally competent. It provides a general reference frame to support the development of educator-specific digital competences in Europe. DigCompEdu is directed towards educators at all levels of education, from early childhood to higher and adult education, including general and vocational education and training, special needs education, and non-formal learning contexts.
![](DigCompEdu.jpg)
DigCompEdu details 22 competences organised in six Areas. The focus is not on technical skills. Rather, the framework aims to detail how digital technologies can be used to enhance and innovate education and training.
The DigCompEdu study builds on previous work carried out to define citizens' Digital Competence in general, and Digitally Competent Education Organisations (DigCompOrg). It contributes to the Commission's recently endorsed Skills Agenda for Europe and to the Europe 2020 flagship initiative Agenda for New Skills for New Jobs.

##Homework
Participants should explore the document and using the grid found at this link related to proficiency level, they should self-assess their own proficiency level.
Participants are asked to start thinking about their competences in relation to the framework and how the documents could be useful to upskills their own competences and those of their organizations.

##References
* [DigCompEdu framework](https://ec.europa.eu/jrc/en/publication/eur-scientific-and-technical-research-reports/european-framework-digital-competence-educators-digcompedu)
* [DigComEdu presentation](https://www.slideshare.net/ChristineRedecker/digcompedu-the-european-framework-for-the-digital-competence-of-educators)
* [DigCompEdu supporting material](https://ec.europa.eu/jrc/en/digcompedu/supporting-materials)
* [DigCompEdu Community](https://ec.europa.eu/jrc/communities/en/community/digcompedu-community)
* Aligning teacher competence frameworks to 21st century challenges: [The case for the European Digital Competence Framework for Educators](https://onlinelibrary.wiley.com/doi/full/10.1111/ejed.12345)