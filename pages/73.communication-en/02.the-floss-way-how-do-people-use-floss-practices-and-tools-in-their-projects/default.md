---
title: 'The FLOSS way - how do people use FLOSS practices and tools in their projects'
length: '240 min'
objectives:
    - 'to provide examples of FLOSS practices and tools in projects'
    - 'to analyse the impact of the use of FLOSS practices'
    - 'to motivate educated policies in organisations'
---

##Introduction
Description of the activity
→ Group discussion on “how is your work today?”.
Trainer will start the introduction to the module by asking participants about the licensing initiatives around them - mostly coming from other organisations or the EU. Answers will be documented and then reused to customize the training material.

##A case of intentional communication online: the Wikipedia example
Presentation and analysis of the [Wikimania organisation](https://wikimania.wikimedia.org/w/index.php?title=2019:Program&dir=prev&action=history). The community's role, in Wikipedia, is to:
* Organize and edit individual pages.
* Structure navigation between pages.
* Resolve conflict between individual members.
* Re-engineer itself – creating rules and patterns of behavior.
* The need to document in an open and reusable way: [Learning Patterns](https://meta.wikimedia.org/wiki/Learning_patterns).

Few principles to understand and develop:
* Empathy: do good.
* Document and share.
* Garden your community.
* The solutions are within your group: what people will say IS useful.
* List of questions to answer, hand in hand with your group.
* Focus on simple, reusable, easy to replicate practices for your actual beneficiaries (empowering).
* Share knowledge, inspire, become a motivator.
* Lead by example.

“Networked citizens” and community builders are change makers
* New members.
* Good practices.
* Experimentation.
* Continuity.
* “Keeping the lights on”.
* Measuring activities.
* Reward and recognition.

Linking Wikipedia to the FLOSS culture (behaviour and principles):
* Be in Charge and Be Humble.
* Understand Bias.
* Appreciate Idiosyncracy (Consensus is critical).
* Or, Act Like Perl.
* Make big plans on Wikipedia.
* Avoid Cabals.
* Follow the Spirit and Letter of the GFDL.
* Be Respectful but Firm.
https://en.wikipedia.org/wiki/GNU_Free_Documentation_License

##FLOSS methodology and impact around us
Company adoption of FOSS has had significant impact on business and software development for more than a decade. For example, previous research based on a survey conducted in Norway shows that “more than 30% of the respondents in our survey have more than 40% of their income from OSS related services or software.” (Hauge et al., 2008, p. 211) In addition, “As long ago as 2008, Gartner surveyed 274 enterprise companies globally and found that 85% had already adopted open source, with most of the remaining expecting to do so within the following year.” (Aberdour, 2011, p. 6).

Further, previous studies have identified the importance of SMEs for FOSS adoption in company contexts. For example, an EU study investigated the impact of FOSS and reported in 2006 that: “In the private sector, FLOSS adoption is driven by medium- and large-sized firms.” (Ghosh, 2006, p. 9).

Initially, companies were reluctant to adopt FOSS. As stated by Unni (2016): “The origins of open source software can be traced back to 1964–65 when Bell Labs joined hands with the Massachusetts Institute of Technology (MIT) and General Electric (GE) to work on the development of MULTICS for creating a dynamic, modular computer system capable of supporting hundreds of users. During the early stages of development, open source software had a very slow beginning primarily because of some preconceived notions surrounding it.” (Unni, 2016, p. 271).

Further, exiting or switching from one software solution to another may incur costs that need to be considered in FOSS adoption scenarios, as argued in previous research: “For an organisation, the abandonment of a specific software solution, sometimes referred to as exit (or switching) costs, can cause different kinds of staffing costs. For example, if existing staff are trained and experienced in a specific technology a change can cause specific costs for the organisation.” (Lundell, 2012).

##Examples of Open Source PM Software
FOSS project management tools continue to evolve and add features. More robust network connectivity and faster more powerful mobile devices has extended their reach. As an example, some of the popular open source project management tools are briefly described here.

MyCollab is the cross-platform collaboration software which can be run in most operating systems. It is well suited for SMEs and is considered a suite of three collaboration modules: project management, customer relationship management (CRM), and document creation and editing software. The FOSS edition of this software does not provide cloud option however it is full featured.

LibrePlan Open Web Planning is a web-based project management application allowing full team collaboration. Features include supporting resource allocation, Gantt charts etc. The design and interface is intuitive and simple to use. The software is well supported with documentation as well as reporting. Professional support is also available to users. Libreplan offers a web based and mobile application.

Projectlibre was founded to provide an Free and Open Source Software replacement of Microsoft Project desktop. Currently there are over 3M downloads of the desktop software with a cloud solution soon to be released. The developers claim the cloud release will be lightweight with simple design. The desktop version offers features such as task management, resource allocation, tracking, Gantt charts etc.

Odoo offers a suite of FOSS business applications which includes project management software in addition to CRM, eCommerce, accounting, inventory, and point of sale. Odoo is a multiplatform solution, supporting GNU/Linux distributions, and several other operating.

The latest OpenProject release contains collaboration features, such as WYSIWYG text editor, intelligent workflow and conditional formatting. In addition to this, OpenProject offers task management, time and cost reporting, Scrum etc. The software is designed to support project teams throughout the whole project lifecycle and offers collaborative project planning, timeline reports, OpenProject provides a very intuitive user interface as well as very extensive support documentation.

##Homework 
See in the following video: [Free software, free society: Richard Stallman at TEDxGeneva 2014](https://www.youtube.com/watch?v=Ag1AKIl_2GM), and then post in the comments section another video that explains - demonstrates similar arguments. Share your thoughts on the new video.

##References
* Nathan L. Ensmenger (2004) [Open Source’s Lessons for Historians](https://doi.org/10.1109/MAHC.2004.32), IEEE Annals of the History of Computing, Vol. 26, No. 4, pp. 104-103.
* Rishab Aiyer Ghosh (2006) Study on the: Economic impact of open source software on innovation and the competitiveness of the Information and Communication Technologies (ICT) sector in the EU, Final report, Prepared on November 20, 2006.
* Øyvind Hauge, Claudia Ayala, and Reidar Conradi (2010) [Adoption of open source software in software-intensive organizations - A systematic literature review](http://dx.doi.org/10.1016/j.infsof.2010.05.008). Information and Software Technology, Vol. 52, No. 11, pp. 1133-1154.
* Björn Lundell (2012) Why do we need Open Standards?, In M. Orviska & K. Jakobs (Eds.), Proceedings 17th EURAS Annual Standardisation Conference ‘Standards and Innovation’ (227-240). The EURAS Board Series, Aachen, ISBN: 978-3-86130-337-4.
* V.K. Unni (2016) [Fifty Years of Open Source Movement: An Analysis through the Prism of Copyright Law](https://law.siu.edu/_common/documents/law-journal/articles-2016/8%20-%20Unni%20Article%20Proof%205%20FINAL%20-%20sm.pdf), Southern Illinois University Law Journal, vol. 40, no. 2, pp. 271-X.