---
title: 'Intentional interpersonal communication with FLOSS'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Intentional interpersonal communication with FLOSS'
lang_id: 'Intentional interpersonal communication with FLOSS'
lang_title: en
objectives:
    - 'The good practices of intentional interpersonal communication'
    - 'How FLOSS culture relates to social media rules and common uses of information'
    - 'Cases of intentional communication online like the Wikipedia community'
materials:
    - 'Personal computer (smartph0one or tablet connected to the internet)'
    - 'Internet connexion'
    - Bearmer
    - 'Paper and pens'
    - Flipchart
    - 'Slidewiki Academy'
skills:
    Keywords:
        subs:
            'FLOSS movement': null
            'FLOSS culture': null
            Netizenship: null
            'Intentional communication': null
            'Civic empowerment': null
            'Community engagement': null
            'FLOSS tools and skills': null
length: '10 hours'
---

Digital technology has changed everything. Remember your first vision of the web? You read pages of information, such as reading a book. You treated the messages as a mail is processed. And gradually, you realized that you did not have to deal with a means of communication quite like the others. Single user of a post office or a virtual storefront store, you felt gradually become an actor…
From the book [Citoyens du Net](http://www.ynternet.org/page/livre).  

In 2019, the web became 30 years old. With its hundreds of millions of forums, wikis, blogs, social networks, microblogs instant, he now justifies talking about webosphere. Google, Facebook, Twitter and Wikipedia are the most famous planets. Populated by hundreds of billions and billions of articles from different fields (title, message body, attachments, images, number of visitors, notes ), the web is the heart which converge all the digital tools that have an open interface, standardized open access and accessible by all existing tools - smartphones, tablets, computers and connected things. The most interesting places are the ones where you can interact by commenting, changing, adding text or images in this matter. 
Intentional communication for civic empowerment and community engagement with FLOSS sets a viable alternative.

##This module will be divided into 3 sessions

First session: Intentional interpersonal communication.  
This session will demonstrate the use of free and open digital technologies for intentional communication in day to day life It will enable learners to realize and develop their intentional communication skills and use free and open digital technologies to develop intentional communication practices and habits

Second session: The FLOSS way: how do people use FLOSS practices and tools in their projects.
The second session will focus on a critical thinking approach around intentional communication in various social and cultural environments-. It will explore methods of online collaboration in small-group activities to learn from each other and support an intentional communication culture.

Third session: FLOSS in your life.
This session will allow for participants to develop and share their intentional communication preferences. We will visit intentional communication as the basis of synergistic participation (with the example of collaborative note taking) and link it to FLOSS tools that can serve participants current activities.