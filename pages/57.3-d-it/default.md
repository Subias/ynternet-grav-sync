---
title: 'Tecnologie di stampa 3-D open source'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Open-source 3-D printing Technologies'
lang_id: 'Open-source 3-D printing Technologies'
lang_title: it
length: '10 ore e 30 minuti'
objectives:
    - 'Usufruire della Fabbricazione Digitale'
    - 'Utilizzare tecnologie digitali aperte per creare, manipolare, preparare e pubblicare contenuti 3D'
    - 'Utilizzare tecnologie digitali aperte per  trovare e sfruttare l’archivio di modelli 3D'
materials:
    - 'Personal computer'
    - 'Connessione internet'
    - Tinkercard
    - Scketchfab
skills:
    'Parole chiave':
        subs:
            'Fabbricazione additiva': null
            'Fabbricazione digitale': null
            'Stampante 3D Open source': null
---

_Nel corso dell’ultimo decennio i significativi progressi nelle tecnologie di fabbricazione additiva (AM), comunemente note come stampa 3D, hanno trasformato i modi in cui i prodotti sono progettati, sviluppati, fabbricati e distribuiti._

_La capacità e i vantaggi della stampa 3D rispetto alla produzione tradizionale aprono numerose opportunità, spaziando dalla progettazione e sviluppo del prodotto, al servizio di personalizzazione, alla ristrutturazione della supply chain per una maggiore efficienza._ 
**Commissione Europea, La natura dirompente della stampa 3D**

L’introduzione della fabbricazione additiva, più nota come stampa 3D, emerge come una tecnologia dirompente che porta con sé diversi cambiamenti e impatti sul prodotto tradizionale. Aziende e nuovi modelli di business basati su questa tecnologia sono in crescita, creando grandi opportunità per l'economia e la società.
Dopo aver fornito agli studenti un background di base di AM, fabbricazione digitale e stampa 3D Open Source (prima sessione), il corso prevede lo studio approfondito di hardware e software open source e piattaforme open source per la creazione di oggetti 3D.

Le varie sessioni permetteranno agli studenti di acquisire alcune delle competenze tecniche necessarie per sfruttare al meglio tutte le opportunità offerte dale tecnologie 3D. I partecipanti prenderanno parte al processo di progettazione creando un oggetto tramite l’utilizzo delle tecnologie di stampa 3D Open-Source.

## Contesto
Le stampanti 3D stanno rapidamente diventando un dispositivo di produzione ampiamente utilizzato che ha rivoluzionato l'industria manifatturiera. Lo scopo di questo corso di formazione è quello di far conoscere ai partecipanti la Fabbricazione Additiva, i più potenti vantaggi delle tecnologie di stampa 3D e l'uso delle tecnologie digitali aperte. 
Alla fine delle sessioni, i partecipanti saranno capaci di creare oggetti 3D usando le tecnologie digitali aperte, condividendo I loro progetti e utilizzando progetti di altri utenti.

Questo modulo sarà diviso in 2 sessioni:
### Prima sessione: Trasformazione digitale nella manifattura: La fabbricazione additiva (AM)
Negli ultimi decenni le comunicazioni, la produzione di immagini, l'architettura e l'ingegneria hanno tutti subito le proprie rivoluzioni digitali. Il termine "trasformazione digitale" si riferisce all'uso dei dati digitali, alla connettività e alla fabbricazione, ricomprendendo ogni aspetto dell'attività produttiva. La fabbricazione additiva ci offre l'opportunità di ripensare completamente il modo in cui i prodotti arrivano all'utente finale. La sessione ha lo scopo di fornire agli studenti un'ampia panoramica sul design e la fabbricazione digitale, introducendo gli studenti alla stampa 3D, al suo funzionamento e al come può essere utilizzata nei vari settori industriali. Gli studenti impareranno cos'è la stampa 3D, come funziona la stampante 3D, e quali oggetti è possibile creare utilizzando questa tecnologia.
### Seconda sessione: Esploriamo le tecnologie digitali aperte per progettare, sviluppare, personalizzare e condividere prodotti 3D
Il futuro della produzione è digitale. Le stampanti 3D stanno rapidamente diventando un dispositivo di produzione ampiamente utilizzato che ha rivoluzionato l'industria manifatturiera. Nel lavoro di stampa 3D, tutto inizia con un modello 3D. La sessione mira a presentare software open source e piattaforme per la progettazione e la condivisione di contenuti 3D. Gli studenti sperimenteranno il processo di progettazione attraverso la progettazione/ricerca di oggetti 3D utilizzando Tinkercad (piattaforma per pubblicare, condividere e scoprire contenuti 3D su web, mobile, AR e VRI) e Sketchfab (strumento per la pubblicazione e la ricerca di modelli 3D online). I partecipanti acquisiranno le competenze tecniche necessarie per approcciarsi alla fabbricazione additiva e sfruttare al meglio tutte le opportunità offerte dalle tecnologie 3D.