---
title: 'Community of practice (Commons + collaborative management)'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Community of practice (Commons + collaborative management)'
lang_id: 'Community of practice (Commons + collaborative management)'
lang_title: en
length: '10 hours'
objectives:
    - 'practically demonstrate and engage learners on exploring the good practices of managing CoP'
    - 'practically demonstrate and engage learners on exploring tecognise how CoPs is present in daily actions'
    - 'get to know the tools that CoPs use and the commons management practices linked to them'
materials:
    - 'Personal computer (smartph0one or tablet connected to the internet)'
    - 'Internet connexion'
    - Bearmer
    - 'Paper and pens'
    - Flipchart
    - 'Slidewiki Academy'
skills:
    Keywords:
        subs:
            'FLOSS mouvement': null
            'FLOSS culture': null
            'Communities of Practice': null
            Commons: null
            'Collaborative management': null
            'Civic empowerment': null
            'Community engagement': null
            'FLOSS tools and skills': null
---

In this module we will explore Communities of Practice (CoP) as groups of people who share a craft or a profession. A lot of the theory behind the concept has been developed by educational theorist Etienne Wenger, while an important benefit to a CoP is the capture of tacit knowledge. 
The motivation to participate in a CoP can include tangible returns (promotion, raises or bonuses), intangible returns (reputation, self-esteem) and community interest (exchange of practice related knowledge, interaction). Communities of practice are an important of initiating and establishing a commons culture. This module is set to show us, how this is already happening.
We will link this area with a presentation of the Commons as on overall philosophy and practice and explore the role of collaborative management as an important element.

This module will be divided into 3 sessions:
###First session - Defining communities of practice (CoP)
This session will define and describe CoP and explore their relation to Commons. It will provide a background and understanding of the limits and challenges of initiating and gardening CoPs
###Second session: (Digital) Commons 
The second session will focus on a critical thinking approach around (digital) commons and commons - based management methods.
###Third session: Living with CoP
This session will allow for participants to study specific initiatives and how CoPs play a role in them, get to know the tools that CoPs use and understand the social contracts behind CoP.