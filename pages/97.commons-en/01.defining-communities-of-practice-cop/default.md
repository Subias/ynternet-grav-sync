---
title: 'Defining communities of practice (CoP)'
length: '3 hours'
objectives:
    - 'to explore the relation of CoP to Commons'
    - 'to provide a background and understanding of CoP'
    - 'to analyse specific case studies'
---

##Introduction
Group discussion
Trainer will start the introduction to the module by asking participants about their current and previous experiences on communities of practice in projects, working and family life. The main goal of this activity is to map the plethora of current communication practices and reuse it during the course activities.
##Define CoP
Communities of practice are formed by people who engage in a process of collective learning in a shared domain of human endeavor: a tribe learning to survive, a band of artists seeking new forms of expression, a group of engineers working on similar problems, a clique of pupils defining their identity in the school, a network of surgeons exploring novel techniques, a gathering of first-time managers helping each other cope. In a nutshell: Communities of practice are groups of people who share a concern or a passion for something they do and learn how to do it better as they interact regularly.
Communities develop their practice through a variety of activities. The following table provides a few typical examples:
* Problem solving
* Requests for information
* Seeking experience
* Reusing assets
* Coordination and strategy
* Building an argument
* Growing confidence
* Discussing developments
* Documenting projects
* Visits
* Mapping knowledge and identifying gaps

##Design your own community of practice
In this activity, we will use existing co-design methodologies for participants to design and propose their own intentional communication scenario.
##Homework
Visit the XES: the Solidarity Economy Network of Catalonia initiative and post a short text describing this activity.
##References
* Cultivating communities of practice: a guide to managing knowledge. By Etienne Wenger, Richard McDermott, and William Snyder, Harvard Business School Press, 2002.
* Communities of practice: the organizational frontier. By Etienne Wenger and William Snyder. Harvard Business Review. January-February 2000, pp. 139-145.