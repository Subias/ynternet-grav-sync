---
title: SlideWiki
lang_menu_title: SlideWiki
lang_id: slidewiki
lang_title: es
length: '10 horas'
objectives:
    - 'Comprender el marco teórico en torno a las aplicaciones web que sirven para gestionar contenido educativo'
    - 'Promover el uso de la aplicación web Slidewiki en Open Adult Education'
    - 'Promover la cocreación y el intercambio de contenido educativo'
    - 'Evaluar el uso de los sistemas de autoría basados en web OpenCourseWare, como SlideWiki'
materials:
    - 'Ordenador personal o teléfono inteligente o tableta digitalr'
    - 'Conexión a Internet'
    - Proyector
    - 'Papel y bolígrafos'
    - Rotafolio
    - 'Academia Slidewiki'
skills:
    'Palabras clave':
        subs:
            Wikipedia: null
            Comunidad: null
            CC0: null
            'Licencias libres': null
            Colaboración: null
            SlideWiki: null
            Presentaciones: null
            'Software abierto y gratuíto': null
            'Cultura FLOSS': null
            'Colaboración en línea': null
---

SlideWiki es un sistema de autoría [OpenCourseWare](https://en.wikipedia.org/wiki/OpenCourseWare) basado en la web abierta. Admite la creación y la gestión de contenidos de aprendizaje y herramientas para colaboración y la aportación de recursos de forma colaborativa, la traducción, la comunicación, la evaluación y la valoración.

SlideWiki es una aplicación web que facilita la colaboración en torno al contenido educativo. Con SlideWiki, las personas usuarias pueden crear y colaborar en la elaboración de diapositivas y organizar las diapositivas en presentaciones. Las presentaciones se pueden organizar jerárquicamente, para estructurarlas razonablemente de acuerdo con su contenido. También es posible crear un repositorio de colaboración a gran escala (también conocido como crowdsourcing) en torno al contenido educativo (que no sean textos). Las diapositivas, presentaciones, diagramas, pruebas de evaluación, etc. son subidas por las usuarias para luego ser reutilizadas por otras personas. Este contenido es creado principalmente por personas tutoras, profesoras, formadoras, expertas, ya sea individualmente o en grupos muy pequeños. El contenido resultante se puede compartir en línea (por ejemplo, usando Slideshare, OpenStudy, Google Docs).

SlideWiki es una plataforma, donde las comunidades potencialmente grandes de docentes, profesoras y académicas tienen el poder de crear contenido educativo sofisticado de manera colaborativa y abierta. SlideWiki permite difundir contenido y formar a estudiantes e investigadores más rápidamente, ya que la carga de crear y estructurar un área de contenido se puede distribuir entre una gran comunidad. Las especialistas en aspectos individuales del nuevo currículum o area de contenido pueden centrarse en la creación de contenido educativo en su área particular de especialización y aún así este contenido puede integrarse fácilmente con otro contenido, reestructurarse y reutilizarse. Un aspecto particular, que es facilitado por SlideWiki, es que es una plataforma multilingue. Dado que todo el contenido tiene versiones y una estructura rica, es fácil traducir contenido semiautomáticamente y realizar un seguimiento de los cambios en varias versiones multilingües del mismo contenido.

Este módulo se dividirá en 3 sesiones:

### Primera sesión: Descubriendo y entendiendo SlideWiki
El objetivo de esta sesión es presentar a los participantes la herramienta de la plataforma SlideWiki y dejar espacio para la discusión sobre los aspectos relacionados con la creación de contenido, el uso de contenido realizado por otras personas y la autoría, pero también sobre la disponibilidad / accesibilidad de información en contextos educativos. Durante esta sesión, las participantes aprenderán cómo crear sus propias presentaciones y cómo importar contenido a la plataforma.

### Segunda sesión: Experimentar SlideWiki: creación y colaboración en SlideWiki
Durante esta sesión, el enfoque estará en editar las presentaciones que importamos de nuestras colecciones personales, pero también las presentaciones que podemos usar de la comunidad en SlideWiki. El proceso de edición y creación conjunta brinda nuevas oportunidades que las participantes tendrán la oportunidad de explorar durante esta sesión.

### Tercera sesión: creación de cursos en línea con SlideWiki y el contenido de aprendizaje organizado
Durante esta última sesión, las participantes aprenderán cómo crear planes de estudio educativos y organizar clases en línea sobre un tema escogido. Las participantes aprenderán cómo la plataforma SlideWiki ofrece oportunidades para promover el aprendizaje en línea para personas adultas.