---
title: 'Recursos de FLOSS para la empleabilidad'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'FLOSS resources for employment'
lang_id: 'FLOSS resources for employment'
lang_title: es
materials:
    - 'Personal computer (Smartphone or tablet connected to the Internet)'
    - 'Internet connexion'
    - Beamer
    - 'Paper and pens'
    - Flipchart
    - Speakers
skills:
    Keywords:
        subs:
            'Algorithms and Employability': null
            'Curriculum Vitae': null
            Recruitment: null
            Skills: null
            Competences: null
            'Online opportunities': null
            Networking: null
            'Entrepreneurship online': null
            'Donation culture': null
            Wikinomics: null
            'Online campaigns': null
            Fundraising: null
            Crowdsourcing: null
            'Mass collaboration': null
objectives:
    - 'Provide background to FLOSS skills for employment'
    - 'Provide background to Human Resources methods'
    - 'Recognize the benefits and constraints of algorithms in HR'
    - 'Understand open algorithms as a new paradigm'
    - 'Understand the use of algorithms in the recruitment process'
    - 'Understand how to adapt a CV to have a better acceptance of algorithms'
    - 'Learn how to create a good online CV, portfolio or a social media profile'
    - 'Learn how to create and update a good work profile online'
    - 'Understand how important it is to be part of an online community in order to seize new economic opportunities'
    - 'Understand how the market is changing and how online opportunities can facilitate entrepreneurship and personal branding'
    - 'Learn new ways of being entrepreneurial with FLOSS practices and tools'
    - 'Know how to run a crowdfunding campaign online'
    - 'Understand different economic models like the donation culture and the wikinomics, among others'
    - 'Know how to improve their personal communication, collaboration and branding skills'
length: '15 hours'
---

## Introduction

## Context