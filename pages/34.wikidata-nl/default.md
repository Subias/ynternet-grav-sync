---
title: 'Wikidata: een vrij en open kennisplatform dat zowel door mens als machine gelezen en aangepast kan worden'
lang_menu_title: Wikidata
lang_id: wikidata
lang_title: nl
length: '10 hours'
objectives:
    - 'Een solide contextuele achtergrond om de voordelen van Wikidata te begrijpen en hoe het zich verhoudt tot de andere Wikimedia projecten, maar ook hoe het zich verhoudt tot andere databanken op het internet'
    - 'De deelnemer zal digitale vaardigheden verwerven door het toevoegen van inhoud aan Wikidata, maar ook in het schrijven van vragen in SPARQL om antwoorden te krijgen op moeilijke vragen'
    - 'De deelnemer zal in staat zijn om Wikidata aan anderen te presenteren en zal genoeg achtergrond hebben om te identificeren hoe het gebruikt zou kunnen worden in een professionele achtergrond'
    - 'De deelnemer zal beter begrijpen welke kwaliteitsprocessen en evaluatiesystemen bevorderlijk zijn voor de samenstelling van kwaliteitsgegevens die elke persoon en elke machine kan gebruiken'
materials:
    - 'PC aangesloten op het internet'
    - Internetverbinding
    - Projector
    - 'Papier en schrijfgerief'
    - Flipchart
    - Wikidata
skills:
    Sleutelwoordens:
        subs:
            WikiData: null
            Database: null
            Kennisgrafiek: null
            Wikipedia: null
            Gemeenschap: null
            CC0: null
            'Gratis licenties': null
            Samenwerking: null
            SPARQL: null
---

WikiData is een open dataplatform dat behoort tot de Wikimedia-familie van websites. Het herbergt 57 miljoen items vanaf juni 2019. Wikidata is een vrije en open kennisbank die verschillende soorten gegevens bevat (bijv. tekst, afbeeldingen, hoeveelheden, coördinaten, geografische vormen, data...). De basis entiteit in Wikidata is een Item. Een item kan een ding, een plaats, een persoon, een idee of iets anders zijn. Wat specifiek is voor Wikidata is dat de informatie op een strak gestructureerde manier wordt opgeslagen om het mogelijk te maken voor zowel mensen als machines om te verwerken. Hoewel het nu een kennisbank is die vrij veel gebruikt wordt door machines, is het nog steeds grotendeels onbekend bij het publiek en bij de potentiële trainers. De bedoeling van deze module is om een Train the Trainer-cursus te geven die hen een goed overzicht geeft van Wikidata en de mogelijkheden die aan dit open-source project verbonden zijn.

Deze module zal worden opgedeeld in 4 sessies:

### Eerste sessie: Ontdekken en begrijpen van WikiData 
Deze sessie geeft een overzicht van wat Wikidata is. Het zal de geschiedenis van het project overlopen en de deelnemers in staat stellen het waarom en de voordelen van het bestaan ervan te begrijpen, voor de wikimedia projecten in het bijzonder, en voor het internet in het algemeen. De sessie zal gaan over de basis van Wikidata, de gegevensstructuur, elementen van de specifieke woordenschat en essentiële ontwerpkenmerken, maar ook over het overzicht van de wikidata gemeenschap en het bestuur, zodat de deelnemers het feit dat wikidata, zo technisch als het lijkt, in de eerste plaats een sociale constructie is, kunnen vastleggen. De deelnemers zullen Wikidata zelf verkennen en een account aanmaken. 

### Tweede sessie: WikiData bewerken
De tweede sessie zal erop gericht zijn de deelnemers zelf inhoud aan Wikidata toe te laten voegen en zo enkele van de bewerkingsfundamentele processen te ontdekken. Het zal ook een diepere kijk geven in de gegevenskwaliteit en gegevensevaluatiesystemen, evenals het ontdekken van de meest populaire tools die het mogelijk maken om de gaten in de diepte en breedte te vullen, evenals tools voor massabewerking en -import. 

### Derde sessie: WikiData opvragen
Deze sessie is bedoeld om de deelnemers de query service voor eenvoudige vragen te laten ontdekken en experimenteren, en vervolgens de SPARQL querying taal te ontdekken voor meer complexe, diepgaande vragen door middel van een stap-voor-stap methodologie, waardoor de moeilijkheidsgraad steeds groter wordt. De sessie bevat praktische activiteiten. 

### Vierde sessie: WikiData toepassen
Deze sessie zal de deelnemers helpen om de toegepaste waarde van Wikidata te begrijpen, in het bijzonder voor toepassingen zoals visualisatie van complexe gegevens, maar ook om potentiële toepassingen voor Wikidata in hun eigen werk te identificeren, hetzij door visualisatie, het inbedden van Wikidata in hun eigen software, of door gebruik te maken van Wikidata op Wikimedia projecten. Het tweede deel van de sessie zal hen leren hoe ze in contact kunnen komen met de Wikidata-gemeenschap voor een effectievere samenwerking.