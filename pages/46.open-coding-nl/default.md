---
title: 'Open Coding met Scratch'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Open coding with Scratch'
lang_id: 'Open coding with Scratch'
lang_title: nl
length: '10 hours'
objectives:
    - 'Coderen in Scratch en het ontwikkelen van interactieve verhalen, spelletjes en animatie'
    - 'Scratch gebruiken om samen te werken (een project van de Scratch Online Editor delen, het project van een andere gebruiker te bekijken en te remixen)'
    - 'Deelnemers kennis te laten maken met de Scratch filosofie en te leren hoe ze hierin moeten coderen'
    - 'Eigen projecten delen en gebruik maken van andere gebruikers projecten'
materials:
    - PC
    - Internetverbinding
    - 'Scratch programma'
skills:
    Sleutelwoorden:
        subs:
            Scratch: null
            'Open coding': null
            'Scratch filosofie': null
            'Sequentieel verwerken': null
            'Code blok': null
            Blokken: null
            Projecten: null
            Stage: null
            Scripts: null
---

Scratch is een gratis programmeertaal en online community ontwikkeld door MIT die gebruikt kan worden om games, animaties, liedjes te maken en online te delen.

Scratch wordt gebruikt door mensen van alle leeftijden in een grote verscheidenheid aan instellingen. De mogelijkheid om computerprogramma's te coderen is een belangrijk onderdeel van de hedendaagse samenleving. Wanneer mensen leren coderen in Scratch, leren ze belangrijke strategieën voor het oplossen van problemen, het ontwerpen van projecten en het communiceren van ideeën.

De statistieken op de officiële website van de taal laten meer dan 40 miljoen projecten zien die worden gedeeld door meer dan 40 miljoen gebruikers, en bijna 40 miljoen maandelijkse bezoekers aan de website.

Deze module zal worden opgedeeld in 3 sessies:

### Eerste sessie: Scratch filosofie: "Stel je voor, programma, deel"
De filosofie van Scratch stimuleert het delen, hergebruiken en combineren van code. Deze sessie geeft een overzicht van de Scratch-filosofie: "Imagine, Program, Share".
Gebruikers kunnen hun eigen projecten maken of het project van iemand anders hergebruiken. Projecten die met Scratch worden gemaakt en geremixt, hebben een licentie onder de Creative Commons Attribution-Share Alike-licentie. 
De sessie is bedoeld om meer inzicht te krijgen in de achtergrond van Scratch en de principes voor het maken, programmeren en delen van materiaal. 
Aan het einde van de sessie kunnen de deelnemers het gebruik en de voordelen van de Scratch-filosofie in het volwassenenonderwijs onder de loep nemen.

### Tweede sessie: Open codering met de gratis visuele programmering van Scratch 
Wanneer mensen leren coderen in Scratch, leren ze belangrijke strategieën voor het oplossen van problemen, het ontwerpen van projecten en het communiceren van ideeën. Dus, het coderen in Scratch helpt gebruikers om creatief te denken, systematisch te redeneren en samen te werken - essentiële vaardigheden voor het leven in de 21e eeuw.
De tweede sessie is gericht op het introduceren van de door MIT ontwikkelde programmeertaal.
Deelnemers leren hoe ze in Scratch kunnen coderen voor het ontwikkelen van interactieve verhalen, spelletjes en animatie.
 
### Derde sessie: De effectiviteit van Scratch in het creëren van een collaboratieve programmeeromgeving
Deze module heeft als doel de basisprincipes van de online gemeenschap te illustreren 
De Sessie heeft als doel om leerlingen te ondersteunen bij het identificeren van en het profiteren van de Scratch online gemeenschap. Deelnemers leren hoe ze een project van de Scratch Online Editor kunnen delen, hoe ze het project van een andere gebruiker kunnen bekijken en remixen, waarbij ze zich tegelijkertijd door anderen laten inspireren.
