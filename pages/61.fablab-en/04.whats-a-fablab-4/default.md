---
title: 'Explore real context needs and thoughts'
objectives:
    - 'To provide an introduction to explore the potential of the use of open source hardware and Software in the framework of a Fablab'
    - 'To discover the technologies that are suitable to start a FabLab initiative'
    - 'To facilitate an overview about open hardware and software'
    - 'To facilitate an overview about machines related with digital fabrication'
    - 'To discover how to use of different Technologies in different types of activities'
    - 'To motivate to use of open technologies'
    - 'To motivate to open and promote a makerspace and maker’s activities in their own organizations'
---

## Introduction
The trainer will talk about the questions that may have arose after the previous session and will comment on homework. 
After that, trainer will introduce the content and the main activities of the session. 	

## First part
The goal of this section is to ensure that students understand that there are hundreds of open technologies that can be useful in a FabLab and to identify different digital fabrication techniques and materials. 

Trainer will present two webpages and will ensure that the participants get into them and enjoy by taking a look to the characteristics of some of the products/technologies. 
* [List of FabLab machines](https://www.fablabs.io/machines): List of Machines, kits and types of technologies and machines to be used in a Fab Lab Space.
* [Different Materials](https://www.shapeways.com/materials)
* Some library of resources you could use, because someone created and shared them on Internet: 
    * [Gravity sketch](https://www.gravitysketch.com/)
    * [Vectary](https://www.vectary.com/)
    * [Tinkercad](https://www.tinkercad.com/)
    * [Thingiverse](https://www.thingiverse.com/)
    * [Beetle Blocks](http://beetleblocks.com/)
    * [Sahpe Ways](https://www.shapeways.com)

Trainer will provide a template to the participants, who will work in groups, so they can build up their own favorite technologies list. 
Scheme will be: 
* Name of the technology
* Materials and sources that are associated to this technology 
* How this technology can be useful. 
* One possible project that could be developed by using this sources

## Second part
The goal of this section is to ensure that students start to think about **a small project** or activity that could be developed by using some concret open technologies related to digital fabrication. Tip: trainer to make sure that the project is “small” 
Trainer will help the students by organizing a group brainstorming, by using some previous examples and answering the following questions:
* Challenge/project to be developed
* Why: which problem/opportunity will take
* Who should be involved? (profiles)
* With which tool/source/machine/material

After the brainstorming, trainer will facilitate the work in small groups, where students are invited to think about this ideas and select one to be further developed.
At the end of the session, participants will present their concrete idea to the general group and will make/receive some comments on their proposals. Trainer will make some general remarks on the ideas: if we have some information reflected in every column, then we have a  great starting point of a FabLab initiative.

## Homework
Continue to plan a mini project or activity where different machines should be used.

Now we have some ideas and some starting point. On the homework of the previous session we thought about the management of the volunteers, the social return of being a volunteer and the Learning and Service methodology. How can we apply this concepts on the project we did work during today’s session?
