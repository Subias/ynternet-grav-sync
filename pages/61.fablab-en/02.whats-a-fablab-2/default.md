---
title: 'Community design and collaboration'
objectives:
    - 'To provide an introduction to recognize the potential of running a FabLab into a community'
    - 'To provide an introduction to personal and community needs versus mass production'
    - 'To facilitate an overview about collaborative spaces and maker culture'
    - 'To discover an analysis of the quadruple helix potential into the territory: company - university - citizenship - public administrations'
    - 'To motivate to detect and collaborate with different agents involved i education, community dynamics and technology'
    - 'To motivate to open and promote a makerspace and maker’s activities in their own organizations'
---

## Introduction
The trainer will talk about the questions that may be arised after the previous session will comment on homework. After that will introduce the content and the main activities of the session.

## First part
The goal of this section is to better understand the maker culture in a commons environment, as a introduction to the potential of running a fablab into a community, with a commons approach. 
The trainer sets out some questions to evaluate the participants “previous knowledge” and opens the discussion about the meaning of “Community design and collaboration”. A brainstorm to catch up what these concepts mean to the participants is suggested. Trainer can use the board (if FtF training) or a virtual tool to collect the participants ideas. 
After this brainstorming, the trainer will propose to read (individually) o watch a video (collectively) with a “maker manifesto” or that explain the “maker culture”. Some examples are provided here: 
* [Manifest MakerConvent](http://conventagusti.com/maker/manifiesto-makerconvent/) (requires translation as it’s written in Catalan and Spanish).
* [The maker movement manifesto](http://raumschiff.org/wp-content/uploads/2017/08/0071821139-Maker-Movement-Manifesto-Sample-Chapter.pdf), Mark Hatch.
* [The Maker Movement Manifesto](https://www.youtube.com/watch?v=Fjrtiyq3ECQ), Mark Hatch.
* [Maker Movement: It’s about creating rather than consuming](https://www.youtube.com/watch?v=I_WQOqKldm4), The Feed.

## Second part
The goal of this section is to better understand the advantages of being in a commons environment, and to visualize some examples, as an introduction to the potential of running a fablab into a community, with a commons approach. 
Let’s see and discuss some examples of maker crowdsourcing (some other examples can be provided) :
A [MakerFaire](https://makerfaire.com): what is a Maker Faire? Who goes there? What activities do they do there?
Other participative networks: [codeclub](https://codeclub.org) and [CoderDojo](https://coderdojo.com). Discussion about their objectives, activities and the values that are involved
After that, the trainer will ask the participants to make an exercise in imagination, answering the following questions (individually):
* I would like to visit (or not) a Make Fair because…
* The biggest advantage of creating within the framework of a maker space or a Fablab is...
* I would like to create a maker space or a FabLab because...
* If I could propose the values inherent in makerspaces, these would be...
* The people who would participate in a maker space in my community would be (teachers, social entities, city council technicians, businessmen, students...).
* If I decide to run a Fablab, I would invite... to join me.
* If  I decide not to start a Fablab, but start to do some small activities closely related, I will start by doing… 

## Third part
The goal of this section is to understand the quadruple hèlix innovation model and to evaluate the chances of implementing it in our community. 
In this part, the trainer will explain how the quadruple hèlix innovation model works (trainer can play [this video](https://www.youtube.com/watch?v=9VoJ3w5ecsw) to make it short) and will provide a template to be filled in order to clearly identify and classify local agents into four categories (quadruple hèlix innovation model). The main idea is to think about people or institutions that could be invited to join the “future Fablab”.  
* Companies 
* University
* Citizenship - leaders of the community
* Public Administrations
As a second step, trainees will be invited to specify the role that this local agents could play into the new FabLab system. 
Tips:
Being as specific and concrete as possible. 

## Homework
Look for an initiative that calls for volunteering:
* Look what they ask
* What data they need
* What they offer

## References
* [Mark Hatch](http://raumschiff.org/wp-content/uploads/2017/08/0071821139-Maker-Movement-Manifesto-Sample-Chapter.pdf)
* [CLIQ project](https://www.youtube.com/watch?v=9VoJ3w5ecsw): how quadruple helix innovation works.
* Yun, J.J.; Liu, Z. [Micro- and Macro-Dynamics of Open Innovation with a Quadruple-Helix Model](https://www.mdpi.com/2071-1050/11/12/3301). Sustainability 2019, 11, 3301.