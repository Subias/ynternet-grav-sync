---
title: 'What''s a FabLab'
objectives:
    - 'To provide an introduction to what is a Makerspace and the different typologies (FabLab, Hackspace, HackLab, GrootSpace ...), to the maker culture and movement, the collaborative spaces and to the digital Fabrication (machines and techniques)'
    - 'To discover a new world of opportunities and goals'
    - 'To motivate to meet the openness culture and to promote a makerspace and maker’s activities in their own organizations'
---

## Introduction
The objective of this first part is to highlight the previous knowledge of the group. 	 Participants are also invited to share information about the context of their participation to the session (motivation, personal goals, etc.)
The trainer organizes an icebreaker to work on some of the key concepts. Start by asking the group what do we know in relation to the following concepts (see above). If it is a face-to-face class, trainer will take note of the answers on the board, underlining those that are most appropriate. If it is an online training, a small debate is organized through a forum.
Maker movement introduction
What we know about?
What is it?
When it starts?
What is a Makerspace?

## First informations
This part of the session is designed to provide basic information on the subject. The proposed, or other videos that the trainer will select in advance, will be used. It’s suggested to use videos recorded in the different countries where the training is implemented. This section typically ensures that participants will be able to identify what is a Fablab, what is a makerspace and have a more concrete idea about it. 
Let's look at the videos and comment...
[FabLab ¿Qué es un FABLAB?](https://youtu.be/L_15Rc79T_c)
[What is a Fab Lab?](https://www.youtube.com/watch?v=lPF7zDSf-LA)
[What Is The MakerSpace?](https://www.youtube.com/watch?v=wti6FMvDAE4)
[Santiago Maker Space](https://youtu.be/i4OWsLY5RBo)

## New concepts
This part of the session is aimed to talk about creativity and makerspaces. Maker spaces are a concept of space where projects are developed according to the philosophy of the maker movement. They emerged as meeting spaces for people who wanted to learn, research and share their knowledge of digital fabrication. They are a direct reflection of what the Hakerspaces of the 1970s were, applied to the Open Hardware world.
The trainer will present the Creative Spiral of Mitchel Resnick. Trainer will highlight the main ideas by using the Wikislides presentation and will explain what the creative spiral is and its advantages in the field of education (and propose to read the article available here as an afterwork)
After that, the group will move to identify the possible strengths and weaknesses of a MakerSpace. Participants will work in pairs, and they can do it by using a basic template (two columns: strengths and weaknesses) or by using different materials to visually "represent" strengths and weaknesses (using cardboard and colored pencils, for example, or lego pieces… ) After 15 minutes of work, they will share the outcome with some other participants. 

Trainer will ensure that some concepts are explored: digital fabrication, collaboration, learning by doing and DIT (Do it Yourself) The maker culture.

If the activity is runned in a virtual way, the suggestion is to do the activity individually and to suggest to build up the list by using an infographic tool (like https://es.venngage.com/templates or https://piktochart.com/formats/infographics/ ) and share it through a fòrum or email list.

## Makerspaces
The goal of this section typically ensures that participants will deploy an active attitude looking for makerspaces. 
The trainer will suggest the participants to open an internet browser and look for “makerspaces” all around the world and their territory (country/region/city). They can do it individually and share the result on a common list. Using https://padlet.com/ is suggested (no register is needed), as they can use this tool to share different kind of sources (text, vídeos, etc.) In this way, they collect and share information very fast.  
Tips:
The trainer must stimulate the participants and help them during the session.

## Homework
Search for a makerspace in your community, investigates what does and where is it. If possible, visit it and include more information about it in the common space at Padlet.
