---
title: 'FabLab Technologies'
objectives:
    - 'To provide an introduction to Do it Yourself (DIY) Methodology'
    - 'To provide an introduction to Learn By Doing Methodology'
    - 'To facilitate a better understanding about how a Fablab or a Maker Space works'
    - 'To discover some good ideas to be considered, some good examples to take into account'
    - 'To motivate participants to be (more) creative and active citizens'
---

## Introduction
The trainer will talk about the questions that may have arisen after the previous session and will comment on homework. 
After that, the trainer will introduce the content and the main activities of the session. 

## First part
The goal of this section is to understand what is and how the DiY method works. 

"**Do it yourself**" ("**DIY**") is the method of building, modifying or repairing things without the direct aid of experts or professionals. Academic research describes DIY as behaviors where "individuals engage raw and semi-raw materials and parts to produce, transform, or reconstruct material possessions, including those drawn from the natural environment (e.g., landscaping)" ([Wikipedia](https://en.wikipedia.org/wiki/Do_it_yourself)) Motivations could be very different: 
* Related to the market: economic benefits, lack of product availability, need for customization, 
* Identity enhancement (craftsmanship, empowerment, community seeking, uniqueness, get some fun…)

Trainer can also challenge the students to look some amazing videos on a video channel (Vimeo, YouTube or similar) based on DiY culture and ethics.
* Let's look for… DiY furniture, DiY plants, DiY clothes…

After the explanation, the trainer can ask some questions (open questions in the classroom or in a e-learning forum): 
* Do you have any DiY experience? If yes… how it was? what did you get?
* Have you heard about it previously? If yes, what do you think is the most important value involved? 
* Would like to try? Can you think about some activity to start?

## Second part
The goal of this section is to understand what is Project **Based Learning** and which is the relation with the maker and DiY culture. 
Trainer will play this videos: 
* Project Based Learning: [Why, How, and Examples](https://www.youtube.com/watch?v=EuzgJlqzjFw)
* [Students make prosthetic hand for classmate using 3D printer](https://www.youtube.com/watch?v=ymxcKDuH87g)

After playing the videos, a video-forum can be run. 
* Have you ever been involved in a Project Based Learning? How was your experience? 
* If you want to organize some PBL, which will be your starting point?
* Let’s consider some real opportunities. Think about problems (ODS could be used in order to stimulate) and challenges… 

The goal is to ensure that the participants understand that Project-based learning (PBL) is a student-centered pedagogy that involves a dynamic classroom approach in which it is believed that students acquire a deeper knowledge through active exploration of real-world challenges and problems. Students learn about a subject by working for an extended period of time to investigate and respond to a complex question, challenge, or problem. It is a style of active learning and inquiry-based learning. PBL contrasts with paper-based, rote memorization, or teacher-led instruction that presents established facts or portrays a smooth path to knowledge by instead posing questions, problems or scenarios. More info at [Wikipedia](https://en.wikipedia.org/wiki/Project-based_learning).

## Third part
The goal of this section is to ensure that students understand how the PBL method can be apply in the **context of 3D printing** and that 3D printing is not just a part of a millionaire industry. 

Trainer will start by playing some inspirational videos, which present several and very concrete ideas: 
* [How to get money on 3D Printing business - 10 Ideas](https://www.youtube.com/watch?v=kIdSftmUENs)
* [15 Things You Didn't Know About the 3D Printing Industry](https://www.youtube.com/watch?v=L5_a6diPI0E)
* [Commercial website about printing and digital printing services](https://www.shapeways.com/create)
* [3D Printing Fashion](https://www.youtube.com/watch?v=3s94mIhCyt4): How I 3D-Printed Clothes at Home
* [Dinner is printed](https://www.youtube.com/watch?v=B0Ty6wgM8KE): is 3D technology the future of food?

Trainer will organize the participants into 3 focus group and will display some questions to be answered: 
* In this videos, which opportunities are shown? 
* Which are the values involved?
* If they talk about opportunities, which ones? 
* If we think about people that could be involved in such kinds of activities, which is the profile? 
* How much money could cost the technology involved? 

After 15 minutes working in groups, participants will summarize the main ideas that have come up. After that, the trainer will conduct a discussion: how can we apply what we have learnt into a communautaire Fablab? 
 

## Homework
Plan a mini project or activity where different machines should be used and describe what you would do with each one

Think about the management of the volunteers that could be interested in participating in the new FabLab. Key concepts: 
* Social return of being a volunteer 
* Learning and service methodology
* Mentoring