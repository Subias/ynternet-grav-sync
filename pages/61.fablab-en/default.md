---
title: 'How to run a FabLab'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'How to run a FabLab'
lang_id: 'How to run a FabLab'
lang_title: en
Materials:
    - 'Personal computer'
    - 'Internet connexion'
length: '10 hours'
objectives:
    - 'Identify what’s a FabLab and the culture that’s involved.'
    - 'Be able to start a FabLab initiative, understand which are the first steps and which technologies are more suitable to start'
    - 'Know, detect and know how to use sources of information that provide resources that are useful in the framework of action of a FabLab'
    - 'Explore FabLab practice communities that use open technologies and tools'
    - 'Enable the participant to provide information, resources, models to FabLabs communities of practice'
    - 'Explore the local community to make the link between company - university - citizenship and public administrations (promote the quadruple helix model)'
skills:
    Keywords:
        subs:
            FabLab: null
            MakerSpace: null
            HackSpace: null
            'Open Design': null
            'Open Hardware': null
            'Open Software': null
            'Do it Yourself (DIY)': null
            'Learning by doing (LBD)': null
            PBL: null
---

## Introduction
A FabLab is much more than a space equipped for small-scale digital manufacturing. It is a concept, it is a point of view, it is a new way of generating wealth and empowerment within a community.
Equipped with free, open and free technology and tools, Fablab is a space capable of generate a dynamic closely linked to collaborative learning, sharing and commons culture and the free exchange of resources and information.
Within the framework of a FabLab, especially if it has a community orientation, new collaborative productive dynamics are generated; it’s part of the “Do it Yourself” (DiY) and Maker Culture.

In each piloting country, partners will be responsible for the organization of training groups with a minimum of 8 and a maximum of 10 participants selected among e-facilitator and training providers in the field of non-formal adult education.
FabLab culture is closely related to the DIY and Maker culture, as well as FLOSS; it’s always something “under construction” and to start one FabLab you don’t need to have many tools or machines, but the right attitude. A FabLab is a concept, and starting a Fablab is working under a concrete philosophy.
This Module will introduce participants in the concept of what is considered a FabLab and what kind of activities can be done there.

## Context
When one imagines a FabLab, one thinks of a creation space, to be collaborative, using open technologies, taking advantage of the resources of the nearest territory and also the sources of information and Internet resources. A space in continuous growth, with some technologies that may increase over time. A space of concurrence of people who will arrive with an open and curious attitude. A starting point that, if inserted and related to the local community, will attract different types of audiences and that will be rich in creativity and will promote the autonomy and individual and individualized learning of people.

The goal of the session is to practically demonstrate and engage learners on :
* Identify what’s a FabLab and the culture that’s involved.
* Be able to start a FabLab initiative, understand which are the first steps and which technologies are more suitable to start. Detect with are the open technologies and tools that are more suitable in a FabLab environment.
* Know, detect and know how to use sources of information that provide resources that are useful in the framework of action of a fablab. 
* Explore FabLab practice communities that use open technologies and tools.
* Enable the participant to provide information, resources, models to FabLabs communities of practice. Open design. Support the participants of a FabLab in the process of learning and producing.               	 
* Explore the local community to make the link between company - university - citizenship and public administrations (promote the quadruple helix model)


This module will be divided into 6 sessions:

### First session: What’s a FabLab?
Digital fabrication, collaboration, learning by doing and DT (Do it Yourself). The maker culture.
### Second session: Community design and collaboration
Recognize the potential of running a fabLab into a community. Personal and community needs versus mass production.
### Third session: Fablab Technologies
Explore the potential of the use of open source hardware and Software in the framework of a Fablab. Discover the technologies that are suitable to start a FabLab initiative.
### Fourth session: Explore real context needs and thoughts
### Fifth session: Firsts steps... creating your own Fablab
Collaboratively design a new community FabLab. 
### Sixth session: Peer review of the design of the community Fablab
First activities to be developed.