---
title: 'Collaborative design a new community FabLab'
objectives:
    - 'To provide an introduction to Collaboratively design a new community FabLab'
    - 'To facilitate protocols, methodologies and guides to detect and identify Digital Social Innovation agents'
    - 'To discover dynamics of the community work'
    - 'To motivate to open and promote a makerspace and maker’s activities in their own organizations'
---

## Introduction
The trainer will talk about the questions that may have arose after the previous session and will comment on homework. 
After that, trainer will introduce the content and the main activities of the session.

## First part
The goal of this section is to introduce several key concepts to be taken into account when working on the creation of comunitarie FabLab or maker Space. 

Trainer will use this video as an introduction: [How to Create a Makerspace](https://youtu.be/BEUDgfsy-8k). It presents several key concepts: from equipment, resources, legal issues, to getting the community involved. 
Then, trainees (working in groups, with the same configuration than in the last session) will build up a list of key-concepts that affect their groupal projects. After working by themselves, the information will be shared with the big group. Obviously, the main idea of the project/initiative will be also shared, so the group can easily evaluate and complete the information.

## Second part
Now, it’s time to dream on building the concept of a space where you could develop “all” the projects that have been introduced by the participants. We will do it in groups and, in order to do it easier, let’s believe that we have a public sponsor that will provide “money enough” for the first year. 

Trainer will ask the participants to think about this projects (as if anybody were asking for a space and sources to work on them) and to start to plan it. We will use a mind map to describe the first planning scenario. Tips: we should try to be realistic. 
* How many projects can we start in the first year? How can we prioritize them, with which criteria? 
* How many people should be involved from the very beginning? Which profiles?
* Which technologies should be applied? How many of them can be shared between the projects we will start on the first year? 
* Who should be aware about our ideas? 
* Who else could sponsor us within this initiative? 
* How do we ensure that the community will take advantage of our proposal?

## Homework
Think about we’ve done during the session.