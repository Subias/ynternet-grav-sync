---
title: 'Project Based Learning'
objectives:
    - 'to provide an introduction to Project Based Learning (PrBL)'
    - 'to facilitate the work with open learning/teaching methods'
    - 'to discover new approaches to the teaching/learning process and to stimulate the motivation and engagement of the participants. To discover the advantages of student-center methods'
    - 'to motivate students to change their teaching mode, and to face new challenges while teaching'
---

## Introduction
Trainer will present the main characteristics of this approach: Project Based Learning is a method with which the student is at the center of the learning process, as a protagonist capable of generating solutions in response to the different opportunities.
A methodology closely related to the working environment, and also the entrepreneurship. It specially stands out for urging students to put into practice a wide range of knowledge, abilities, skills and attitudes
By using a Project Based Learning methodology...
* We need to get a (material or intellectual) product. This is very important. 
* Cooperation and collaboration among students to achieve this goal is a must.
* Promotes initiative, proactivity, independence and innovation in different areas: professional, social and personal.
* The challenge acts like a motor (motivation and determination) to achieve the goal. Engagement is the main characteristic!
When using this approach, it happens that: 
* The students generate value beyond the classroom environment. As the outcome is publicity launched, students get feedback from the real world. 
* Motivation increases with the positive effect on their social context.
* Their self-esteem is also improved.
* They work on real situation that is, or could be, part of the professional context.
With this method: We can really involve the real world. We can have a real impact on our social context, and learning has a very good entrepreneurship approach, not only from the economic (business) point of view, but also from a social perspective. Students became a reference on the content-area where they have been working on. 

**Differences between PBL and PrBL**: 
* PBL starts with an structured problem. PrBL starts by building a product or an artefact in their minds. 
* PBL problems may not be related with the student's real context. PrBL always work with real-life opportunities, as the outcome must be useful in their own context. 
* PBL use questions and solutions. PrBL use products what will be presented at the end of the learning process. 
* Success in PBL is achieved by finding solutions to the problem. Success in PrBL is achieved by creating solutions to the problem and presenting them to the community. 
After presenting the information, the trainer will ask the participants to brainstorm about some opportunities that could be presented to their learners (for example, to create a local newspapper, to create an APP…). Trainer will take notes on the board/flipchart. 
Trainer can lunch some challenging question to the participants: which part of the Technovation Girls study case is “PBL” and which part is “PrBL”? It’s important for the students to learn that “100% pure method” is not always deployed and that they can take part of the methods and mix them, in order to get some different outputs. The methods are always suggestions, but the trainer must be flexible.

## PrBL
Trainer will present the PrBL scheme and explain that it’s all about following the process: 
1. Detection of the opportunity to work on.
2. Organization of the work teams (different profiles, complementary).
3. Final definition of the challenge, the solution to be achieved.
4. Preparation of the plan.
5. Training and information research.
6. Analysis and synthesis. The students share their work by exchanging ideas, discussing solutions, doing suggestions, etc.
7. Elaboration of the product by applying everything they have learnt.
8. Presentation of the product or project.
9. Implementation of improvements, if necessary.
10. Assessment and self-assessment.

## Everyone opens his/her laptop
The goal of this part of the session is to have the participants building up a concrete proposal that could be presented to the students. Participants will work in groups and use a computer to create a digital presentation with their proposal. 
They: 
* Will think about opportunities that may be suitable to work with this method. 
* Think about how to organize the group.
* Prepare a draft plan.
* Identify the knowledge are to be explored (research) and suggest some different methods.
* Think about several kind of products that could be created when working with the students.
* Suggest some “presentations” modes to be used (public presentation, video-recording, etc.)
* Think about how to assess and self-assess the work.
After 40’ working in small groups, the groups will share their proposal with the class.