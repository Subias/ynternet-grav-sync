---
title: 'Flipped Classroom'
objectives:
    - 'To provide an introduction to Flipped Classroom and the main concepts involved'
    - 'To facilitate the transformation of a training into a flipped classroom'
    - 'To discover the Bloom’s Taxonomy and the differences between the low level thinking skills and the high level thinking skills, as well as the different categories of skills (knowledge, comprehension, application, analysis, synthesis and evaluation)'
    - 'To motivate students to change their teaching mode'
---

## Introduction
The objective of this first part is to highlight the previous knowledge of the group. 	 Participants are also invited to share information about the context of their participation to the session (motivation, personal goals, etc.)
**After that, the trainer will present the "The Flipped Classroom (FC) concept by using the slides prepared at Slidewiki.**
In brief, "[The Flipped Classroom (FC)](http://www.theflippedclassroom.es/what-is-innovacion-educativa/) is a pedagogical model that moves the work of certain learning processes outside of the classroom and invests the class session time, along with the teacher experience, to promote and boost other processes of acquisition and knowledge practice inside the classroom". Trainer use the available digital resources and tools to provide the students with an advance of the necessary contents, which they will review by their own before attending the class.

Once the students are in the classroom, in contact with their peers and teacher/s, they invest their time working with the questions that they were not able to solve on their own on the previous stage. Peer to peer support is essential in this method. 

By using a Flipped Classroom model…
* Students learn new content online by watching video lectures, usually at home →  comprehension and knowledge activities
* And what used to be homework (assigned problems) is now done in class with teacher offering more personalized guidance and interaction with students. → Application -  Analysis - Synthesis and Evaluation activities. 
Comparison:
![](traditional.jpg)
![](flipped.jpg)
## Group discussion
In small groups, the participants will work on Bloom’s taxonomy and think about several different activities that could be developed at home / at class, according to the Flipped Classroom model. After 5’ of individual reflexion, the trainer will start a brainstorming and will take notes on participant’s suggestion (10’). 
Finally, trainer will show/distribute the detailed Bloom’s taxonomy and explain how it works (5’):
![](taxonomy.png)

## Everybody opens his/her laptop
Participants will (individually or in group) take the editable document with the taxonomy and add a new row, in order to add the digital tools, software or other digital resources that could be useful to achieve the “actions” and the  “outcomes”  (third row of the document). Trainer will make enfasys on the use of FLOSS and open and free technologies. 
Participants will also think about which part of the taxonomy can be developed “at home” or “in class” and try to suggest (in abstract) some activities that could be done during an specific training (using a concrete example can be very useful)
e.g.: 
at home: 
* Knowledge-related (comprehensive reading, filling-in questionnaires, create a glossary, etc.)
* Understanding-related: creating a infographic, a, summary, a post, a digital presentation…
In class, with peers and teachers
* Application-related: an interview to an expert, a virtual scenario, a timeline with the sequence of an action plan...
* Analysis-related: conceptual map, comparative chart...
* Assessment-related: discussing, debate, real or simulated tests or experiments, etc.
* Creation-related: video, postcast...

After 45’, participants will share their findings with the group and comment on their experience.

## Homework
Think about a training you have taught before, and the activities you proposed to your students: if you applied the Flipped Classroom model, what part would be developed at home and what part would be done in the class?
