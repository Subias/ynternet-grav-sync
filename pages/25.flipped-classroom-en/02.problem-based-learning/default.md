---
title: 'Problem Based Learning'
objectives:
    - 'To provide an introduction to Problem Based Learning'
    - 'To facilitate the work with open learning/teaching methods'
    - 'To discover new approaches to the teaching/learning process and to stimulate the motivation and engagement of the participants. To discover the advantages of student-center methods'
    - 'To motivate students to change their teaching mode'
---

## Introdution
The trainer will talk about the questions that may be arised after the previous session will comment on homework. After that will introduce the content and the main activities of the session.

Trainer will introduce the “Problem Based Learning” concept and method. Starting point will be a “case review” and we suggest to use the “Technovation Girls” project (see at https://technovationchallenge.org/) , which is a project that challenge teams of girls to think about local problems and to code a solution (they build an app). It’s better to find a local example, so we suggest the trainer to look for local examples at YouTube by searching “Technovation + name of the country” After playing some of the available videos, trainer will highlight some elements of this (or a similar) project: 
it’s a challenge 
they work in groups
they work to solve a real problem but there’s not need that the problem is present in the contex of the students (they can work on other’s problems)
they learn what they need to provide a solution (that is not the same for the different groups)
Trainer will explain that nowadays it’s very easy to identify real problems, by using [the 17 goals to transform our world](https://www.un.org/sustainabledevelopment/sustainable-development-goals/):

![](sustainable.png)

Problem based learning? is a method that start with the teacher laying out a question or a problem a challenging the students to give an answer to the question or to find the way to solve the problem. Sometimes, teacher invite the students to think about the problem. PBL allows the teacher and the student to settle a milestone in the learning process, where the student is the principal actor.

By using Problem Based Learning methodology, students…
* Learn to make decisions, individually or negotiating (with the group)
* Interact with peers and also with teachers
* Use the information of their close context
* Search for the information they need
* Produce and share ideas
* Discuss other possible solutions and also the mechanisms to find them.
The problems must be in accordance with the abilities of the students and must be significant to them. Problems are aroused from the real world and could be “real” or “potential”. 
It’s better to use real scenario to make things easier.

* The problem must motivate students to seek out a deeper understanding of concepts.
* The problem should require students to make reasoned decisions and to defend them.
* The problem should incorporate the content objectives in such a way as to connect it to previous courses/knowledge.
* If used for a group project, the problem needs a level of complexity to ensure that the students must work together to solve it.
* If used for a multistage project, the initial steps of the problem should be open-ended and engaging to draw students into the problem.
(Duch, Groh, and Allen, 2001)

![](gold.png)

[Source of the image](https://www.pblworks.org/blog/gold-standard-pbl-essential-project-design-elements)

With this method, students can improve/change their knowledge, abilities and skills and attitudes. 
At this time, trainer will ask the participants to brainstorm about some problems that could be presented to their learners. Trainer will take notes on the board/flipchart.

## PBL
Trainer will present the PBL scheme and explain that it’s all about following the process: first step is the analysis of the problem, then participants search for the necessary information they need to understand the problem, and learn the necessary skills to understand the information. Then they can integrate the information and they can apply it to of a certain amount of knowledge and wisdom to solve the problem.
It’s important to understand that every learner need to learn/get different skills, as they previous knowledge is different. 
The stages are: 
1. Presentation of the problem to be solved/identify a problem that group is interested on. 
2. Analysis of the previous knowledge
3. Identification of the knowledge and the resources needed to develop a solution
4. List of actions or activities that should be accomplished to find a solution.
5. Preparation of the work plan
6. Do what it’s been planned: searching for the necessary information, carrying out actions, communication of the partial results
7. Elaboration of the answer and argumentation about the method used to find it. 
As the trainer has presented an exemple (Technovation girls one), will transform the exemple into stages, so the participants can realize how easy is to do it.

## Everyone opens his/her laptop
The goal of this part of the session is to have the participants building up a concrete proposal that could be presented to the students. Participants will work in groups and use a computer to create a digital presentation with their proposal. 
They: 
* will identify and present a very concrete problem, ODS related, and identify a potential group of learners (age, background, special needs… )
* identify some knowledge/skills that his/her students should have to solve the problem (then students will be allowed to identify if they have or they need to learn/acquire part of this knowledge/skills). 
* prepare an initial list of activities that should be performed. Eg: visiting a place, interview someone, search on Internet, run a survey, make some calculation with open-data; program an app or a videogame, make a presentation, make an infographic… 
* prepare a potential work plan (that will be modified by the learners when they do the activity)
After 40’ working in small groups, the groups will share their proposal with the class.

## Homework
After listening the different proposals, think about a concrete problem to be solved with your students in your class. Think about pros and against of using this method in your class.

## References
* Duch, B. J., Groh, S. E, & Allen, D. E. (Eds.). (2001). The power of problem-based learning. Sterling, VA: Stylus.
* Grasha, A. F. (1996). Teaching with style: A practical guide to enhancing learning by understanding teaching and learning styles. Pittsburgh: Alliance Publishers.
* The Center for Innovation in Teaching & Learning (CITL): [Problem-Based Learning (PBL)](https://citl.illinois.edu/citl-101/teaching-learning/resources/teaching-strategies/problem-based-learning-(pbl))
* [PBL through the Institute for Transforming Undergraduate Education at the University of Delaware](http://www1.udel.edu/inst/)

