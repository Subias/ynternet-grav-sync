---
title: 'Personal o grupal development of a learning/teaching project'
objectives:
    - 'to provide an introduction to'
    - 'to facilitate '
    - 'to discover'
    - 'to motivate'
---

## Introduction
The trainer will talk about the questions that may be arised after the previous session will comment on homework. After that will introduce the content and the main activities of the session.

The objective of this session is facilitate the participants the creation of their own project, based on some of the methods presented on the previous sessions. 
Trainer will make a recap of all the content and methods that have been presented and invite de participants to build up teams or to choose to work individually on a training plan. They should select a target group and a topic and then start to design their own training. 
They should think and decide on: 
* Target group
* Goals / objectives / skills and competencies 
* Background
* Calendar
* Main method and strategy. 
* Introduction to the training
* Description of the main activities and the training sequence. 
Participants are invited to integrate the new knowledge acquired thanks to the Open-AE training.

## Everyone opens his/her laptop
Trainer will invite the individuals/groups to present their project. After the first round of presentation, a peer-to-peer assessment will be organized. 
* One of the possibilities is to do it in a small format: a group meets with another group (or an individual with another individual) and comments, constructively, strengths and weaknesses of the project, open software or hardware resources that they can use and others complementary details.
* The other possibility is to do it with the group-class as a brainstorming. Any member of the class can send questions to the person / people who has presented their project, make suggestions and contribute to the peer-to-peer assessment.

## Debriefing
This is the final part of this module (and of the training, if possible) 
Review of what we have learned and the individual commitments acquired, if any, and final group photo. Good luck and see you very soon!