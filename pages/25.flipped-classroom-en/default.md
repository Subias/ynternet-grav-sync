---
title: 'Flipped classroom / project based-problem based learning (+ peer review)'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Flipped classroom / project based-problem based learning (+ peer review)'
lang_id: 'Flipped classroom / project based-problem based learning (+ peer review)'
lang_title: en
length: '10 hours'
objectives:
    - 'Understand the basics of the Flipped Classroom (FC) strategy and be able to implement it and to create a mind-map to explain it to the others'
    - 'Draft the structure of a new flipped classroom module (or transform a classic module) to teach/learn a topic, using open sources and tools'
    - 'Understand the basics of the Problem Based Learning strategy and be able to explain them to the students and colleagues'
    - 'Design a PBL activity (or transform one that already exist) that could be applied to teach the use of some open software tools'
    - 'Understand the basics of Project Based Learning strategy'
    - 'Design a Project BL activity (or transform one that already exist) that could be applied to teach the importance of promoting FLOSS'
materials:
    - 'Personal computer (smartphone or tablet connected to the internet)'
    - 'Internet connexion'
    - Bearmer
    - 'Paper and pens'
    - Flipchart
    - Speakers
skills:
    Keywords:
        subs:
            'Flipped Classroom (FC)': null
            'Problem based learning (PBL)': null
            'Project Based Learning (Project BL)': null
            Open: null
            Methods: null
            Ongoing: null
            Voluntary: null
            Self-motivated: null
---

## Introduction
“_Tell me and I forget, teach me and I may remember, involve me and I learn.” Benjamin Franklin. “The greatest sign of success for a teacher... is to be able to say, 'The children are now working as if I did not exist._” Maria Montessori, Italian educator and author.

Flipped Classroom, project based learning, problem based learning ... different methods, techniques or methodologies that have something in common: the position in which students are placed is proactive, in the management of the power of their need to learn. If the students are working in groups, as in real life scenarios, learning becomes more significative. With these strategies the protagonism is placed in people with a desire to learn and the role of the teacher/trainer is that of a companion who has certain knowledge but also wants to learn with the students. 
In brief, **The Flipped Classroom (FC)** is a pedagogical model that moves the work of certain learning processes outside of the classroom and invests the class session time, along with the teacher experience, to promote and boost other processes of acquisition and knowledge practice inside the classroom.
With **Problem based learning (PBL)**, the teacher lays out a question or a problem and the students have to give an answer or find a way to solve it through a process; they, teacher and student group, settle all together a milestone in the learning process. 
In **Project Based Learning (Project BL)**, the students are also at the center of the learning process, as a protagonist capable of generating solutions in response to the different opportunities; the process is closely related to the working environment and the main outcome is a product.
The module will focus on this strategies, showing the most relevant ideas and key factors and will challenge the participants to re-think about their pedagogical practices.

This module will be divided into 4 sessions. 

### First session: Flipped Classroom
We will review the components, stages and phases. Development of resources for the Flipped Classroom with FLOSS technologies, tools and strategies.

### Second session: Problem Based Learning
Stages. Approach of relevant problems for students. Brainstorming problems to be solved. Open source resources available for the resolution of problems. Usual tools.

### Third session: Project Based Learning
Components, stages, phases. Working in real situations. The challenge as a motor of learning. Collaboration and cooperation between students. 

### Fourth session: Personal o grupal development of a learning/teaching project

## Context
The goal of this modules is to understand the theoretical framework and the key factors of the 3 methods/strategies: Flipped Classroom, Problem Based Learning and Project Based Learning.
Focus will be on learning how to design or re-design the teaching-learning strategy using open sources and tools and creating and evaluating the activities based on this methods. 

Learning outcomes: 

* Understand the basics of the Flipped Classroom (FC) strategy and be able to implement it and to create a mind-map to explain it to the others.
* Draft the structure of a new flipped classroom module (or transform a classic module) to teach/learn a topic, using open sources and tools.
* Understand the basics of the Problem Based Learning strategy and be able to explain them to the students and colleagues.
* Design a PBL activity (or transform one that already exist) that could be applied to teach the use of some open software tools. 
* Understand the basics of Project Based Learning strategy
* Design a Project BL activity (or transform one that already exist) that could be applied to teach the importance of promoting FLOSS.