---
title: 'Praktijkgemeenschap (Commons + collaboratief beheer)'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Community of practice (Commons + collaborative management)'
lang_id: 'Community of practice (Commons + collaborative management)'
lang_title: nl
length: '10 hours'
objectives:
    - 'practically demonstrate and engage learners on exploring the good practices of managing CoP'
    - 'practically demonstrate and engage learners on exploring tecognise how CoPs is present in daily actions'
    - 'get to know the tools that CoPs use and the commons management practices linked to them'
materials:
    - 'Personal computer (smartph0one or tablet connected to the internet)'
    - 'Internet connexion'
    - Bearmer
    - 'Paper and pens'
    - Flipchart
    - 'Slidewiki Academy'
skills:
    Keywords:
        subs:
            'FLOSS mouvement': null
            'FLOSS culture': null
            'Communities of Practice': null
            Commons: null
            'Collaborative management': null
            'Civic empowerment': null
            'Community engagement': null
            'FLOSS tools and skills': null
---

