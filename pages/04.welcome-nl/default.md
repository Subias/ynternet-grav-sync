---
title: 'Welkom bij de OPEN-AE Academy!'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Welcome to the OPENAE Academy'
lang_id: 'Welcome to the OPENAE Academy'
lang_title: nl
---

De opleidingsprogramma's zijn het fundament van de [OPEN - AE](https://open-ae.eu/open-ae-curriculum/) Toolkit (Intellectual Output 2). Deze zijn ontwikkeld rekening houdend met zowel bureau- als veldonderzoek naar goede praktijkvoorbeelden met betrekking tot het onderwijs van open-source technologieën in elk partnerland/regio (Intellectual Output 1). De modules werden ontwikkeld op basis van de unieke situatie en de behoeften van elk land betreffend de FLOSS-cultuur.

Deze opleidingsprogramma's zijn bedoeld om de trainers en de leerlingen te begeleiden tijdens de trainingsperiode, zowel in de cursussen als tijdens de online oefeningen. In elk deelnemend land zijn de partners verantwoordelijk voor de organisatie van groepen met een minimum aantal van 8 en een maximum van 10 deelnemers. Deze zijn geselecteerd uit e-facilitators en trainers op het gebied van de niet-formele volwasseneneducatie.

Het leermateriaal (presentaties en andere) wordt online opgeslagen en gedigitaliseerd op een open samenwerkingsplatform genaamd [SlideWiki](https://slidewiki.org/deck/127952-2/open-ae-_-academy-slidewiki/deck/127952-2?language=en). Dit open-source en open-toegankelijke platform maakt gebruik van crowdsourcingmethoden om het schrijven, delen, hergebruiken en verwerken van open cursusmateriaal te ondersteunen. 

### Onze motivatie

Steeds vaker zien we digitale vaardigheden en competenties gekoppeld aan propriëtaire software oplossingen. Hoewel FLOSS-technologieën bedoeld zijn om open te zijn en vrij toegankelijk, hadden de meeste FLOSS-gebruikers al vooraf een aantal competenties op het gebied van licenties en eigendom bij het nemen van de beslissing om naar FLOSS oplossingen over te stappen. Nieuwe gebruikers met weinig vaardigheden worden vaak geïntimideerd of zijn onzeker over hun eigen capaciteiten om FLOSS-technologieën te gebruiken en kiezen er dus vaker voor om propriëtaire opties te gebruiken omdat deze meer geassocieerd zijn met vaardigheden. Het OPEN-AE project heeft als doel deze kloof te overbruggen en praktijken en hulpmiddelen aan te bieden om een open cultuur en vrije software meer toegankelijk te maken voor nieuwe gebruikers.