---
title: 'Programació amb enes obertes: Scratch'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Open coding with Scratch'
lang_id: 'Open coding with Scratch'
lang_title: ct
length: '10 horas'
objectives:
    - 'Codificar en Scratch per desenvolupar històries interactives, jocs i animació'
    - 'Utilitzar Scratch per treballar en col·laboració (compartir un projecte des de l''editor en línia d''Scratch, veure i barrejar el projecte d''una altra persona usuària)'
    - 'Presentar als participants la filosofia Scratch i aprendre a codificar en Scratch'
    - 'Compartir projectes propis i usar projectes d''altres usuaris/àries'
materials:
    - 'Ordinador personal'
    - 'Conexió a internet'
    - 'Programa Scratch'
skills:
    'Paraules clau':
        subs:
            Scratch: null
            'Codi obert': null
            'Filosofia de Scratch': null
            'Processament seqüencial': null
            'Bloc de codi': null
            Blocs: null
            Projectes: null
            Etapa: null
            'Rutines - scripts': null
---

Scratch és un llenguatge de programació gratuït i una comunitat en línia desenvolupada pel MIT que es pot utilitzar per crear jocs, animacions, cançons i compartir-les en línia.

Scratch és utilitzat per persones de totes les edats en una àmplia varietat d'entorns. La capacitat de codificar programes d'ordinador és una part important de l'alfabetització en la societat actual. Quan les persones aprenen a codificar en Scratch, aprenen estratègies importants per resoldre problemes, dissenyar projectes i comunicar idees.

Les estadístiques al lloc web oficial mostren més de 40 milions de projectes compartits per més de 40 milions d'usuaris i gairebé 40 milions de visites mensuals a la pàgina web.

Aquest mòdul es dividirà en 3 sessions:
### Primera sessió: Filosofia Scratch: "Imagina, programa, comparteix"
La filosofia de Scratch fomenta l'intercanvi, la reutilització i la combinació de codi. Aquesta sessió proporcionarà una visió general de la filosofia Scratch "Imagina, programa, comparteix".
Els/les usuaris/àries poden crear els seus propis projectes o reutilitzar el projecte d'una altra persona. Els projectes creats i remesclats amb Scratch tenen llicència de Creative Commons Reconeixement-Compartir Igual (Attribution-Share Alike License).
La sessió té com a objectiu obtenir una major comprensió del rerefons i principis d'Scratch per crear, programar i compartir material.
A la fi de la sessió, els participants podran examinar l'ús i els avantatges de la Filosofia Scratch en l'educació d'adults.
### Segona sessió: codificació oberta amb la programació visual gratuïta Scratch
Quan les persones aprenen a codificar en Scratch, aprenen estratègies importants per resoldre problemes, dissenyar projectes i comunicar idees. Per tant, la codificació en Scratch ajuda als usuaris a pensar de manera creativa, raonar sistemàticament i treballar en col·laboració, habilitats essencials per a la vida al segle XXI.
La segona sessió és presentar el llenguatge de programació desenvolupat pel MIT.
Els / les participants aprendran a codificar en Scratch per desenvolupar històries interactives, jocs i animació.
### Tercera sessió: l'efectivitat d'Scratch en la creació d'un entorn de programació col·laborativa
Aquest mòdul té com a objectiu il·lustrar els fonaments de la comunitat en línia.
La sessió té com a objectiu ajudar els alumnes a identificar i beneficiar-se de la comunitat en línia d'Scratch. Els / les participants aprendran com compartir un projecte des de l'Editor en línia d'Scratch, i com veure i combinar amb el projecte d'un altre / a usuari / ària, trobant a el mateix temps inspiració en altres persones usuàries d'Scratch.