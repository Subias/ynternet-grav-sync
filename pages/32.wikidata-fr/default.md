---
title: Wikidata
lang_menu_title: Wikidata
lang_id: wikidata
lang_title: fr
length: '10 heures'
objectives:
    - 'Un solide contexte de base pour comprendre les avantages de Wikidata et ses liens avec les autres projets de Wikimedia, mais aussi avec les autres bases de données disponibles sur Internet'
    - 'Le participant acquerra des compétences numériques en étant capable d''ajouter du contenu à Wikidata mais aussi en étant capable d''écrire des requêtes dans Sparql pour obtenir des réponses à des questions difficiles'
    - 'Le participant sera capable de présenter Wikidata à d''autres personnes et aura suffisamment d''expérience pour identifier comment il pourrait être utilisé dans un contexte professionnel'
    - 'Le participant comprendra mieux quels processus de qualité et quels systèmes d''évaluation favorisent la constitution de données de qualité que chaque personne et chaque machine peut utiliser'
materials:
    - 'Ordinateur personnel connecté à internet'
    - 'Connexion Internet'
    - Beamer
    - 'Papier et stylos'
    - Flipchart
    - Wikidata
skills:
    Keywords:
        subs:
            WikiData: null
            'Base de données': null
            'Graphique de connaissances': null
            Wikipédia: null
            Communauté: null
            CC0: null
            'Licences libres': null
            Collaboration: null
            SPARQL: null
---

WikiData est une plate-forme de données ouverte qui appartient à la famille des sites Web Wikimedia. Il héberge 57 millions d'articles en date de juin 2019. Wikidata est une base de connaissances libre et ouverte qui contient différents types de données (par exemple, texte, images, quantités, coordonnées, formes géographiques, dates...).
L'entité de base de Wikidata est un objet. Un article peut être une chose, un lieu, une personne, une idée ou autre chose. Ce qui est spécifique à Wikidata, c'est que l'information est stockée d'une manière structurée et rigide pour permettre à la fois aux humains et aux machines de la traiter.
Bien qu'il s'agisse aujourd'hui d'une base de connaissances assez largement utilisée par les machines, elle est encore largement méconnue du public ainsi que de ses formateurs potentiels. Le but de ce module est de fournir un cours de formation des formateurs qui leur fournira un solide aperçu de la base de données Wikidata et des opportunités liées à ce projet open source.

Ce scénario sera divisé en 4 modules :
#### Premier module : Découvrir et comprendre WikiData 
Ce module donnera un aperçu de ce qu'est Wikidata. Il couvrira son histoire et permettra aux participants de comprendre le pourquoi et le comment de son existence, pour les projets wikimédia en particulier, et pour Internet en général. Le module couvrira les bases de Wikidata, la structure des données, les éléments de vocabulaire et les caractéristiques essentielles de sa conception, mais aussi la communauté wikidata et sa gouvernance pour que les participants comprennent le fait que wikidata, aussi technique que cela semble, est une construction essentiellement sociale. Les participants exploreront Wikidata par eux-mêmes et créeront un compte.
#### Second module : Éditer WikiData
Le deuxième module aura pour but d'amener les participants à ajouter eux-mêmes du contenu à Wikidata et ainsi découvrir certains des processus fondamentaux de l'édition. Il s'intéressera également à la qualité des données et aux systèmes d'évaluation des données, et découvrira les outils les plus populaires qui permettent de combler les lacunes en profondeur et en largeur, ainsi que des outils d'édition et d'importation de masse.
#### Troisième module : Interroger WikiData
Ce module a pour but de permettre aux participants de découvrir et d'expérimenter le service de recherche pour des requêtes simples, puis de découvrir le langage de recherche sparql pour des requêtes plus complexes en profondeur grâce à une méthodologie étape par étape, augmentant ainsi progressivement la difficulté. La séance comprendra des activités pratiques. 
#### Quatrième module : Appliquer WikiData
Ce module aidera les participants à comprendre la valeur appliquée de Wikidata, en particulier pour des applications comme la visualisation de données complexes, mais aussi à identifier des applications potentielles pour Wikidata dans leur propre travail, que ce soit par la visualisation, l'intégration de Wikidata dans leur propre logiciel ou l'utilisation de Wikidata sur des projets Wikimedia. La deuxième partie de ce module leur apprendra comment se connecter avec la communauté Wikidata pour une collaboration plus efficace.