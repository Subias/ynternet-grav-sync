---
title: 'Interroger WikiData'
length: '120 min'
objectives:
    - 'examiner l''intérêt du langage de requêtes et des outils pour obtenir des réponses à des questions complexes'
    - 'motiver les participants à apprendre à faire des requêtes de base ou même des requêtes avancées'
    - 'apprendre à faire des requêtes en Sparql'
---

##1.- Requêtes (90 min) (formateur)
→ Comprendre le service de requêtes
Le service d'interrogation aura été très brièvement présenté au cours du module 2. Cette question sera examinée plus en profondeur.
→ découvrir SPARQL en profondeur et savoir faire des requêtes
Cependant, avec de nombreux publics dans des situations professionnelles, il est nécessaire de fournir un environnement séparé pour mieux comprendre les langages Sparql et comment construire des requêtes fortes.
→ Les participants créent leurs propres requêtes (dans l'ordinateur portable)
Laissez le public écrire ses propres requêtes avec l'aide du service de requêtes.
Astuces :
Rappelez-vous, pour de nombreux publics, ce sera la première fois qu'ils travailleront avec un langage de requête (SQL, SPARQL, etc.). Par conséquent, il est très important de partir des éléments les plus fondamentaux et d'augmenter progressivement la complexité de l'information. Une bonne tactique pour ce faire, est de construire plusieurs requêtes de complexité différente devant le public. Par exemple, voir Wikidata:SPARQL query service/Building a query.
Assurez-vous de montrer comment prendre un exemple de requête et de le modifier pour répondre à un besoin distinct. De nombreuses personnes n'écrivent pas immédiatement des requêtes Wikidata à partir de zéro, mais modifient plutôt des requêtes existantes. L'aide Wikidata Query est particulièrement utile pour les audiences qui modifient des requêtes.
Assurez-vous d'inclure à la fois l'écriture d'une requête guidée, où les instructeurs guident l'écriture de la requête, et une fenêtre de temps pour que l'auditoire puisse écrire ses propres requêtes. Le fait que les participants écrivent leur propre requête, renforce le processus et les compétences de modification ou de rédaction d'une requête, afin qu'ils puissent le faire en toute confiance à l'avenir.
Si l'audience est principalement composée de Wikimédiens, prenez le temps de leur montrer comment appliquer Wikidata Queries à divers outils utilisés par la communauté Wikimédia, notamment Petscan, Listeriabot et Histropedia.
Sources et lectures :
*https://www.wikidata.org/wiki/Wikidata:SPARQL_query_service/Wikidata_Query_Help
*https://www.wikidata.org/wiki/Wikidata:SPARQL_query_service/queries/examples
*https://www.wikidata.org/wiki/File:Querying_Wikidata_with_SPARQL_for_Absolute_Beginners.webm

##2.- Récapitulation et questions (15 min) (formateur)
Pour conclure le module, le formateur animera un moment de récapitulation où les participants sont encouragés à exprimer leurs questions, leurs doutes, leurs idées et leurs sentiments sur les sujets abordés.

##3.- Explications sur le travail à la maison (15 min) (formateur)
Mettre en place une campagne sur l'outil ISA et demander aux participants de participer à la campagne avant le prochain module.
https://tools.wmflabs.org/isa/campaigns

##4.- Devoirs pour préparer le module suivant : (1h)
Set up a campaign on ISA tool and ask the participants to participate to the campaign before the next module https://tools.wmflabs.org/isa/campaigns