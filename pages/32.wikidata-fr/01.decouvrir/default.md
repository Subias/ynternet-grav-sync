---
title: 'Découvrir et comprendre WikiData'
length: '180 minutes'
objectives:
    - 'donner un aperçu de ce qu''est Wikidata'
    - 'expliquer comment les données sont stockées dans le projet et pourquoi'
    - 'découvrir la communauté wikidata et quelques éléments clés de la gouvernance communautaire'
    - 'comprendre l''interface et créer un compte'
---

 ## Introduction
→ Discussion de groupe
Le formateur commencera l'introduction du module en interrogeant les participants sur leur expérience actuelle et passée avec wikidata ou tout projet wikimedia. Ils seront également invités à partager des informations sur le contexte de leur participation au module.
→ Début de la recherche
Le but serait de les aider à voir comment les Wikidata peuvent avoir un impact pratique sur leur vie quotidienne et attirer leur attention. Montrez comment Wikidata peut aider à "répondre" à des questions qui étaient auparavant très difficiles à faire dans des projets spécifiques à un domaine ou non-Wikimedia. Pensez à partager quelques exemples de questions auxquelles Wikipédia pourrait théoriquement répondre, mais qui nécessiteraient la lecture de grandes quantités de Wikipédia (ou d'autres sources) pour recueillir cette information. Adapter les exemples de questions à l'auditoire. 

Les résultats d'une ou deux requêtes seront proposés.
1. Première démo
Interrogez le public sur le maire de leur ville. Est-elle une femme ? S'ils ne savent pas, où peuvent-ils trouver l'information ? mentionner google, mais aussi des assistants personnels comme Siri
Interrogez le public sur les villes de leur pays avec des femmes maires. Où et comment peuvent-ils trouver l'information ?
Demandez-leur la liste des pays classés selon le nombre de leurs villes avec une femme maire. Où ou comment peuvent-ils trouver l'information ?
Exécutez la requête. "liste des pays classés selon le nombre de leurs villes ayant une Maire de sexe féminin" pour démontrer l'utilisation des données hébergées dans wikidata
2. Deuxième démo (destinée à un public professionnel)
Exécutez “query” la liste de recherche des articles savants avec le mot "zika" dans le titre.
Utiliser pour démontrer le lien de WikiData avec d'autres bases de données : (ici PubMed_ID)
Montrer la réutilisation par une tierce partie sur scholia : https://tools.wmflabs.org/scholia/topic/Q202864

##2.- Aperçu de Wikidata (formateur) (35 min)
→ Introduction à Wikidata (25 min)
Cette section garantit généralement que les participants seront en mesure de : 
Comprendre les objectifs de Wikidata
Comprendre comment Wikidata se rapporte à d'autres projets Wikimedia, en particulier Wikipédia.
Comprendre pourquoi ils devraient être motivés pour contribuer ou utiliser Wikidata, en particulier dans le contexte de leur propre travail.
Il comprendront les éléments suivants :

* Concepts clés : Décrire les caractéristiques de base de Wikidata (CC0 libre, collaboratif, multilingue et agnostique, interconnecté) et ses objectifs (référentiel de connaissances humaines, support des projets Wikimedia et des tiers, hub dans le web de données ouvert lié) statistiques générales sur le contenu + statistiques de contenu général + accès aux statistiques générales
* Comment Wikidata sert les projets de Wikimedia, et comment il comble d'importantes lacunes dans l'infrastructure pour la communauté. C'est-à-dire les liens interlinguistiques, ou aider à empêcher les Wikipédia Infoboxes de devenir obsolètes dans les wikis en langue locale, en particulier pour les connaissances non "locales" de la langue (c'est-à-dire un politicien d'une autre langue/culture). Ceci devrait inclure l'historique de la création de WikiData, et des exemples d'impacts sur Wikipedia et Wikimedia Commons (données structurées pour Commons).
* La communauté WikiData (WM DE, qui la gère, bref aperçu de la gouvernance)
* Pourquoi WikiData est important pour Internet (exemples : CC0 permettent à WD d'être la base de connaissances de nombreuses IA comme Siri ou Alexa ; les données des institutions culturelles et l'information civique sont plus accessibles, transparentes et neutres). 
→ Quizz et restitution (10 min) 
A few questions with multiple answers to select. Can be done in pair. After completion, collective restitution and further explanations if required.

##3.- Aperçu de Wikidata (formateur) (35 min)
→ Introduction à Wikidata (25 min)
Cette section garantit généralement que les participants seront en mesure de : 
Comprendre les objectifs de Wikidata
Comprendre comment Wikidata se rapporte à d'autres projets Wikimedia, en particulier Wikipédia.
Comprendre pourquoi ils devraient être motivés pour contribuer ou utiliser Wikidata, en particulier dans le contexte de leur propre travail.
Il comprendront les éléments suivants :

* Concepts clés : Décrire les caractéristiques de base de Wikidata (CC0 libre, collaboratif, multilingue et agnostique, interconnecté) et ses objectifs (référentiel de connaissances humaines, support des projets Wikimedia et des tiers, hub dans le web de données ouvert lié) statistiques générales sur le contenu + statistiques de contenu général + accès aux statistiques générales
* Comment Wikidata sert les projets de Wikimedia, et comment il comble d'importantes lacunes dans l'infrastructure pour la communauté. C'est-à-dire les liens interlinguistiques, ou aider à empêcher les Wikipédia Infoboxes de devenir obsolètes dans les wikis en langue locale, en particulier pour les connaissances non "locales" de la langue (c'est-à-dire un politicien d'une autre langue/culture). Ceci devrait inclure l'historique de la création de WikiData, et des exemples d'impacts sur Wikipedia et Wikimedia Commons (données structurées pour Commons).
* La communauté WikiData (WM DE, qui la gère, bref aperçu de la gouvernance)
* Pourquoi WikiData est important pour Internet (exemples : CC0 permettent à WD d'être la base de connaissances de nombreuses IA comme Siri ou Alexa ; les données des institutions culturelles et l'information civique sont plus accessibles, transparentes et neutres). 
→ Quizz et restitution (10 min) 
A few questions with multiple answers to select. Can be done in pair. After completion, collective restitution and further explanations if required.

##4.- Structure des données et comment les données sont stockées dans WikiData (formateur) (55 min)
→ Présentation de la structure des données dans WikiData (40 min) 
Le but de cette section est généralement de s'assurer que les participants seront en mesure de :
Comprendre le modèle sémantique de données de base du Resource Description Framework (RDF) et comment il se compare avec le modèle Wikidata complet.
Comprendre les différents concepts pertinents à l'édition de Wikidata et comment déterminer quel contenu appartient à ces champs, y compris : étiquette, description, énoncé, propriété, qualificatif et références.
Comprendre la relation entre les "informations lisibles par les utilisateurs" (étiquettes et descriptions) et les "données lisibles par machine" (Q#s et P#s) dans l'interface Wikidata. 
Elements vus :
* Vocabulaire (types de bases de données, identificateurs, données liées, données ouvertes)
* Les avantages et les inconvénients des données couplées
* Présentation des différents éléments de WikiData (éléments, propriétés, déclarations, identificateurs, qualificatifs, références et rang)
* Lecture des données WikiData. Comparez une entrée Wikipédia, une entrée Wikidata et une entrée Reasonator. Comprendre l'interface de WikiData 
→ Activité (sur ordinateur portable, 15 min) 
Individuellement, les participants sont invités à
* Aller à Q485989840. Qu'est-ce que c'est ? Qu'est-ce qui pourrait être confondu avec ?
* Maintenant allez à Q1492. Dans combien d'endroits l'information sur le W1492 est-elle utilisée ?
* Utilisez la barre de recherche pour trouver un élément WikiData qui vous intéresse. Explorez ses propriétés et ses références. Notez comment elle est organisée, ce qui pourrait manquer, s'il y a des sources, etc. Pensez à la façon de l'améliorer.

Asuces :
L'une des meilleures façons d'introduire le modèle de données RDF/Triple est en dehors de l'interface Wikidata. Par exemple, à partir d'énoncés qui décrivent le monde en langage (c.-à-d. Terre → point le plus élevé → Mont Everest), le transformer en énoncés avec étiquettes (c.-à-d. Terre (Q2) → point le plus élevé (P610) → Mont Everest (Q513)) et montrer les énoncés lisibles à la machine (Q2 → P610 → Q513). S'adapter à l'auditoire
Présentation du modèle de données avec un élément de qualité (de nombreuses présentations utilisent Q42 pour la blague de la culture Geek associée à l'élément, d'autres éléments de qualité peuvent être trouvés dans Showcase_items
Sources
* https://www.wikidata.org/wiki/Wikidata:In_one_page
* https://www.wikidata.org/wiki/Wikidata:Showcase_items

##5.- WikiData est construit grâce à la collaboration (formateur) (55 min)
→ Wikidata community (30 min) 
The goal of this section typically ensures that participants will be able to understand that WikiData is primarily a social construction.
* Qui ajoute du contenu aux WikiData (humains, machines gérées par des humains, autres sources)
* Qui définit les règles et comment. Les différents rôles de la communauté.
* Comportement et règles sociales.
* Règles éditoriales. Ce qui entre et ce qui ne rentre pas. Références.
* Avantages d'avoir un compte
Astuces :
Si votre public est intéressé à contribuer à Wikidata à l'échelle personnelles ou à des fins professionnelles, il est important d'expliquer comment Wikidata décide du modèle de données, des propriétés et des éléments. Par exemple, il est important d'expliquer la dynamique sociale de "n'importe qui peut créer un nombre Q, mais les nombres P sont soumis à un processus de consultation communautaire étroitement contrôlé". 
→ Premières étapes d'édition (sur ordinateur portable, 20 min) 
Demandez aux élèves de créer leur propre compte (ou planifiez à l'avance, il y a une limite à la création quotidienne d'un compte à partir de la même adresse IP). Ajouter un message sur leur page de discussion, avec un lien vers la "page du module de formation" mise en place à l'avance. Assurez-vous de collecter tous les noms d'utilisateur créés pendant le module. Suggestions aux utilisateurs :
1) créer un compte. Présentez-vous dans la page utilisateur.
2) Modifiez les préférences pour définir la langue préférée.
3) Ajouter des gadgets dans les préférences : "Recoin", "Reasonator", et "labelLister".
4) Déposer un message sur la page de formation
5) Utilisez la barre de recherche pour trouver un élément WikiData qui vous intéresse. Vérifiez son achèvement avec Recoin. 

##6.- Débriefing et questions / introduction au module suivant (formateur) (15 min)
Pour conclure le module, le formateur animera un moment de débriefing où les participants sont encouragés à exprimer leurs questions, leurs doutes, leurs idées et leurs sentiments sur les sujets discutés.
Le prochain module portera sur l'apprentissage de l'ajout de contenu à WikiData

##7.- Devoirs pour préparer le module suivant : (1h)
Avant le deuxième module, le participant sera invité à réfléchir aux entrées qu'il aimerait améliorer, à rechercher des informations, y compris des références et des sources, qui seront brièvement présentées au groupe au début du prochain module.

##Références pour le formateur :
* https://www.wikidata.org/wiki/Wikidata:Planning_a_Wikidata_workshop
* https://dashboard.wikiedu.org/training/wikidata-professional/evaluating-wikidata
* https://www.wikidata.org/wiki/Help:Contents
* https://www.wikidata.org/wiki/Wikidata:Tools
* https://www.wikidata.org/wiki/Wikidata:Wikidata_educational_resources