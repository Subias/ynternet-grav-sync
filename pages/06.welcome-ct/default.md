---
title: 'Benvinguda a la Academia Open-AE!'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Welcome to the OPENAE Academy'
lang_id: 'Welcome to the OPENAE Academy'
lang_title: ct
---

Els escenaris de formació són els elements fonamentals de la caixa d’eines [OPEN - AE](https://open-ae.eu/open-ae-curriculum/). Han estat desenvolupats tenint en compte el treball de recerca previ, incloent-hi el treball de camp, de cada una de les entitats membre del projecte. Els diferents mòduls han estat desenvolupats prenent com a base l’estat de la qüestió i les necessitats de cada país en relació a la cultura FLOSS (free/libre and open-source software).

Aquests escenaris són una guia per a persones formadores i estudiants durant el període de formació en els dos ambients, classes presencials i formació en línia. A cada país on s'implementa la formació, les organitzacions promotores seran les responsables de l'organització de la formació en grup, amb un mínim de 8 i un màxim de 10 persones participants seleccionades entre les persones que treballen com a dinamitzadores o formadores en el terreny de la formació de persones adultes.

El material d'aprenentatge (presentacions digitals i altres elements complementaris) s'allotgen i documenten de forma digital al repositori col·laboratiu [SlideWiki](https://slidewiki.org/deck/127952-2/open-ae-_-academy-slidewiki/deck/127952-2?language=en). La plataforma SlideWiki de codi i accés oberts convida a les persones a compartir els seus recursos per donar suport a l'autoria, l'intercanvi, la reutilització i la remescla de cursos oberts.

### La nostra motivació

Cada vegada més veiem habilitats i competències digitals vinculades a solucions de programari patentades. Les tecnologies FLOSS estan destinades a ser obertes i de lliure accés, i la majoria de les persones que utilitzen aquests recursos ja tenen alguns coneixements sobre les llicències de propietat intel·lectual i de el propi concepte de propietat; aquests sabers van resultar clau en el moment de prendre la decisió d'usar FLOSS. Les persones que tenen poques habilitats digitals sovint se senten intimidades o insegures amb les seves pròpies capacitats per utilitzar les tecnologies FLOSS, i per tant solen optar per usar opcions propietàries, ja que algunes marques estan més associades amb aquests perfils. El projecte OPEN-AE té com a objectiu tancar aquesta bretxa i promoure pràctiques i eines per fer que la cultura oberta i el programari lliure siguin més accessibles per a les noves usuàries.