---
title: 'La cultura FLOSS'
lang_menu_title: 'The FLOSS culture'
lang_id: 'The FLOSS culture'
lang_title: ct
length: '10 hours'
objectives:
    - 'Mapejar i explorar els conceptes principals subjacents en la cultura FLOSS'
    - 'Comprendre la interdependència d''aquesta cultura amb els Recursos Educatius Oberts (REA o, en anglès, Open Educational Resources -OERs-)'
    - 'Vincular la formació als REA'
    - 'Descobrir els arguments ètics, legals, socials, econòmics i d''impacte a favor i en contra del FLOSS'
    - 'Decidir quines plataformes / eines / serveis són més útils per a nosaltres mateixes i per a les nostres comunitats'
materials:
    - 'Ordinador personal o telèfon intel·ligent o tauleta digital'
    - 'Connexió a Internet'
    - Projector
    - 'Paper i bolígrafs'
    - Paperògrafs
skills:
    'Paraules clau':
        subs:
            FLOSS: null
            'Programari lliure i de codi obert': null
            'Economia política': null
            Copyleft: null
            'Moviment FLOSS': null
            'Marc polític': null
            'Cultura FLOSS': null
            Ciutadania: null
image: open_AE1.png
---

![https://proto4.ynternet.org/#overview](open_AE1.png)
# Els fonaments de FLOSS: motivacions, ideologia i pràctica.
Tot i que les pràctiques i els peatges del FLOSS no són un fenomen nou, molts aspectes d'aquest domini encara semblen desconeguts. Aquest escenari de formació proporciona pràctiques i eines concretes per formar en la cultura FLOSS, il·lustrant els fonaments que estan darrere de el moviment de codi obert i lliure, i l'estat de la qüestió de l'FLOSS a Europa. El nostre objectiu és ajudar l'alumnat a considerar i utilitzar el programari de codi obert gratuït / lliure com a eina per al desenvolupament social i econòmic.

En general, l'escenari formatiu proporciona un marc històric i polític de les tecnologies FLOSS. Està dissenyat per a promoure l'ús de l'FLOSS en l'educació de persones adultes i estimular la participació conscient en la cultura lliure i oberta, com a part del que anomenem "ciutadania". Aquesta visió particular de la ciutadania es refereix a una forma més àmplia de ser i actuar com a ciutadanes d'Internet, particularment enfocada a comprendre i valorar els béns comuns, en comunicar-se de forma intencional i en adoptar un enfocament actiu cap a les pràctiques i les tecnologies en la xarxa d'Internet.

![](floss.jpg)

##Context
L'objectiu de la sessió és demostrar de forma pràctica i atractiva que:
El FLOSS promou la col·laboració i les contribucions de diferents agents en els processos de producció i innovació de programari.
El FLOSS té un gran potencial socioeconòmic a través d'estàndards oberts, evitant el bloqueig i permetent solucions flexibles. L'alumnat podrà descriure els arguments ètics, legals, socials, econòmics i d'impacte a favor i en contra de l'FLOSS.

Després de decidir quines plataformes / eines / serveis són més útils per a ells i la seva comunitat, la persona participant desenvoluparà un perfil personal per mostrar les seves fites i resultats. Les sessions proporcionaran un marc històric i polític de les tecnologies FLOSS i promouran l'ús de FLOSS en l'educació de persones adultes. Estimularem la participació intencional en la cultura lliure i oberta com a part de l'concepte de ciutadania a través de plataformes i eines específiques.

Aquest mòdul es dividirà en 3 sessions:

### Primera sessió: Els fonaments de l'FLOSS: motivacions, ideologia i pràctica
Amb aquesta sessió les persones participants tindran l'oportunitat de mapejar i explorar els conceptes principals que estan darrere de la cultura del FLOSS. També s'examinaran els diferents objectius dins el moviment FLOSS. Durant la sessió, es mostraran diverses comprensions d'aquests conceptes, metes i resultats. Els vincularem a la formació de Recursos Educatius Oberts (REA).

### Segona sessió: FLOSS està a tot arreu
La segona sessió tindrà com a objectiu establir l'estat de la qüestió al voltant del FLOSS. Es presentaran i compararan les pràctiques del FLOSS en els països de la UE. Les participants realitzaran una anàlisi crítica de el marc de polítiques del FLOSS a la UE i dissenyaran una política inicial per a la seva pròpia organització.

### Tercera sessió: FLOSS com aprenentatge col·lectiu, FLOSS en l’aprenentatge col·lectiu
Aquesta sessió permetrà una anàlisi crítica de la importància del FLOSS i els REA en el camp de la formació no formal. Examinarem l'ús i els avantatges de les tecnologies digitals obertes a l'educació, mentre fomentem el compromís actiu i creatiu de les estudiants a través de les tecnologies FLOSS, en els plànols teòric i pràctic.