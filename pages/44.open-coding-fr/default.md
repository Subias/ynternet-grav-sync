---
title: 'Codage ouvert avec scratch'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Open coding with Scratch'
lang_id: 'Open coding with Scratch'
lang_title: fr
length: '10 heures'
objectives:
    - 'Coder dans Scratch pour développer des histoires, des jeux et des animations interactifs'
    - 'Utiliser Scratch pour travailler en collaboration (partager un projet à partir de Scratch Online Editor, afficher et remixer le projet d''un autre utilisateur)'
    - 'Initier les participants à la philosophie Scratch et apprendre à coder dans Scratch'
    - 'Partager ses propres projets et utiliser les projets d''autres utilisateurs'
materials:
    - 'Ordinateur personnel'
    - 'Connexion Internet'
    - 'Programme Scratch'
skills:
    'Mots clés':
        subs:
            Scratch: null
            'codage ouvert': null
            'philosophie du scratch': null
            'traitement séquentiel': null
            'bloc de code': null
            blocs: null
            projets: null
            étape: null
            scripts: null
---

Scratch est un langage de programmation gratuit et une communauté en ligne développé par le MIT qui peut être utilisé pour créer des jeux, des animations, des chansons et les partager en ligne.

Scratch est utilisé par des personnes de tous âges dans une grande variété de paramètres. La capacité de coder des programmes informatiques est un élément important de l’alphabétisation dans la société actuelle. Lorsque les gens apprennent à coder dans Scratch, ils apprennent des stratégies importantes pour résoudre des problèmes, concevoir des projets et communiquer des idées.

Les statistiques sur le site officiel de la langue montrent plus de 40 millions de projets partagés par plus de 40 millions d'utilisateurs et près de 40 millions de visites mensuelles sur le site.

Ce module sera divisé en 3 sessions :
### Première séance : Philosophie Scratch : «Imaginez, programmez, partagez»
La philosophie de Scratch encourage le partage, la réutilisation et la combinaison de code. Cette session donnera un aperçu de la philosophie de Scratch "Imaginez, programmez, partagez". Les utilisateurs peuvent créer leurs propres projets ou réutiliser le projet de quelqu'un d'autre. Les projets créés et remixés avec Scratch sont sous licence Creative Commons Attribution-Share Alike License. La session vise à mieux comprendre le contexte et les principes de Scratch pour la création, la programmation et le partage de matériel. À la fin de la session, les participants seront en mesure d'examiner l'utilisation et les avantages de la philosophie Scratch dans l'éducation des adultes.
### Deuxième séance : Codage ouvert avec le programmation visuelle gratuite Scratch
Lorsque les gens apprennent à coder dans Scratch, ils apprennent des stratégies importantes pour résoudre des problèmes, concevoir des projets et communiquer des idées au 21e siècle. La deuxième session vise à introduire le langage de programmation développé par le MIT. apprendra à coder dans Scratch pour développer des histoires interactives, des jeux et des animations.
### Troisième séance : L'efficacité de Scratch dans la création d'un environnement de programmation collaborative
Ce module vise à illustrer les principes fondamentaux de la communauté en ligne La session vise à aider les apprenants à identifier et à tirer profit de la communauté en ligne Scratch. Les participants apprendront comment partager un projet à partir de Scratch Online Editor, afficher et remixer le projet d'un autre utilisateur en s'inspirant des autres en même temps.