---
title: 'Storytelling digitale per il potenziamento degli studenti'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Digital Storytelling: create and share stories using open digital technologies'
lang_id: 'Digital Storytelling: create and share stories using open digital technologies'
lang_title: it
length: '12 ore'
objectives:
    - 'Utilizzare ed integrare media diversi (testo, immagini, suoni, video) in un ambiente online'
    - 'Utilizzare e le tecnologie digitali per creare e condividere storie'
    - 'Produrre e condividere una storia digitale per stimolare la personalizzazione e l’impegno attivo degli studenti'
    - 'Esplorare metodi di collaborazione grazie ad attività in piccoli gruppi per imparare gli uni dagli altri'
materials:
    - 'Personal computer'
    - 'Connessione internet'
    - 'Carte Dixit'
skills:
    'Parole chiave':
        subs:
            Storytellins: null
            'Storia digitale': null
            Storyboard: null
            'Storytelling circle': null
            Multimedia: null
            'Storytelling interattivo': null
            'Software open source': null
---

Lo storytelling digitale (DS) è un modo semplice e accessibile di raccontare una storia attraverso un cortometraggio. Lo storytelling digitale è un mestiere che utilizza gli strumenti della tecnologia digitale per raccontare storie sulla nostra vita.
Esso è una parte essenziale del sistema educativo del mondo moderno, uno strumento di apprendimento attivo che fornisce ai lettori capacità e competenze necessarie per creare, gestire e condividere storie digitali open source.
Per creare una storia digitale, gli studenti devono seguire un processo di storytelling digitale:

![](storytelling1.png)

Quando creano una storia digitale, gli autori sono tenuti a produrre un video modificando immagini, suoni, musica, testo e voce. In questo modo, molteplici competenze possono essere sviluppate e praticate attraverso la realizzazione di storie digitali. Utilizzando la metodologia DS, gli studenti hanno l'opportunità di creare e condividere storie utilizzando tecnologie digitali aperte (ad esempio, software di editing video open source; software di editing audio open source, ecc.) 

Lo storytelling digitale permette agli studenti di esprimere le proprie esperienze personali, organizzarle in una storia (Introspezione e utilizzo di tecniche digitali per raccontare storie personali). Oltre ad essere un potente strumento tecnologico per migliorare la competenza digitale degli studenti nell'uso di un software aperto che combina vari multimedia (testo, immagini fisse, audio, video e web publishing), il DS può essere anche uno strumento efficace che fornisce agli studenti le quattro C del 21° secolo (Pensiero critico; Creatività; Collaborazione; Comunicazione).

## Contesto
L’obiettivo di questo corso formativo è quello di avvicinare i partecipanti al DS e far loro apprendere come utilizzarlo come metodo per raccontare storie, utilizzando tecnologie digitali aperte.  
Alla fine del corso i partecipanti saranno capaci di creare una storyboard della propria storia digitale, creare e raccogliere materiale utile per la stessa (immagini, voce, musica, suoni, testi, titoli) attraverso tecnologie aperte digitali.
I corsi hanno anche lo scopo di far acquisire una maggiore comprensione degli orientamenti etici, copyright, licenza Creative Commons ed autorizzazioni.

Questo modulo sarà diviso in 2 sessioni:
### Prima sessione: Un’introduzione allo storytelling digitale: background e vantaggi  
Il DS è un approccio multimediale alla condivisione di una narrazione basata sull'integrazione di fotografia, suono, testo, musica per condividere la storia digitale dell’autore in formato digital-mediatico. Lo storytelling digitale è un metodo che ci aiuta a focalizzarci su noi stessi e sulla nostra storia. Ci aiuta a formulare la nostra storia, a drammatizzarla, ad illustrarla con fotografie e a raccontarla ad altri.
La sessione ha lo scopo di far acquisire una maggiore comprensione del background del DS e dei principi per creare, raccogliere e condividere storie digitali utilizzando tecnologie digitali aperte. 
La sessione si concentra anche sul potenziale impatto della metodologia DS. Infatti, far combinare agli studenti molteplici elementi comunicativi in una struttura narrativa per creare e condividere la loro storia digitale ha alcuni vantaggi chiave:
* migliora la competenza digitale degli studenti, aiutandoli a diventare abili con software audio e video open source
* offre agli studenti un nuovo modo di collaborare

Al termine della sessione, i partecipanti saranno in grado di riconoscere cosa è una storia digitale, identificando le caratteristiche e i passaggi principali del DS. I partecipanti saranno anche in grado di identificare i vantaggi del DS nell'educazione degli adulti, con particolare attenzione allo sviluppo delle competenze del ventunesimo secolo. Infatti, quando le persone imparano a creare e condividere la loro storia utilizzando la metodologia del DS, sviluppano anche le competenze del XXI secolo di cui i cittadini di oggi hanno bisogno per avere successo nella loro carriera nell'era dell’informazione.
### Seconda sessione: Crea la tua storia digitale con le tecnologie digitali aperte!
La seconda sessione mira a fornire le competenze per la produzione di una storia digitale, che include storyboard, materiale, editing. 
I partecipanti impareranno a creare uno storyboard della loro storia, utilizzando un software aperto che combina una varietà di multimedia (testi, immagini statiche, audio, video e web publishing) e riconosceranno il materiale con licenza libera sul web.