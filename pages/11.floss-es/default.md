---
title: 'La cultura FLOSS'
lang_menu_title: 'The FLOSS culture'
lang_id: 'The FLOSS culture'
lang_title: es
length: '10 horas'
objectives:
    - 'Mapear y explorar los conceptos principales que subyacen en la cultura FLOSS'
    - 'Comprender la interdependencia de esta cultura con los Recursos Educativos Abiertos (REA o, en inglés, Open Educational Resources -OERs-)'
    - 'Vincular la formación a los REA'
    - 'Descubrir los argumentos éticos, legales, sociales, económicos y de impacto a favor y en contra del FLOSS'
    - 'Decidir qué plataformas / herramientas / servicios son más útiles para nosotras mismas y para nuestras comunidades'
materials:
    - 'Ordenador personal o teléfono inteligente o tableta digital'
    - 'Conexión a Internet'
    - Proyector
    - 'Papel y boligrafos'
    - Rotafolio
skills:
    'Palabras clave':
        subs:
            FLOSS: null
            'Software libre y de código abierto': null
            'Economía política': null
            Copyleft: null
            'Movimiento FLOSS': null
            'Marco político': null
            'Cultura FLOSS': null
            Ciudadanía: null
image: open_AE1.png
---

![https://proto4.ynternet.org/#overview](open_AE1.png)
# Los fundamentos de FLOSS: motivaciones, ideología y práctica
Aunque las prácticas y los peajes de FLOSS no son un fenómeno nuevo, muchos aspectos de este dominio aún parecen desconocidos. Este escenario de formación proporciona prácticas y herramientas concretas para formar en la cultura FLOSS, ilustrando los fundamentos que están detrás del movimiento de código abierto y libre, y el estado de la cuestión del FLOSS en Europa. Nuestro objetivo es ayudar al alumnado a considerar y utilizar el software de código abierto gratuito / libre como herramienta para el desarrollo social y económico.

En general, el escenario formativo proporciona un marco histórico y político de las tecnologías FLOSS. Está diseñado para promover el uso del FLOSS en la educación de personas adultas y estimular la participación consciente en la cultura libre y abierta, como parte de lo que llamamos "ciudadanía". Esta visión particular de la ciudadanía se refiere a una forma más amplia de ser y actuar como ciudadanas de Internet, particularmente enfocada en comprender y valorar los bienes comunes, en comunicarse de forma intencional y en adoptar un enfoque activo hacia las prácticas y las tecnologías en la red de Internet.

![](floss.jpg)

##Contexto
El objetivo de la sesión es demostrar de forma práctica y atractiva que: 
El FLOSS promueve la colaboración y las contribuciones de diferentes agentes en los procesos de producción e innovación de software. El FLOSS tiene un gran potencial socioeconómico a través de estándares abiertos, evitando el bloqueo y permitiendo soluciones flexibles. El alumnado podrá describir los argumentos éticos, legales, sociales, económicos y de impacto a favor y en contra del FLOSS.

Después de decidir qué plataformas / herramientas / servicios son más útiles para ellos y su comunidad, la persona participante desarrollará un perfil personal para mostrar sus metas y resultados. Las sesiones proporcionarán un marco histórico y político de las tecnologías FLOSS y promoverán el uso de FLOSS en la educación de personas adultas. Estimularemos la participación intencional en la cultura libre y abierta como parte del concepto de ciudadanía a través de plataformas y herramientas específicas.

Este módulo se dividirá en 3 sesiones:

### Primera sesión: Los fundamentos del FLOSS: motivaciones, ideología y práctica
En esta sesión las personas participantes tendrán la oportunidad de mapear y explorar los conceptos principales que están detrás de la cultura del FLOSS. También se examinarán los diferentes objetivos dentro del movimiento FLOSS. Durante la sesión, se mostrarán diversas comprensiones de estos conceptos, metas y resultados. Los vincularemos a la formación de Recursos Educativos Abiertos (REA).

### Segunda sesión: FLOSS está en todas partes
La segunda sesión tendrá como objetivo establecer el estado de la cuestión en torno al FLOSS. Se presentarán y compararán las prácticas de FLOSS en los países de la UE. Las participantes realizarán un análisis crítico del marco de políticas de FLOSS en la UE y diseñarán una política inicial para su propia organización.

### Tercera sesión: FLOSS como aprendizaje colectivo, FLOSS en aprendizaje colectivo
Esta sesión permitirá un análisis crítico de la importancia del FLOSS y REA en el campo de la capacitación no formal. Examinaremos el uso y las ventajas de las tecnologías digitales abiertas en la educación, mientras fomentamos el compromiso activo y creativo de las estudiantes a través de las tecnologías FLOSS, en los planos teórico y práctico.