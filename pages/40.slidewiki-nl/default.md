---
title: SlideWiki
lang_menu_title: SlideWiki
lang_id: slidewiki
lang_title: nl
length: '10 hours'
objectives:
    - 'Het theoretische kader begrijpen rond webgebaseerde toepassingen voor het beheer van educatieve inhoud'
    - 'Het gebruik van de Slidewiki webapplicatie in het Open Volwassenenonderwijs bevorderen'
    - 'Het bevorderen van het (mede)creëren en delen van educatieve inhoud'
    - 'Laten zien hoe je open web-based OpenCourseWare-auteurssystemen kunt gebruiken, zoals SlideWiki'
materials:
    - 'PC met internetverbinding'
    - Internetverbinding
    - Projector
    - 'Papier en schrijfgerief'
    - Flipchart
    - 'Slidewiki Academy'
skills:
    Sleutelwoorden:
        subs:
            Wikipedia: null
            Gemeenschap: null
            CC0: null
            'Gratis licenties': null
            Samenwerking: null
            SlideWiki: null
            Presentaties: null
            'Libre en open software': null
            FLOSS-cultuur: null
            ' Online samenwerking': null
---

SlideWiki is een [OpenCourseWare](https://en.wikipedia.org/wiki/OpenCourseWare)-auteurs systeem dat gebaseerd is op het open web. Het ondersteunt de creatie en het beheer van leerinhouden en instrumenten voor samenwerking op het gebied van hulpmiddelen, vertaling, communicatie, evaluatie en beoordeling.

SlideWiki is een webapplicatie die de samenwerking rond educatieve inhoud vergemakkelijkt. Met SlideWiki kunnen gebruikers dia's maken, samenwerken en dia's organiseren in presentaties. Presentaties kunnen hiërarchisch worden georganiseerd om ze te structureren volgens hun inhoud. Het is ook mogelijk om een grootschalig samenwerkingsverband op te zetten (ook wel crowdsourcing genoemd) rond educatieve content. Dia's, presentaties, diagrammen, quizzen, etc. worden geüpload door gebruikers en vervolgens hergebruikt door anderen. Deze inhoud wordt voornamelijk gecreëerd door docenten, leerkrachten, opleiders en deskundigen, hetzij individueel, hetzij in zeer kleine groepen. De resulterende inhoud kan online worden gedeeld (bijvoorbeeld met behulp van Slideshare, OpenStudy, Google Docs).

SlideWiki is een platform, waar potentieel grote gemeenschappen van leraren en academici de kracht hebben om rijke educatieve inhoud te creëren op een collaboratieve en open manier. Met SlideWiki kan inhoud worden verspreid en kunnen studenten en onderzoekers sneller worden opgeleid, omdat de last van het creëren en structureren van een inhoudsgebied kan worden verdeeld over een grote gemeenschap. Specialisten in individuele aspecten van het nieuwe curriculum of inhoudsgebied kunnen zich richten op het creëren van educatieve inhoud in hun specifieke vakgebied en toch kan deze inhoud gemakkelijk worden geïntegreerd met andere inhoud, worden geherstructureerd en worden hergebruikt. Een bijzonder aspect aan SlideWiki is dat het een meertalig platform is. Aangezien alle inhoudelijke versies een duidelijke structuur heeft, is het gemakkelijk om de inhoud semi-automatisch te vertalen en om veranderingen in verschillende meertalige versies van dezelfde inhoud bij te houden.

Deze module zal worden opgedeeld in 3 sessies:

### Sessie 1: SlideWiki ontdekken en begrijpen
Het doel van deze sessie is om de deelnemers kennis te laten maken met het SlideWiki platform tool en ruimte te laten voor discussie over aspecten die te maken hebben met het creëren van inhoud, het gebruik van inhoud door anderen en het auteurschap, maar ook over de beschikbaarheid/toegankelijkheid van informatie in onderwijscontexten. Tijdens deze sessie leren de deelnemers hoe ze hun eigen presentaties kunnen maken en hoe ze inhoud kunnen importeren in het platform.

### Sessie 2: Ervaring met SlideWiki: SlideWiki creëren en samenwerken
Tijdens deze sessie zal de focus liggen op het bewerken van de presentaties die we importeren uit onze persoonlijke collecties, maar ook op de presentaties die we kunnen gebruiken vanuit de community op SlideWiki. Het proces van redactie en co-creatie biedt nieuwe mogelijkheden die de deelnemers tijdens deze sessie kunnen verkennen.

### Sessie 3: Het maken van online cursussen met SlideWiki en georganiseerde leerinhouden
Tijdens deze laatste sessie zullen de deelnemers leren hoe ze onderwijscurricula kunnen maken en online lessen over een gekozen onderwerp kunnen organiseren. Deelnemers leren hoe het SlideWiki-platform mogelijkheden biedt om online leren voor volwassenen te bevorderen.