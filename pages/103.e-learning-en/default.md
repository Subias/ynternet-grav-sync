---
title: 'E-Learning with FLOSS tools'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'E-Learning with FLOSS tools'
lang_id: 'E-Learning with FLOSS tools'
lang_title: en
length: '10 hours'
objectives:
    - 'to recognise the advantages of E-Learning in education'
    - 'to explore the main Open Source Learning Management System: MOODLE'
    - 'to discover and benefit from the Moodle community'
materials:
    - 'Personal computer'
    - 'Internet connexion'
    - Moodle
skills:
    Keywords:
        subs:
            'Lifelong learning': null
            'Learning Management System (LMS)': null
            'Asynchronous Learning': null
            Moodle: null
            MOOC: null
---

_E-Learning exploits interactive technologies and communication sys-tems to improve the learning experience. It has the potential to transform the way we teach and learn across the board. It can raise standards, and widen participation in lifelong learning. It cannot replace teachers and lecturers, but alongside existing methods it can enhance the quality and reach of their teaching, and reduce the time spent on administration. It can enable every learner to achieve his or her potential, and help to build an educational workforce empowered to change. It makes possible a truly ambitious education system for a future learning society._                                                                                                                 **Towards a Unified e-Learning Strategy, The DfES e-Learning Strategy Unit, 2003**

The training scenario provides an introduction to the E-Learning meth-odologies and tools. As the E-learning tools redesigned the educational environment, the sessions are designed to give an overview of the eLearning background, key features and methods. 

The training will go through some advantages of online learning. In par-ticular, it will allow modern learners to explore the potential of E-Learning as a tool for fostering online collaboration. In fact, eLearning can an effective tool for both learners and educators, especially for promoting online collaboration (E-Learning platforms).
Participants will experience the Moodle learning platform to work and learn together in forums, wikis, glossaries, database activities, and much more. The training will provide learners with a critical understanding of the learning process.

This module will be divided into 2 sessions:
###First session: E-Learning basics – Background and Evolution
E-Learning is the employment of technology to aid and enhance learning. The purpose of E-Learning is to allow people to learn without physically attending a traditional educational setting. 
To better understand how E-Learning benefits both educators and learners today, it’s helpful to look at its past. The term 'E-Learning' has only been in existence since 1999, when was first utilized at a CBT systems seminar. The educational technology expert Elliott Maisie coined the term “E-Learning” in 1999, marking the first time the phrase was used professionally. However, the principles behind e-learning have been well documented throughout history, and there is even evidence which suggests that early forms of e-learning existed as far back as the 19th century. Today, E-Learning is more popular than ever. 

The session also gives and overview of some of the technologies involved in E-Learning, such as content authoring, LMS and TMS.
In the 'Information Age', lifelong learning is seen as key to the continued success of modern society. E-Learning is considered by many as the best viable solution to the problem of delivering the resources required to facilitate lifelong learning.
What factors have facilitated eLearning in becoming the most popular way to deliver training today? Why use E-Learning? This session gives an overview of the extent potential and advantages of the E-Learning.  
###Second session: Ready to go E-learning! Using the E-Learning to promote Open Education
This session will provide a broad overview of what Moodle can do. It will explore the use and benefits of Moodle, one of the most popular open-source learning management system (LMS). The training will provide the basic information necessary for learners to get started with the learning platform and benefit from the online getting support from the users all around the world and sharing ideas.

The evolutionary changes in educational technology have changed the concept of teaching and learning as we know it. 
There are certainly many benefits to teaching/learning in an online environment. E-Learning can make learning simpler, easier, and more effective. Besides serving different learning styles, E-Learning is also a way to provide quick delivery of lessons. It is also a relevant tool in terms of collaboration and community building. In fact, E-Learning doesn’t have to be an individual journey. Through features like forums and live tutorials, users can have access to others in the learning community. Engagement with other users promotes collaboration and team culture, which has benefits beyond the training environment.

At the end of the sessions, participants will be able to use and benefit from Moodle, one of the main Open Source Learning Platform which delivers a powerful set of learner-centric tools and collaborative learning environments that empower both teaching and learning.