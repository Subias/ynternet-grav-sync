---
title: 'De opkomst van copyleft- en FLOSS-licenties'
lang_menu_title: Copyleft
lang_id: Copyleft
lang_title: nl
length: '10 hours'
materials:
    - 'Persoonlijke computer (smartphone of tablet verbonden met het internet)'
    - Internetverbinding
    - Projector
    - 'Papier en pennen'
    - Flipchart
    - 'Slidewiki Academie'
objectives:
    - 'Copyright en de auteursrechten zijn het doelwit geweest van kritiek door de ontwikkeling van technologieën die het kopiëren en delen van informatie vergemakkelijken'
    - 'De doelstellingen van copyleft en FLOSS staan ten dienste van de onderwijsgemeenschap en -inhoud'
    - 'Het Creative Commons-kader, gericht op auteurs die hun werk willen delen en het gemeenschappelijk erfgoed willen verrijken (de Commons), kan worden geactiveerd'
skills:
    Sleutelwoorden:
        subs:
            FLOSS-licentie: null
            'Free/Libre Open-Source Software': null
            'politieke economie': null
            Copyleft: null
            FLOSS-beweging: null
            vergunningskader: null
            FLOSS-cultuur: null
            Netizenship: null
            GPL: null
            EUPL: null
---

"Alle rechten voorbehouden", "Handelsmerk", "Patent", "Kopieren of reproductie beperkt tot strikt privé gebruik" ... Als we het over "cultuur" hebben, worden we teruggebracht naar het concept van toe-eigening (eigendom), in dit geval intellectueel. De gemeenschappelijke gedachte in de vrije cultuur is echter dat ideeën aan iedereen toebehoren en net als lucht en water een basisbehoeften zijn. De Copyleft cultuur, ook wel vrije cultuur genoemd, is ontstaan uit de wereld van de software en de vele bijdragers die één ding gemeen hadden: hun gevoel voor het collectieve goed.

De uitdrukking "vrije software" verwijst naar de vrijheid, niet naar de prijs. Om het concept te begrijpen moet men denken aan "vrijheid van meningsuiting", niet aan "vrije toegang". Geïnspireerd door deze innovatieve mentaliteit met betrekking tot het beheer van creatieve productie, hebben andere initiatieven geleidelijk aan copyleft buiten de wereld van de software gebracht.

# Context
Het doel van deze sessie is om leerlingen praktisch te demonstreren en te betrekken bij de manier waarop: 
- Het copyright en de rechten van de auteur het doelwit zijn geweest van kritiek, met de ontwikkeling van technologieën die het kopiëren en delen van informatie vergemakkelijken.
- De doelstellingen van copyleft en FLOSS staan ten dienste van de onderwijsgemeenschap en inhoud. 
- Het Creative Commons-kader, gericht op auteurs die hun werk willen delen en het gemeenschappelijke erfgoed willen verrijken (de Commons), kan worden geactiveerd.

De leerling zal de ethische, juridische, sociale, economische en impact argumenten voor en tegen FLOSS-licenties kunnen beschrijven. Nadat hij heeft besloten welke licentie/platforms/tools/diensten het meest nuttig zijn voor zichzelf en hun gemeenschap, zal de onderzoeker een persoonlijk profiel ontwikkelen om zijn onderzoeksprofiel en -output duidelijk te maken.

De sessies zullen een historisch en beleidsmatig kader van FLOSS-vergunningen bieden en hun in het volwassenenonderwijs promoten. Het zal de bewuste deelname aan de vrije en open cultuur als onderdeel van het Netizenship stimuleren door middel van specifieke platformen en instrumenten.

Deze module zal worden opgedeeld in 3 sessies:
## Eerste sessie: Copywhat ? 
Inzicht in en gebruik van FLOSS-licenties. Deze sessie zal een historisch en beleidskader bieden voor copyleft- en FLOSS-licenties. Het begrijpen en kunnen gebruiken van FLOSS-licenties als een instrument voor de ontwikkeling van eigendoms-, gemeenschaps- en auteursrechtvragen rond de maatschappij en technologie. De deelnemers zullen in staat zijn om auteurs- en FLOSS-licenties te identificeren en hun doelstellingen te herkennen.

### Tweede sessie: FLOSS-licenties
In het echte leven zijn scenario's overal aanwezig. De tweede sessie is bedoeld om de context rond FLOSS-licenties te bepalen met concrete casestudy's. De deelnemers zullen een kritische analyse maken van het gebruik van FLOSS-licenties in verschillende entiteiten (EU en daarbuiten) en een eerste licentiebeleid voor hun eigen organisatie ontwerpen.

### Derde sessie: FLOSS-licenties voor open onderwijs, kunst en collectief leren
Deze sessie maakt een kritische analyse mogelijk van het belang van FLOSS-licenties op het gebied van niet-formele opleidingen. Er wordt verder ingegaan op de voordelen van copyleft- en vrije licenties in verschillende onderwijsomgevingen en er wordt aansluiting gezocht bij bestaande FLOSS-licentie bewegingen.