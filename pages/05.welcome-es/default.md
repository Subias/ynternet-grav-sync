---
title: 'Bienvenidos a la Academia Open-AE!'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Welcome to the OPENAE Academy'
lang_id: 'Welcome to the OPENAE Academy'
lang_title: es
---

Los escenarios de formación son los elementos fundamentales de la caja de herramientas [OPEN - AE](https://open-ae.eu/open-ae-curriculum/). Han sido desarrollados teniendo en cuenta el trabajo de investigación previo, incluyendo el trabajo de campo, de cada uno de las entidades miembro del proyecto. Los diferentes módulos han sido desarrollados tomando como base el estado de la cuestión y las necesidades de cada país en relación a la cultura FLOSS (free/libre and open-source software).

Estos escenarios son una guía para personas formadoras y estudiantes durante el periodo de formación en ambos ambientes, clases presenciales y formación en línea. En cada país donde se implementa la formación, las organizaciones promotoras serán las responsables de la organización de la formación en grupo, con un mínimo de 8 y un máximo de 10 personas participantes seleccionadas entre las personas que trabajan como dinamizadoras o formadoras en el terreno de la formación de personas adultas.

El material de aprendizaje (presentaciones digitales y otros elementos complementarios) se alojan y documentan de forma digital en el repositorio colaborativo [SlideWiki](https://slidewiki.org/deck/127952-2/open-ae-_-academy-slidewiki/deck/127952-2?language=en). La plataforma SlideWiki de código y acceso abiertos invita a las personas a compartir sus recursos para dar apoyo a la autoría, el intercambio, la reutilización y la remezcla de cursos abiertos. 

### Nuestra motivación

Cada vez más vemos habilidades y competencias digitales vinculadas a soluciones de software patentadas. Las tecnologías FLOSS están destinadas a ser abiertas y de libre acceso, y la mayoría de las personas que utilizan estos recursos ya tienen algunos conocimientos sobre las licencias de propiedad intelectual y del propio concepto de propiedad; estos saberes resultaron clave en el momento de tomar la decisión de usar FLOSS. Las personas que tienen pocas habilidades digitales a menudo se sienten intimidadas o inseguras con sus propias capacidades para usar las tecnologías FLOSS, y por lo tanto suelen optar por usar opciones propietarias, puesto que algunas marcas están más asociadas con estos perfiles. El proyecto OPEN-AE tiene como objetivo cerrar esta brecha y promover prácticas y herramientas para hacer que la cultura abierta y el software libre sean más accesibles para los nuevos usuarios.