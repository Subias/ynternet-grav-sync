---
title: 'Wikidata: una piattaforma  di conoscenza libera e aperta che può essere letta e modificata sia dall''uomo che dalle macchine'
lang_menu_title: Wikidata
lang_id: wikidata
lang_title: it
length: '10 ore'
objectives:
    - 'Un solido background contestuale per comprendere i benefici di Wikidata e come si relaziona con gli altri progetti Wikimedia, ma anche come si relaziona con altri database disponibili su Internet'
    - 'Il partecipante acquisirà competenze digitali che gli permetterano di aggiungere contenuti a Wikidata, ma anche scrivere domande in Sparql per ottenere risposte a domande difficili'
    - 'Il partecipante sarà capace di presentare Wikidata agli altri e avrà un background sufficiente per identificare come potrebbe essere utilizzato in ambito professionale'
    - 'Il partecipante comprenderà meglio quali processi di qualità e sistemi di valutazione favoriscono la costituzione di dati di qualità che ogni persona e ogni macchina può utilizzare'
materiels:
    - 'Personal computer (connesso a Internet)'
    - 'Connessione internet'
    - Bearmer
    - 'Carta e penne'
    - Flipchart
    - Wikidata
skills:
    'Parole chiave':
        subs:
            WikiData: null
            Database: null
            'Knowledge Graph': null
            Wikipedia: null
            Community: null
            CC0: null
            'Licenze libere': null
            Collaborazione: null
            SPARQL: null
---

WikiData è una piattaforma di dati aperta che appartiene alla famiglia di siti Web Wikimedia. Ospita 57 milioni di articoli a giugno 2019. Wikidata è una base di conoscenza libera e aperta che contiene vari tipi di dati (ad es. Testo, immagini, quantità, coordinate, forme geografiche, date ...).
L'entità di base in Wikidata è un oggetto. Un oggetto può essere una cosa, un luogo, una persona, un'idea o qualsiasi altra cosa. Ciò che è specifico di Wikidata è che le informazioni sono memorizzate in modo rigidamente strutturato per consentire l'elaborazione sia a persone che a macchine.
Sebbene ora sia una base di conoscenza abbastanza ampiamente utilizzata dalle macchine, è ancora ampiamente sconosciuta al pubblico e ai suoi potenziali istruttori, meno compresi. L'intento di questo modulo è quello di fornire un corso Train the Trainer che fornirà loro una solida panoramica di Wikidata e delle opportunità che sono collegate a questo progetto open source.

Questo modulo sarà diviso in 4 sessioni:
### Prima sessione - Scoprire e comprendere WikiData
Questa sessione fornirà una panoramica su cosa è Wikidata. Tratterà la sua storia e permetterà ai partecipanti di comprendere i motivi e i vantaggi della sua esistenza, per i progetti wikimedia in particolare, e per Internet in generale.

La sessione tratterà le nozioni base di Wikidata, la struttura dei dati, gli elementi di vocabolario e le caratteristiche essenziali del design, ma anche la comunità di wikidata e una panoramica sull’amministrazione in modo che i partecipanti possano percepire che wikidata, per quanto tecnico, sia innanzitutto una costruzione sociale. I partecipanti esploreranno Wikidata da soli e creeranno un account.
### Seconda sessione - Modificare WikiData
La seconda sessione avrà l'obiettivo di far sì che i partecipanti aggiungano contenuti a Wikidata per conto proprio e quindi scoprano alcuni dei processi fondamentali di modifica. Si affronteranno, in maniera approfondita, anche i sistemi di qualità e di valutazione dei dati, gli strumenti più noti che permettono di colmare le lacune, così come quelli per l'editing di massa e l'importazione (di dati?).
### Terza sessione - Querying WikiData
Questa sessione è volta a far conoscere e provare ai partecipanti i servizi di query per query semplici, per poi procedere con l’introduzione del linguaggio di query sparql per richieste più complesse, attraverso una metodologia step-by-step, aumentando progressivamente la difficoltà. La sessione comprenderà attività pratiche.
### Quarta sessione - Applicare WikiData
Questa sessione aiuterà i partecipanti a comprendere il valore applicato di Wikidata, in particolare per applicazioni come la visualizzazione di dati complessi, ma anche per identificare potenziali applicazioni per Wikidata nel proprio lavoro, sia attraverso la visualizzazione, incorporando Wikidata nel proprio software, o utilizzando Wikidata su progetti Wikimedia. La seconda parte della sessione insegnerà loro come connettersi con la comunità Wikidata per una collaborazione più efficace.