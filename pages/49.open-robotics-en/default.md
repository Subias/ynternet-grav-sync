---
title: 'Open robotics with Arduino'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Open robotics with Arduino'
lang_id: 'Open robotics with Arduino'
lang_title: en
length: '10 hours'
objectives:
    - 'To make a trainer completely autonomous, and able to learn by himself'
    - 'The use of an online version as well as the use of community sites is necessary for information sharing. Thus, in addition to training, trainers will be able to help the community themselves. The trainers themselves will learn how to use projects to modify them as they see fit'
materials:
    - 'Personal computer (smartph0one or tablet connected to the internet)'
    - 'Internet connexion'
    - 'Arduino Board (kit)'
skills:
    Keywords:
        subs:
            Arduino: null
            Open-source: null
            Software: null
            Hardware: null
            Projects: null
            Programmation: null
            Coding: null
            DIY: null
            Science: null
---

## Introduction
Arduino is an open-source hardware and software company, project and user community that designs and manufactures single-board microcontrollers and microcontroller kits for building digital devices. Its products are licensed under the GNU Lesser General Public License (LGPL) or the GNU General Public License (GPL), permitting the manufacture of Arduino boards and software distribution by anyone. Arduino boards are available commercially in preassembled form or as do-it-yourself (DIY) kits.

The Open-AE Project, which is aimed at promoting FLOSS technologies in non-formal adult education to support the digital upskilling of both educators and learners.
We must agree that nowadays digital tools are everywhere and we are entering a DIY era, which means everyone wants to create and not only use technology.

Arduino is present at many degrees. You can use it as a kid, with basics projects very simple to realize. It’s already used as an extraccuricular activity in many Brussels secondary schools. Arduino is also used by university students as a main project in their cursus. Moreover, thanks to the internet and the community, you can, as an adult, carry out projects easily at home and as a self-taught person.

In conclusion, Arduino can be used at almost any age, and almost anywhere. It doesn't require a deep knowledge of computers, and has a strong community ready to help at any time, and thousands of tutorials to understand the world of programming and robotics.

This module will be divided into 3 sessions:
### First session: How is Arduino revolutionizing the world of electronics?
Arduino is very easily accessible to all types of audiences, from children to adults. Its simplified programming language, the affordable motherboard and its strong community offer very high accessibility.
Arduino allows almost everyone to free themselves from the world of the "digital divide", i.e. the use of electronic devices without knowing how they were built or programmed.
It is therefore possible to recreate either a thermometer, a distance sensor, etc... At the slightest touch and customized. Arduino is much based around projects shared on a large scale by the community.
First of all, at the beginning of the session, the trainer will ask the participants if they know what Arduino is, and if so, to give all the elements that could come close or far to what Arduino is.
Then, the trainer will ask the participants, after explaining at length what an Arduino is, how they could make life easier with a few small projects.
At the end of the session, participants should be able to identify an Arduino alternative to all existing electronic devices, should also have a small list of projects they would like to realize.
### Second session: Learning the logic of programming
When people want to get into coding, they often run into the problem of programming logic. All the terms as if... so, loops, integrations, are very complex terms that are inherent in every language. So it is important to think about an inclusive and easy way to get into the complicated world of programming.
To do this, we need to popularize as much as possible by taking physical examples and real-life situations, and that's what we'll have to do as we go along.
The aim of the session will be that each person will be able to give an example of a programming concept (as if... then), comparing it with a real life situation.
### Third session: Carrying out Arduino projects through self-training
For the participants, it is essential to learn how to document themselves because even hundreds of hours of classes will not be enough to exploit all that Arduino has to offer.
During the session, participants will learn how to use, on sites, projects left by the community. Each group will then choose the not too complicated projects that they will have searched for themselves.
The aim of this session is that at the end, each participant will be able to carry out a simple project using a reliable and well-documented site, as well as being able to modify parameters, add or remove modules in order to go further in empiricism and self-learning.

## Context
The goal of the session is to practically demonstrate and engage learners on how:
* The aim of the sessions will be to make a trainer completely autonomous, and able to learn by himself.
* The use of an online version as well as the use of community sites is necessary for information sharing. Thus, in addition to training, trainers will be able to help the community themselves. The trainers themselves will learn how to use projects to modify them as they see fit.
At the end of the sessions participants will be able to create, program and share material. Learning Arduino can help users learn to think outside the box, reason systematically and work collaboratively.