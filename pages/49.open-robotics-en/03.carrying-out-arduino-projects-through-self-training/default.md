---
title: 'Carrying out Arduino projects through self-training'
objectives:
    - 'To identify the benefits of the Arduino online community'
    - 'To encourage the sharing, reuse, and combination of code'
    - 'To foster learners’ collaborative learning'
---

## Join and benefit from the Arduino community
The last part will be the project part. The goal is that the participants will be able to build a project by themselves using Instructables or TinkerCad.
Instructables is a community site that gathers more projects on Arduino than any other site. Just type Instructables in google, and on the magnifying glass, type the project you want to build. The instructions are extremely clear, and usually the code is explained at length. This is the biggest knowledge sharing community in terms of Arduino, Raspberry Pi, etc...

[Instructables](https://www.instructables.com/)

TinkerCad is a basic 3D modeling software, but some time ago, they integrated an additional Arduino tab, which shows us the diagram of the connections in an extremely precise way. Moreover, for each extension module, the code is already completely provided.  The participants will only have to do the connections, copy/paste the code and possibly mix codes to correlate several modules. TinkerCad is surely one of the best tools available to get started on Arduino.
You must connect to TinkerCad by creating an account, and click on the "Circuits" tab.

[TinkerCad](https://www.tinkercad.com/circuits)

It is of course also possible to use the "Examples" tab on the Arduino IDE.

## Homework
The homework will be to take one of the projects, and reach the goal of the first session: to be able to improve a project found on the Internet with the aim of lifehacking its own environment. This last homework will be part of the last exercise to make participants self-taught DIY enthusiasts!

## References
* [Instructables](https://www.instructables.com/)
* [TinkerCad Circuits](https://www.tinkercad.com/circuits)
* [Arduino](https://create.arduino.cc/)