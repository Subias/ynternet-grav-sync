---
title: 'Learning the logic of programming'
objectives:
    - 'To discover the benefits of programmation for the development of 21st Century skills'
    - 'To design a program'
    - 'To learn autonomous from tutorial and videos'
    - 'To access almost any programming language'
---

## Introduction
Group discussion on “Coding with Arduino?”
Trainer will start the introduction to the module by asking participants about their programming language experience.

## Let’s popularize programmation
_“Digital fluency requires not just the ability to chat, browse, and interact but also the ability to design, create, and invent with new media” (…). The ability to program provides important benefits. Inparticular, programming supports “computational thinking,” helping you learn important problem-solving and design strategies that carry over to no programming domains._
**Presentation and analysis of the benefits of programming**.

It is essential to learn programming and compare its concepts to real-life situations. The trainer should popularize these concepts as much as possible, as with unplugged activities.

An example could be the robot sandwich: The teacher plays the robot and the participants have 10 minutes to tell the robot how to make a chocolate sandwich by writing all the instructions on paper. It is important that the robot does absolutely what the instruction says, and that it does not get it wrong. The goal is to show at which points the lines of code must be accurate, complete, and in the correct order.

What can also be done is to graphically represent a concept with an example. Let's take the example of the bank to illustrate the "if... then" condition. Participants are asked to explain, using the "if... then" condition, how an ATM distributes money to us. The trainer will do the exercise with the participants, and then the participants will have to find an example on their own. Here is how to proceed: Insert the credit card. If card inserted, then go to the home screen and choose the amount. If there is enough money in the account, then go to the code verification step. If balance is not sufficient, decline and go back to select amount, etc...

![](arduino%201.png)

[Learn to code, code to learn](https://www.youtube.com/watch?v=Ok6LbV6bqaE)
[Why is coding so important](https://www.forbes.com/sites/quora/2018/08/16/why-is-coding-so-important/#3866fc362adc)
[What is computational thinking?](https://www.youtube.com/watch?time_continue=1&v=sxUJKn6TJOI)

## Let’s code!
Now that we have popularized programming, the trainer will have to explain the basic functions and loops, as well as the syntax specific to Arduino. The basic language of Arduino is in C.
Let's look at an example where we're trying to light an LED

![](arduino2.png)

Here is the scheme:

![](arduino3.jpg)

### Constants
Writing in programming: **const int**
Now let's talk about the constants. Let's start from the premise that a computer cannot interpret sentences. Apart from the precise vocabulary as mentioned above, everything else will only be understood if a value is assigned. For example, if we connect a LED to digital pin 2, we will have to declare that LED = 2, so that our machine can understand that pin 2 is associated with the LED. These procedures are also mandatory so that we ourselves can understand our code. 
 We write the name of the constants in capital letters (it's part of the programming conventions)
 
 ![](arduino4.png)
 
 so you know that they are constants. The name of our constant (LED) is not written in blue because it is not a constant reserved for the IDE but for us.
**HIGH**, **LOW** and **OUTPUT** are constants.

### Variables
Writing in programming: **int**
The variables look like constants, but the value will not remain fixed. When you declare a variable, the goal is to be able to change the value as you wish. For example, if between each line of code, you want to put variable pause times, you can integrate a variable named pauseTime. At first, it is necessary to declare the variable, but it is also necessary to give it a basic value, for example 0.
For variables there is a nice, readable and above all conventional way of naming them: you write the name all attached and capitalize every word except the first one. Here is an example of a variable name:
Let's take the example of the LED. If we want to increase the interval time between switching on

![](arduino%205.png)

and off, it will be much easier to use one variable instead of writing everything one after the other.
Note that even if the variable is declared with the term int (which means to integrate), there are a whole bunch of other terms depending on the value you want to assign. Here are the most important ones: **Functions**

### Functions

![](arduino6.png)

Writing in programming: **int** … {  }
Functions are pieces of code that are used so often that they are encapsulated in certain keywords so that you can use them more easily. Now, let's compare our function to the young child taken rather as an example.  We're going to teach him how to make chocolate toast. We will try to detail as much as possible because he has never learned how to do it. Here is an example:
* Take the jar of chocolate from the cupboard
* Place the jar of chocolate on the table
* Open the jar of chocolate
* Taking a knife from a drawer
* Take a slice of bread from the bag of bread
* Dip the knife in the chocolate
* Remove the knife from the pot
* Spread the chocolate on the slice
We can call this function: prepare a chocolate toast.

![](arduino7.png)

Void setup and void loop are both considered as functions.

### Syntax in programming
To fully understand synthax, nothing is better than analyzing an example. Let's start with one of the simplest programs, which consists in displaying in the monitor the message "Hello World". The monitor is used to communicate information about the program; it is located in the top right corner of the software.

![](arduino8.png)

### Void setup() et void loop()
Let's analyze it together. First, we can notice the **void setup** function, where we will put all the initialization instructions. Let's compare this function to an airplane pilot. He will first have to check a lot of parameters before taking off, like the kerosene level or even the tire pressure. On every flight the pilot has to make sure that the parameters are correct.
Now let's talk about the **void loop**. The content we'll put between the curly braces will repeat itself ad infinitum. Most of the content will go into this function.

### Double slashes and slash star ( // ou /* … */ )
They are used to write comments. It's very important, even for a regular programmer, to describe everything you do. This makes the code readable for others, but also for oneself. Anything written after a double slash will not affect the code.  Double slashes allow you to write a comment on one line, but if you want to write a comment on several lines or even make part of the code inactive without deleting it, you just have to open the comment with a /* and close it with a */.

### Brackets and curly brackets
Brackets and braces are paramount in programming. After each function, a brace to the right and then a brace to the left will be added. In these two braces we will put our instructions.
The parentheses are placed after each line of code before the braces or the semicolon. In these brackets, we will put values that will form the parameters. Parameters are separated by commas if there is more than one.
For example, in the **Serial.begin** (which corresponds to display the monitor on the Arduino software), it is written 9600. This means that the refresh rate is 9600 frames per second.
Just at the bottom, in the **Serial.print** code line, we'll put the message we want to display in quotation marks in the brackets.

### Conditions
The condition is a test that the program will perform by asking itself a question. If it can answer 'true' then it executes an action block (which will be put between braces), otherwise it does not execute it.
Here is the corresponding pseudo code:
_program start
if (test)
{
    this code executes if we can answer true to the test.
}
the program continues_

To illustrate the example, let's take again the program "Hello World", let's add a display condition.

![](arduino9.png)

You will notice that theSerial.println() statement displays the text in quotes and returns to the line. Modify the program using theSerial.print() statement by removing 'ln' at the end. You will see that the words stick together. So we retain that:
Serial.println() : display with line break
Serial.print() : displays without line break
We notice that to test a value in a condition we put a double equal: ==

![](arduino10.png)

A beginner's error is to confuse the = which allows you to assign a value to a variable or constant, and the == which allows you to test equality.

### The "for" loop
Programming allows us to make loops in a program. That is, we can execute a block of code several times before moving on to the next one. This repetition opens up a large number of possibilities.
The for loop allows a code to be repeated a number of times known in advance.
It's a bit like a condition: it starts with a keyword (for), followed by some things in brackets, then braces that contain the code to be executed. Here is an example:
_for (int t=0;t<10;t=t+1)
{
    executable code
}_

The most difficult to understand is between the brackets. There are three important pieces of information separated by semicolons:
1. Initialization of the variable that will be used as the counter. In our example, we declare and initialize (at the same time) a variable t of type "int":int t=0 .
2. Condition to be tested to continue or not the repetition. In this case, the code is executed as long as t is less than 10: t<10.
3. Action on the counter at each loop turn. In our case, t is increased by 1 at each pass: t=t+1.

## Homework
Participants are asked to perform the following tasks: 
* Taking the “Hello World” example or the LED one
* Trying to modify parameters and see what’s going on
* Based on the concepts we observed together, try to extend the code by adding a correlation between the two examples seen.

## References
* [Tutorial for Arduino](https://create.arduino.cc/projecthub)
* [Getting started with Arduino](https://www.arduino.cc/en/Guide/HomePage)
* [Arduino official Youtube](https://www.youtube.com/user/arduinoteam)
* [Youtube Arduino tutorial](https://www.youtube.com/watch?v=kLd_JyvKV4Y)
* [OpenClassroom](https://openclassrooms.com/fr/courses/2778161-programmez-vos-premiers-montages-avec-arduino) (French)