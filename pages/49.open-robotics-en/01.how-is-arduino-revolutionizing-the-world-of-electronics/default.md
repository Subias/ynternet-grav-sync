---
title: 'How is Arduino revolutionizing the world of electronics?'
objectives:
    - 'To discover the background of Arduino and its history, and its close link with Open Source in general'
    - 'To examine the possibilities offered by Arduino in terms of life enhancement'
---

## Introduction
For about thirty minutes, the participants will be divided into groups of two, and will be asked, without using the internet at first, to name everything that is close or far from Arduino.
Afterwards, the trainer will explain what Arduino consists of and what are the fields of application.

## Discover the world of Arduino
### Who Invented the Arduino?
The Arduino project was started at the Interaction Design Institute Ivrea (IDII) in Ivrea, Italy. The project goal was to create simple, low cost tools for creating digital projects by non-engineers, because it was difficult for students to afford expensive motherboards.The beginning project as called Wiring, and it was a development platform. The Wiring platform consisted of a printed circuit board (PCB) with an ATmega168 microcontroller (this is a microcontroller who has memory storage to launch programs), an IDE based on Processing and library functions to easily program the microcontroller.

After a few months, the italians students decided to rename the Wiring project by Arduino project. Not because of the name remembering hardware for Arduino, but because of the bar the students were used to meet at to talk about the project himself.
The initial Arduino core team consisted of Massimo Banzi, David Cuartielles, Tom Igoe, Gianluca Martino, and David Mellis.

### Why Arduino ?
Thanks to its simple and accessible user experience, Arduino has been used in thousands of different projects and applications. The Arduino software is easy-to-use for beginners, yet flexible enough for advanced users. It runs on Mac, Windows, and Linux. Teachers and students use it to build low cost scientific instruments. Plus, Arduino has a improving community all around the world with mutiples sites like Instructables or Arduino himself.The software and the board get updated very often, and the community is numerous and pretty active.

It’s very cheap: Arduino boards are relatively inexpensive compared to other microcontroller platforms. You can also buy kit for less than 50€. This contains multiples addons as sensors, buttons and a few chipsets. 
It’s crossplatform: The Arduino Software (IDE) runs on Windows, Macintosh OSX, and Linux operating systems. Most microcontroller systems are limited to Windows.
It’s easy to use: The programmationlangage is the easiest one, and you only have to download the IDE to start programming. You can also extend the programmationlangage to C++ and add multiples librairies.

### What is an Arduino board?
Arduino boards are thus composed of a motherboard with a processor and random access memory. It is a small computer capable of using a variety of microprocessors and controllers.  The boards are equipped with sets of digital and analog input/output (I/O) pins that may be interfaced to various expansion boards ('shields') or breadboards (For prototyping) and other circuits. The boards feature serial communications interfaces, including Universal Serial Bus (USB) on some models, which are also used for loading programs from personal computers.

The basic Arduino card is called the Arduino Uno, but there are a bunch of other Arduino cards, such as the Arduino Mega, which offers a much more powerful processor, as well as more digital and analog inputs and outputs. There is the Arudino nano, which for almost the same power of the microcontroller, is much smaller in size. Arduino Mini, Arduino Due, Arduino ADK Android, so many Arduino cards that can meet the specific needs of each project.

Also note that as Arduino is an open-source hardware and software, many other similar cards of different qualities have appeared on the market, and it is therefore very easy to get them at lower prices, as for the brand Elegoo, Seeedstudio, Sparkfun, Watterott. These other brands, which sometimes collaborate with Arduino, can offer specific modules, such as a card with integrated wifi, a card specially made to communicate with a web server, etc.

## Homework
Participants will have as homework to go on the internet and look for a project they might like, and learn about the code as well as the connections.
They will have to explain the project and how it could be useful in their everyday life, as well as the modifications they could make to make the project more adapted to their requests.

## References
* [Arduino](https://www.arduino.cc/)
* [TOP 10 Arduino projects of 2019](https://www.youtube.com/watch?v=ydgLQCQMBuUScratch website)
* [Wikipedia: Arduino](https://fr.wikipedia.org/wiki/Arduino)