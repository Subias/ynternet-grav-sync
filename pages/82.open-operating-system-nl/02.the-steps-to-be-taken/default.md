---
title: 'The steps to be taken'
objectives:
    - 'to provide necessary tools to successfully migrate to GNU / Linux'
    - 'to facilitate technological tools that can help us to preserve and retrieve information'
    - 'to facilitate pedagogical tools to plan migration'
    - 'to encourage migration, lose fear. It''s just a machine!'
---

##Migration strategies
Analyze the pros and cons of the different migration strategies in GNU / Linux.
1. Step by step migration. Go migrating each tool first, end up migrating the OS when people are already used to work with the tools.
2. Mixed migration: partial migration the center; you only do it at some machines to get people used to it.
3. Dual migration: install the two systems in parallel, so that each user can choose which one want to use at any time. 
4. Hard migration: the entire system is migrated once and for all. The people affected by the change are informed (or not).

**Objective**: to see how, in fact, step-by-step migration is already happening, many of the tools we use at the moment are FLOSS (Mozilla Firefox, Gimp, LibreOffice ...). Encouraging hard migrations.
Online activity, discussion forum about each strategy. At the end of the activity, the participants must be able to chose a migration system.

##Backup amb clonezilla
This activity is optional.
Clonezilla is a powerful and intuitive tool, that helps us make backups of entire installations. It will be usefull for: 
* make copies of a computer (operating system and contents) and store them on a hard disk.
* make copies and replicate an installation quickly, instead of making the same installation and configuration repeatedly.

The activity consists of installing a bootable USB with clonezilla, following the instructions of the software itself. People who carry out this activity will end up having a bootable usb with clonezilla, and they must be able to boot a PC with this USB. They can try to make a backup of the existing installation.
A support forum is opened to resolve the technical doubts that may arise.

##My SO
Download and install the previously chosen OS on  a USB bootable. It will be done according to the instructions provided by each system.
Each participant must have its bootable operating system from an external medium.

##Let's do it
How to boot using the BIOS (support: presentation or web) is explained in this part of the lesson. 
* what is the BIOS, how to access the BIOS and how to choose the boot order.
* UEFI And its compatibility with ubuntu.
Once the USB is prepared, the installation of the SO is carried out.
* it is recommended to perform a single (non-dual) installation, since it simplifies the process.
* if you choose to perform a dual installation, it is advisable to follow the partitioning suggested by the installer. If in doubt, ask the trainer!
If there is no problem, at the end of the process the computer is restarted and the new SO is working.