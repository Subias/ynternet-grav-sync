---
title: 'Tecnologies d''impressió 3D de codi obert'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Open-source 3-D printing Technologies'
lang_id: 'Open-source 3-D printing Technologies'
lang_title: ct
length: '10 hours and 30 minutes'
objectives:
    - 'Beneficiar-se de la fabricació digital'
    - 'Utilitzar tecnologies digitals obertes per crear, manipular, preparar i publicar contingut 3D'
    - 'Utilitzar tecnologies digitals obertes per trobar i aprofitar el repositori de models 3D'
materials:
    - 'Ordinador personal'
    - 'Connexió a Internet'
    - Tinkercard
    - Scketchfab
skills:
    'Paraules clau':
        subs:
            'Fabricació additiva': null
            'Fabricació digital': null
            'Impressora 3D de codi obert': null
---

_Els significatius avenços en les tecnologies de fabricació additiva (AM), comunament conegudes com impressió 3D, han transformat en l'última dècada les formes en què els productes es dissenyen, desenvolupen, fabriquen i distribueixen._

_La capacitat i els avantatges de la impressió 3D sobre la fabricació tradicional obren moltes oportunitats per a les empreses verticals (aquelles que ofereixen béns i serveis específics per a una indústria, comerç, professió o un altre grup de clients amb necessitats especialitzades): des del disseny i desenvolupament de productes, el servei de personalització, fins a la reestructuració de la cadena de subministrament per a una major eficiència._ 
**Comissió Europea valora positivament la naturalesa disruptiva de la impressió 3D**

La introducció de la fabricació additiva (AM per les seves sigles en anglès), millor coneguda com impressió 3D, sorgeix com una tecnologia disruptiva que porta amb si mateixa diversos canvis i impactes en la producció tradicional. Un nombre creixent d'empreses i nous models de negoci basats en l'AM estan sorgint, creant grans oportunitats per a l'economia i la societat. Després de proporcionar a l'alumnat una formació bàsica sobre l'AM, fabricació digital i impressió 3D de codi obert (primera sessió), el curs implicarà un aprenentatge intensiu de maquinari i programari de codi obert i plataformes de codi obert per a la creació d'objectes 3D.

Les sessions permetran a l'alumnat adquirir algunes habilitats tècniques necessàries per aprofitar al màxim totes les oportunitats que ofereixen les tecnologies 3D. Les persones participants experimentaran el procés de disseny dissenyant un objecte utilitzant tecnologies d'impressió 3D de codi obert.

## Context
Les impressores 3D s'estan convertint ràpidament en un dispositiu de fabricació àmpliament utilitzat que ha revolucionat la indústria manufacturera. El propòsit d'aquest curs de capacitació és presentar als participants l'AM i els beneficis més poderosos de les tecnologies d'impressió 3D i l'ús de tecnologies digitals obertes.
En finalitzar les sessions, les persones participants podran crear objectes 3D utilitzant tecnologies digitals obertes, compartint els seus propis projectes i utilitzant els projectes d'altres usuaris.

Aquest mòdul es dividirà en 2 sessions:
### Primera sessió: transformació digital amb la "Fabricació additiva" (AM)
En les últimes dècades, les comunicacions, el món del disseny, l'arquitectura i l'enginyeria han experimentat les seves pròpies revolucions digitals. El terme "transformació digital" es refereix a l'ús de dades digitals, connectivitat i processament que abasta tots els aspectes de l'activitat de fabricació. La AM ens brinda l'oportunitat de repensar per complet la forma en què els productes arriben a l'usuari final. La sessió té com a objectiu proporcionar als alumnes una visió general àmplia de el disseny i la fabricació digital, presentant a l'alumnat la impressió 3D, com funciona i com es pot utilitzar en diverses indústries. Aquests / es aprendran què és la impressió 3D, com funcionen les impressores 3D i els tipus d'objectes que poden fer amb aquesta tecnologia.
### Segona sessió: explorem tecnologies digitals obertes per dissenyar, desenvolupar, personalitzar i compartir productes 3D
El futur de la fabricació és digital. Les impressores 3D s'estan convertint ràpidament en un dispositiu de fabricació àmpliament utilitzat que ha revolucionat la indústria manufacturera. En el treball d'impressió 3D, tot comença amb un model 3D. Les sessions tenen com a objectiu introduir al programari i plataformes de codi obert per a dissenyar i compartir continguts en 3D. Els / les estudiants experimentaran el procés de disseny dissenyant / trobant objectes 3D usant Tinkercad (plataforma per publicar, compartir i descobrir contingut 3D a la web, dispositius mòbils, AR i VRI) i Sketchfab (eina per a publicar i trobar models 3D en línia) . Els / les participants adquiriran les habilitats tècniques necessàries per començar a la AM i per aprofitar al màxim totes les oportunitats que ofereixen les tecnologies 3D.

Les impressores 3D s'estan convertint ràpidament en un dispositiu de fabricació àmpliament utilitzat que ha revolucionat la indústria manufacturera. El propòsit d'aquest curs de capacitació és presentar a les persones participants l'AM i els beneficis més poderosos de les tecnologies d'impressió 3D i l'ús de tecnologies digitals obertes. En finalitzar les sessions, els / les participants podran crear objectes 3D utilitzant tecnologies digitals obertes, compartint els seus propis projectes i utilitzant els projectes d'altres usuaris.