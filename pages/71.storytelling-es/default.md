---
title: 'Narración digital: crear y compartir historias utilizando tecnologías digitales abiertas'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Digital Storytelling: create and share stories using open digital technologies'
lang_id: 'Digital Storytelling: create and share stories using open digital technologies'
lang_title: es
length: '12 horas'
objectives:
    - 'Usar e integrar diferentes medios (texto, imágenes, sonido, video) en un entorno en línia'
    - 'Usar tecnologías digitales abiertas para crear y compartir historias'
    - 'Producir y compartir una historia digital para estimular la personalización y el compromiso activo del alumnado'
    - 'Explorar métodos de colaboración en actividades de pequeños grupos para aprender unos de otros'
materials:
    - 'Ordenador personal'
    - 'Conexión a internet'
    - 'Tarjetas Dixit'
skills:
    'Palabras clave':
        subs:
            Narración: null
            'Historia digital': null
            'Guión gráfico': null
            'Círculo de narraciones': null
            Multimedia: null
            'Narración interactiva': null
            'Software de código abierto': null
---

La narración digital (Digital Storytelling, DS) es una forma sencilla y accesible de contar una historia a través de un cortometraje. La DS es una actividad que utiliza las herramientas de la tecnología digital para contar historias sobre nuestras vidas.
La DS es una parte esencial del sistema educativo mundial moderno, una herramienta de aprendizaje activo que puede proporcionar al alumnado las habilidades y competencias necesarias para crear, gestionar y compartir historias digitales utilizando recursos de código abierto. Para producir una historia digital, el alumnado tiene que trabajar a través de un proceso de narración digital:

![](storytelling1.png)

Al crear una historia digital, las creadores deben producir imágenes para edición de video, sonido, música, texto y voz. Por lo tanto, se pueden desarrollar y practicar gran variedad de habilidades a través de la creación de historias digitales. Al usar la metodología DS, el alumnado tiene la oportunidad de crear y compartir historias utilizando tecnologías digitales abiertas (por ejemplo, software de edición de video de código abierto; software de edición de audio de código abierto, etc.).

La DS permite al alumnado expresar sus propias experiencias personales, organizarlas en una historia (autorreflexión) y usar técnicas digitales para contar historias personales. Además es una poderosa herramienta tecnológica para mejorar la competencia digital del alumnado en el uso de software abierto que combina una variedad de multimedia (texto, imágenes fijas, audio, video y publicación en la web). La DS también puede ser una herramienta eficaz que proporciona a las estudiantes las cuatro habilidades C del siglo XXI (pensamiento Crítico; Creatividad; Colaboración; Comunicación).

## Contexto
El propósito de este curso de capacitación es presentar a los participantes la DS y aprender a usarla como método para contar historias mediante el uso de tecnologías digitales abiertas.
Al final de las sesiones, los/las participantes podrán crear un guión gráfico para su propia historia digital, crear y recopilar materiales relevantes para su historia digital (imágenes, voz, música, sonidos, textos, títulos) a través de tecnologías digitales abiertas.
Las sesiones también tienen como objetivo obtener una mayor comprensión de las pautas éticas, los derechos de autor, la licencia y el consentimiento de Creative Commons.

Este módulo se dividirá en 2 sesiones:
### Primera sesión: una introducción a la DS: antecedentes y beneficios

La DS es un enfoque multimedia para compartir una narración basada en la integración de fotografía, sonido, texto, música para compartir la narración digital de la persona creadora en un formato de medios digitales. La DS es un método que nos ayuda a centrarnos en nosotros/as mismos/as y en nuestra propia historia. Nos ayuda a expresar nuestra historia, dramatizarla, ilustrarla con fotografías y contarla a otras personas. La sesión tiene como objetivo obtener una mayor comprensión de los antecedentes y los principios de la DS para crear, recopilar y compartir historias digitales utilizando tecnologías digitales abiertas.

La sesión también se centra en el impacto potencial de la metodología DS. De hecho, lograr que el alumnado ​combine una variedad de elementos comunicativos dentro de una estructura narrativa para crear y compartir su historia digital tiene algunos beneficios clave:
* Mejora la competencia digital del alumnado, ayudándolos a ser competentes con software de audio y video de código abierto
* Ofrece al alumnado una nueva forma de colaborar

Al final de la sesión, los/las participantes podrán reconocer qué es y qué no es una narración digital, identificando las principales características y pasos de la DS. Los/las participantes también podrán identificar las ventajas de la DS en la educación de adultos, con un enfoque especial en el desarrollo de las habilidades del siglo XXI. De hecho, cuando las personas aprenden a crear y compartir su historia utilizando la metodología DS, también desarrollan las habilidades del siglo XXI que la ciudadanía de hoy necesita para tener éxito en sus carreras durante la era de la información.

### Segunda sesión: ¡Crea tu poderosa narración digital con tecnologías digitales abiertas!

La segunda sesión tiene como objetivo permitir la producción de una narración digital, incluyendo guión gráfico, material, edición. Los/las participantes aprenderán cómo crear un guión gráfico de su historia, utilizando un software abierto que combina una variedad de medios (texto, imágenes fijas, audio, video y publicación en la web) y reconocerán el material de licencia gratuita en la web.

El propósito de este curso de capacitación es presentar a los participantes la DS y aprender a usarlo como método para contar historias mediante el uso de tecnologías digitales abiertas. Al final de las sesiones, los participantes podrán crear un guión gráfico para su propia historia digital, crear y recopilar materiales relevantes para su historia digital (imágenes, voz, música, sonidos, textos, títulos) a través de tecnologías digitales abiertas. Las sesiones también tienen como objetivo obtener una mayor comprensión de las pautas éticas, los derechos de autor, la licencia y el consentimiento de Creative Commons.