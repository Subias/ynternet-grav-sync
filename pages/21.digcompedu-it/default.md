---
title: 'Il quadro DigCompEdu per un’educazione comune e aperta'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'DigCompEdu framework for a common and open education'
lang_id: 'DigCompEdu framework for a common and open education'
lang_title: it
skills:
    'Parole chiave':
        subs:
            DigCompEdu: null
            Framework: null
            'Competenze digitali': null
            'Istruzione aperta': null
            'Istruzione degli adulti': null
            'Cultura FLOSS': null
            Insegnamento: null
            Competenze: null
materials:
    - 'Computer personale'
    - 'Connessione Internet'
    - 'DigCompEdu framework'
    - 'Carta e penna'
objectives:
    - 'Comprendere il quadro DigCompEdu'
    - 'Mappare le proprie competenze seguendo il quadro'
    - 'Pianificare l’upskilling delle competenze digitali per scopi educaivi'
    - 'Come impostare attività didattiche creative grazie alle competenze digitali'
length: '8 ore'
---

"....un quadro scientificamente valido che descrive cosa significa per gli educatori essere competenti in campo digitale. Fornisce un quadro di riferimento generale per sostenere lo sviluppo di competenze digitali specifiche degli educatori in Europa".
Questo modulo fornisce una panoramica del quadro delle competenze digitali per gli educatori. Questo quadro europeo elenca 22 competenze organizzate in sei aree. L'attenzione non è focalizzata sulle abilità tecniche, in quanto il quadro mira piuttosto a descrivere in dettaglio come le tecnologie digitali possono essere utilizzate per migliorare e innovare l'istruzione e la formazione. Il modulo si concentrerà su quelle aree altamente rilevanti per l'educazione degli adulti, spiegando in che modo il framework può essere di grande utilità per gli educatori e quali sono i benefici dello sviluppo di tali competenze tramite l’uso delle tecnologie open source. Saranno forniti esempi pratici con riferimento alle sei aree per consentire agli studenti di sapere quali risorse e strumenti aperti sono disponibili nel settore.

## Contesto
L’obiettivo della sessione è coinvolgere gli studenti e dimostrare loro praticamente come:
-        Comprendere il quadro DigCompEdu. 
-        Mappare le proprie competenze seguendo il quadro 
-        Pianificare l’upskilling delle competenze digitali per scopi educaivi
-        Come impostare attività didattiche creative grazie alle competenze digitali 

Lo scopo di questo corso formativo è far conoscere ai partecipanti il quadro DigCompEdu per permettere loro di sfruttare il documento per insegnare e apprendere.
Alla fine delle sessioni I partecipanti saranno capaci di comprendere il quadro e individuare gli strumenti e le metodologie esistenti che possono aiutare nello sviluppo di competenze descritte nel DigCompEdu.

Questo modulo sarà diviso in 2 sessioni:

### Prima sessione: Il quadro DigCompEdu  
In questa prima sessione il quadro delle competenze verrà presentato ai pertecipanti, per fornire loro nozioni teoriche sulla struttura e gli scopi del documento.  La comprensione e la capacità di riconoscere le competenze digitali necessarie nel ventunesimo secolo è un fattore chiave nell'istruzione. I partecipanti saranno preparati per individuare le loro lacune e mappare le loro competenze nella seconda sessione.

### Seconda sessione: Come utilizzare il quadro europeo per mappare le tue competenze e svilupparle
La conoscenza dei partecipanti, in riferimento al Quadro europeo delle competenze, troverà qui applicazione pratica. A partire dalle sei aree di DigCompEdu, i formatori e gli insegnanti dovranno mappare le proprie competenze al fine di identificare le lacune e pianificare il loro miglioramento. Saranno presentati alcuni casi di studio per fornire ai partecipanti alcuni esempi concreti su come sfruttare i vantaggi del quadro di riferimento per i loro contesti educativi.