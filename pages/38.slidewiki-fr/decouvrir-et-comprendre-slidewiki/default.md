---
title: 'Découvrir et comprendre SlideWiki'
objectives:
    - 'donner un aperçu de ce qu''est SlideWiki et comprendre le cadre théorique autour des applications Web pour gérer le contenu éducatif'
    - 'expliquer en quoi l''outil de méthodologie est un atout dans le projet et pourquoi l''utilisation de l''application Web SlideWiki dans Open Adult Education est recommandée'
    - 'pour découvrir la communauté SlideWiki et quelques éléments clés de la communauté'
    - 'comprendre l''interface: créer un compte et favoriser la (co)création et le partage de contenus pédagogiques'
---

