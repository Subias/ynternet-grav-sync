---
title: SlideWiki
lang_menu_title: Slidewiki
lang_id: slidewiki
lang_title: fr
length: '10 heures'
objectives:
    - 'Comprendre le cadre théorique autour des applications Web pour gérer le contenu éducatif'
    - 'Promouvoir l''utilisation de l''application Web Slidewiki dans Open Adult Education'
    - 'Promouvoir la (co)création et le partage de contenu éducatif'
    - 'Montrer comment utiliser des systèmes de création OpenCourseWare ouverts sur le Web, tels que SlideWiki'
materials:
    - 'Ordinatuer personnel connecté à internet'
    - 'Connexion internet'
    - Bearmer
    - 'Papier et stylos'
    - Flipchart
    - 'Academie SlideWiki'
skills:
    Keywords:
        subs:
            Wikipédia: null
            Communauté: null
            CC0: null
            'Licences libres': null
            Collaboration: null
            SlideWiki: null
            Présentations: null
            'Logiciel libre et ouvert': null
            'Culture FLOSS': null
            'Collaboration en ligne': null
---

SlideWiki est un système de création [OpenCourseWare](https://en.wikipedia.org/wiki/OpenCourseWare) basé sur le Web. Il prend en charge la création et la gestion de contenu d'apprentissage, ainsi que des outils de collaboration / crowdsource, de traduction, de communication et d'évaluation.

SlideWiki est une application Web facilitant la collaboration autour du contenu éducatif. Avec SlideWiki, les utilisateurs peuvent créer et collaborer sur des diapositives et les organiser dans des présentations. Les présentations peuvent être organisées de manière hiérarchique, de manière à les structurer raisonnablement en fonction de leur contenu. Un repositorium de collaboration à grande échelle (également appelé crowd-sourcing) autour de contenus éducatifs (autres que des textes) est pris en charge. Les diapositives, les présentations, les diagrammes, les tests d'évaluation, etc. sont téléchargés par les utilisateurs pour être ultérieurement réutilisés par d'autres utilisateurs. Ce contenu est principalement créé par des tuteurs, des enseignants, des conférenciers et des professeurs individuellement ou en petits groupes. Le contenu résultant peut être partagé en ligne (par exemple, à l’aide de Slideshare, OpenStudy, Google Docs).

SlideWiki est une plateforme qui permet à de vastes communautés d'enseignants, de charger de cours et d'universitaires de créer un contenu éducatif sophistiqué de manière collaborative et ouverte. SlideWiki permet de diffuser du contenu et d’élever les étudiants et les pairs chercheurs plus rapidement, car la charge de la création et de la structuration du nouveau domaine peut être répartie au sein d’une grande communauté. Les spécialistes des différents aspects du nouveau domaine peuvent se concentrer sur la création de contenu éducatif dans leur domaine d’expertise spécifique. Ce contenu peut néanmoins être facilement intégré à d’autres contenus, restructuré et redéfini. Un aspect particulier, facilité par SlideWiki, est la multilinguisme. Étant donné que tout le contenu est versionné et richement structuré, il est facile de traduire le contenu de manière semi-automatique et de suivre les modifications apportées à diverses versions multilingues du même contenu.

Ce scénario sera divisé en 3 séances :
### Première séance : Découvrir et comprendre SlideWiki
Le but de cette session est de présenter aux participants la plateforme SlideWiki et d’offrir un espace de discussion autour des thèmes de la création de contenu, de l’utilisation du contenu par d’autres personnes et de leurs auteurs, mais également de la disponibilité / accessibilité des informations dans des contextes éducatifs. Durant cette séance, les participants apprendront à créer leurs propres présentations et à importer du contenu sur la plate-forme.

### Deuxième séance : Expérimenter SlideWiki : création et collaboration sur SlideWiki
Au cours de cette séance, l’accent sera mis sur l’édition des présentations que nous avons importées de nos collections personnelles, mais également sur celles que nous pouvons utiliser de la communauté dans SlideWiki. Le processus d'édition et de co-création offre de nouvelles opportunités que les participants auront l'occasion d'explorer au cours de cette séance.

### Troisième séance : Créer des cours en ligne en utilisant SlideWiki et du contenu d'apprentissage organisé
Au cours de cette dernière session, les participants apprendront à créer des programmes d’enseignement et à organiser des cours en ligne sur un sujet intéressant. Les participants découvriront comment la plateforme SlideWiki crée des opportunités pour promouvoir de l'apprentissage en ligne pour les adultes.