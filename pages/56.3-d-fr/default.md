---
title: 'Technologies d''impression 3D open source'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Open-source 3-D printing Technologies'
lang_id: 'Open-source 3-D printing Technologies'
lang_title: fr
length: '10 heures et 30 minutes'
objectives:
    - 'Bénéficier de la fabrication numérique'
    - 'Utiliser les technologies numériques ouvertes pour créer, manipuler, préparer et publier du contenu 3D'
    - 'Utiliser les technologies numériques ouvertes pour trouver et tirer parti du référentiel de modèles 3D'
materials:
    - 'Ordinateur personnel'
    - 'Connexion Internet'
    - Tinkercard
    - Scketchfab
skills:
    'Mots clés':
        subs:
            'Fabrication additive': null
            'Fabrication numérique': null
            'Imprimante 3D open source': null
---

_Des avancées significatives dans les technologies de fabrication additive (AM), communément appelées impression 3D, au cours de la dernière décennie ont transformé les façons dont les produits sont conçus, développés, fabriqués et distribués._

_La capacité et les avantages de l'impression 3D par rapport à la fabrication traditionnelle ouvrent de nombreuses opportunités pour les secteurs verticaux, allant de la conception et du développement de produits, du service de personnalisation, à la restructuration de la chaîne d'approvisionnement pour une plus grande efficacité._ 
**Commission européenne, Le caractère perturbateur de l'impression 3D**

L'introduction de la fabrication additive (AM), mieux connue sous le nom d'impression 3D, émerge comme une technologie perturbatrice qui entraîne plusieurs changements et impacts sur le produit traditionnel. Un nombre croissant d'entreprises et de nouveaux modèles commerciaux basés sur la MA émergent, créant de grandes opportunités pour l'économie et la société. Après avoir fourni aux étudiants une formation de base en MA, fabrication numérique et impression 3D open source (1ère session), le cours comprendra un apprentissage intensif du matériel et des logiciels open source et des plateformes open source pour la création d'objets 3D.

Les séances permettront aux apprenants d'acquérir certaines compétences techniques nécessaires pour profiter pleinement de toutes les opportunités offertes par les technologies 3D. Les participants découvriront le processus de conception en concevant un objet à l'aide des technologies d'impression 3D open source.

## Contexte
Les imprimantes 3D deviennent rapidement un appareil de fabrication largement utilisé qui a révolutionné l'industrie manufacturière. Le but de cette formation est de présenter aux participants la MA et les avantages les plus puissants des technologies d'impression 3D et de l'utilisation des technologies numériques ouvertes.
À la fin des sessions, les participants seront capables de créer des objets 3D en utilisant des technologies numériques ouvertes, en partageant leurs propres projets et en utilisant les projets d'autres utilisateurs.

Ce module sera divisé en 2 séances :
### Première séance : Transformation numérique dans la fabrication "Fabrication additive" (AM)
Au cours des dernières décennies, les communications, l'imagerie, l'architecture et l'ingénierie ont toutes connu leur propre révolution numérique. Le terme «transformation numérique» fait référence à l'utilisation des données numériques, la connectivité et le traitement englobent tous les aspects de l'activité de fabrication. AM nous donne l'occasion de repenser complètement la façon dont les produits parviennent à l'utilisateur final. La session vise à fournir aux apprenants un large aperçu de la conception et de la fabrication numérique, en présentant aux apprenants l'impression 3D, comment elle fonctionne et comment elle peut être utilisée dans diverses industries. Les élèves apprendront ce qu'est l'impression 3D, comment fonctionnent les imprimantes 3D et les types d'objets que vous pouvez fabriquer à l'aide de cette technologie.

### Deuxième séance : Explorons les technologies numériques ouvertes pour concevoir, développer, personnaliser et partager des produits 3D
L'avenir de la fabrication est numérique. Les imprimantes 3D deviennent rapidement un appareil de fabrication largement utilisé qui a révolutionné l'industrie manufacturière. Dans le travail d'impression 3D, tout commence par un modèle 3D. La session vise à introduire des logiciels et des plateformes open source pour la conception et le partage de contenus 3D. Les étudiants découvriront le processus de conception en concevant / trouvant des objets 3D en utilisant Tinkercad (plateforme pour publier, partager et découvrir du contenu 3D sur le web, mobile, AR et VRI) et Sketchfab (outil pour publier et trouver des modèles 3D en ligne). Les participants acquerront les compétences techniques pour débuter en MA nécessaires pour profiter pleinement de toutes les opportunités offertes par les technologies 3D.

Les imprimantes 3D deviennent rapidement un appareil de fabrication largement utilisé qui a révolutionné l'industrie manufacturière. Le but de cette formation est de présenter aux participants la MA et les avantages les plus puissants des technologies d'impression 3D et de l'utilisation des technologies numériques ouvertes. À la fin des sessions, les participants seront capables de créer des objets 3D en utilisant des technologies numériques ouvertes, en partageant leurs propres projets et en utilisant les projets d'autres utilisateurs.