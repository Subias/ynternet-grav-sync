---
title: 'Narració digital: crear i compartir històries utilitzant tecnologies digitals obertes'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Digital Storytelling: create and share stories using open digital technologies'
lang_id: 'Digital Storytelling: create and share stories using open digital technologies'
lang_title: ct
length: '12 hores'
objectives:
    - 'Utilitzar i integrar diferents mitjans (text, imatges, so, vídeo) en un entorn en línia'
    - 'Utilitzar tecnologies digitals obertes per a crear i compartir històries'
    - 'Produir i compartir una història digital per estimular la personalització i el compromís actiu de l''alumnat'
    - 'Explorar mètodes de col·laboració en activitats de petits grups per aprendre els uns dels altres'
materials:
    - 'Ordinador personal'
    - 'Connexió a internet'
    - 'Targetes Dixit'
skills:
    'Paraules clau':
        subs:
            Narració: null
            'Història digital': null
            'Guió gràfic': null
            'Cercle de narracions': null
            Multimèdia: null
            'Narració interactiva': null
            'Programari de codi obert': null
---

La narració digital (DS) és una forma senzilla i accessible d'explicar una història a través d'un curtmetratge. La DS és una activitat que utilitza les eines de la tecnologia digital per explicar històries sobre les nostres vides. La DS és una part essencial de el sistema educatiu mundial modern, una eina d'aprenentatge actiu que pot proporcionar a l'alumnat les habilitats i competències necessàries per crear, gestionar i compartir històries digitals amb eines de codi obert. Per produir una història digital, l'alumnat ha de treballar a través d'un procés de narració digital:

![](storytelling1.png)

Durant la creació d’una història digital, els / les creadors / es han de produir imatges per edició de vídeo, so, música, text i veu . Per tant, es poden desenvolupar i practicar gran varietat d'habilitats a través de la creació d'històries digitals. A l'usar la metodologia DS, l'alumnat té l'oportunitat de crear i compartir històries utilitzant tecnologies digitals obertes (per exemple, programari d'edició de vídeo de codi obert; programari d'edició d'àudio de codi obert, etc.).

La DS permet a l’alumnat expressar les seves pròpies experiències personals, organitzar-les en una història (autoreflexió) i usar tècniques digitals per explicar històries personals. A més, és una poderosa eina tecnològica per millorar la competència digital de l'alumnat en l'ús de programari obert que combina una varietat de multimèdia (text, imatges fixes, àudio, vídeo i publicació a la web). La DS també pot ser una eina eficaç que proporciona a les estudiants les quatre habilitats C de segle XXI (pensament Crític; Creativitat; Col·laboració; Comunicació).

## Context
El propòsit d'aquest curs de capacitació és presentar als participants la DS i aprendre a usar-la com a mètode per explicar històries mitjançant l'ús de tecnologies digitals obertes.
A la fi de les sessions, els / les participants podran crear un guió gràfic per a la seva pròpia història digital, crear i recopilar materials rellevants per a la seva història digital (imatges, veu, música, sons, textos, títols) a través de tecnologies digitals obertes.
Les sessions també tenen com a objectiu obtenir una major comprensió de les pautes ètiques, els drets d'autor, la llicència i el consentiment de Creative Commons.

Aquest mòdul es dividirà en 2 sessions:
### Primera sessió: una introducció a la DS: antecedents i beneficis
La DS és un enfocament multimèdia per compartir una narració basada en la integració de fotografia, so, text, música per compartir la narració digital de la persona creadora en un format de mitjans digitals. La DS és un mètode que ens ajuda a centrar-nos en nosaltres / as mateixos / es i en la nostra pròpia història. Ens ajuda a expressar la nostra història, dramatitzar-la, il·lustrar-la amb fotografies i explicar-la a altres persones. La sessió té com a objectiu obtenir una major comprensió dels antecedents i els principis de la DS per crear, recopilar i compartir històries digitals utilitzant tecnologies digitals obertes.

La sessió també se centra en l'impacte potencial de la metodologia DS. De fet, aconseguir que l'alumnat combini una varietat d'elements comunicatius dins d'una estructura narrativa per crear i compartir la seva història digital té alguns beneficis clau:
* Millora la competència digital de l'alumnat, ajudant-los a ser competents amb programari d'àudio i vídeo de codi obert.
* Ofereix a l'alumnat una nova forma de col·laborar

En finalitzar la sessió, les participants podran reconèixer què és i què no és una narració digital, identificant les principals característiques i passos de la DS. Les participants també podran identificar els avantatges de la DS en l'educació d'adults, amb un enfocament especial en el desenvolupament de les habilitats de segle XXI. De fet, quan les persones aprenen a crear i compartir la seva història utilitzant la metodologia DS, també desenvolupen les habilitats de segle XXI que la ciutadania d'avui necessita per tenir èxit en les seves carreres durant l'era de la informació.

### Segona sessió: Crea la teva poderosa narració digital amb tecnologies digitals obertes!

La segona sessió té com a objectiu permetre la producció d'una narració digital, incloent guió gràfic, material, edició. Els / les participants aprendran com crear un guió gràfic de la seva història, utilitzant un programari obert que combina una varietat de mitjans (text, imatges fixes, àudio, vídeo i publicació a la web) i reconeixeran el material de llicència gratuïta al web.

El propòsit d'aquest curs de capacitació és presentar als participants la DS i aprendre a usar-lo com a mètode per explicar històries mitjançant l'ús de tecnologies digitals obertes. En finalitzar les sessions, els participants podran crear un guió gràfic per a la seva pròpia història digital, crear i recopilar materials rellevants per a la seva història digital (imatges, veu, música, sons, textos, títols) a través de tecnologies digitals obertes. Les sessions també tenen com a objectiu obtenir una major comprensió de les pautes ètiques, els drets d'autor, la llicència i el consentiment de Creative Commons.