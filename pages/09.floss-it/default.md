---
title: 'Cultura FLOSS'
lang_menu_title: 'The FLOSS culture'
lang_id: 'The FLOSS culture'
lang_title: it
length: '10 ore'
objectives:
    - 'To map and explore the main concepts behind FLOSS culture'
    - 'To understand the interdependence of FLOSS and Open Educational Resources (OERs)'
    - 'To link them to the formation of Open Educational Resources (OERs)'
    - 'To discover the ethical, legal, social, economic, and impact arguments for and against FLOSS'
    - 'To decide which platforms/tools/services are most useful for themselves and their community'
materials:
    - 'Personal computer (connesso a Internet)'
    - 'Connessione internet'
    - Proiettore
    - 'Carta e penne'
    - Flipchart
skills:
    'Parole chiave':
        subs:
            FLOSS: null
            'Free/Libre Open-Source Software': null
            'Economia politica': null
            Copyleft: null
            'Movimento FLOSS': null
            'Quadro politico': null
            'Cultura FLOSS': null
            Netizenship: null
image: open_AE1.png
---

Gli scenari dei corsi di formazione sono gli elementi fondamentali di OPEN - AE Toolkit (Intellectual Output 2). Sono stati sviluppati tenendo conto sia della ricerca documentale che delle migliori pratiche relative all'insegnamento delle tecnologie open source in ciascun paese / regione partner (Intellectual output 1). I moduli sono stati sviluppati in base allo status quo e alle esigenze di ciascun paese per quanto riguarda la cultura FLOSS.

Questi scenari hanno come obiettivo quello di guidare formatori e discenti nel periodo di formazione sia nelle lezioni frontali che negli esercizi online. In ogni paese pilota, I partner saranno responsabili per l’organizzazione dei gruppi di formazione con un minimo di 8 e un Massimo di 10 partecipanti selezionati tra gli e-facilitator e gli organismi di formazione nell’ambito dell’istruzione non-formale degli adulti.

## Contesto
Gli scenari dei corsi di formazione sono gli elementi fondamentali di OPEN - AE Toolkit (Intellectual Output 2). Sono stati sviluppati tenendo conto sia della ricerca documentale che delle migliori pratiche relative all'insegnamento delle tecnologie open source in ciascun paese / regione partner (Intellectual output 1). I moduli sono stati sviluppati in base allo status quo e alle esigenze di ciascun paese per quanto riguarda la cultura FLOSS.

Questi scenari hanno come obiettivo quello di guidare formatori e discenti nel periodo di formazione sia nelle lezioni frontali che negli esercizi online. In ogni paese pilota, I partner saranno responsabili per l’organizzazione dei gruppi di formazione con un minimo di 8 e un Massimo di 10 partecipanti selezionati tra gli e-facilitator e gli organismi di formazione nell’ambito dell’istruzione non-formale degli adulti.

Il modulo sarà diviso in 3 sessioni:
### Prima sessione: I principi FLOSS: motivazioni, ideologie e pratica
Questa sessione darà l’opportunità ai suoi partecipanti di esplorare i concetti principali alla base della cultura FLOSS, ed i suoi obiettivi. Durante la sessione verranno presentate diverse interpretazioni di tali concetti, obiettivi e risultati. Li collegheremo alla formazione di Risorse educative aperte (OERs).
### Seconda sessione: FLOSS è ovunque
La seconda sessione punterà a definire lo stato dell’arte del FLOSS. Verranno presentate e confrontate le pratiche FLOSS nei Paesi Europei. I partecipanti si dedicheranno ad un’analisi critica  del quadro politico del FLOSS in Europa e a sviluppare  una politica preliminare per la loro organizzazione.
### Terza sessione: FLOSS come apprendimento collettivo, FLOSS nell’apprendimento collettivo.
Questa sessione consentirà un’analisi critica dell’importanza di FLOSS e OERs nel campo dell’istruzione non formale. Si proseguirà poi con la valutazione dell’uso e dei vantaggi delle tecnologie digitali aperte nell’istruzione, promuovendo l’impegno attivo e creativo degli studenti attraverso le tecnologie FLOSS, sia a livello teorico che pratico.