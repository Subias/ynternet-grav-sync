---
title: SlideWiki
lang_menu_title: SlideWiki
lang_id: slidewiki
lang_title: ct
length: '10 hours'
objectives:
    - 'Comprendre el marc teòric al voltant de les aplicacions web que serveixen per a gestionar contingut educatiu'
    - 'Promoure l''ús de l''aplicació web Slidewiki en Open Adult Education'
    - 'Promoure la co-creació i l''intercanvi de contingut educatiu'
    - 'Avaluar l''ús dels sistemes d''autoria basats en web OpenCourseWare, com SlideWiki'
materials:
    - 'Ordinador personal o telèfon intel·ligent o tauleta digital'
    - 'Connexió a Internet'
    - Projector
    - 'Paper i bolígrafs'
    - Paperògrafs
    - 'Academia Slidewiki'
skills:
    'Paraules clau':
        subs:
            Wikipedia: null
            Comunitat: null
            CC0: null
            'Llicències llibres': null
            Col·laboració: null
            SlideWiki: null
            Presentacions: null
            'Programari obert i gratuït': null
            'Cultura de FLOSS': null
            'Col·laboració en línia': null
---

SlideWiki és un sistema d'autoria [OpenCourseWare](https://en.wikipedia.org/wiki/OpenCourseWare) basat en la web oberta. Admet la creació i la gestió de continguts d'aprenentatge i eines per a col·laboració i l'aportació de recursos de forma col·laborativa, la traducció, la comunicació, l'avaluació i la valoració.

SlideWiki és una aplicació web que facilita la col·laboració al voltant del contingut educatiu. Amb SlideWiki, les persones usuàries poden crear i col·laborar en l'elaboració de diapositives i organitzar les diapositives en presentacions. Les presentacions es poden organitzar de forma jeràrquica, de manera que es poden estructurar d'acord amb el seu contingut. També és possible crear un repositori de col·laboració a gran escala (també conegut com crowdsourcing) al voltant del contingut educatiu (que no siguin textos). Les diapositives, presentacions, diagrames, proves d'avaluació, etc. que pugen les usuàries poden desprès ser reutilitzades per altres persones. Aquest contingut és creat principalment per persones tutores, professores, formadores, expertes, ja sigui individualment o en grups molt petits. El contingut resultant es pot compartir en línia (per exemple, usant Slideshare, OpenStudy, Google Docs).

SlideWiki és una plataforma, on les comunitats potencialment grans de docents, professores i acadèmiques tenen el poder de crear contingut educatiu sofisticat de manera col·laborativa i oberta. SlideWiki permet difondre contingut i formar estudiants i investigadors més ràpidament, ja que la càrrega de crear i estructurar una àrea de contingut es pot distribuir entre una gran comunitat. Les especialistes en aspectes individuals de el nou currículum o àrea de contingut poden centrar-se en la creació de contingut educatiu en la seva àrea particular d'especialització i tot i així aquest contingut pot integrar-se fàcilment amb un altre contingut, reestructurar i reutilitzar-se. Un aspecte particular, que és facilitat per SlideWiki, és que és una plataforma multilingüe. Atès que tot el contingut té versions i una estructura rica, és fàcil traduir contingut semiautomàticament i realitzar un seguiment dels canvis en diverses versions multilingües de el mateix contingut.

Aquest mòdul es dividirà en 3 sessions:

### Primera sessió: Descobrint i entenent SlideWiki
L'objectiu d'aquesta sessió és presentar als participants l'eina de la plataforma SlideWiki i deixar espai per a la discussió sobre els aspectes relacionats amb la creació de contingut, l'ús de contingut realitzat per altres persones i l'autoria, però també sobre la disponibilitat / accessibilitat d'informació en contextos educatius. Durant aquesta sessió, les participants aprendran com crear les seves pròpies presentacions i com importar contingut a la plataforma.

### Segona sessió: Experimentar SlideWiki: creació i col·laboració en SlideWiki
Durant aquesta sessió, l'enfocament estarà a editar les presentacions que importem de les nostres col·leccions personals, però també les presentacions que podem fer servir de la comunitat en SlideWiki. El procés d'edició i creació conjunta brinda noves oportunitats que les participants tindran l'oportunitat d'explorar durant aquesta sessió.

### Tercera sessió: creació de cursos en línia amb SlideWiki i el contingut d'aprenentatge organitzat
Durant aquesta última sessió, les participants aprendran com crear plans d'estudi educatius i organitzar classes en línia sobre un tema escollit. Les participants aprendran com la plataforma SlideWiki ofereix oportunitats per promoure l'aprenentatge en línia per a persones adultes.