---
title: 'And now what?'
objectives:
    - 'to learn about the main software installation mechanisms'
    - 'to establish guidelines for installation of updates as a basis for good maintenance'
    - 'to compile repositories of reference documentation'
    - 'to know the policy of administrative permissions and users of your OS'
---

##How do I install new programs?

##Upgrade Policies

##Reference documentation

##Users and administrators