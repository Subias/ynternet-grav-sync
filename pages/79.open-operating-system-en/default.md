---
title: 'Open operating system as a transition to FLOSS: GNU / Linux'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Open operating system as a transition to FLOSS: GNU / Linux'
lang_id: 'Open operating system as a transition to FLOSS: GNU / Linux'
lang_title: en
length: '10 hours and 30 minutes'
objectives:
    - 'to provide a save environment to participants in order to migrate to GNU / Linux in their work spaces'
    - 'to create a support network among participants'
    - 'to promote the creation of a peer-to-peer network, to provide support among the different agents of the territory involved in the dissemination of PLL'
materials:
    - 'Computer with internet connection'
    - 'Update web browser'
    - 'A PC where you can do the installation tests (you can lose the saved data)'
    - '1 or 2 USB for OS installations'
skills:
    Keywords:
        subs:
            'GNU / Linux': null
            Linus: null
            OS: null
            'Operative Systems': null
            migration: null
            'system administration': null
---

The technological infrastructure of a telecentre or a digital training centre is the starting point from which to develop its pedagogical activities. Designing the infrastructure will define the intervention policy that we will be able to develop and the topics that we will be able to work on, as well as the perspectives of our work.  

Whether we work in on basic digital literacy projects or in a more complex digital specialties (digital video-edition, maker's world, advance coding skills...) we ended up teaching specific digital products and technologies. The transition to working with free technological tools (FLOSS) can be (must be?) accompanied by working, also, with free operating systems, offering the ability to understand the technology as an open and shared knowledge beyond the closed options offered by the market.

Two different methodologys will be used during this module:
* **Deliberation**: Discussion groups and discussion forums
* **Execution**: real practices in computers provided by students


 This module is divided into three differentiated thematic blocks:
###First session: our needs
In this session we will work from analyzing the needs of each telecenter/digital-training center, and situate it in its context. You will map out the different agents with whom you can relate and your roles: public administration, communities of free software development, universities, other training centers. Also, FLOSS tools will be specified, those that can answer to the educational needs of each space.
###Second session: The steps to give
Based on the needs detected, we will design a migration plan, choosing the best SO that can fit the needs. Different migration mechanisms will be analyzed and assessed, according to the needs of each center. A test installation will be carried out, and maintenance and update guidelines will be developed to ensure that the center keeps in a good shape. 
###Third session: And now what?
Once the installation is completed, and the maintenance and updating rules are settled, we will learn how to install new programs safely. 
###Fourth session: Awakening and dissemination
It is necessary to work on a pedagogical level, such as explaining how this works to the people participating in each center and main reasons of using this kind of technology. We will study and develop strategies for the dissemination of FLOSS systems and to adapt the didactic material of each center to continue its main task: digital literacy or training in digital competences. 