---
title: 'Digital Storytelling : créez et partagez des histoires à l''aide de technologies numériques ouvertes'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Digital Storytelling: create and share stories using open digital technologies'
lang_id: 'Digital Storytelling: create and share stories using open digital technologies'
lang_title: fr
length: '12 heures'
objectives:
    - 'Utiliser et d''intégrer différents médias (texte, images, son, vidéo) dans un environnement en ligne transparent'
    - 'Utiliser les technologies numériques ouvertes pour créer et partager des histoires'
    - 'Produire et partager une histoire numérique pour stimuler la personnalisation et l''engagement actif des apprenants'
    - 'Explorer des méthodes de collaboration dans des activités en petits groupes pour apprendre les uns des autres'
materials:
    - 'Ordinateur personnel'
    - 'Connexion Internet'
    - 'Cartes Dixit'
skills:
    'Mots clés':
        subs:
            Storytellins: null
            'Histoire numérique': null
            Storyboard: null
            'Cercle de narration': null
            Multimédia: null
            'Conte interactif': null
            'Logiciels open source': null
---

La narration numérique (DS) est un moyen simple et accessible de raconter une histoire à travers un court métrage. DS est un métier qui utilise les outils de la technologie numérique pour raconter des histoires sur nos vies. DS est une partie essentielle du système éducatif mondial moderne, un outil d'apprentissage actif qui peut fournir aux apprenants les compétences et les compétences nécessaires pour créer, gérer et partager des histoires numériques open source. Afin de produire une histoire numérique, les apprenants doivent suivre un processus de narration numérique :

![](storytelling1.png)

Lors de la création d'une histoire numérique, les créateurs sont tenus de produire une vidéo de montage d'images, de son, de musique, de texte et de voix. Ainsi, une variété de compétences peut être développée et mise en pratique via la création d'histoires numériques. En utilisant la méthodologie DS, les apprenants ont la possibilité de créer et de partager des histoires en utilisant des technologies numériques ouvertes (par exemple, un logiciel d'édition vidéo open source; un logiciel d'édition audio open source, etc.).

DS permet aux apprenants d'exprimer leurs propres expériences personnelles, de les organiser dans une histoire (auto-réflexion et utiliser des techniques numériques pour raconter des histoires personnelles. En plus d'être un puissant outil technologique pour améliorer la compétence numérique des apprenants en utilisant un logiciel ouvert qui combine une variété de multimédia (texte, images fixes, audio, vidéo et publication Web), DS peut également être un outil efficace qui fournit aux apprenants les quatre C du 21e siècle (pensée critique; créativité; collaboration; communication).

## Contexte
Le but de ce cours de formation est de présenter aux participants DS et d'apprendre à l'utiliser comme méthode pour raconter des histoires en utilisant des technologies numériques ouvertes.
À la fin des sessions, les participants seront en mesure de créer un storyboard pour leur propre histoire numérique, de créer et de collecter du matériel pertinent pour leur histoire numérique (images, voix, musique, sons, textes, titres) grâce aux technologies numériques ouvertes.
Les séances visent également à mieux comprendre les directives éthiques, le droit d'auteur, la licence Creative Commons et le consentement.

Ce module sera divisé en 2 séances:
### Première séance : Une introduction à DS : contexte et avantages

DS est une approche multimédia pour partager un récit basé sur l'intégration de la photographie, du son, du texte, de la musique pour partager l'histoire numérique du créateur dans un format numérique. DS est une méthode qui nous aide à nous concentrer sur nous-mêmes et sur notre propre histoire. Il nous aide à formuler notre histoire, à la dramatiser, à l'illustrer avec des photographies et à la raconter aux autres. La session vise à mieux comprendre les antécédents et les principes de DS pour créer, collecter et partager des histoires numériques à l'aide de technologies numériques ouvertes.

La session se concentre également sur l'impact potentiel de la méthodologie DS. En fait, amener les apprenants à combiner une variété d'éléments de communication au sein d'une structure narrative pour créer et partager leur histoire numérique présente certains avantages clés:
* Il améliore les compétences numériques des apprenants, les aidant à devenir compétents avec les logiciels audio et vidéo open source
* Il donne aux apprenants une nouvelle façon de collaborer

À la fin de la session, les participants seront en mesure de reconnaître ce qui est et ce qui n'est pas une histoire numérique, en identifiant les principales caractéristiques et étapes de DS. Les participants seront également en mesure d'identifier les avantages du DS dans l'éducation des adultes, avec un accent particulier sur le développement des compétences du XXIe siècle. En fait, lorsque les gens apprennent à créer et à partager leur histoire en utilisant la méthodologie DS, ils développent également les compétences du 21e siècle dont les citoyens d'aujourd'hui ont besoin pour réussir dans leur carrière à l'ère de l'information.
### Deuxième séance : Créez votre histoire numérique puissante, des technologies numériques ouvertes!
La deuxième session vise à permettre la production d'une histoire numérique, y compris le storyboard, le matériel, l'édition. Les participants apprendront à créer un storyboard de leur histoire, en utilisant un logiciel ouvert qui combine une variété de multimédia (texte, images fixes, audio, vidéo et publication Web) et reconnaîtront le matériel de licence gratuit sur le Web.

Le but de ce cours de formation est de présenter aux participants DS et d'apprendre à l'utiliser comme méthode pour raconter des histoires en utilisant des technologies numériques ouvertes. À la fin des sessions, les participants seront en mesure de créer un storyboard pour leur propre histoire numérique, de créer et de collecter des documents pertinents pour leur histoire numérique (images, voix, musique, sons, textes, titres) grâce aux technologies numériques ouvertes. Les séances visent également à mieux comprendre les directives éthiques, le droit d'auteur, la licence Creative Commons et le consentement.