---
title: 'Create online courses using SlideWiki and organised learning content'
objectives:
    - 'to create a collaborative space for adult education'
    - 'to motivate participants to learn how to create basic online courses or even advanced classes on SlideWiki'
    - 'to learn how to plan, structure, create and deliver online courses using SlideWiki as a tool of open adult education'
---

##Introduction: Create and deliver online courses using SlideWiki as a tool of open adult education
→ Understand the SlideWiki as a collaborative space for adult education
Participants will discuss how SlideWiki can help building spaces for people to learn and share knowledge. How can this collaborative tool can promote online adult education by being a place to share content but also to import. The trainer will promote understanding of how SlideWiki can be a space for people to learn and even follow a full online curriculum.

→ Participants create their own online course
Trainer will invite participants to develop their own online course (on any topic of their liking) using SlideWiki as a main tool to promote online adult education. Trainer will guide participants on the main stages to consider when preparing an online training course. Also, trainer will help participants to start creating their planning sessions and materials. The idea is to help participants create their own course ideas and provide support to help them develop those courses.

##Self learning
Participants will navigate the tutorial deks in order to further discover the potential of the collaborative tool when creating workshops and online courses. Youtube videos  and Wiki articles are also encouraged to be used, as participants discover new ways to create educational resources. This is a moment for participants to explore the platform with a certain level of autonomy and empowerment, as they are the ones guiding their needs and online course.

##Homework
Participants will make their online course as a public deck and send them to the group making the content available to the class but also the larger community of SlideWiki