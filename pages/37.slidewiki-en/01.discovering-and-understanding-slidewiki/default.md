---
title: 'Discovering and understanding SlideWiki'
length: '2 hours'
objectives:
    - 'to provide an overview of what SlideWiki is and understand the theoretical framework around Web applications to manage educational content'
    - 'to explain how the methodology tool is an asset in the project and why the use of Slidewiki web application in Open Adult Education is recommended'
    - 'to discover the SlideWiki community and some key elements of community'
    - 'to understand the interface: create an account and promote the (co)creation and sharing of educational content'
---

##Introduction
Trainer will start the introduction to the module by asking participants about their current and previous experience with SlideWiki or any wiki-family project. They will also be invited to share information about the context of their participation to the session. The goal of this first conversation is to help them see how SlideWiki may practically impact their daily lives and attracts their attention. With SlideWiki the goal is to make educational content dramatically more accessible, interactive, engaging and qualitative in an open and free culture.

##Introduction to SlideWiki
This section typically ensures that participants will be able to: 
1. Understand the goals of SlideWiki
2. Understand how SlideWiki relates to other Wikimedia Projects, and other digital tools
3. Understand why they should be motivated to contribute or use SlideWiki presentations, especially in the context of their own work.
4. Understand that SlideWiki is primarily a social construction.
* Who adds content to SlideWiki(humans, machines run by humans, other sources)
* Who defines rules and how. The different roles of the community. 
* Behavior and social rules. 
* Editorial rules. What gets in and what does not. References. 
* Benefits of having an account 
* Key concepts: Describing the basic characteristics of SlideWiki (free cc0, collaborative, multilingual and language agnostic, interconnected) and it’s goals (repository of human knowledge, support for the Wikimedia projects and 3rd parties, hub in the linked open data web) + general content stats + general access stats
* How SlideWiki serves Wikimedia projects and the community by providing a platform to make educational content dramatically more accessible, interactive, engaging and qualitative. 
* How SlideWiki serves the purposes of the european Open AE project and how  it is important to create a community of practice for this project around this  collaborative open tool.

##SlideWiki is build through collaboration

→ First editing steps

Get students to create their own account. Make sure to collect all username created during the session. Suggestions to users:
1. Create an account. Introduce yourself in the user page.
2. Explore the platform and start discovering both the content and the mechanics of the online collaboration tool.
3. Search content already available on SlideWikion any topic of the interest of the participant. Discover the type of presentations are already available.
4. Discuss the value of online collaboration for making content available

##Homework
Before the second session, the participant will be asked to think of entry field they would like to improve, search for information, including references and sources, to be shortly presented to the group at the beginning of next session

##References
* [SlideWiki: revolutionise how educational materials can be authored, shared and reused](http://aksw.org/Projects/SlideWiki.html)
* [Wikipedia](https://en.wikipedia.org/wiki/SlideWiki)
* [SlideWiki](https://slidewiki.org/)
* [SlideWiki - Towards a Collaborative and Accessible Platform for Slide Presentations](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=9&ved=2ahUKEwiU9ZnHsOTlAhUK86YKHfzFDdwQFjAIegQIBBAC&url=http%3A%2F%2Fceur-ws.org%2FVol-2193%2Fpaper6.pdf&usg=AOvVaw0WHW7Nrzf7KuBJhJ6G4Klx)
