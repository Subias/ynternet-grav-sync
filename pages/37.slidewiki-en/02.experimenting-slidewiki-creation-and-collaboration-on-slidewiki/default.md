---
title: 'Experimenting SlideWiki: creation and collaboration on slideWiki'
objectives:
    - 'to understand how SlideWiki content is created, shared and imported'
    - 'to learn how to add content to SlideWiki , and how to contribute to the community to build the database'
    - 'to promote the use of open web-based OpenCourseWare authoring systems'
    - 'to autonomously learn from tutorials and videos'
---

##Questions about last session and discussion about homework
The trainer will facilitate a debriefing moment where participants are encouraged to express their questions, doubts, ideas and feelings toward the topics discussed in the previous lesson.
Also, the trainer will present the content for this session, which will be about learning how to add content to SlideWiki using presentation from the Slidewiki community.

##Introduction to editing
→ Importing presentations and Editing overview
Participants will be invited to learn how to import their own presentations (in different formats: PowerPoint, slides, etc) to new decks in SlideWiki. They will learn how to create and edit new decks and also how to manage them the main decks (private, public, shared in groups, etc). The goal of this activity is to present the main characteristics of the collaborative tool and guide the participants in a first moment.
→ Editing activity
Participants will be encouraged to further develop their presentations previously uploaded to slidewiki. The goal is to edit and add content to the presentation, exploring the different tools the platform allows like importing different presentations to the same deck, or import presentation from the SlideWiki community. 
Participants are encouraged to explore the platform freely and ask questions.

##SlideWiki Collaborative processes
→ SlideWiki a tool for collaboration and crowdsourcing
Trainer will guide the participants in the discovery of the collaborative side of the SlideWiki tool. Participants will explore the presentations available in the global repositorium and will learn how to import presentation from other authors. The possibility to work on some else's work brings many advantages  like saving time, money and avoid to start from scratch.
Participants will learn how to not only take from the community but also how to share their own work back with the same community.

→ Editing and collaboration features presentation
The goal of this section is to better understand SlideWiki editing processes, in particular we will focus o n some of the functions of the platform like the licenses, the transparency and the possibility to track every edit, and automatic translations. Participants will also learn how to create groups of editors and playlist of decks inside those groups. This is a fulcral step when collaborating online using this platform.

##Autonomously learn from tutorials and videos
Participants will be invited to search and import the tutorial decks of SlideWiki to their personal decks. After this step, participants will navigate the tutorial deks in order to further discover the potential of the collaborative tool. Youtube videos  and Wiki articles are also encouraged to be used, as participants discover new ways to learn.

##Homework
Participants will organise all the decks they have imported from the community and the decks they have created by theme/topic of interest. The idea is to group presentations of the same subject in the same deck in a structured way.

##References for the trainer
[WEBINAR SLIDEWIKI PLATFORM](https://www.youtube.com/watch?v=WIdbPToAwNs)