---
title: SlideWiki
lang_menu_title: SlideWiki
lang_id: slidewiki
lang_title: en
length: '10 hours'
objectives:
    - 'Understand the theoretical framework around Web applications to manage educational content'
    - 'Promote the use of Slidewiki web application in Open Adult Education'
    - 'Promote the (co)creation and sharing of educational content'
    - 'How to use open web-based OpenCourseWare authoring systems, like SlideWiki'
materials:
    - 'Personal computer'
    - 'Internet connexion'
    - Bearmer
    - 'Paper and pens'
    - Flipchart
    - 'Slidewiki Academy'
skills:
    Keywords:
        subs:
            Wikipedia: null
            Community: null
            CC0: null
            'Free licences': null
            Collaboration: null
            SlideWiki: null
            Presentations: null
            'Open and free software': null
            'FLOSS culture': null
            'Online collaboration': null
---

SlideWiki is an open web-based [OpenCourseWare](https://en.wikipedia.org/wiki/OpenCourseWare) authoring system. It supports learning content authoring and management and tools for Collaboration/crowd-sourcing, translation, communication, evaluation and assessment.

SlideWiki is a Web application facilitating the collaboration around educational content. With SlideWiki users can create and collaborate on slides and arrange slides in presentations. Presentations can be organized hierarchically, so as to structure them reasonably according to their content. A large-scale collaboration (also referred to as crowd-sourcing) repositorium around educational content (other than texts) is supported. Slides, presentations, diagrams, assessment tests etc are uploaded by users to be later reused by other users. This content is mainly created by tutors, teachers, lecturers and professors individually or in very small groups. The resulting content can be shared online (e.g. using Slideshare, OpenStudy, Google Docs).

SlideWiki is a platform, where potentially large communities of teachers, lecturers, academics are empowered to create sophisticated educational content in a collaborative and open way. SlideWiki allows disseminating content and educating students and peer-researchers more rapidly, since the burden of creating and structuring the new field can be distributed among a large community. Specialists for individual aspects of the new field can focus on creating educational content in their particular area of expertise and still this content can be easily integrated with other content, re-structured and re-purposed. A particular aspect, which is facilitated by SlideWiki is multi-linguality. Since all content is versioned and richly structured, it is easy to semi-automatically translate content and to keep track of changes in various multilingual versions of the same content.

This module will be divided into 3 sessions:

###First session - Discovering and understanding SlideWiki
The goal of this session is to introduce participants to the SlideWiki platform tool and allow the space for discussion around the topics of creation of content, usage of content done by other people and authorship, but also availability/accessibility of information in educational contexts. During this session participants will learn how to create their own presentations and how to import content to the platform.
###Second session - Experimenting SlideWiki: creation and collaboration on SlideWiki
During this session the focus will be on editing the presentations  we imported from our personal collections but also the presentations we can use from the community in SlideWiki. The process of editing and co-creation brings new opportunities that participants will have the chance to explore during this session.
###Third session - Create online courses using SlideWiki and organised learning content 
During this last session, participants will learn how to create educational curriculums and organise online classes around a desirable topic. Participants will learn how the SlideWiki platform creates opportunities to promote online learning for adults.