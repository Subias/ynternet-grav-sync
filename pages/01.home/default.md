---
title: 'Welcome to the OPENAE Academy!'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Welcome to the OPENAE Academy'
lang_id: 'Welcome to the OPENAE Academy'
lang_title: en
---

## This is our training toolkit for the Open AE: Promote Open Source Technologies in non-formal Adult Education project  including our training scenarios and online learning space

The training courses scenarios are the fundamental elements of the [OPEN - AE](https://open-ae.eu/open-ae-curriculum/) Toolkit (Intellectual Output 2). They were developed taking into consideration both the desk and field research of best practices regarding teaching open source technologies in each partner country/region (Intellectual Output 1). Modules were developed based on the status quo and needs of each country regarding FLOSS culture.

These scenarios aim to guide trainers and learners during the training period in both the face-to-face classes and online exercises. In each piloting country, partners will be responsible for the organization of training groups with a minimum of 8 and a maximum of 10 participants selected among e-facilitator and training providers in the field of non-formal adult education.

The learning material (presentations or other) are stored and documented online on an open collaborative repository called [SlideWiki](https://slidewiki.org/deck/127952-2/open-ae-_-academy-slidewiki/deck/127952-2?language=en). The open-source and open-access SlideWiki platform employs crowdsourcing methods in order to support the authoring, sharing, reusing and remixing of open courseware. 

### Our motivation

Increasingly we see digital skills and competences pegged to proprietary software solutions. While FLOSS technologies are meant to be open and are freely accessible, most FLOSS users already have some competences in licensing and ownership when making the decision to use FLOSS. New users with low skills are often intimidated or insecure with their own capacities to use FLOSS technologies, and thus may choose to use proprietary options because some brands are more associated with skills. The OPEN-AE project aims to bridge this gap and promote practices and tools to make open culture and free software more accessible for new users.