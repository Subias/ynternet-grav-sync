// Curriculum Template Code
//
// What it does
// * Handles navigation between the activity steps
// * Shows the correct step based on the URL (when sending someone a link to a specific step, for example)
// * Keeps the left-hand navigation fixed to the top of the page when in desktop mode

var navTop, navEl, windowHeight, navHeight;

$(document).ready(function(){
  navEl = $(".agenda-navigation");
  var navOffset = navEl.offset();
  navTop = navOffset.top;

  navigate(window.location.hash);

  // If there is already a hardcoded menu in the markup.
  var hasMenu = $(".activity-menu").length > 0;

  if(hasMenu) {
    $(".activity-menu .toggle").on("click",function(){
      $(".activity-menu ol").slideToggle();
      $(this).toggleClass("menu-open");
      return false;
    });
  }

  buildActivityMenu(title, activities);
  // if(!hasMenu) {
  //   activities.forEach(function( data, textStatus, jqxhr ) {
  //   });
  // }

  navEl.on("click","a",function(){
    var step = $(this).attr("href");
    navigate(step);
    return false;
  });

  $(window).on("scroll",function(){
    if($(".wrapper").width() > 600){
      scroll();
    }
  });

  $("aside img").on("load",function(){
    navOffset = navEl.offset();
    navTop = navOffset.top;
  });
});


function buildActivityMenu(title, activities){
  let activitiesSortedByLang = {};
  let key = 0;
  activities.forEach(function(activity) {
    if (activity.langId && activity.langTitle) {
      if (!activitiesSortedByLang[activity.langId]) {
        key++;
        activitiesSortedByLang[activity.langId] = activity;
        activitiesSortedByLang[activity.langId].subs = [];
        activitiesSortedByLang[activity.langId].key = key;
      }
      activitiesSortedByLang[activity.langId].subs.push(activity)
    } else {
      key++;
      activitiesSortedByLang[activity.slug] = activity;
      activitiesSortedByLang[activity.slug].key = key;
    }
  });

  var activityNum = 1;
  Object.entries(activitiesSortedByLang).forEach(function(activity, key) {
    activity = activity[1];
    if (activity.subs) {
      activity.subs.forEach(function(subAactivity, key) {
        if (subAactivity.slug == currentSlug) {
          activityNum = activity.key;
        }
      });
    } else {
      if (activity.slug == currentSlug) {
        activityNum = activity.key;
      }
    }
  });

  // activities.forEach(function(activity, key) {
  //   if (activity.slug == currentSlug) {
  //     activityNum = key + 1;
  //   }
  // });

  var menu = $("<div class='activity-menu'></div>");

  var activityCount = Object.entries(activitiesSortedByLang).length;
  // var activityCount = activities.length;

  var toggle = $("<a class='toggle'><strong>" + title + "</strong> - Activity <span class='current-activity'>1</span> of " + activityCount + "</a>");
  var activityList = $("<ol class='activity-list'/>");

  menu.append(toggle);
  menu.append(activityList);

  Object.entries(activitiesSortedByLang).forEach(function(activity, key) {
    activity = activity[1];
    var activityItem = null;
    if (!activity.subs) {
      activityItem = $("<li><a href='" + activity.url + "'>" + activity.title + "</a></li>");
    } else {
      let item = '';
      activity.subs.forEach(function(subActivity) {
        if (!item) {
          item = "<li class='subs'>" + activity.langMenuTitle + " (";
        } else {
          item += ", ";
        }
        item += "<a href='" + subActivity.url + "'>" + subActivity.langTitle + "</a>";
      });
      item += ")</li>";
      activityItem = $(item);

    }
    activityList.append(activityItem);
  });

  // for(var i = 0; i < activities.length; i ++){
  //   var activity = activities[i];
  //   var activityItem = $("<li><a href='" + activity.url + "'>" + activity.title + "</li>");
  //   activityList.append(activityItem);

  //   if(activity.extension){
  //     var link = $("<br/><span class='extension'>Extension - <a href='"+activity.extension.url+"'>"+activity.extension.title+"</a></span>");
  //     activityItem.append(link);
  //   }
  // }

  $(toggle).find(".current-activity").text(activityNum);
  $(activityList).find("li:nth-child("+activityNum+")").addClass("current");

  $(toggle).on("click",function(){
    $(".activity-menu ol").slideToggle();
    $(this).toggleClass("menu-open");
    return false;
  });

  $(".main").prepend(menu);
}


function navigate(hash){
  // First, we'll hide all of the conten
  $(".agenda > li").hide();
  $("section.overview").hide();

  // Next, we'll try to figure out what step to show based on the hash.
  hash = hash.toLowerCase();
  var numberOfSteps = $(".agenda > li").length;
  var overview = true;
  if(hash.indexOf("step") > 0) {
    var step = hash.replace("#step-","");
    if(step <= numberOfSteps){
      overview = false;
    }
  }

  // If there's a step number in the hash, we'll show that step.
  // Otherwise, we'll default to the overview.
  if(overview) {
    hash = "#overview";
    $("section.overview").show();
    $("body").attr("mode","overview");
  } else {
    $(".agenda > li:nth-child("+step+")").show();
    $("body").attr("mode","step");
  }

  // Add the selected class to the activity navigation link.
  navEl.find(".selected").removeClass("selected");
  navEl.find("a[href="+hash+"]").parent().addClass("selected");

  // Scroll the page to the top
  $(window).scrollTop(0);

  window.location.hash = hash;
}

function scroll(){
  var scrolled = $(window).scrollTop();
  var imgHeight = $("aside .image").height() || 0;

  if(scrolled > imgHeight){
    $(".agenda-navigation").addClass("fixed")
  } else {
    $(".agenda-navigation").removeClass("fixed");
  }
}
